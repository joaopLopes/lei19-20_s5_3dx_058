#!/bin/sh
NAME=$1
ENV=$2
export ASPNETCORE_ENVIRONMENT=$ENV
cd ../MyCutlery3DXG058/MasterDataFactory/MasterDataFactory.Infrastructure
dotnet ef --startup-project ../MasterDataFactory.Web migrations add $NAME
cd ../MasterDataFactory.Web
dotnet ef database update
