#!/bin/sh
NAME=$1
ENV=$2
export ASPNETCORE_ENVIRONMENT=$ENV
cd ../MyCutlery3DXG058/MasterDataProduction/MasterDataProduction.Infrastructure
dotnet ef --startup-project ../MasterDataProduction.Web migrations add $NAME
cd ../MasterDataProduction.Web
dotnet ef database update
