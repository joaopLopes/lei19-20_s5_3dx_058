# Use cases:
* MDF01 - Create Machine Type and Associate Operations 
* MDF02 - Create Machine, defining its respective machine type
* MDF03 - Create Production Line, associating the respective machines
* MDF04 - Create Operation
* MDP01 - Create Product, defining its Fabrication Plan
* MDF05 - Change a machine type's operations 
* MDF06 - Change a machine's machine type 
* MDF07 - Consult a Machine Type 
* MDF08 - Consult the operations of a machine type 
* MDF09 - Consult a Machine 
* MDF10 - Consult all the machines of a machine type 
* MDP02 - Consult a Product 
* MDP03 - Consult a Product's fabrication plan

# Non-functioning requirements
* Use ASP.Net Core
* Use MS SQL Server or MySQL
* Database and aplication deployed in the cloud(e.g. Azure) 
   * Optional
* Use of DDD (Root, Entity, VO, Repository, Service, ...)
* Use of “DTO”/”ViewModel” Pattern
* Use of Entity Framework Migrations
* Use of version control (e.g. Git+Bitbucket)
* Unit tests (in ASP.Net Core)
* Integration tests (em Postman ou em ASP.Net Core)
* Arquitectural design with system granularity
  * Level 1
* Arquitectural design of one of the developed applications
  * Level 2