# Level 1 - System Context - Process View

### MDF01 - Create Machine Type and Associate Operations
![Create MachineType, defining its respective operations](MDF01-CreateMachineType/mdf01-process-view-lvl1.jpg)

### MDF02 - Create Machine, defining its respective machine type
![Create Machine, defining its respective machine type](MDF02-CreateMachine/MDF02-Level1-ProcessView.jpg)

### MDF03 - Create Production Line, associating the respective machines
![Create Production Line, associating the respective machines](MDF03-CreateProductionLine/CreatesAProductionLineLVL1.jpg)

### MDF04 - Create Operation
![Create Operation](MDF04-CreateOperation/mdf04-process-view-level1.jpg)

### MDF05 - Change a machine type's operations
![Change a machine type's operations](MDF05-ChangeOperationsOfMachineType/MDF05ChangeOperationOfMachineType.jpg)

### MDF06 - Change a machine's machine type
![Change a machine's machine type](MDF06-ChangeAMachinesMachineType/mdf06-process-view-lvl1.jpg)

### MDF07 - Consult a Machine Type
![Consult a MachineType](MDF07-ConsultMachineType/mdf07-process-view-lvl1.jpg)

### MDF08 - Consult the operations of a machine type
![Consult the operations of a machine type](MDF08-ConsultOperationsOfMachineType/ConsultOperationsMachineTypeLvl1.jpg)

### MDF09 - Consult a Machine
![MDF09-ConsultMachine](MDF09-ConsultMachine/MDF09-Level1-ProcessView.jpg)

### MDF10 - Consult all the machines of a machine type
![Consult all the machines of a machine type](MDF10-ConsultMachinesOfAMachineType/mdf10-process-view-lvl1.jpg)

### MDF11 - Activate or deactivate Machine Working State
![Activate or Deactivate Machine Working State](MDF11-ActivateDeactivateMachine/mdf11-process-view-lvl1.jpg)

### MDP01 - Create Product, defining its Fabrication Plan
![Create Product SD](MDP01-CreateProduct/mdp01-process-view-level1.jpg)

### MDP02 - Consult a Product
![Consult Product SD](MDP02-ConsultProduct/mdp02-process-view-level1.jpg)

### MDP03 - Consult a Product's fabrication plan
![Consult Product's Fabrication Plan SD](MDP03-ConsultProductsFabricationPlan/mdp03-process-view-level1.jpg)

### GE01 - Create a Client
![Create Client SD](GE01-CreateClient/ge01-sequence-lvl1.jpg)

### GE02  - Create Order
![Create Order SD](GE02-CreateOrder/create-order-lvl1.jpg)

### GE03 - Update Client
![Update Client SD](GE03-UpdateClient/update-client-lvl1.jpg)

### GE04 - Update Order
![Update Order SD](GE04-UpdateOrder/update-order-lvl1.jpg)

### GE05 - Cancel Order
![Cancel Order SD](GE05-CancelOrder/cancel-order-lvl1.jpg)

### GE06 - Consult a Client
![Consult Client SD](GE06-ConsultClient/ge06-sequence-lvl1.jpg)

### GE07 - Consult Order
![Consult Order SD](GE07-ConsultOrder/consult-order-lvl1.jpg)

### GE08 - Login Service
![Consult Client SD](GE08-LoginService/ge08-sequence-lvl1.jpg)

### PP1 - Preview Date
![Preview Date of Delivery.](PP1-PreviewedDates/previewed-dates-lvl1.jpg)

### PP2 - Agendas 
![Get Agenda](PP2-Agendas/agenda-lvl1.jpg)

### VE1 - Agendas
![Get Agenda](VE1-Agendas/agenda_lvl1.jpg)

### [Back](../level1-system.md)
