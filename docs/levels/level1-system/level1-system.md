# Level 1 - System Context


## Business View (Domain Model)
* [Domain Model](business-view/level1-business-view.md)

## Logical View
* [Logical View](logical-view/Level1-LogicalView.jpg)

## Process View 
* [Process View](process-view/level1-process-view.md)

## Use Case View 
![Use Case View](use-case-view/DCUFinalSystem.jpg)


### [Back](../../../README.md)