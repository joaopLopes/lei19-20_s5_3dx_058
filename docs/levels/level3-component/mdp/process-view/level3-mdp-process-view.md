# Level 3 - Components Context - Master Data Production - Process View


### MDP01 - Create Product, defining its Fabrication Plan
![Create Product SD](MDP01-CreateProduct/mdp01-process-view-level3.jpg)

### MDP02 - Consult a Product
![Consult Product SD](MDP02-ConsultProduct/mdp02-process-view-level3.jpg)

### MDP03 - Consult a Product's fabrication plan
![Consult Product's Fabrication Plan SD](MDP03-ConsultProductsFabricationPlan/mdp03-process-view-level3.jpg)





### [Back](../level3-mdp.md)