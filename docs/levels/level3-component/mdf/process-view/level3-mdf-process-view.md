# Level 3 - Components Context - Master Data Factory - Process View


### MDF01 - Create Machine Type and Associate Operations
![Create MachineType, defining its respective operations](MDF01-CreateMachineType/mdf01-process-view-lvl3.jpg)

### MDF02 - Create Machine, defining its respective machine type
![Create Machine, defining its respective machine type](MDF02-CreateMachine/MDF02-Level3-ProcessView.jpg)


### MDF03 - Create Production Line, associating the respective machines
![Create Production Line, associating the respective machines](MDF03-CreateProductionLine/createProductionLineLvl03.jpg)


### MDF04 - Create Operation
![Create Operation](MDF04-CreateOperation/mdf04-process-view-level3.jpg)


### MDF05 - Change a machine type's operations
![MDF05-ChangeOperationOfMachineTypes](MDF05-ChangeOperationOfMachineTypes/ChangeOperationOfMachineTypeLvl3.jpg)


### MDF06 - Change a machine's machine type
![Change a machine's machine type](MDF06-ChangeAMachinesMachineType/mdf06-process-view-lvl3.jpg)

### MDF07 - Consult a Machine Type
![Consult a MachineType](MDF07-ConsultMachineType/mdf07-process-view-lvl3.jpg)


### MDF08 - Consult the operations of a machine type
![Consult the operations of a machine type](MDF08-ConsultOperationsOfMachineType/LVL3ConsultOperationsOfMachineType.jpg)


### MDF09 - Consult a Machine
![Consult a Machine](MDF09-ConsultMachine/MDF09-Level3-ProcessView.jpg)


### MDF10 - Consult all the machines of a machine type
![Consult all the machines of a machine type](MDF10-ConsultMachinesOfAMachineType/mdf10-process-view-lvl3.jpg)

### MDF11 - Activate or Deactivate Machine Working State
![Activate or Deactivate Machine](MDF11-ActivateDeactivateMachine/mdf-activate-deactivate-machine.jpg)



### [Back](../level3-mdf.md)
