# Level 3 - UI Diagrams - Process View

## UI01 - Consult Machine Type
![UI01](UI01-ConsultMachineType/ui01-sequence-diagram-lvl3.jpg)

## UI02 - Consult Operations of a Machine Type
![UI02](UI02-ConsultOperationsOfMachineType/ui02-sequence-diagram-lvl3.jpg)

NOTE: The diagrams presented above are pretty much identical, because the team found that it would be more intuitive for the user to see all the information of a Machine Type displayed at once. As such, while these are two different functions, regarding their code, for the user there is no difference - the "Consult Machine Type" function also does the "Consult Operations of a Machine Type" function.

## UI03 - Consult a Machine
![UI03](UI03-ConsultMachine/SPA-UI03-Level3-ProcessView.jpg)]

## UI04 - Consult Machines of a Machine Type.
![UI04](UI04-ConsultMachinesOfAMachineType/ui04-consult-machines-of-a-machine-type.jpg)

## UI05 - Create a Machine type
![UI05](UI05-CreateMachineType/ui05-sequence-diagram-lvl3.jpg)

## UI06 - Consult a Machine
![UI06](UI06-CreateMachine/SPA-UI06-Level3-ProcessView.jpg)

## UI07 - Create Production Line, associating its respective machines.
![UI07](UI07-CreateProductionLine/ui07-create-production-line.jpg)

## UI08 - Create Operation
![Create Operation](UI08-CreateOperation/ui08-create-operation.jpg)

## UI09 - Change a machine's type operations.
![UI09](UI09-ChangeOperationOfMachineTypes/ui09-change-machine-type-operations.jpg)

### UI11 - Consult a Product
![Consult Product SD](UI11-ConsultProduct/ui11-process-view-level3.jpg)

### UI12 - Consult a Product's fabrication plan
![Consult Product's Fabrication Plan SD](UI12-ConsultProductsFabricationPlan/ui12-process-view-level3.jpg)

### UI13 - Create Product, defining its Fabrication Plan
![Create Product SD](UI13-CreateProduct/ui13-process-view-level3.jpg)

### UI14 - Create Client
![Create Client SD](UI14-CreateClient/ui-sequence-createclient.jpg)

### UI15 - Create Order
![Create Order SD](UI15-CreateOrder/ui-sequence-createorder.jpg)

### UI16 - Update Client
![Update Client SD](UI16-UpdateClient/update-client-spa-03.jpg)

### UI17 - Update Order
![Update Order SD](UI17-UpdateOrder/update-order-spa.jpg)

### UI18 - Cancel Order
![Cancel Order SD](UI18-CancelOrder/cancel-order-spa.jpg)

### UI19 - Cconsult Client
![Consult Client SD](UI19-ConsultClient/ui-sequence-consultclient.jpg)

### UI20 - Consult Order
![Consult Order SD](UI20-ConsultOrder/ui-sequence-consultorder.jpg)

### UI21 - Login Service
![Login Service SD](UI21-LoginService/ui-sequence-loginservice.jpg)

### [Back](../level3-spa.md)
