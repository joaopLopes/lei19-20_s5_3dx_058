# Level 3 - Components Context - Order Management - Process View


### GE01 - Create Product, defining its Fabrication Plan
![Create Client SD](GE01-CreateClient/ge01-sequence-lvl3.jpg)

### GE02 - Create Order
![Create Order SD](GE02-CreateOrder/create-order-process-view-level3.jpg)

### GE04 - Update Order
![Update Order SD](GE04-UpdateOrder/update-order-process-view-level3.jpg)

### GE05 - Cancel Order
![Cancel Order SD](GE05-CancelOrder/cancel-order-process-view-level3.jpg)

### GE06 - Consult a Product's fabrication plan
![Consult Client SD](GE06-ConsultClient/ge06-sequence-lvl3.jpg)

### GE07 - Consult Order
![Consult Order SD](GE07-ConsultOrder/consult-order-process-view-level3.jpg)

### GE08 - Login Service
![Login Service SD](GE08-LoginService/ge08-sequence-lvl3.jpg)

### [Back](../ge-level3.md)
