# Level 2 - Containers Context - Logical View


![Logical View](level2-logical-view.jpg)

## Responsibilities of each component: 
* #### Master Data Factory: 
  * Handles management of the factory's equipment (production lines, machines, operations, machine types)
  * Only admin accounts have access
* #### Master Data Production:
  * Handles management of products (adding new products, with their fabrication plans and prices)
  * Only admin accounts have access
  * Consumes Master Data Factory to retrieve Operations for a Products's fabrication plan.
* #### Order Management:
  * Handles the authentication of users (configurators and clients), as well as the creation and management of orders.
  * Resources accessible to both clients and admins, based on a permissions file
  * Consumes Master Data Production to know which products the clients can order.
* #### Production Planning:
  * Handles the scheduling of fabrication orders for a given time period.
  * Consumes Master Data Factory, Master Data Production and Order Management in order to schedule all the orders on the lines of the factory in the best way possible.
  * Accessible to admin accounts.
* #### Execution Visualization: 
  * Handles the editing of the factory floor in a drag and drop manner.
  * Animates the latest production plan scheduled.
  * Consumes Master Data Factory to know which production lines and machines the factory has, and consumes production planning to retrieve the scheduled production plan.
  * Accessible to admin accounts only.
* #### Client Single Page Application:
  * Graphical User Interface for the users (admins and clients) to interact with the APIs.
  * Consumes Master Data Factory, Master Data Production, Order Management, Production Planning and has Execution Visualization embedded.
  * Runs on the client's browser.
  
## Access control policies:
* Policy Administration Point (PAP)
  * Who: Order Management
  * What: Accounts are created with given roles (client or admin) and admins and/or the product owner decide which resources each role can access (through the configuration file)
  * Why: This is the case because the access to certain resources depend of their ownership (e.g. orders), and therefore it is simpler for the component that handles orders to also handle the administration of permissions.
* Policy Decision Point (PDP)
  * Who: Order Management
  * What: Given a the user's credentials, this component will verify if they correspond to an existing account and, if it does, it must return a token with the accounts's permissions to each resource (based on the account's role.)
  * Why: For the same reason that Order Management is the PAP, only it has access to the ownership of certain resources, and therefore if the PAP were a different dedicated component there would be requests back and forth between various APIs in order to validate each user, greatly creating overhead. 
* Policy Enforcement Point (PEP)
  * Who: MDF, MDP and Order Management
  * What: Given a request to access a resource, with a given token, each API will, based on the token permissions, verify if the role present in the token should have access to the resource.
  * Why: Each API uses middlewares to validate if a token exists when accessing any resource, and if so, verifies if the token contains access to the resource, based on the decisions made in PDP.
* Policy Information Point (PIP)
  * Who: Order Management
  * What: Where the information regarding accounts and roles is stored.
  * Why: Because it is in Order Management that accounts are managed (created, deleted, updated, stored).
* Policy Retrieval Point (PRP)
  * Who: Order Management
  * What: This is where the decisions made in PAP are stored. Usually it is a database, but in our case it is the file resources/permissions.yml
  * Why: Because, in our situation, the file must be in our PAP.






### [Back](../level2-container.md)