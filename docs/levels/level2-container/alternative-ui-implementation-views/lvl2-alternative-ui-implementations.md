# Level 2 - Alternatives of UI implementation.

# Alternative with Facade
![Alternative with Facade](alternative-facade/alternative-facade.jpg)

### Pros:
- New level of abstraction.
- Easier to scale.

### Cons:
- Harder to implement.
- Significant overhead.

## Deployment Diagram of Alternative with Facade
![Alternative Deployment Diagram with Facade](alternative-facade/deployment-facade.jpg)


# Alternative with multiple Single Page Application
![Alternative with multiple SPAs](alternative-multiple-spas/alternative-multiple-spas.jpg)

### Pros:
- Different SPAs can have different styles/logic.

### Cons:
- Both SPAs have the same end user, it does not make sense to have them separated.
- Some functionalities only work with both WebAPIs running (create product), so it would be harder/slower to synchronize information between modules.

## Deployment Diagram of Alternative with multiple Single Page Application
![Alternative Deployment Diagram with Facade](alternative-multiple-spas/deployment-multiple-spas.jpg)




