import {browser, by, element} from 'protractor';

export class MyOrdersPage {
  navigateTo(username: string) {
    return browser.get('clients/' + username + '/orders') as Promise<any>;
  }

  getOrders() {
    return element.all(by.className('order'));
  }

  getFirstOrder() {
    return element(by.className('order')).click();
  }

  getTitle() {
    return element(by.cssContainingText('.h2', 'My Orders'));
  }
}
