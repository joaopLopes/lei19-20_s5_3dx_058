import {MyOrdersPage} from './my-orders.po';
import {LoginPage} from '../login/login.po';
import {OrderDetailPage} from './order-detail.po';

describe('my orders e2e tests', () => {
  const myOrders: MyOrdersPage = new MyOrdersPage();
  const loginPage: LoginPage = new LoginPage();
  const detailPage: OrderDetailPage = new OrderDetailPage();

  beforeAll(async () => {
    await loginPage.loginAsClient();
  });

  it('should create', () => {
    myOrders.navigateTo('clientForTesting');
  });

  it('should have correct title', () => {
    myOrders.navigateTo('clientForTesting');
    expect(myOrders.getTitle().getText()).toEqual('My Orders');
  });

  it('should list all his orders', () => {
    myOrders.navigateTo('clientForTesting');
    const num = myOrders.getOrders().count();
    expect(num).toEqual(4);
  });

  it('should go to detail of first order', async () => {
    await myOrders.navigateTo('clientForTesting');
    await myOrders.getFirstOrder();
    const name = detailPage.getClient();
    expect(name).toBe('clientForTesting');
  });
});
