import {browser, by, element} from 'protractor';

export class OrderDetailPage {

  navigateTo(ref: string) {
    return browser.get('/orders/' + ref + '/detail') as Promise<any>;
  }

  getTitle() {
    return element(by.css('.title'));
  }

  getClient() {
    return element(by.cssContainingText('.word', 'User')).getText();
  }
}
