import {LoginPage} from '../login/login.po';
import {OrdersPage} from './orders.po';
import {OrderDetailPage} from './order-detail.po';

describe('orders e2e tests', () => {
  const page: OrdersPage = new OrdersPage();
  const detailPage: OrderDetailPage = new OrderDetailPage();
  const loginPage: LoginPage = new LoginPage();

  beforeAll(async () => {
    await loginPage.loginAsAdmin();
  });

  it('should exist', () => {
    page.navigateTo();
  });

  it('should have correct title', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('Orders');
  });

  it('should list all orders', async () => {
    await page.navigateTo();
    const num = await page.getOrders().count();
    expect(num).toBe(5);
  });

  it('should go to detail of first order', async () => {
    await page.navigateTo();
    await page.getFirstOrder();
    const name = detailPage.getClient();
    expect(name).toBe('pedro');
  });
});
