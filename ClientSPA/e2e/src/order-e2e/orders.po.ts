import {browser, by, element} from 'protractor';

export class OrdersPage {
  navigateTo() {
    return browser.get('/orders') as Promise<any>;
  }

  getOrders() {
    return element.all(by.css('.order'));
  }

  getFirstOrder() {
    return element(by.className('order')).click();
  }

  getTitle() {
    return browser.element(by.cssContainingText('.h2', 'Orders')).getText();
  }
}
