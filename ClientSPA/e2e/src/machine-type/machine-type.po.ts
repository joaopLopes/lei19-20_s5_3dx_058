import {browser, by, element} from 'protractor';

export class MachineTypePage {
  navigateToMainPage() {
    return browser.get('/machine-types') as Promise<any>;
  }

  clickThisButton(name: string) {
    return browser.element(by.cssContainingText('.machine-type', name)).click();
  }

  clickThisToggleButton() {
    return browser.element(by.id('toggleButton')).click();
  }

  clickFirstOperation() {
    return browser.element(by.id('operation')).click();
  }

  getHeaderText() {
    return element(by.cssContainingText('.h2', 'Machine Types')).getText() as Promise<string>;
  }

  getTitle() {
    return element(by.css('.title')).getText();
  }

  getAllMachineTypes() {
    return element.all(by.css('.machine-type'));
  }
}
