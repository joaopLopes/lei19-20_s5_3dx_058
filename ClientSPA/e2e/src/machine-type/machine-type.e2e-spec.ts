import {MachineTypePage} from './machine-type.po';
import {LoginPage} from '../login/login.po';

describe('workspace-project Machine Types', () => {
  let page: MachineTypePage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    await loginPage.loginAsAdmin();
    page = new MachineTypePage();
  });
  it('should exist', async () => {
    await page.navigateToMainPage();
  });
  it('title should be Machine Types', async () => {
    await page.navigateToMainPage();
    expect(page.getHeaderText()).toEqual('Machine Types');
  });
  it('detail page should correspond to the right machine type', async () => {
    await page.navigateToMainPage();
    page.clickThisButton('machineType10');
    expect(page.getTitle()).toEqual('Machine Type machineType10 - Details');
  });
  it('should click on one of the Operations on detail page, and go to detail of said Operation', async () => {
    await page.navigateToMainPage();
    page.clickThisButton('machineType10');
    page.clickThisToggleButton();
    page.clickFirstOperation();
    expect(page.getTitle()).toEqual('Operation for-screws - Details');
  });
  it('should list all machine types ', async () => {
    await page.navigateToMainPage();
    const num = 9;
    const array = page.getAllMachineTypes().count();
    expect(array).toBe(num);
  });
});
