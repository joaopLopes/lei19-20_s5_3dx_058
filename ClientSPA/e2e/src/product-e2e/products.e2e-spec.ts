import {ProductsPage} from './products.po';
import {LoginPage} from '../login/login.po';
import {browser} from 'protractor';

describe('products page', () => {
  let page: ProductsPage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    page = new ProductsPage();
    await loginPage.loginAsAdmin();
  });

  it('should have correct title', () => {
    page.navigateTo();
    expect(page.getTitle().getText()).toEqual('Products');
  });
  it('should display all products',  () => {
    page.navigateTo();
    expect(page.getProducts().count()).toEqual(2);
  });
  it('should have new product label', () => {
    page.navigateTo();
    expect(page.getNewProductButton().getText()).toEqual('+ New Product');
  });
  it('should navigate to product detail', () => {
    page.navigateTo();
    page.clickProduct();
    expect(page.getTitle().getText()).toEqual('Product fork - Details');
  });
});
