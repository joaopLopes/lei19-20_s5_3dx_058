import {browser, by, element} from 'protractor';

export class ProductDetailPage {

  navigateTo() {
    return browser.get('/products/fork/detail') as Promise<any>;
  }

  getTitle() {
    return element(by.cssContainingText('.title', 'Product'));
  }

  getName() {
    return element(by.cssContainingText('.word', 'Name')).getText();
  }

  getFabricationPlan() {
    return element.all(by.css('.operation'));
  }

  getHideShowButton() {
    return element(by.css('.dropbtn'));
  }
}
