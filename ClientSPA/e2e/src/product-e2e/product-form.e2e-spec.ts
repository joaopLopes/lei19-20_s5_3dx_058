import {ProductFormPage} from './product-form.po';
import {LoginPage} from '../login/login.po';

describe('product form page', () => {
  let page: ProductFormPage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    page = new ProductFormPage();
    await loginPage.loginAsAdmin();
  });
  it('should have correct title', () => {
      page.navigateTo();
      expect(page.getTitle()).toEqual('New Product');
  });
  it('should have name form', () => {
      page.navigateTo();
      expect(page.getNameForm().getText()).toEqual('Name:');
  });
  it('should have submit button disabled', () => {
      page.navigateTo();
      expect(page.getSubmitButton().click()).toBeFalsy();
  });
  it('should have button to view operations', () => {
      page.navigateTo();
      expect(page.getOperationsButton().getText()).toEqual('Operations');
  });
  it('should show all available operations', () => {
      page.navigateTo();
      page.getOperationsButton().click();
      expect(page.getAllOperations().count()).toEqual(12);
  });
  it('should have no chosen operations when page started', () => {
      page.navigateTo();
      expect(page.getChosenOperationsList().count()).toEqual(0);
  });
});
