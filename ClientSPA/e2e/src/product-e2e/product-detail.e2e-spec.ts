import {ProductDetailPage} from './product-detail.po';
import {LoginPage} from '../login/login.po';

describe('product detail page', () => {
  let page: ProductDetailPage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    page = new ProductDetailPage();
    await loginPage.loginAsAdmin();
  });

  it('should have correct title', async () => {
    await page.navigateTo();
    expect(page.getTitle().getText()).toEqual('Product fork - Details');
  });

  it('should have correct name', async () => {
    await page.navigateTo();
    expect(page.getName()).toEqual('Name: fork');
  });

  it('should have button to show fabrication plan', async () => {
    await page.navigateTo();
    expect(page.getHideShowButton().getText()).toEqual('Show');
    page.getHideShowButton().click();
    expect(page.getHideShowButton().getText()).toEqual('Hide');
  });

  it('should have all operations in fabrication plan', async () => {
    await page.navigateTo();
    page.getHideShowButton().click();
    expect(page.getFabricationPlan().count()).toEqual(2);
  });
});
