import {browser, by, element} from 'protractor';

export class ProductsPage {

  navigateTo() {
    return browser.get('/products') as Promise<any>;
  }

  getTitle() {
    return element(by.css('h2'));
  }

  getNewProductButton() {
    return element(by.css('.new'));
  }

  getProducts() {
    return element.all(by.css('.product'));
  }

  clickProduct() {
    return browser.element(by.cssContainingText('.product', 'fork')).click();
  }

}
