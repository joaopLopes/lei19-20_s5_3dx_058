import {browser, by, element} from 'protractor';

export class ProductFormPage {

  navigateTo() {
    return browser.get('/products/new') as Promise<any>;
  }

  getTitle() {
    return element(by.css('.title')).getText();
  }

  getSubmitButton() {
    return element(by.css('.submit'));
  }

  getNameForm() {
    return element(by.cssContainingText('.word', 'Name'));
  }

  getAllOperations() {
    return element.all(by.css('.operation'));
  }

  getOperationsButton() {
    return element(by.css('.dropbtn'));
  }

  getChosenOperationsList() {
    return element.all(by.css('.chosen-operation'));
  }


}
