import {DashboardPage} from './dashboard.po';
describe('workspace-project Dashboard', () => {
  let page: DashboardPage;

  beforeEach(() => {
    page = new DashboardPage();
  });
  it('should exist', () => {
    page.navigateToMainPage();
  });
  it('title should be Dashboard', () => {
    page.navigateToMainPage();
    expect(page.getHeaderText()).toEqual('User Dashboard');
  });
  it('should click on page of Clients', () => {
    page.navigateToMainPage();
    page.clickThisButton('Client Area');
    expect(page.getHeader2Text()).toEqual('Clients');
  });
  it('should click on page of Admins', () => {
    page.navigateToMainPage();
    page.clickThisButton('Admin Area');
    expect(page.getHeader2Text()).toEqual('Admins');
  });
  it('should have the correct number of buttons ', () => {
    page.navigateToMainPage();
    const num = 2;
    const array = page.getAllButtons().count();
    expect(array).toBe(num);
  });
});
