import {browser, by, element} from 'protractor';

export class DashboardPage {
  navigateToMainPage() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  clickThisButton(name: string) {
    return browser.element(by.cssContainingText('.button', name)).click();
  }

  getHeaderText() {
    return element(by.css('h3')).getText() as Promise<string>;
  }

  getHeader2Text() {
    return element(by.css('h2')).getText() as Promise<string>;
  }


  getTitle() {
    return element(by.css('.title')).getText();
  }

  getAllButtons() {
    return element.all(by.css('.button'));
  }


}
