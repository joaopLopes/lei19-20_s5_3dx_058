import {browser, by , element} from 'protractor';

export class MachinePage {

  navigateToMainPage() {
    return browser.get('/machines') as Promise<any>;
  }

  clickThisButton(name: string) {
    return browser.element(by.cssContainingText('.machine', name)).click();
  }

  clickMachinesMachineType() {
    return browser.element(by.className('machineType')).click();
  }

  getHeaderText() {
    return element(by.cssContainingText('.h2', 'Machines')).getText();
  }

  getTitle() {
    return element(by.css('.title')).getText();
  }

  getAllMachines() {
    return element.all(by.css('.machine'));
  }
}
