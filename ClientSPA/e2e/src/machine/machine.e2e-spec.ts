import {MachinePage} from './machine.po';
import {LoginPage} from '../login/login.po';

describe('workspace-project App', () => {
  let page: MachinePage;
  const loginPage: LoginPage = new LoginPage();

  beforeAll(async () => {
    page = new MachinePage();
    await loginPage.loginAsAdmin();
  });
  it('should exist', () => {
    page.navigateToMainPage();
  });
  it('title should be Machines',  () => {
    page.navigateToMainPage();
    const result = page.getHeaderText();
    expect(result).toEqual('Machines');
  });
  it('All machines',  () => {
    page.navigateToMainPage();
    const num = 4;
    const value =  page.getAllMachines().count();
    expect(value).toBe(num);
  });
  it('detail page should correspond to the right machine',  () => {
    page.navigateToMainPage();
    page.clickThisButton('884');
    expect( page.getTitle()).toEqual('Machine 884 - Details');
  });
  it('clicking on a machines machine type should redirect to the machine type detail page', () => {
     page.navigateToMainPage();
     page.clickThisButton('884');
     page.clickMachinesMachineType();
     expect( page.getTitle()).toEqual('Machine Type machineType87 - Details');
  });
});
