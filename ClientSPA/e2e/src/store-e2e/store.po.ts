import {browser, by, element} from 'protractor';

export class StorePage {
  navigateTo() {
    return browser.get('store') as Promise<any>;
  }
  getTitle() {
    return browser.element(by.css('.h2'));
  }

  async getTimesFilter() {
    await element(by.cssContainingText('option', 'Most Ordered')).click();
    await element(by.buttonText('Filter')).click();
  }

  getQuantitiesFilter() {
    element(by.cssContainingText('option', 'Most Quantity Ordered')).click();
    element(by.buttonText('Filter')).click();
  }

  getFastestFilter() {
    element(by.cssContainingText('option', 'Fastest Production')).click();
    element(by.buttonText('Filter')).click();
  }

  getFirstProduct() {
    return element(by.className('product')).getText();
  }

  goToFirstProduct() {
    const name = this.getFirstProduct();
    return browser.get('store/' + name) as Promise<any>;
  }

  getProducts() {
    return element.all(by.css('.product'));
  }
}
