import {LoginPage} from '../login/login.po';
import {StorePage} from './store.po';

describe('store e2e tests', () => {
  const store: StorePage = new StorePage();
  const loginPage: LoginPage = new LoginPage();

  beforeAll(async () => {
    await loginPage.loginAsClient();
  });

  it('should exist', () => {
    store.navigateTo();
  });

  it('should have correct title', () => {
    store.navigateTo();
    expect(store.getTitle().getText()).toEqual('Store');
  });

  it('should list by times',  () => {
    store.navigateTo();
    store.getTimesFilter();
    const name = store.getFirstProduct();
    expect(name).toBe('Spoon');
  });

  it('should list by quantities',  () => {
    store.navigateTo();
    store.getQuantitiesFilter();
    const name = store.getFirstProduct();
    expect(name).toBe('Fork');
  });

  it('should list by production times', () => {
    store.navigateTo();
    store.getFastestFilter();
    const name = store.getFirstProduct();
    expect(name).toBe('Spoon');
  });
});
