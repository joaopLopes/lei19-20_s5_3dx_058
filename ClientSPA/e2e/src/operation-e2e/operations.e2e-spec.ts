import {OperationsPage} from './operations.po';
import {LoginPage} from '../login/login.po';
import {browser} from 'protractor';

describe('operations page', () => {
  let page: OperationsPage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    await loginPage.loginAsAdmin();
    page = new OperationsPage();
  });

  it('should have correct title', () => {
    page.navigateTo();
    expect(page.getTitle().getText()).toEqual('Operations');
  });
  it('should display all operations', () => {
    page.navigateTo();
    browser.wait(async () => {
      return await page.getOperations().count() > 1;
    }, 5000).catch();
    const value = page.getOperations().count();
    expect(value).toEqual(13);
  });
  it('should have new operation label', () => {
    page.navigateTo();
    expect(page.getNewOperationButton().getText()).toEqual('+ New Operation');
  });
  it('should navigate to operation detail', () => {
    page.navigateTo();
    page.clickOperation();
    expect(page.getTitle().getText()).toEqual('Operation 15mm-cut - Details');
  });
});
