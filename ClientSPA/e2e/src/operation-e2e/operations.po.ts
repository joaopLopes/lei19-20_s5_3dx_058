import {browser, by, element} from 'protractor';

export class OperationsPage {

  navigateTo() {
    return browser.get('/operations') as Promise<any>;
  }

  getTitle() {
    return element(by.css('h2'));
  }

  getNewOperationButton() {
    return element(by.css('.new'));
  }

  getOperations() {
    return element.all(by.css('.operation'));
  }

  clickOperation() {
    return browser.element(by.cssContainingText('.operation', '15mm-cut')).click();
  }

}
