import {browser, by, element} from 'protractor';

export class OperationDetailPage {

  navigateTo() {
    return browser.get('/operations/15mm-cut/detail') as Promise<any>;
  }

  getTitle() {
    return element(by.css('.title'));
  }

  getDescription() {
    return element(by.cssContainingText('.word', 'Description')).getText();
  }

  getTool() {
    return element(by.cssContainingText('.word', 'Tool')).getText();
  }

  getExecutionTime() {
    return element(by.cssContainingText('.word', 'Execution Time')).getText();
  }

  getSetupTime() {
    return element(by.cssContainingText('.word', 'Setup Time')).getText();
  }

}
