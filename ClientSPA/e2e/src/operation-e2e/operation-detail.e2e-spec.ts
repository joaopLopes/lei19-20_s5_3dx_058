import {OperationDetailPage} from './operation-detail.po';
import {LoginPage} from '../login/login.po';

describe('operation detail page', () => {
  let page: OperationDetailPage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    await loginPage.loginAsAdmin();
    page = new OperationDetailPage();
  });
  it('should have correct title', () => {
      page.navigateTo();
      expect(page.getTitle().getText()).toEqual('Operation 15mm-cut - Details');
  });
  it('should have correct description', () => {
      page.navigateTo();
      expect(page.getDescription()).toEqual('Description: 15mm-cut');
  });
  it('should have correct tool', async () => {
      page.navigateTo();
      expect(page.getTool()).toEqual('Tool: very sharp object');
  });
  it('should have correct execution time', () => {
      page.navigateTo();
      expect(page.getExecutionTime()).toEqual('Execution Time: 5 seconds');
  });
  it('should have correct setup time', () => {
      page.navigateTo();
      expect(page.getSetupTime()).toEqual('Setup Time: 2 seconds');
  });
});
