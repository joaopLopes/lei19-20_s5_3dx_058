import {OperationFormPage} from './operation-form.po';
import {LoginPage} from '../login/login.po';

describe('operation form page', () => {
  let page: OperationFormPage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    await loginPage.loginAsAdmin();
    page = new OperationFormPage();
  });
  it('should have correct title', () => {
      page.navigateTo();
      expect(page.getTitle()).toEqual('New Operation');
  });
  it('should have description form', () => {
      page.navigateTo();
      expect(page.getDescriptionForm().getText()).toEqual('Description:');
  });
  it('should have tool form', () => {
      page.navigateTo();
      expect(page.getToolForm().getText()).toEqual('Tool:');
  });
  it('should have execution time form', () => {
      page.navigateTo();
      expect(page.getExecutionTimeForm().getText()).toEqual('Execution Time in seconds:');
  });
  it('should have setup time form', () => {
      page.navigateTo();
      expect(page.getSetupTimeForm().getText()).toEqual('Setup Time in seconds:');
  });
  it('should have submit button disabled', () => {
      page.navigateTo();
      expect(page.getSubmitButton().click()).toBeFalsy();
  });
});
