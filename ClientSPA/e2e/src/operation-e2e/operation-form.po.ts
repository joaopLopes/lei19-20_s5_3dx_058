import {browser, by, element} from 'protractor';

export class OperationFormPage {

  navigateTo() {
    return browser.get('/operations/new') as Promise<any>;
  }

  getTitle() {
    return element(by.css('.title')).getText();
  }

  getSubmitButton() {
    return element(by.css('.submit'));
  }

  getDescriptionForm() {
    return element(by.cssContainingText('.word', 'Description'));
  }

  getToolForm() {
    return element(by.cssContainingText('.word', 'Tool'));
  }

  getExecutionTimeForm() {
    return element(by.cssContainingText('.word', 'Execution Time'));
  }

  getSetupTimeForm() {
    return element(by.cssContainingText('.word', 'Setup Time'));
  }


}
