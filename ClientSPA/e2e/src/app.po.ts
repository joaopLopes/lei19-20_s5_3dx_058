import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getSidenav() {
    return element(by.css('app-root .sidenav'));
  }

  getSidenavTitle() {
    return element(by.css('app-root h4')).getText();
  }

  getSidenavDashboard() {
    return element(by.cssContainingText('app-root a', 'Dashboard'));
  }

  getSidenavProducts() {
    return element(by.cssContainingText('app-root a', 'Products'));
  }

  getSidenavOperations() {
    return element(by.cssContainingText('app-root a', 'Operations'));
  }

  getSidenavMachines() {
    return element(by.cssContainingText('app-root a', 'Machines'));
  }

  getSidenavMachineTypes() {
    return element(by.cssContainingText('app-root a', 'Machine Types'));
  }

  getSidenavProductionLines() {
    return element(by.cssContainingText('app-root a', 'Production Lines'));
  }

  getSidenavExecutionVis() {
    return element(by.cssContainingText('app-root a', 'Execution Visualization'));
  }

  clickLogin() {
    return element(by.cssContainingText('app-root a', 'Login')).click();
  }

  getUsernameField() {
    return element(by.cssContainingText('.username', ''));
  }

  getPasswordField() {
    return element(by.cssContainingText('.password', ''));
  }

  getSubmitButton() {
    return element(by.cssContainingText('.btn', 'Login'));
  }

  getClientArea() {
    return element(by.cssContainingText('app-root a', 'Client Area'));
  }

  getAdminArea() {
    return element(by.cssContainingText('app-root a', 'Admin Area'));
  }

  getSidenavAbout() {
    return element(by.cssContainingText('app-root a', 'Team'));
  }

}
