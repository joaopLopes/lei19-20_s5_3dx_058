import {browser, by , element} from 'protractor';

export class ProductionLinePage {

  navigateToMainPage() {
    return browser.get('/production-lines') as Promise<any>;
  }

  getHeaderText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }


  getProductionLines() {
    return element.all(by.css('.production-line'));
  }

}
