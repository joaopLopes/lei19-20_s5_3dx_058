import {ProductionLinePage} from './production-line.po';
import {LoginPage} from '../login/login.po';
describe('workspace-project App', () => {
  let page: ProductionLinePage;
  const loginPage: LoginPage = new LoginPage();
  beforeAll(async () => {
    page = new ProductionLinePage();
    await loginPage.loginAsAdmin();
  });
  it('should exist', () => {
    page.navigateToMainPage();
  });
  it('title should be Production Lines', () => {
      page.navigateToMainPage();
      expect(page.getHeaderText()).toEqual('Production Lines');
  });
  it('All production lines', () => {
      page.navigateToMainPage();
      const num = 1;
      const array = page.getProductionLines().count();
      expect(array).toBe(num);
  });
});
