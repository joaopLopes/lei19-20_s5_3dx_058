import {browser, by, element} from 'protractor';

export class LoginPage {
  private credentialsAdmin = {
    username: 'admin',
    password: 'admin#99'
  };
  // TODO: corrigir
  private credentialsClient = {
    username: 'clientForTesting',
    password: 'client#99'
  };

  navigateTo() {
    return browser.get('/login') as Promise<any>;
  }

  async loginAsAdmin() {
    await this.navigateTo();
    await browser.element(by.className('username')).sendKeys(this.credentialsAdmin.username);
    await browser.element(by.className('password')).sendKeys(this.credentialsAdmin.password);
    await browser.element(by.className('btn')).click();
    return browser.wait(async () => {
      const url = await browser.getCurrentUrl();
      return /home/.test(url);
    }, 10000);
  }

  async loginAsClient() {
    await this.navigateTo();
    await browser.element(by.className('username')).sendKeys(this.credentialsClient.username);
    await browser.element(by.className('password')).sendKeys(this.credentialsClient.password);
    await browser.element(by.className('btn')).click();
    return browser.wait(async () => {
      const url = await browser.getCurrentUrl();
      return /home/.test(url);
    }, 10000);
  }
}
