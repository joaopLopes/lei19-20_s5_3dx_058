import {AppPage} from './app.po';
import {browser, logging} from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display sidenav', () => {
    page.navigateTo();
    expect(page.getSidenav()).toBeDefined();
  });
  it('sidenav should have title', () => {
    page.navigateTo();
    expect(page.getSidenavTitle()).toEqual('My Cutlery');
  });
  it('should have Dashboard menu', () => {
    page.navigateTo();
    expect(page.getSidenavDashboard().getText()).toEqual('Dashboard');
  });
  it('should have Products menu', () => {
    page.navigateTo();
    expect(page.getSidenavProducts().getText()).toEqual('Products');
  });
  it('should have Operations menu', () => {
    page.navigateTo();
    expect(page.getSidenavOperations().getText()).toEqual('Operations');
  });
  it('should have Machines menu', () => {
    page.navigateTo();
    expect(page.getSidenavMachines().getText()).toEqual('Machines');
  });
  it('should have Machine Types menu', () => {
    page.navigateTo();
    expect(page.getSidenavMachineTypes().getText()).toEqual('Machine Types');
  });
  it('should have About menu', () => {
    page.navigateTo();
    expect(page.getSidenavAbout().getText()).toEqual('The Team');
  });
  it('should have Production Lines menu', () => {
    page.navigateTo();
    expect(page.getSidenavProductionLines().getText()).toEqual('Production Lines');
  });
  /*
    afterEach(async () => {
      // Assert that there are no errors emitted from the browser
      const logs = await browser.manage().logs().get(logging.Type.BROWSER);
      expect(logs).not.toContain(jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry));
    });
    */
});
