import {User} from './user';

export class Admin extends User {

  constructor(name, pass: string){
    super(name, pass);
  }
}
