import {PredictDate} from './predict-date';

export class ProductForOrder {
  name: string;
  price: number;
  quantity: number;
  deliveryDate: Date;
  predictedDeliveryDate: Date;

  constructor(name: string, price: number, quantity: number, deliveryDate?: Date, predictedDeliveryDate?: Date) {
    this.name = name;
    this.price = price;
    this.quantity = quantity;
    this.deliveryDate = deliveryDate;
    if (predictedDeliveryDate) {
      this.predictedDeliveryDate = predictedDeliveryDate;
    }
  }
}

export class Order {
  reference: string;
  client: string;
  shippingAddress: string;
  placementDate: Date;
  creditCardUsed: string;
  totalPrice: number;
  state: string;
  products: ProductForOrder[];

  constructor(client?: string, shippingAddress?: string, creditCardUsed?: string, products?: ProductForOrder[]) {
    this.client = client;
    this.shippingAddress = shippingAddress;
    this.creditCardUsed = creditCardUsed;
    this.products = products;
  }
}
