import {User} from './user';

export class Client extends User {
  email: string;
  address: string;
  phoneNumber: string;
  nif: string;
  age: string;
  firstName: string;
  lastName: string;
  lastUsedCard: string;
  priority: number;

  constructor(name, pass, email, phoneNumber, nif, age, address: string, firstName: string, lastName: string) {
    super(name, pass);
    this.email = email;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.nif = nif;
    this.age = age;
    this.firstName = firstName;
    this.lastName = lastName;
  }

}
