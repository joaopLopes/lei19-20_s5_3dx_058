export class FabricationPlan {
  operations: string[];

  constructor(operations: string[]) {
    this.operations = operations;
  }
}
