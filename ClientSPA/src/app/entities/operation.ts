export class Operation {
  tool: string;
  description: string;
  executionTime: number;
  setupTime: number;

  constructor(description: string, tool: string, executionTime: number, setupTime: number) {
    this.description = description;
    this.tool = tool;
    this.executionTime = executionTime;
    this.setupTime = setupTime;
  }
}
