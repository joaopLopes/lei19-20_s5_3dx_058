import {FabricationPlan} from './fabrication-plan';

export class Product {
  constructor(name: string, fabricationPlan: FabricationPlan, price: number) {
    this.name = name;
    this.price = price;
    this.fabricationPlan = fabricationPlan;
  }

  name: string;
  price: number;
  fabricationPlan: FabricationPlan;

  setFabricationPlan(plan: FabricationPlan): void {
    this.fabricationPlan = plan;
  }
}
