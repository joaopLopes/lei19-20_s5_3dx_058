export class MachineAssociated {
  machineSerialNumber: string;
  slot: number;

  constructor(serialNumber: string, slot: number) {
    this.machineSerialNumber = serialNumber;
    this.slot = slot;
  }
}

export class ProductionLine {

  productionLineIdentifier: string;
  listAssociatedMachinesDto: MachineAssociated[];


  constructor(productionLineIdentifier: string, listAssociatedMachinesDTO: MachineAssociated[]) {
    this.productionLineIdentifier = productionLineIdentifier;
    this.listAssociatedMachinesDto = listAssociatedMachinesDTO;
  }
}
