
export class ProductProductionPlanning {
  constructor(reference: string, product: string) {
    this.reference = reference;
    this.product = product;
  }

  reference: string;
  product: string;

}
