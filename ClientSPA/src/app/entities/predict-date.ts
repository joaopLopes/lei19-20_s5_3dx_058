export class PredictDate {
  constructor(ano: number, mes: number, dia: number) {
    this.ano = ano;
    this.mes = mes;
    this.dia = dia;
  }

  ano: number;
  mes: number;
  dia: number;

}
