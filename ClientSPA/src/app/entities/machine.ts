export class Machine {
  serialNumber: string;
  model: string;
  brand: string;
  machineType: string;
  machineWorkingState: string;

  constructor(SerialNumber: string, Model: string, Brand: string, MachineType: string) {
    this.serialNumber = SerialNumber;
    this.model = Model;
    this.brand = Brand;
    this.machineType = MachineType;
    this.machineWorkingState = 'Deactivated';
  }
}
