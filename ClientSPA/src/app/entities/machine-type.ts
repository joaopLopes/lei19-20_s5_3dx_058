export class MachineType {
  name: string;
  listMacTypeOperations: string[];

  constructor(name: string, ops: string[]) {
    this.name = name;
    this.listMacTypeOperations = ops;
  }
}
