export class Line {

  /**
   * Constructor for the entity.
   * @param line A string representative of the order machines.
   */
  constructor(linha: string) {
    this.linha = linha;
  }

  linha: string; // Attribute of the order machines.


}

