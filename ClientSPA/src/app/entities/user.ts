export abstract class User {
  username: string;
  password: string;

  protected constructor(name, pass: string) {
    this.username = name;
    this.password = pass;
  }
}
