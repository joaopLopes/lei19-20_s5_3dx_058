export class OrderMachines {

  /**
   * Constructor for the entity.
   * @param orders A string representative of the order machines.
   */
  constructor(orders: string) {
    this.orders = orders;
  }

  orders: string; // Attribute of the order machines.


  }


