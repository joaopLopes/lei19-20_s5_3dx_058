import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ProductsComponent} from './components/product/products/products.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatButtonModule,
  MatFormFieldModule,
  MatMenuModule,
  MatSelectModule, MatDatepickerModule, MAT_DATE_LOCALE, MatNativeDateModule, MatInputModule, MatRadioModule, MatCardModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {ProductDetailComponent} from './components/product/product-detail/product-detail.component';
import {MainPageComponent} from './components/main-page/main-page.component';
import {AboutComponent} from './components/about/about.component';
import {ProductFormComponent} from './components/product/product-form/product-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OperationDetailComponent} from './components/operation/operation-detail/operation-detail.component';
import {OperationsComponent} from './components/operation/operations/operations.component';
import {OperationFormComponent} from './components/operation/operation-form/operation-form.component';
import {AlertComponent} from './components/alert/alert.component';
import {ErrorInterceptor} from './interceptor';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MachineTypesComponent} from './components/machine-type/machine-types/machine-types.component';
import {ProductionLinesComponent} from './components/productionLine/production-lines/production-lines.component';
import {ProductionLineDetailComponent} from './components/productionLine/production-line-detail/production-line-detail.component';
import {ProductionLineFormComponent} from './components/productionLine/production-line-form/production-line-form.component';
import {MachinetypeFormComponent} from './components/machine-type/machine-type-form/machinetype-form.component';
import {MachinesComponent} from './components/machine/machines/machines.component';
import {MachineFormComponent} from './components/machine/machine-form/machine-form.component';
import {MachineDetailComponent} from './components/machine/machine-detail/machine-detail.component';
import {VizCanvasComponent} from './components/viz-canvas/viz-canvas.component';
import {MachineTypeUpdateOperationsComponent} from './components/machine-type/machine-type-update/machine-type-update-operations.component';
import {MachineTypeDetailComponent} from './components/machine-type/machine-type-detail/machine-type-detail.component';
import {LoginFormComponent} from './components/login/login-form/login-form.component';
import {CreateAccountComponent} from './components/login/create-account/create-account.component';
import {OrderDetailComponent} from './components/orders/order-detail/order-detail.component';
import {OrdersComponent} from './components/orders/orders/orders.component';
import {ClientAreaComponent} from './components/client-area/client-area.component';
import {AdminAreaComponent} from './components/admin-area/admin-area.component';
import {MyOrdersComponent} from './components/orders/my-orders/my-orders.component';
import {ClientDetailComponent} from './components/client/client-detail/client-detail.component';
import {ClientUpdateComponent} from './components/client/client-update/client-update.component';
import {OrderUpdateComponent} from './components/orders/order-update/order-update.component';
import {ClientsComponent} from './components/client/clients/clients.component';
import {LogoutComponent} from './components/logout/logout.component';
import { StoreComponent } from './components/store/store.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import { ProductDetailStoreComponent } from './components/product/product-detail-store/product-detail-store.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProductionPlanningComponent } from './components/production-planning/production-planning.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductDetailComponent,
    MainPageComponent,
    AboutComponent,
    ProductFormComponent,
    MachineTypesComponent,
    MachinetypeFormComponent,
    OperationDetailComponent,
    OperationsComponent,
    OperationFormComponent,
    AlertComponent,
    ProductionLinesComponent,
    ProductionLineDetailComponent,
    ProductionLineFormComponent,
    MachinesComponent,
    MachineFormComponent,
    MachineDetailComponent,
    MachineTypeUpdateOperationsComponent,
    VizCanvasComponent,
    MachineTypeDetailComponent,
    LoginFormComponent,
    CreateAccountComponent,
    OrderDetailComponent,
    OrdersComponent,
    ClientAreaComponent,
    AdminAreaComponent,
    MyOrdersComponent,
    ClientDetailComponent,
    CreateAccountComponent,
    ClientUpdateComponent,
    OrderUpdateComponent,
    ClientsComponent,
    LogoutComponent,
    StoreComponent,
    ShoppingCartComponent,
    ProductDetailStoreComponent,
    PageNotFoundComponent,
    ProductionPlanningComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatMenuModule,
    ReactiveFormsModule,
    DragDropModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    FormsModule,
    MatRadioModule,
    MatCardModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
  }, {
    provide: MAT_DATE_LOCALE, useValue: 'en-GB',
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
