export class PermissionType {
  static readonly LIST_CLIENTS = new PermissionType('list-clients', 'all', 'none', null, 'name-only');
  static readonly CONSULT_CLIENT = new PermissionType('consult-client', 'any', 'none', 'himself', 'name-only');
  static readonly UPDATE_CLIENT = new PermissionType('update-client', 'any', 'none', 'himself');
  static readonly DELETE_CLIENT = new PermissionType('delete-client', 'any', 'none', 'himself');
  static readonly LIST_ORDERS = new PermissionType('list-orders', 'any', 'none', 'himself');
  static readonly CONSULT_ORDER = new PermissionType('consult-order', 'any', 'none', 'himself');
  static readonly CREATE_ORDER = new PermissionType('create-order', 'yes', 'no');
  static readonly UPDATE_ORDER = new PermissionType('update-order', 'any', 'none', 'himself');
  static readonly CANCEL_ORDER = new PermissionType('cancel-order', 'any', 'none', 'himself');

  // private to disallow creating other instances of this type
  private constructor(private readonly key: string,
                      private readonly allowedValue: string,
                      private readonly disallowedValue: string,
                      private readonly himself?: string,
                      private readonly limitedInfo?: string) {
  }

  isAllowed(permission: string, userOnly: boolean, limitedInfo: boolean): boolean {
    switch (permission) {
      case this.allowedValue:
        return true;
      case this.disallowedValue:
        return false;
      case this.himself:
        return userOnly;
      case this.limitedInfo:
        return limitedInfo;
      default:
        return false;
    }
  }

  toString() {
    return this.key;
  }
}
