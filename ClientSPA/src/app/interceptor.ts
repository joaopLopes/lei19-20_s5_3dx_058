import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {LogService} from './services/log.service';
import {AlertService} from './services/alert.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private logService: LogService, private alertService: AlertService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      this.logService.log(err.error);
      const message = err.error;
      return throwError(message);
    }));
  }
}
