import {TestBed} from '@angular/core/testing';

import {OrderService} from './order.service';
import {CookieService} from 'ngx-cookie-service';
import {OperationService} from './operation.service';
import {MockOperationService} from './mocks/mock-operation.service';
import {MockOrderService} from './mocks/mock-order.service';

describe('OrderService', () => {

  beforeEach(() => TestBed.configureTestingModule({
      providers: [{
        provide: OrderService, useClass: MockOrderService
      }]
    }
  ));

  it('should be created', () => {
    const service: OrderService = TestBed.get(OrderService);
    expect(service).toBeTruthy();
  });
});
