import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {MachineType} from '../entities/machine-type';
import {LogService} from './log.service';
import {Operation} from '../entities/operation';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class MachineTypeService {

  private url = environment.MDF + '/machineTypes';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  /** GET machine types from the server */
  getMachineTypes(): Observable<MachineType[]> {
    this.setTokenHeader();
    return this.http.get<MachineType[]>(this.url, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched machine types'))
      );
  }

  /** GET operations of machine type */
  getMachineTypeOperations(machineTypeId: string): Observable<Operation[]> {
    this.setTokenHeader();
    const url = `${this.url}/${machineTypeId}/operations`;
    return this.http.get<Operation[]>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched machine type operations of machine type id=${machineTypeId}`))
    );
  }

  /**
   * Updates the machine types operations in database
   */
  updateMachineTypeOperations(id: string, listOperations: MachineType): Observable<MachineType> {
    this.setTokenHeader();
    return this.http.put<MachineType>(this.url + '/' + id, listOperations, this.httpOptions).pipe(
      tap((updateMachineType: MachineType) => this.log(`Updated machine type with ID =${updateMachineType.name}`)));
  }

  private log(message: string) {
    this.logService.log(`ProductionLineService: ${message}`);
  }

  /** GET operation by id. Will 404 if id not found */
  getMachineType(name: string): Observable<MachineType> {
    this.setTokenHeader();
    const url = `${this.url}/${name}`;
    return this.http.get<MachineType>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched machine type name=${name}`))
    );
  }

  //////// Save methods //////////

  /** POST: add a new operation to the server */
  addMachineType(machinetype: MachineType): Observable<Operation> {
    this.setTokenHeader();
    return this.http.post<Operation>(this.url, machinetype, this.httpOptions).pipe(
      tap((newOperation: Operation) => this.log(`added operation w/ description=${newOperation.description}`))
    );
  }
}
