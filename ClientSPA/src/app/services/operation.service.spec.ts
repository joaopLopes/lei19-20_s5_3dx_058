import {TestBed} from '@angular/core/testing';

import {OperationService} from './operation.service';
import {MockOperationService} from './mocks/mock-operation.service';

describe('OperationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{provide: OperationService, useClass: MockOperationService}]
  }));

  it('should be created', () => {
    const service: OperationService = TestBed.get(OperationService);
    expect(service).toBeTruthy();
  });
});
