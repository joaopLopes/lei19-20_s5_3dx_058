import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {Product} from '../entities/product';
import {FabricationPlan} from '../entities/fabrication-plan';
import {LogService} from './log.service';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';


@Injectable({providedIn: 'root'})
export class ProductService {

  private productsUrl = environment.MDP + '/products';
  private productsGEUrl = environment.GE + '/products';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  /** GET heroes from the server */
  getProducts(): Observable<Product[]> {
    this.setTokenHeader();
    return this.http.get<Product[]>(this.productsUrl, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched products'))
      );
  }

  /** GET heroes from the server */
  getProductsForStore(): Observable<Product[]> {
    this.setTokenHeader();
    return this.http.get<Product[]>(this.productsGEUrl, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched products'))
      );
  }

  getProductsWithFilter(criteria: string): Observable<Product[]> {
    this.setTokenHeader();
    return this.http.get<Product[]>(this.productsGEUrl + '/sortby/' + criteria, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched products with filter: ' + criteria))
      );
  }

  /** GET product by id. Will 404 if id not found */
  getProductForStoreById(name: string): Observable<Product> {
    this.setTokenHeader();
    const url = `${this.productsGEUrl}/${name}`;
    return this.http.get<Product>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched product name=${name}`))
    );
  }

  /** GET product by id. Will 404 if id not found */
  getProduct(name: string): Observable<Product> {
    this.setTokenHeader();
    const url = `${this.productsUrl}/${name}`;
    return this.http.get<Product>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched product name=${name}`))
    );
  }

  /** GET products fabrication plan by id. Will 404 if id not found */
  getProductsFabricationPlan(name: string): Observable<FabricationPlan> {
    this.setTokenHeader();
    const url = `${this.productsUrl}/${name}/fabrication-plan`;
    return this.http.get<FabricationPlan>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched product name=${name}`)));
  }

  //////// Save methods //////////

  /** POST: add a new product to the server */
  addProduct(product: Product): Observable<Product> {
    this.setTokenHeader();
    return this.http.post<Product>(this.productsUrl, product, this.httpOptions).pipe(
      tap((newProduct: Product) => this.log(`added product w/ name=${newProduct.name}`)));
  }

  /** Log a ProductService message with the MessageService */
  private log(message: string) {
    this.logService.log(`ProductService: ${message}`);
  }
}
