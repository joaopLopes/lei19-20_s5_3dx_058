import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {LogService} from './log.service';
import {Order, ProductForOrder} from '../entities/order';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';
import {Product} from '../entities/product';


@Injectable({providedIn: 'root'})
export class StoreService {

  private orderUrl = environment.GE + '/orders';
  private shoppingCart: Order;


  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    withCredentials: true
  };

  constructor(
    private http: HttpClient, private logService: LogService, private authService: AuthService) {
    if (!localStorage.getItem('cart')) {
      this.shoppingCart = new Order();
      this.shoppingCart.products = [];
      localStorage.setItem('cart', JSON.stringify(this.shoppingCart));
    } else {
      this.shoppingCart = JSON.parse(localStorage.getItem('cart'));
    }
  }

  private addProduct(price: number, prod: string, quantity: number, deliveryDate: Date) {
    this.shoppingCart.products.push(new ProductForOrder(prod, price, quantity, deliveryDate));
  }

  private removeProduct(prod: string) {
    const indexToBeRemoved = this.shoppingCart.products.findIndex(value => {
      return value.name = prod;
    });
    if (indexToBeRemoved > -1) {
      this.shoppingCart.products.splice(indexToBeRemoved, 1);
      localStorage.setItem('cart', JSON.stringify(this.shoppingCart));
    }
  }

  addProductToShoppingCart(prod: Product, quantity: number, deliveryDate: Date) {
    this.addProduct(prod.price, prod.name, quantity, deliveryDate);
    localStorage.setItem('cart', JSON.stringify(this.shoppingCart));
  }

  fillOrderDetails(address: string, creditCard: string) {
    this.shoppingCart.creditCardUsed = creditCard;
    this.shoppingCart.shippingAddress = address;
    this.shoppingCart.client = this.authService.getLoggedUser();
    localStorage.setItem('cart', JSON.stringify(this.shoppingCart));
  }

  /** POST: add a new operation to the server */
  executeOrder(): Observable<Order> {
    if (this.shoppingCart.products.length === 0) {
      throw new Error('Your shopping cart is empty!');
    }
    this.setTokenHeader();
    return this.http.post<Order>(this.orderUrl, this.shoppingCart, this.httpOptions).pipe(
      tap((newOrder: Order) => this.log(`added order w/ reference=${newOrder.reference}`))
    );
  }

  getProductsToBeOrdered() {
    return this.shoppingCart.products;
  }

  removeProductFromOrder(name: string) {
    this.removeProduct(name);
  }

  clearShoppingCart() {
    this.shoppingCart = new Order();
    this.shoppingCart.products = [];
    localStorage.removeItem('cart');
  }

  setTokenHeader() {
    const tokenStr = this.authService.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  private log(message: string) {
    this.logService.log(`ProductionLineService: ${message}`);
  }

  hasItems() {
    if (!this.shoppingCart || !this.shoppingCart.products) {
      return false;
    } else {
      return this.shoppingCart.products.length > 0;
    }
  }
}
