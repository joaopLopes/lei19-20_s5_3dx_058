import { TestBed } from '@angular/core/testing';

import { ProductionPlanningService } from './production-planning.service';
import {MockProductionPlanningService} from './mocks/mock-production-planning.service';

describe('ProductionPlanningService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionPlanningService = TestBed.get(MockProductionPlanningService);
    expect(service).toBeTruthy();
  });
});
