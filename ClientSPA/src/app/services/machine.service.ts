import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {Machine} from '../entities/machine';
import {LogService} from './log.service';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class MachineService {

  private url = environment.MDF + '/machines';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };


  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  /** POST: add a new machine to the server */
  addMachine(machine: Machine): Observable<Machine> {
    this.setTokenHeader();
    return this.http.post<Machine>(this.url, machine, this.httpOptions).pipe(
      tap((newMachine: Machine) => this.log(`added machine w/ name=${newMachine.serialNumber}`)));
  }

  /** GET machines from the server */
  getMachines(): Observable<Machine[]> {
    this.setTokenHeader();
    return this.http.get<Machine[]>(this.url, this.httpOptions)
      .pipe(
        tap(_ => this.log('Fetched Machines'))
      );
  }

  /** GET machine by its identifier. Will 404 if id not found */
  getMachine(machineSerialNumber: string): Observable<Machine> {
    this.setTokenHeader();
    const url = `${this.url}/${machineSerialNumber}`;
    return this.http.get<Machine>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched Machine with Serial Number=${machineSerialNumber}`))
    );
  }

  /** GET machine by machineType. Will 404 if id not found */
  getMachineByMachineType(machineType: string): Observable<Machine[]> {
    this.setTokenHeader();
    const url = `${this.url}/by-type/${machineType}`;
    return this.http.get<Machine[]>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched Machine with Type=${machineType}`))
    );
  }

  putMachine(machine: Machine): Observable<Machine> {
    this.setTokenHeader();
    const url = `${this.url}/${machine.serialNumber}`;
    return this.http.put<Machine>(url, machine, this.httpOptions).pipe(
      tap((updatedMachine: Machine) => this.log(`updated machine w/ name=${updatedMachine.serialNumber}`)));
  }

  private log(message: string) {
    this.logService.log(`MachineService: ${message}`);
  }

  /**
   * Activates a machine.
   */
  activateMachine(machineSerialNumber: string): Observable<Machine> {
    this.setTokenHeader();
    const url = `${this.url}/activate/${machineSerialNumber}`;
    return this.http.put<Machine>(url, machineSerialNumber, this.httpOptions).pipe(
      tap(_ => this.log(`Activated Machine with serial Number =${machineSerialNumber}`)));

  }

  /**
   * Allows a user to deactivate a machine.
   */
  deactivateMachine(machineSerialNumber: string): Observable<Machine> {
    this.setTokenHeader();
    const url = `${this.url}/deactivate/${machineSerialNumber}`;
    return this.http.put<Machine>(url, machineSerialNumber, this.httpOptions).pipe(
      tap(_ => this.log(`Activated Machine with serial Number =${machineSerialNumber}`)));
  }


}
