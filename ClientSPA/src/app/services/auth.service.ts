import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LogService} from './log.service';
import {Login} from '../entities/login';
import * as jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import {Observable, of, throwError} from 'rxjs';
import {logger} from 'codelyzer/util/logger';
import {PermissionType} from '../authentication/PermissionType';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private url = environment.GE + '/login';
  private localTokenName = 'session-token';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    observe: 'response' as 'body',
    withCredentials: true
  };

  constructor(
    private http: HttpClient, private logService: LogService) {
  }

  login(username, password: string): Promise<string> {
    const data = new Login(username, password);
    return new Promise<string>(async (resolve, reject) => {
      await this.http.post<Response>(this.url, data, this.httpOptions).subscribe(authRes => {
        if (authRes.status === 201) {
          const token = authRes.headers.get('Authorization');
          if (!token) {
            this.logService.log('error: No auth token received.');
            reject('No auth token received.');
            return;
          }
          localStorage.setItem(this.localTokenName, token);
          this.logService.log('User logged in.');
          resolve('User logged in.');
          return;
        } else {
          this.logService.log('error: Credentials do not match any user.');
          reject('Credentials do not match any user.');
          return;
        }
      }, error => {
        this.logService.log(error);
        reject('Credentials do not match any user.');
        return;
      });
    });
  }

  logout(): void {
    localStorage.removeItem(this.localTokenName);
  }

  isLoggedIn(): boolean {
    try {
      const token = this.getToken();
      return moment().isBefore(this.getExpiration());
    } catch (e) {
      return false;
    }
  }

  loggedInStatus(): Observable<boolean> {
    return of(this.isLoggedIn());
  }

  private getExpiration() {
    const token = this.getToken();
    const tokenString = JSON.stringify(jwt_decode(token));
    const tokenJSON = JSON.parse(tokenString);
    const expiration = moment().add(tokenJSON.exp, 'second');
    return moment(expiration);
  }

  getRole(): string {
    const token = this.getToken();
    const tokenString = JSON.stringify(jwt_decode(token));
    const tokenJSON = JSON.parse(tokenString);
    return tokenJSON.role;
  }

  getLoggedUser(): string {
    const token = this.getToken();
    const tokenString = JSON.stringify(jwt_decode(token));
    const tokenJSON = JSON.parse(tokenString);
    return tokenJSON.user;
  }

  getToken(): any {
    const token = localStorage.getItem(this.localTokenName);
    if (!token) {
      throwError('Not logged in');
    } else {
      return token;
    }
  }

  isAdmin(): boolean {
    try {
      return this.getRole() === 'admin';
    } catch (e) {
      logger.error(e);
      return false;
    }
  }

  hasPermission(method: PermissionType, userOnly: boolean, limitedInfo?: boolean): boolean {
    try {
      const token = this.getToken();
      const tokenString = JSON.stringify(jwt_decode(token));
      const tokenJSON = JSON.parse(tokenString);
      const permissions = tokenJSON.permissions;
      if (!permissions) {
        throwError('No permissions found.');
      }
      const permission = permissions[method.toString()];
      if (!permission) {
        return false;
      } else {
        return method.isAllowed(permission, userOnly, limitedInfo);
      }
    } catch (e) {
      return false;
    }
  }


  isClient() {
    try {
      return this.getRole() === 'client';
    } catch (e) {
      logger.error(e);
      return false;
    }
  }
}
