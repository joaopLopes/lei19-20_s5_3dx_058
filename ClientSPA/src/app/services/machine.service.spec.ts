import { TestBed } from '@angular/core/testing';

import { MachineService } from './machine.service';
import {HttpClientModule} from '@angular/common/http';

describe('MachineService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientModule]}));

  it('should be created', () => {
    const service: MachineService = TestBed.get(MachineService);
    expect(service).toBeTruthy();
  });
});
