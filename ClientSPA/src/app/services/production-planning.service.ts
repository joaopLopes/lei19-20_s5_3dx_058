import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ProductProductionPlanning} from '../entities/product-production-planning';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {PredictDate} from '../entities/predict-date';
import {OrderMachines} from '../entities/order-machines';
import {Line} from '../entities/line';

@Injectable({
  providedIn: 'root'
})
export class ProductionPlanningService {

  private productionLinesURLGera = environment.PROLOG + '/generate';  // URL to web api to generate the database in Prolog.
  // URL to web api to get the date of delivery of a product.
  private productionLinesGetDatas = environment.PROLOG + '/product-delivery-date';
  private bringsJsonURL = environment.PROLOG + '/schedule'; // URL that gets the balance in a text string.
  private balanceLinesURL = environment.PROLOG + '/balance-lines'; // URL that balances the lines.
  private activateMachineURL = environment.PROLOG + '/activate-machine'; // URL that activates a machine.
  private deactivateMachineURL = environment.PROLOG + '/deactivate-machine'; // URL that deactivates a machine.

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  /**
   * Generates the production planning knowledge base
   */
  generatePrologDatabase(startDate: Date, endDate: Date): Observable<string> {
    const message = '';
    const startDateString: string = startDate.getDate() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getFullYear();
    const endDateString: string = endDate.getDate() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getFullYear();
    const url = this.productionLinesURLGera + '?startDate=' + startDateString + '&endDate=' + endDateString;
    return this.http.post<string>(url, message, this.httpOptions).pipe(tap(_ => console.log('generated.')));
  }

  /**
   * HTTP method that is the integration with PROLOG that returns the order that the machines
   * are going to act in the execution visualization.
   * Json comes in a string due to technological limitations.
   * In case of errors it brings to console temporarily.
   */
  bringsOrderJSON(line: Line): Observable<OrderMachines> {
    const url = `${this.bringsJsonURL}?linha=${line.linha}`;
    return this.http.get<OrderMachines>(url, this.httpOptions).pipe(tap(_ => console.log('got scheduling information')));
  }

  /**
   * Method that directly changes a machine in the prolog database to activated.
   * @param machineDomainIdentifier a string that is the machine domain identifier.
   */
  activateMachine(machineDomainIdentifier: string): void {
    const url = `${this.activateMachineURL}?machine=${machineDomainIdentifier}`;
    this.http.get<void>(url, this.httpOptions).pipe(tap(_ => console.log('Activate   Machine'))).subscribe();
  }

  /**
   * Method that directly changes a machine in the prolog database to deactivated.
   * @param machineDomainIdentifier a string that is the machine domain identifier.
   */

  deactivateMachine(machineDomainIdentifier: string): void {
    const url = `${this.deactivateMachineURL}?machine=${machineDomainIdentifier}`;
    this.http.get<void>(url, this.httpOptions).pipe(tap(_ => console.log('Deactivated Machine'))).subscribe();

  }


  /**
   * HTTP method that is the integration with PROLOG that returns the
   * order that the machines are going to act in the execution visualization.
   * Json comes in a string due to technological limitations.
   * In case of errors it brings to console temporarily.
   */
  balanceLines(): Observable<void> {
    const url = `${this.balanceLinesURL}`;
    return this.http.get<void>(url, this.httpOptions).pipe(tap(_ => console.log('Balanced lines')));
  }

  /**
   * Returns the dates of a product dates.
   * @param productOnlyName the json that is being posted.
   */
  getProductsPredictedDeliveryDate(productOnlyName: ProductProductionPlanning): Observable<PredictDate> {
    const url = `${this.productionLinesGetDatas}?reference="?${productOnlyName.reference}?"&product="?${productOnlyName.product}?"`;
    return this.http.get<PredictDate>(url, this.httpOptions).pipe(
      tap(_ =>
        console.error(`fetched DATE!`)
      ));
  }

}
