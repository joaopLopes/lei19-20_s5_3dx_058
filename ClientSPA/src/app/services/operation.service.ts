import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {Operation} from '../entities/operation';
import {LogService} from './log.service';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';


@Injectable({providedIn: 'root'})
export class OperationService {

  private operationUrl = environment.MDF + '/operations';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  /** GET operations from the server */
  getOperations(): Observable<Operation[]> {
    this.setTokenHeader();
    return this.http.get<Operation[]>(this.operationUrl, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched operations'))
      );
  }


  /** GET operation by id. Will 404 if id not found */
  getOperation(description: string): Observable<Operation> {
    this.setTokenHeader();
    const url = `${this.operationUrl}/${description}`;
    return this.http.get<Operation>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched operation name=${description}`))
    );
  }

  //////// Save methods //////////

  /** POST: add a new operation to the server */
  addOperation(operation: Operation): Observable<Operation> {
    this.setTokenHeader();
    return this.http.post<Operation>(this.operationUrl, operation, this.httpOptions).pipe(
      tap((newOperation: Operation) => this.log(`added operation w/ description=${newOperation.description}`))
    );
  }

  /** Log an OperationService message with the MessageService */
  private log(message: string) {
    this.logService.log(`OperationService: ${message}`);
  }
}
