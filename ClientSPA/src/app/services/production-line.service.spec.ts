import { TestBed } from '@angular/core/testing';

import { ProductionLineService } from './production-line.service';
import {HttpClientModule} from '@angular/common/http';

describe('ProductionLineService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [HttpClientModule]}));

  it('should be created', () => {
    const service: ProductionLineService = TestBed.get(ProductionLineService);
    expect(service).toBeTruthy();
  });
});
