import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {ProductionLine} from '../entities/production-line';
import {LogService} from './log.service';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';


@Injectable({providedIn: 'root'})
export class ProductionLineService {

  private productionLinesURL = environment.MDF + '/productionLines';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  /** GET operations from the server */
  getProductionLines(): Observable<ProductionLine[]> {
    this.setTokenHeader();
    return this.http.get<ProductionLine[]>(this.productionLinesURL, this.httpOptions)
      .pipe(
        tap(_ => this.log('Fetched Production Lines'))
      );
  }

  /** GET Production Line by its identifier. Will 404 if id not found */
  getProductionLine(productionLineIdentifier: string): Observable<ProductionLine> {
    this.setTokenHeader();
    const url = `${this.productionLinesURL}/${productionLineIdentifier}`;
    return this.http.get<ProductionLine>(url, this.httpOptions).pipe(
      tap(_ => this.log(`fetched production Line Name name=${productionLineIdentifier}`))
    );
  }

  /**
   * Fetches the available machines to the user so that he can latter build a valid production Line.
   */

  getAvailableMachines(): Observable<string[]> {
    this.setTokenHeader();
    const url = `${this.productionLinesURL}/availableMachines`;
    return this.http.get<string[]>(url, this.httpOptions)
      .pipe(
        tap(_ => this.log('Available Machines'))
      );
  }

  /** POST: add a new production Line to the server */
  addProductionLine(productionLine: ProductionLine): Observable<ProductionLine> {
    this.setTokenHeader();
    return this.http.post<ProductionLine>(this.productionLinesURL, productionLine, this.httpOptions).pipe(
      // tslint:disable-next-line:max-line-length
      tap((newProductionLine: ProductionLine) => this.log(`added Production Line w/ Identifier=${newProductionLine.productionLineIdentifier}`)));
  }

  /** PUT: changes a production line's machine list with a new one */
  updateProductionLine(productionLine: ProductionLine): Observable<string> {
    this.setTokenHeader();
    const url = `${this.productionLinesURL}/${productionLine.productionLineIdentifier}`;
    return this.http.put<string>(url, productionLine, this.httpOptions).pipe(
      // tslint:disable-next-line:max-line-length
      tap(_ => this.log(`updated Production Line w/ Identifier=${productionLine.productionLineIdentifier}`)));
  }

  /** DELETE: deletes a production line, given that it has no machines associated to it */
  deleteProductionLine(productionLine: ProductionLine): Observable<string> {
    this.setTokenHeader();
    const url = `${this.productionLinesURL}/${productionLine.productionLineIdentifier}`;
    return this.http.delete<string>(url, this.httpOptions).pipe(
      // tslint:disable-next-line:max-line-length
      tap(_ => this.log(`deleted Production Line w/ Identifier=${productionLine.productionLineIdentifier}`)));
  }
  private log(message: string) {
    this.logService.log(`ProductionLineService: ${message}`);
  }
}
