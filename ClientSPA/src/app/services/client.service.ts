import {Client} from '../entities/client';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LogService} from './log.service';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})

export class ClientService {

  private url = environment.GE + '/accounts';
  private url2 = environment.GE + '/clients';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    withCredentials: true
  };

  private log(message: string) {
    this.logService.log(`MachineService: ${message}`);
  }

  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  /** POST: add a new client to the server */
  addClient(client: Client): Observable<string> {
    return this.http.post<string>(this.url, client, this.httpOptions).pipe(
      tap((_) => this.log(`added client w/ name=${client.username}`)));
  }

  /**
   * Gets the client by username.
   */
  getClientByUsername(username: string): Observable<Client> {
    this.setTokenHeader();
    const urlClientUsername = `${this.url2}/${username}`;
    return this.http.get<Client>(urlClientUsername, this.httpOptions).pipe(tap(_ => this.log(`fetched client username=${username}`)));
  }

  /**
   * Updates the client with the new info.
   */
  updateClient(username: string, client: Client): Observable<Client> {
    this.setTokenHeader();
    const urlClientUsername = `${this.url2}/${username}`;
    return this.http.put<Client>(urlClientUsername, client, this.httpOptions).pipe(tap(_ =>
      this.log(`Updated client with username=${username}`)));
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }

  deleteClient(username: string): Observable<Client> {
    this.setTokenHeader();
    const urlClientUsername = `${this.url2}/${username}`;
    return this.http.delete<Client>(urlClientUsername, this.httpOptions).pipe(
      tap(_ => this.log('client ' + username + ' was deleted.'))
    );
  }

  getClients(): Observable<Client[]> {
    this.setTokenHeader();
    return this.http.get<Client[]>(this.url2, this.httpOptions)
      .pipe(
        tap(_ => this.log('fetched clients'))
      );
  }
}
