import { TestBed } from '@angular/core/testing';

import { MockOperationService } from './mock-operation.service';

describe('MockOperationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockOperationService = TestBed.get(MockOperationService);
    expect(service).toBeTruthy();
  });
});
