import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminMockAuthService {

  constructor() {
  }

  loggedUser: string;

  login(username, password: string): Promise<string> {
    this.loggedUser = username;
    return new Promise<string>(resolve => {
      resolve('Logged in');
    });
  }

  logout(): void {
    // do nothing
  }

  isLoggedIn(): boolean {
    return true;
  }


  getRole(): string {
    return 'admin';
  }

  getLoggedUser(): string {
    return this.loggedUser;
  }

  getToken(): string {
    return 'someToken';
  }

  isAdmin(): boolean {
    return true;
  }

  isClient(): boolean {
    return false;
  }

  hasPermission(): boolean {
    return true;
  }
}
