import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Machine} from '../../entities/machine';
import {MachineType} from '../../entities/machine-type';
import {Operation} from '../../entities/operation';


@Injectable({
  providedIn: 'root'
})
export class MockMachineService {

  private machines: Machine[] = [
    new Machine('CS23', 'Cutter and Saw M3', 'Johnson Machinery', 'type1'),
    new Machine('D11', 'Driller M1', 'Javier Machinery', 'type2')
  ];

  constructor() { }

  addMachine(machine) {
    this.machines.push(machine);
  }

  getMachines(): Observable<Machine[]> {
    return of(this.machines);
  }

  getMachine(serialNumber: string): Observable<Machine> {
    for (const machine of this.machines.values()) {
      if (machine.serialNumber === serialNumber) {
        return of(machine);
      }
    }
    return of(this.machines.pop());
  }


}
