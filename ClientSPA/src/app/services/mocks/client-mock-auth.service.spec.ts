import { TestBed } from '@angular/core/testing';

import { ClientMockAuthService } from './client-mock-auth.service';

describe('ClientMockAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientMockAuthService = TestBed.get(ClientMockAuthService);
    expect(service).toBeTruthy();
  });
});
