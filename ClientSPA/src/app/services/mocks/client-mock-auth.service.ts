import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientMockAuthService {

  constructor() {
  }

  loggedUser: string;

  login(username, password: string): Promise<string> {
    this.loggedUser = username;
    return new Promise<string>(resolve => {
      resolve('Logged in');
    });
  }

  logout(): void {
    // do nothing
  }

  isLoggedIn(): boolean {
    return true;
  }


  getRole(): string {
    return 'client';
  }

  getLoggedUser(): string {
    if (this.loggedUser) {
      return this.loggedUser;
    } else {
      return 'pedro';
    }
  }

  getToken(): string {
    return 'someToken';
  }

  isAdmin(): boolean {
    return false;
  }

  isClient(): boolean {
    return true;
  }

  hasPermission(): boolean {
    return true;
  }
}
