import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Operation} from '../../entities/operation';

@Injectable({
  providedIn: 'root'
})
export class MockOperationService {

  private operations: Operation[] = [
    new Operation('press-5mm', 'hammer', 10, 5),
    new Operation('cut-1mm', 'drill', 20, 10)
  ];


  constructor() {
  }

  /** GET operations from the server */
  getOperations(): Observable<Operation[]> {
    return of(this.operations);
  }


  /** GET operation by id. Will 404 if id not found */
  getOperation(description: string): Observable<Operation> {
    for (const operation of this.operations.values()) {
      if (operation.description === description) {
        return of(operation);
      }
    }
    return of(this.operations.pop());
  }

  /** POST: add a new operation to the server */
  addOperation(operation) {
    this.operations.push(operation);
  }
}
