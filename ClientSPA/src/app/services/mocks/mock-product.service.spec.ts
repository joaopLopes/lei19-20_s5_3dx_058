import { TestBed } from '@angular/core/testing';

import { MockProductService } from './mock-product.service';

describe('MockProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockProductService = TestBed.get(MockProductService);
    expect(service).toBeTruthy();
  });
});
