import { TestBed } from '@angular/core/testing';

import { AdminMockAuthService } from './admin-mock-auth.service';

describe('MockAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminMockAuthService = TestBed.get(AdminMockAuthService);
    expect(service).toBeTruthy();
  });
});
