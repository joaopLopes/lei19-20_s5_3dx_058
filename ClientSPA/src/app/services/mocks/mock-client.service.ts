import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Client} from '../../entities/client';
import {User} from '../../entities/user';


@Injectable({
  providedIn: 'root'
})

export class MockClientService {

  private clients: Client[] = [
    new Client('userTest', 'madmax#99', 'user@gmail.com', '914334189', '812850785', '20', 'rua do Amizade 111', 'Daniel', 'Estrela')
  ];

  constructor() { }


  /**
   * Mocked method that returns the client that we want to.
   * @param name The username of the client that we want to return.
   */

  getClientByUsername(name: string): Observable<Client> {
    for (const client of this.clients.values()) {
      if (client.username === name) {
        return of(client);
      }
    }
    return of(this.clients.pop());
  }


  /**
   * Mocked Method that allows a client to be updated.
   * @param username The username that we are fetching.
   * @param address The new address of the client.
   * @param firstName The first name of the client.
   * @param lastName The last name of the client.
   */
  updateClient(username: string, address: string, firstName: string, lastName: string): Observable<string> {
    for (const client of this.clients.values()) {
      if (client.username === username) {
        client.firstName = firstName;
        client.lastName = lastName;
        client.address = address;
        return of('');
      }
    }
  }

}
