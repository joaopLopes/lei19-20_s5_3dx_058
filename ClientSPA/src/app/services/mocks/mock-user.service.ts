import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Operation} from '../../entities/operation';
import {Client} from '../../entities/client';

@Injectable({
  providedIn: 'root'
})
export class MockUserService {

  private clients: Client[] = [
    new Client('joao', 'joao123#ASD', 'joao@gmail.pt', '938495558', '253622253', '30', 'Rua do Joao', 'Joao', 'Sousa')
  ];


  constructor() {
  }

  /** GET operations from the server */
  getClients(): Observable<Client[]> {
    return of(this.clients);
  }


  /** GET operation by id. Will 404 if id not found */
  getClientByUsername(username: string): Observable<Client> {
    for (const client of this.clients.values()) {
      if (client.username === username) {
        return of(client);
      }
    }
    return of(this.clients.pop());
  }

  /** POST: add a new operation to the server */
  addClient(client) {
    this.clients.push(client);
  }
}
