import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Order, ProductForOrder} from '../../entities/order';

@Injectable({
  providedIn: 'root'
})
export class MockOrderService {

  private orders: Order[] = [];
  private prod1: ProductForOrder = new ProductForOrder('spoon', 100, 60);
  private prod2: ProductForOrder = new ProductForOrder('fork', 200, 30);
  private products1: ProductForOrder[] = [];
  private products2: ProductForOrder[] = [];
  private order1: Order = new Order();
  private order2: Order = new Order();

  constructor() {
    this.prod1.deliveryDate = new Date('10/10/2030');
    this.prod2.deliveryDate = new Date('09/09/2030');
    this.products1.push(this.prod1);
    this.products1.push(this.prod2);
    this.products2.push(this.prod1);
    this.order1.products = this.products1;
    this.order1.creditCardUsed = 'PT501234123412341234';
    this.order1.shippingAddress = 'Rua das Flores';
    this.order1.reference = 'test-ref-ord1';
    this.order1.state = 'ONGOING';
    this.order1.placementDate = new Date('12/12/2019');
    this.order1.totalPrice = 12000;
    this.order1.client = 'pedro';

    this.order2.products = this.products2;
    this.order2.creditCardUsed = 'PT501234123412344321';
    this.order2.shippingAddress = 'Rua dos Martelos';
    this.order2.reference = 'test-ref-ord2';
    this.order2.state = 'ONGOING';
    this.order2.placementDate = new Date('12/12/2019');
    this.order2.totalPrice = 600;
    this.order2.client = 'joao';

    this.orders.push(this.order1);
    this.orders.push(this.order2);
  }

  /** GET orders from the server */
  getOrders(token: string): Observable<Order[]> {
    return of(this.orders);
  }

  /** GET orders from the server */
  getOrdersByClient(client: string, token: string): Observable<Order[]> {
    if (client === 'pedro') {
      return of([this.order1]);
    } else {
      return of([this.order2]);
    }
  }

  /** GET order by reference. Will 404 if id not found */
  getOrder(reference: string, token: string): Observable<Order> {
    if (reference === 'test-ref-ord1') {
      return of(this.order1);
    } else {
      return of(this.order2);
    }
  }

  //////// Save methods //////////

  /** POST: add a new operation to the server */
  addOrder(order: Order, token: string): Observable<Order> {
    this.orders.push(order);
    return of(order);
  }


  cancelOrder(reference: string, token: string): Observable<string> {
    if (reference === 'test-ref-ord1') {
      this.order1.state = 'CANCELLED';
      return of('');
    } else {
      this.order2.state = 'CANCELLED';
      return of('');
    }
  }

  updateOrder(reference: string, order: Order, token: string): Observable<string> {
    if (reference === 'test-ref-ord1') {
      this.order1.shippingAddress = order.shippingAddress;
      return of('');
    } else {
      this.order2.shippingAddress = order.shippingAddress;
      return of('');
    }
  }
}
