import { TestBed } from '@angular/core/testing';

import { MockStoreService } from './mock-store.service';

describe('MockStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockStoreService = TestBed.get(MockStoreService);
    expect(service).toBeTruthy();
  });
});
