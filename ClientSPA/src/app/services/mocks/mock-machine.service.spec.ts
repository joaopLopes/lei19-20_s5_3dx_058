import { TestBed } from '@angular/core/testing';

import { MockMachineService } from './mock-machine.service';

describe('MockMachineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockMachineService = TestBed.get(MockMachineService);
    expect(service).toBeTruthy();
  });
});
