import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {MachineType} from '../../entities/machine-type';
import {Operation} from '../../entities/operation';

@Injectable({
  providedIn: 'root'
})
export class MockMachinetypeService {

  private types: MachineType[] = [
    new MachineType('type1', ['cut', 'saw']),
    new MachineType('type2', ['drill'])
  ];

  private operations: Operation[] = [
    new Operation('press-5mm', 'hammer', 10, 5),
    new Operation('cut-1mm', 'drill', 20, 10)
  ];


  constructor() {
  }

  /** GET all machine types from the server */
  getMachineTypes(): Observable<MachineType[]> {
    return of(this.types);
  }


  /** GET machine type by id. Will 404 if id not found */
  getMachineType(description: string): Observable<MachineType> {
    for (const type of this.types.values()) {
      if (type.name === description) {
        return of(type);
      }
    }
    return of(this.types.pop());
  }

  /** POST: add a new machine type to the server */
  addMachineType(operation) {
    this.types.push(operation);
  }

  getMachineTypeOperations(description: string): Observable<Operation[]> {
      for (const type of this.types.values()) {
        if (type.name === description) {
          return of(this.operations);
        }
      }
      return of(this.operations);
  }
}
