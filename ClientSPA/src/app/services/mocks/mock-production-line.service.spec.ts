import { TestBed } from '@angular/core/testing';

import { MockProductionLineService } from './mock-production-line.service';

describe('MockProductionLineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockProductionLineService = TestBed.get(MockProductionLineService);
    expect(service).toBeTruthy();
  });
});
