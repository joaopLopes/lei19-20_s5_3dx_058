import {Injectable} from '@angular/core';
import {Order, ProductForOrder} from '../../entities/order';
import {Product} from '../../entities/product';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockStoreService {

  cart: Order;

  constructor() {
    this.cart = new Order();
    this.cart.products = [];
    this.cart.products.push(new ProductForOrder('Spoon', 15, 50, new Date()));
    this.cart.products.push(new ProductForOrder('Fork', 45, 5, new Date()));
  }

  private addProductToShoppingCart(price: number, prod: string, quantity: number, deliveryDate: Date) {
    this.cart.products.push(new ProductForOrder(prod, price, quantity, deliveryDate));
  }

  private removeProductFromOrder(prod: string) {
    const indexToBeRemoved = this.cart.products.findIndex(value => {
      return value.name = prod;
    });
    if (indexToBeRemoved > -1) {
      this.cart.products.splice(indexToBeRemoved, 1);
    }
  }

  fillOrderDetails(address: string, creditCard: string) {
    this.cart.creditCardUsed = creditCard;
    this.cart.shippingAddress = address;
    this.cart.client = 'clientForTesting';
  }

  executeOrder(): Observable<Order> {
    return of(this.cart);
  }

  getProductsToBeOrdered() {
    return this.cart.products;
  }


  clearShoppingCart() {
    this.cart = new Order();
  }

  hasItems() {
    if (!this.cart || !this.cart.products) {
      return false;
    } else {
      return this.cart.products.length > 0;
    }
  }
}
