import { TestBed } from '@angular/core/testing';

import { MockProductionPlanningService } from './mock-production-planning.service';

describe('MockProductionPlanningService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockProductionPlanningService = TestBed.get(MockProductionPlanningService);
    expect(service).toBeTruthy();
  });
});
