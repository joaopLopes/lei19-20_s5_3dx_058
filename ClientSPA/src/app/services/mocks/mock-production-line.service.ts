import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {MachineAssociated, ProductionLine} from '../../entities/production-line';
import {Machine} from '../../entities/machine';


@Injectable({
  providedIn: 'root'
})
export class MockProductionLineService {

  private productionLines: ProductionLine[] = [
    new ProductionLine('123', [new MachineAssociated('machine80', 1), new MachineAssociated('machine30', 2)]),
    new ProductionLine('321', [new MachineAssociated('machine21', 1), new MachineAssociated('machine22', 2)])];
  private availableMachines: string[] = ['machine10', 'machine23', 'machine59'];

  constructor() {
  }

  getProductionLines(): Observable<ProductionLine[]> {
    return of(this.productionLines);
  }

  getProductionLine(name: string): Observable<ProductionLine> {
    for (const productionline of this.productionLines.values()) {
      if (productionline.productionLineIdentifier === name) {
        return of(productionline);
      }
    }
    return of(this.productionLines.pop());
  }

  addProductionLine(productionline) {
    this.productionLines.push(productionline);
  }

  getAvailableMachines(): Observable<string[]> {
    return of(this.availableMachines);
  }
}
