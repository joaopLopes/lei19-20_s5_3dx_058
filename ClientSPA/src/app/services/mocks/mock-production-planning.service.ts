import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Line} from '../../entities/line';
import {OrderMachines} from '../../entities/order-machines';
import {ProductProductionPlanning} from '../../entities/product-production-planning';
import {PredictDate} from '../../entities/predict-date';

@Injectable({
  providedIn: 'root'
})
export class MockProductionPlanningService {

  constructor() {
  }

  generatePrologDatabase(startDate: Date, endDate: Date): Observable<string> {
    return of('generated');
  }

  /**
   * HTTP method that is the integration with PROLOG that returns the order that the machines
   * are going to act in the execution visualization.
   * Json comes in a string due to technological limitations.
   * In case of errors it brings to console temporarily.
   */
  bringsOrderJSON(line: Line): Observable<OrderMachines> {
    return of(new OrderMachines('{"productionLines": []}'));
  }

  /**
   * Method that directly changes a machine in the prolog database to activated.
   * @param machineDomainIdentifier a string that is the machine domain identifier.
   */
  activateMachine(machineDomainIdentifier: string): void {
    // do nothing
  }

  /**
   * Method that directly changes a machine in the prolog database to deactivated.
   * @param machineDomainIdentifier a string that is the machine domain identifier.
   */

  deactivateMachine(machineDomainIdentifier: string): void {
    // do nothing
  }


  /**
   * HTTP method that is the integration with PROLOG that returns the
   * order that the machines are going to act in the execution visualization.
   * Json comes in a string due to technological limitations.
   * In case of errors it brings to console temporarily.
   */
  balanceLines(): Observable<void> {
    return of();
  }

  /**
   * Returns the dates of a product dates.
   * @param productOnlyName the json that is being posted.
   */
  getProductsPredictedDeliveryDate(productOnlyName: ProductProductionPlanning): Observable<PredictDate> {
    return of(new PredictDate(2042, 10, 10));
  }
}
