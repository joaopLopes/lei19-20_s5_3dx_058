import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Product} from '../../entities/product';
import {FabricationPlan} from '../../entities/fabrication-plan';

@Injectable({
  providedIn: 'root'
})
export class MockProductService {

  private products: Product[] = [
    new Product('spoon', new FabricationPlan([
      'press-5mm',
      'cut-1mm'
    ]), 100),
    new Product('fork', new FabricationPlan([
      'press-5mm',
      'cut-1mm'
    ]), 100)];

  private productsInverted: Product[] = [
    new Product('fork', new FabricationPlan([
      'press-5mm',
      'cut-1mm'
    ]), 100),
    new Product('spoon', new FabricationPlan([
      'press-5mm',
      'cut-1mm'
    ]), 100)];


  constructor() {
  }

  getProducts(): Observable<Product[]> {
    return of(this.products);
  }

  getProductsFabricationPlan(name: string): Observable<FabricationPlan> {
    for (const product of this.products.values()) {
      if (product.name === name) {
        return of(product.fabricationPlan);
      }
    }
    return of(this.products.pop().fabricationPlan);
  }

  getProduct(name: string): Observable<Product> {
    for (const operation of this.products.values()) {
      if (operation.name === name) {
        return of(operation);
      }
    }
    return of(this.products.pop());
  }

  addProduct(product) {
    this.products.push(product);
  }

  getProductsForStore(): Observable<Product[]> {
    return this.getProducts();
  }

  getProductsWithFilter(criteria: string): Observable<Product[]> {
    if (criteria === 'times') {
        return this.getProducts();
    } else if (criteria === 'quantities') {
      const list = this.productsInverted;
      return of(list);
    } else {
      return this.getProducts();
    }
  }

  /** GET product by id. Will 404 if id not found */
  getProductForStoreById(name: string): Observable<Product> {
    return of(this.products.pop());
  }
}
