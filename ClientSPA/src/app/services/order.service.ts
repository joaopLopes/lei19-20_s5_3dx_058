import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Order, ProductForOrder} from '../entities/order';
import {LogService} from './log.service';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';

@Injectable({providedIn: 'root'})
export class OrderService {

  private orderUrl = environment.GE + '/orders';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    withCredentials: true
  };

  constructor(
    private http: HttpClient, private logService: LogService, private auth: AuthService) {
  }

  /** GET orders from the server */
  getOrders(): Observable<Order[]> {
    this.setTokenHeader();
    return this.http.get<Order[]>(this.orderUrl, this.httpOptions)
      .pipe(
        tap(orders => {
          this.log('fetched orders');
          orders.forEach(order => {
            order.placementDate = new Date(order.placementDate);
          });
        })
      );
  }

  /** GET orders from the server */
  getOrdersByClient(client: string): Observable<Order[]> {
    this.setTokenHeader();
    const url = `${this.orderUrl}/client/${client}`;
    return this.http.get<Order[]>(url, this.httpOptions)
      .pipe(
        tap(orders => {
          this.log('fetched orders of client ' + client);
          orders.forEach(order => {
            order.placementDate = new Date(order.placementDate);
          });
        })
      );
  }

  /** GET order by reference. Will 404 if id not found */
  getOrder(reference: string): Observable<Order> {
    this.setTokenHeader();
    const url = `${this.orderUrl}/${reference}`;
    return this.http.get<Order>(url, this.httpOptions).pipe(
      tap(order => {
          const aux = order.placementDate;
          order.placementDate = new Date(aux);
          order.products.forEach((prod: ProductForOrder) => {
            const aux2 = prod.deliveryDate;
            prod.deliveryDate = new Date(aux2);
          });
          this.log(`fetched order name=${reference}`);
        }
      )
    );
  }

  //////// Save methods //////////

  /** POST: add a new operation to the server */
  addOrder(order: Order): Observable<Order> {
    this.setTokenHeader();
    return this.http.post<Order>(this.orderUrl, order, this.httpOptions).pipe(
      tap((newOrder: Order) => this.log(`added order w/ reference=${newOrder.reference}`))
    );
  }

  /** Log an OperationService message with the MessageService */
  private log(message: string) {
    this.logService.log(`OrderService: ${message}`);
  }

  cancelOrder(reference: string): Observable<string> {
    const url = `${this.orderUrl}/${reference}/cancel`;
    this.setTokenHeader();
    return this.http.put<string>(url, '', this.httpOptions).pipe(
      tap(_ => {
          // logs
          this.log(`cancelled order reference=${reference}`);
        }
      )
    );
  }

  updateOrder(reference: string, order: Order): Observable<string> {
    const url = `${this.orderUrl}/${reference}`;
    this.setTokenHeader();
    return this.http.put<string>(url, order, this.httpOptions).pipe(
      tap(message => {
          // logs
          this.log(`updated order reference=${reference} with message ${message}`);
        }
      )
    );
  }

  setTokenHeader() {
    const tokenStr = this.auth.getToken();
    this.httpOptions.headers = new HttpHeaders({'Content-Type': 'application/json', token: tokenStr});
  }
}
