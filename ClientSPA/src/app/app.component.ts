import {ChangeDetectorRef, Component, DoCheck, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {AuthService} from './services/auth.service';
import {PermissionType} from './authentication/PermissionType';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck {
  title = 'My Cutlery';
  loggedIn: boolean;

  public constructor(private titleService: Title, public auth: AuthService, private cdref: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
  }

  ngDoCheck() {
    setTimeout(() => {
      this.auth.loggedInStatus().subscribe(status => {
        this.loggedIn = status;
      });
    });
    this.cdref.detectChanges();
  }

  canSeeClients() {
    return this.auth.hasPermission(PermissionType.LIST_CLIENTS, false, true);
  }

  canSeeOrders(): boolean {
    return this.auth.hasPermission(PermissionType.LIST_ORDERS, false);
  }

}
