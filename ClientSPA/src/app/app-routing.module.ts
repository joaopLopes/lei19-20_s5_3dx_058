import {Component, NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductsComponent} from './components/product/products/products.component';
import {MainPageComponent} from './components/main-page/main-page.component';
import {ProductDetailComponent} from './components/product/product-detail/product-detail.component';
import {AboutComponent} from './components/about/about.component';
import {ProductFormComponent} from './components/product/product-form/product-form.component';
import {OperationsComponent} from './components/operation/operations/operations.component';
import {OperationDetailComponent} from './components/operation/operation-detail/operation-detail.component';
import {OperationFormComponent} from './components/operation/operation-form/operation-form.component';
import {MachineTypesComponent} from './components/machine-type/machine-types/machine-types.component';
import {ProductionLinesComponent} from './components/productionLine/production-lines/production-lines.component';
import {ProductionLineDetailComponent} from './components/productionLine/production-line-detail/production-line-detail.component';
import {ProductionLineFormComponent} from './components/productionLine/production-line-form/production-line-form.component';
import {MachinetypeFormComponent} from './components/machine-type/machine-type-form/machinetype-form.component';
import {MachinesComponent} from './components/machine/machines/machines.component';
import {MachineTypeUpdateOperationsComponent} from './components/machine-type/machine-type-update/machine-type-update-operations.component';
import {MachineTypeDetailComponent} from './components/machine-type/machine-type-detail/machine-type-detail.component';
import {MachineDetailComponent} from './components/machine/machine-detail/machine-detail.component';
import {MachineFormComponent} from './components/machine/machine-form/machine-form.component';
import {VizCanvasComponent} from './components/viz-canvas/viz-canvas.component';
import {LoginFormComponent} from './components/login/login-form/login-form.component';
import {OrderDetailComponent} from './components/orders/order-detail/order-detail.component';
import {CreateAccountComponent} from './components/login/create-account/create-account.component';
import {OrdersComponent} from './components/orders/orders/orders.component';
import {ClientAreaComponent} from './components/client-area/client-area.component';
import {AdminAreaComponent} from './components/admin-area/admin-area.component';
import {MyOrdersComponent} from './components/orders/my-orders/my-orders.component';
import {ClientDetailComponent} from './components/client/client-detail/client-detail.component';
import {ClientUpdateComponent} from './components/client/client-update/client-update.component';
import {OrderUpdateComponent} from './components/orders/order-update/order-update.component';
import {ClientsComponent} from './components/client/clients/clients.component';
import {LogoutComponent} from './components/logout/logout.component';
import {StoreComponent} from './components/store/store.component';
import {ProductDetailStoreComponent} from './components/product/product-detail-store/product-detail-store.component';
import {ShoppingCartComponent} from './components/shopping-cart/shopping-cart.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {ProductionPlanningComponent} from './components/production-planning/production-planning.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'store', component: StoreComponent},
  {path: 'home', component: MainPageComponent},
  {path: 'about', component: AboutComponent},
  {path: 'products', component: ProductsComponent},
  {path: 'machines', component: MachinesComponent},
  {path: 'machine-types/:name/update', component: MachineTypeUpdateOperationsComponent},
  {path: 'machine-types/:name/detail', component: MachineTypeDetailComponent},
  {path: 'machine-types/new', component: MachinetypeFormComponent},
  {path: 'machines/:serialNumber/detail', component: MachineDetailComponent},
  {path: 'machines/new', component: MachineFormComponent},
  {path: 'operations', component: OperationsComponent},
  {path: 'production-lines', component: ProductionLinesComponent},
  {path: 'products/new', component: ProductFormComponent},
  {path: 'operations/new', component: OperationFormComponent},
  {path: 'products/:name/detail', component: ProductDetailComponent},
  {path: 'store/:name', component: ProductDetailStoreComponent},
  {path: 'machine-types', component: MachineTypesComponent},
  {path: 'production-lines/:productionLineIdentifier/detail', component: ProductionLineDetailComponent},
  {path: 'production-lines/new', component: ProductionLineFormComponent},
  {path: 'execution-visualization', component: VizCanvasComponent},
  {path: 'login', component: LoginFormComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'create-account', component: CreateAccountComponent},
  {path: 'orders', component: OrdersComponent},
  {path: 'orders/:reference/detail', component: OrderDetailComponent},
  {path: 'orders/:reference/update', component: OrderUpdateComponent},
  {path: 'clients/:username/orders', component: MyOrdersComponent},
  {path: 'clients/:username/profile', component: ClientDetailComponent},
  {path: 'clients', component: ClientsComponent},
  {path: 'client-area', component: ClientAreaComponent},
  {path: 'admin-area', component: AdminAreaComponent},
  {path: 'create-account', component: CreateAccountComponent},
  {path: 'clients/:username/update', component: ClientUpdateComponent},
  {path: 'shopping-cart', component: ShoppingCartComponent},
  {path: 'production-planning', component: ProductionPlanningComponent},
  {path: 'operations/:description/detail', component: OperationDetailComponent},
  {path: '**', component: PageNotFoundComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
