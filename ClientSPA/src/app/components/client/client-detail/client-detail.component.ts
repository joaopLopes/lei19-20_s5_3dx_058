import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClientService} from '../../../services/client.service';
import {Client} from '../../../entities/client';
import {AlertService} from '../../../services/alert.service';
import {Location} from '@angular/common';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';


@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css']
})
export class ClientDetailComponent implements OnInit {
  @Input() client: Client;
  private resourceUser: string;
  access: boolean;

  constructor(private alerts: AlertService,
              private auth: AuthService,
              private route: ActivatedRoute,
              private clientService: ClientService,
              private location: Location) {
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      const sameUser = this.isSameUser();
      if (this.auth.hasPermission(PermissionType.CONSULT_CLIENT, sameUser, false)) {
        this.getClient();
        this.access = true;
      } else {
        this.access = false;
        this.alerts.error('Not authorized');
      }
    } else {
      this.access = false;
      this.alerts.error('You must be logged in to access this area!');
    }
  }

  private isSameUser(): boolean {
    this.resourceUser = this.route.snapshot.paramMap.get('username');
    const loggedUser = this.auth.getLoggedUser();
    return this.resourceUser === loggedUser;
  }

  /**
   * Gets the client for the username from the service.
   */
  getClient(): void {
    this.clientService.getClientByUsername(this.resourceUser).subscribe(client => this.client = client,
      error => {
        alert('An error occurred');
      });
  }

  deleteClient(): void {
    this.clientService.deleteClient(this.resourceUser).subscribe(next => {
      // do nothing
    }, error => {
      this.alerts.error('An error has ocurred. Please try again.');
    }, () => {
      this.location.back();
      alert('Your account has been deleted.');
      this.auth.logout();
    });
  }

  canUpdate(): boolean {
    const sameUser = this.isSameUser();
    return this.auth.hasPermission(PermissionType.UPDATE_CLIENT, sameUser, false);
  }

  canDelete(): boolean {
    const sameUser = this.isSameUser();
    return this.auth.hasPermission(PermissionType.DELETE_CLIENT, sameUser, false);
  }
}
