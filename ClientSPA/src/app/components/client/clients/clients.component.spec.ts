import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ClientsComponent} from './clients.component';
import {CookieService} from 'ngx-cookie-service';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../../services/order.service';
import {MockOrderService} from '../../../services/mocks/mock-order.service';
import {ReactiveFormsModule} from '@angular/forms';
import {MockUserService} from '../../../services/mocks/mock-user.service';
import {ClientService} from '../../../services/client.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('ClientsComponent', () => {
  let component: ClientsComponent;
  let fixture: ComponentFixture<ClientsComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClientsComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [{provide: ClientService, useClass: MockUserService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct title', () => {
    const expected = 'Clients';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should list all clients', () => {
    const clients = element.getElementsByClassName('client');
    expect(clients.length).toBe(1);
  });

});
