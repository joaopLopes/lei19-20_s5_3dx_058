import {Component, OnInit} from '@angular/core';
import {Client} from '../../../entities/client';
import {ClientService} from '../../../services/client.service';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients: Client[];
  access: boolean;

  constructor(private clientService: ClientService, private auth: AuthService, private alert: AlertService) {
  }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.access = false;
      this.alert.error('Please log in.');
    } else {
      if (!this.auth.hasPermission(PermissionType.LIST_CLIENTS, false, true)) {
        this.access = false;
        this.alert.error('Not authorized to access area!');
      } else {
        this.getClients();
        this.access = true;
      }
    }
  }

  getClients(): void {
    this.clientService.getClients()
      .subscribe(users => this.clients = users);
  }

}
