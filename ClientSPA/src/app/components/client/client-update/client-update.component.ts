import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClientService} from '../../../services/client.service';
import {Client} from '../../../entities/client';
import {AlertService} from '../../../services/alert.service';
import {FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';

@Component({
  selector: 'app-client-update',
  templateUrl: './client-update.component.html',
  styleUrls: ['./client-update.component.css']
})
export class ClientUpdateComponent implements OnInit {
  @Input() username: string;
  @Input() firstName: string;
  @Input() lastName: string;
  @Input() address: string;

  addressText = new FormControl('', Validators.nullValidator);
  firstNameText = new FormControl('', Validators.nullValidator);
  lastNameText = new FormControl('', Validators.nullValidator);
  private resourceUser: string;

  access: boolean;

  constructor(private auth: AuthService,
              private route: ActivatedRoute,
              private clientService: ClientService,
              private alertService: AlertService) {
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.resourceUser = this.route.snapshot.paramMap.get('username');
      const loggedUser = this.auth.getLoggedUser();
      const sameUser: boolean = this.resourceUser === loggedUser;
      if (this.auth.hasPermission(PermissionType.UPDATE_CLIENT, sameUser, false)) {
        this.getClient();
        this.access = true;
      } else {
        this.access = false;
        this.alertService.error('Permission denied.');
      }
    } else {
      this.access = false;
      this.alertService.error('You must be logged in to access this area!');
    }
  }

  /**
   * Gets the client for the username from the service.
   */
  getClient(): void {
    this.username = this.route.snapshot.paramMap.get('username');
  }

  /**
   * Updates the info of the user-client.
   */
  updateClient(): void {
    const username = this.route.snapshot.paramMap.get('username');
    this.firstName = this.firstNameText.value;
    this.lastName = this.lastNameText.value;
    this.address = this.addressText.value;
    const client = new Client('', '', '', '', '', '', this.address, this.firstName, this.lastName);

    this.clientService.updateClient(username, client).subscribe(_ => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Client has been updated!.');
    });

  }

}
