import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ClientUpdateComponent} from './client-update.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {ClientService} from '../../../services/client.service';
import {MockClientService} from '../../../services/mocks/mock-client.service';
import {AuthService} from '../../../services/auth.service';
import {ClientMockAuthService} from '../../../services/mocks/client-mock-auth.service';

describe('ClientUpdateComponent', () => {
  let component: ClientUpdateComponent;
  let fixture: ComponentFixture<ClientUpdateComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClientUpdateComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [{provide: ClientService, useClass: MockClientService},
        {provide: AuthService, useClass: ClientMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ClientUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  /*
  it('should have correct Title', () => {
    component.username = 'userTest';
    const expected = 'Update Account: userTest ';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('address field to be invalid if address is empty', () => {
    component.username = 'userTest';
    const addressForm = component.addressText;
    addressForm.setValue('');
    expect(addressForm.valid).toBeFalsy();
  });
  it('First Name field to be invalid if first name is empty', () => {
    component.username = 'userTest';
    const firstNameForm = component.firstNameText;
    firstNameForm.setValue('');
    expect(firstNameForm.valid).toBeFalsy();
  });
  it('Last Name  field to be invalid if last name is empty ', () => {
    component.username = 'userTest';
    const lastTimeForm = component.lastNameText;
    lastTimeForm.setValue('');
    expect(lastTimeForm.valid).toBeFalsy();
  });

   */
});
