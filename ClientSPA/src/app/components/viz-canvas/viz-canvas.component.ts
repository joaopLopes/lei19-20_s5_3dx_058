import {Component, OnInit} from '@angular/core';
import {main} from '../../../assets/3d/js/main';
import {ProductionLineService} from '../../services/production-line.service';
import {ProductionLine} from '../../entities/production-line';
import {Machine} from '../../entities/machine';
import {MachineService} from '../../services/machine.service';
import {AuthService} from '../../services/auth.service';
import {AlertService} from '../../services/alert.service';


let show = false;
let rendered = false;

@Component({
  selector: 'app-viz-canvas',
  templateUrl: './viz-canvas.component.html',
  styleUrls: ['./viz-canvas.component.css']
})
export class VizCanvasComponent implements OnInit {

  prodLines: ProductionLine[];
  machines: Machine[];
  availableMachines: string[];
  access: boolean;

  constructor(private prodLinesService: ProductionLineService,
              private machineService: MachineService,
              private authService: AuthService,
              private alert: AlertService) {
  }

  ngOnInit() {
    try {
      if (this.authService.isAdmin()) {
        this.getProductionLines();
        this.getMachines();
        this.getAvailableMachines();
        this.access = true;
      } else {
        this.alert.error('Area reserved to admins');
        this.access = false;
      }
    } catch (e) {
      this.alert.error('Please log in.');
      this.access = false;
    }
  }

  private getProductionLines(): void {
    this.prodLinesService.getProductionLines().subscribe(lines => this.prodLines = lines);
  }

  private getMachines(): void {
    this.machineService.getMachines().subscribe(machines => this.machines = machines);
  }

  private getAvailableMachines(): void {
    this.prodLinesService.getAvailableMachines().subscribe(availableMachines => this.availableMachines = availableMachines);
  }


  show3d(): void {
    if (rendered === false) {
      main(this.prodLines, this.machines, this.availableMachines, this.prodLinesService);
      rendered = true;
    }
    if (show === true) {
      show = false;
    } else {
      show = true;
    }
    const container = document.getElementById('3d-container');
    const canvas = document.getElementById('canvas');
    const gui = document.getElementById('gui-container');

    if (show === true) {
      // container.style.display = 'block';
      container.hidden = false;
      // canvas.style.display = 'block';
      // gui.style.display = 'block';
    } else {
      container.hidden = true;
      // container.style.display = 'none';
      // canvas.style.display = 'none';
      // gui.style.display = 'none';
    }

  }
}
