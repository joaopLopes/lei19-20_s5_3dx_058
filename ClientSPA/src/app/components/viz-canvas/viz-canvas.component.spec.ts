import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VizCanvasComponent} from './viz-canvas.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductionLineService} from '../../services/production-line.service';
import {MockProductionLineService} from '../../services/mocks/mock-production-line.service';
import {AlertComponent} from '../alert/alert.component';
import {MachineService} from '../../services/machine.service';
import {MockMachineService} from '../../services/mocks/mock-machine.service';
import {AuthService} from '../../services/auth.service';
import {AdminMockAuthService} from '../../services/mocks/admin-mock-auth.service';

describe('VizCanvasComponent', () => {
  let component: VizCanvasComponent;
  let fixture: ComponentFixture<VizCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VizCanvasComponent],
      imports: [RouterTestingModule],
      providers: [
        {
          provide: ProductionLineService,
          useClass: MockProductionLineService
        }, {
          provide: MachineService,
          useClass: MockMachineService
        },
        {
          provide: AuthService,
          useClass: AdminMockAuthService
        }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VizCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
