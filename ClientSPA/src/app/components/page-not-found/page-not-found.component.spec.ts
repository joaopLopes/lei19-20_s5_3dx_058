import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotFoundComponent } from './page-not-found.component';

describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct titles', () => {
    const expected = 'Oops!';
    const expected2 = '404. Not found.';

    const actual = element.getElementsByClassName('h2').item(0).textContent;
    const actual2 = element.getElementsByClassName('h3').item(0).textContent;

    expect(expected).toBe(actual);
    expect(expected2).toBe(actual2);
  });
});
