import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineTypeUpdateOperationsComponent} from './machine-type-update-operations.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MachineTypeService} from '../../../services/machine-type.service';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {OperationService} from '../../../services/operation.service';
import {MockOperationService} from '../../../services/mocks/mock-operation.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('MachineTypeUpdateOperationsComponent', () => {
  let component: MachineTypeUpdateOperationsComponent;
  let fixture: ComponentFixture<MachineTypeUpdateOperationsComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineTypeUpdateOperationsComponent],
      imports: [RouterTestingModule],
      providers: [{provide: MachineTypeService, useClass: MockMachinetypeService},
        {provide: OperationService, useClass: MockOperationService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MachineTypeUpdateOperationsComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTypeUpdateOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'Update Machine Type: type2';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('machines operations should have 2 when page loads', () => {
    expect(component.operationsOfMachine.length).toBe(2);
  });
  it('operations list should have all operations in system when page loads', () => {
    expect(component.availableOperations.length).toBe(2);
  });
  it('should have a button to list all operations', () => {
    const button = element.getElementsByClassName('submit').item(0).textContent;
    expect(button).toEqual('Update! ');
  });
});
