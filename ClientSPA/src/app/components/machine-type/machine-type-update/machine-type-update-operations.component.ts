import {Component, Input, OnInit} from '@angular/core';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {AlertService} from '../../../services/alert.service';
import {Operation} from '../../../entities/operation';
import {OperationService} from '../../../services/operation.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-machine-type-update-operations',
  templateUrl: './machine-type-update-operations.component.html',
  styleUrls: ['./machine-type-update-operations.component.css']
})
export class MachineTypeUpdateOperationsComponent implements OnInit {

  @Input() operationsOfMachine: Operation[];
  @Input() availableOperations: Operation[];
  @Input() machineType: MachineType;
  change = false;
  access: boolean;

  constructor(private operationService: OperationService,
              private machineTypeService: MachineTypeService,
              private alertService: AlertService,
              private route: ActivatedRoute,
              private auth: AuthService) {
  }


  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.access = true;
      this.getMachineType();
      this.getMachineTypeOperations();
      this.getAvailableOperations();
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  removeOp(operation: Operation) {
    this.change = true;
    const index = this.operationsOfMachine.indexOf(operation);
    if (index > -1) {
      this.operationsOfMachine.splice(index, 1);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    this.change = true;
    moveItemInArray(this.operationsOfMachine, event.previousIndex, event.currentIndex);
  }

  chooseOperation(operation: Operation) {
    this.change = true;
    this.operationsOfMachine.push(operation);
  }

  /**
   * Shows and hides the operations
   */
  showOperations(): void {
    document.getElementById('myDropdown').classList.toggle('show');
  }

  /**
   * Filters the user input when looking for operations
   */
  filterOperations() {
    let input;
    let filter;
    let div;
    let a;
    let i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    div = document.getElementById('myDropdown');
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      input.txtValue = a[i].textContent || a[i].innerText;
      if (input.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  /**
   * Gets the machine types from the server.
   */

  getAvailableOperations(): void {
    this.operationService.getOperations().subscribe(operations => this.availableOperations = operations);
  }

  getMachineTypeOperations(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.machineTypeService.getMachineTypeOperations(name).subscribe(operationsMT => this.operationsOfMachine = operationsMT);
  }

  /**
   * Updates the machine Types Operations.
   */
  updateMachineType(): void {
    const auxList = [];
    for (let counter = 0; counter < this.operationsOfMachine.length; counter++) {
      auxList[counter] = this.operationsOfMachine[counter].description;
    }
    const updateMachineType = new MachineType(this.machineType.name, auxList);
    this.machineTypeService.updateMachineTypeOperations(this.machineType.name, updateMachineType).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Machine Type has been updated!.');
    });
  }

  private getMachineType() {
    const name = this.route.snapshot.paramMap.get('name');
    this.machineTypeService.getMachineType(name).subscribe(machineType => this.machineType = machineType);
  }
}


