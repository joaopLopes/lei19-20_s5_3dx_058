import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MachineTypesComponent} from './machine-types.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('MachinetypesComponent', () => {
  let component: MachineTypesComponent;
  let fixture: ComponentFixture<MachineTypesComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineTypesComponent],
      imports: [RouterTestingModule],
      providers: [{provide: MachineTypeService, useClass: MockMachinetypeService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MachineTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have title operations', () => {
    const expected = 'Machine Types';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
  it('should have new operation button', () => {
    const expected = '+ New Machine Type';
    const result = element.getElementsByClassName('new').item(0).textContent;
    expect(expected).toBe(result);
  });
  it('should list all machine types', () => {
    const type1 = new MachineType('type1', ['cut', 'saw']);
    const type2 = new MachineType('type2', ['drill']);
    const expectedMachineTypes: MachineType[] = [type1, type2];
    expect(component.types).toEqual(expectedMachineTypes);
  });
});
