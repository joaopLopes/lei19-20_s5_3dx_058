import {Component, OnInit} from '@angular/core';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-machinetypes',
  templateUrl: './machine-types.component.html',
  styleUrls: ['./machine-types.component.css']
})
export class MachineTypesComponent implements OnInit {

  types: MachineType[];
  access: boolean;

  getTypes(): void {
    this.machineTypeService.getMachineTypes().subscribe(types => this.types = types);
  }

  constructor(private machineTypeService: MachineTypeService,
              private auth: AuthService,
              private alert: AlertService) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.getTypes();
      this.access = true;
    } else {
      this.access = false;
      this.alert.error('Not allowed.');
    }
  }

}
