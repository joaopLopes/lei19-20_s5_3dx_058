import {Component, Input, OnInit} from '@angular/core';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-machine-type-detail',
  templateUrl: './machine-type-detail.component.html',
  styleUrls: ['./machine-type-detail.component.css']
})
export class MachineTypeDetailComponent implements OnInit {

  @Input() machineType: MachineType;
  access: boolean;

  constructor(private route: ActivatedRoute,
              private machineTypeService: MachineTypeService,
              private authService: AuthService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    if (this.authService.isAdmin()) {
      this.getMachineType();
      this.access = true;
    } else {
      this.alertService.error('Not allowed.');
      this.access = false;
    }
  }

  getMachineType(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.machineTypeService.getMachineType(name)
      .subscribe(machineType => this.machineType = machineType);
  }

  /* When the user clicks on the button,
  toggle between hiding and showing the dropdown content */
  myFunction(): void {
    document.getElementById('myDropdown').classList.toggle('show');
    const doc = document.getElementById('toggleButton');
    if (doc.innerText === 'Show') {
      doc.innerText = 'Hide';
    } else {
      doc.innerText = 'Show';
    }
  }
}
