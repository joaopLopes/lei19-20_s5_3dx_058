import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTypeDetailComponent } from './machine-type-detail.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MachineTypeService} from '../../../services/machine-type.service';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('MachineTypeDetailComponent', () => {
  let component: MachineTypeDetailComponent;
  let fixture: ComponentFixture<MachineTypeDetailComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineTypeDetailComponent ],
      imports: [RouterTestingModule],
      providers: [{provide: MachineTypeService, useClass: MockMachinetypeService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
    .compileComponents();
    fixture = TestBed.createComponent(MachineTypeDetailComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  }));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'Machine Type type2 - Details';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have operations', () => {
    const items = element.getElementsByClassName('dropdown-content');
    const list = items.item(0);
    const op1 = list.getElementsByClassName('operation').item(0).textContent;
    expect(op1).toEqual('drill');
  });
});
