import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {OperationService} from '../../../services/operation.service';
import {AlertService} from '../../../services/alert.service';
import {Operation} from '../../../entities/operation';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {AuthService} from '../../../services/auth.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-machinetype-form',
  templateUrl: './machinetype-form.html',
  styleUrls: ['./machinetype-form.css']
})
export class MachinetypeFormComponent implements OnInit {
  stringOps: string[] = Array();
  opsObj: Operation[];
  name = new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9|\-|_]*$/)]);
  chosenOperations: Operation[] = Array();
  access: boolean;

  constructor(private operationService: OperationService,
              private machinetypeService: MachineTypeService,
              private alertService: AlertService,
              private auth: AuthService) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.operationService.getOperations().subscribe(opsObj => this.opsObj = opsObj);
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  drop(event: CdkDragDrop<Operation[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  OperationToStrings(array: Operation[]) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < array.length; i++) {
      this.stringOps.push(array[i].description);
    }
  }

  onSubmit() {
    this.OperationToStrings(this.chosenOperations);
    const newMachineType = new MachineType(this.name.value, this.stringOps);
    this.machinetypeService.addMachineType(newMachineType).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
      this.ngOnInit();
    }, () => {
      this.alertService.info('Machine Type has been created.');
      location.reload();
    });
    // clean data
    this.name.reset();
    // deletes all operations from list
    this.chosenOperations = [];
  }

}
