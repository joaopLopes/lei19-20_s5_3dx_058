import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MachineTypeService} from '../../../services/machine-type.service';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { MachinetypeFormComponent } from './machinetype-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {OperationService} from '../../../services/operation.service';
import {MockOperationService} from '../../../services/mocks/mock-operation.service';
import {Operation} from '../../../entities/operation';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';


describe('MachinetypeFormComponent', () => {
  let component: MachinetypeFormComponent;
  let fixture: ComponentFixture<MachinetypeFormComponent>;
  let element: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachinetypeFormComponent ],
      imports: [RouterTestingModule, ReactiveFormsModule, DragDropModule],
      providers: [{provide: MachineTypeService, useClass: MockMachinetypeService},
        {provide: OperationService, useClass: MockOperationService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MachinetypeFormComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinetypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'New Machine Type';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('form invalid when empty', () => {
    expect(component.name.valid).toBeFalsy();
  });
  it('name field to be invalid if description has spaces', () => {
    component.name.setValue('super ultra type 9000');
    expect(component.name.valid).toBeFalsy();
  });
  it('chosen list should be empty when page loads', () => {
    expect(component.chosenOperations.length).toBe(0);
  });
  it('operations list (objects) should have all operations in system when page loads', () => {
    expect(component.opsObj.length).toBe(2);
  });
  it('chosen operations list (strings) should have no chosen operations when page starts', () => {
    expect(component.stringOps.length).toBe(0);
  });
  it('should list all operations in the system', () => {
    const op1 = new Operation('press-5mm', 'hammer', 10, 5);
    const op2 = new Operation('cut-1mm', 'drill', 20, 10);
    const expectedOps: Operation[] = [op1, op2];
    expect(component.opsObj).toEqual(expectedOps);
  });
});
