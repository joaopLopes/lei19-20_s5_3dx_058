import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StoreComponent} from './store.component';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthService} from '../../services/auth.service';
import {ClientMockAuthService} from '../../services/mocks/client-mock-auth.service';
import {ProductService} from '../../services/product.service';
import {MockProductService} from '../../services/mocks/mock-product.service';
import {StoreService} from '../../services/store.service';
import {MockStoreService} from '../../services/mocks/mock-store.service';
import {LoginFormComponent} from '../login/login-form/login-form.component';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert.service';

describe('StoreComponent', () => {
  let component: StoreComponent;
  let fixture: ComponentFixture<StoreComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StoreComponent],
      imports: [FormsModule, RouterTestingModule],
      providers: [{provide: AuthService, useClass: ClientMockAuthService},
        {provide: ProductService, useClass: MockProductService},
        {provide: StoreService, useClass: MockStoreService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have title', () => {
    const title = 'Store';
    const result = element.getElementsByClassName('title');
    expect(true).toBeTruthy();
  });
  it('should list all products', () => {
    const expected = 2;
    const result = element.getElementsByClassName('gridMember').length;
    expect(expected === result).toBeTruthy();
  });
  it('checks if first product is correct', () => {
    const expected = 'spoon$100';
    const elem = element.getElementsByClassName('gridMember').item(0).textContent;
    expect(expected).toBe(elem);
  });
});
