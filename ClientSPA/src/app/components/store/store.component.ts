import {Component, OnInit} from '@angular/core';
import {Product} from '../../entities/product';
import {FabricationPlan} from '../../entities/fabrication-plan';
import {ProductService} from '../../services/product.service';
import {AlertService} from '../../services/alert.service';
import {StoreService} from '../../services/store.service';
import {AuthService} from '../../services/auth.service';
import {PermissionType} from '../../authentication/PermissionType';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  products: Product[];
  selectedOption: string;
  access: boolean;

  constructor(private prodService: ProductService,
              private alertService: AlertService,
              public storeService: StoreService,
              private auth: AuthService) {
  }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.access = false;
      this.alertService.error('Please log in.');
    } else {
      this.getProducts();
      this.access = true;
      this.selectedOption = 'none';
    }
  }

  filterProducts(criteria: string): void {
    if (criteria && criteria !== '') {
      if (criteria === 'none') {
        this.getProducts();
      } else {
        this.prodService.getProductsWithFilter(criteria).subscribe(prods => this.products = prods);
      }
    } else {
      this.alertService.error('You must select a valid filter first.');
    }
  }

  getProducts(): void {
    this.prodService.getProductsForStore()
      .subscribe(products => this.products = products);
  }

  add(name: string, fabricationPlan: FabricationPlan, price: number): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.prodService.addProduct(new Product(name, fabricationPlan, price))
      .subscribe(prod => {
        this.products.push(prod);
      });
  }

  canMakeOrder(): boolean {
    return this.auth.hasPermission(PermissionType.CREATE_ORDER, false, false);
  }
}
