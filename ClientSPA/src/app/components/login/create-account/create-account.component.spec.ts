import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateAccountComponent } from './create-account.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

describe('CreateAccountComponent', () => {
  let component: CreateAccountComponent;
  let fixture: ComponentFixture<CreateAccountComponent>;
  let element: HTMLElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateAccountComponent],
      imports: [RouterTestingModule, ReactiveFormsModule, HttpClientModule],
    }).compileComponents();
    fixture = TestBed.createComponent(CreateAccountComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
     expect(component).toBeTruthy();
  });
  it('should have correct title', () => {
    const expected = 'Create Account';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have submit button', () => {
    const button = element.getElementsByClassName('btn').item(0).textContent;
    expect(button).toEqual('Create!');
  });
  it('name field invalid if empty', () => {
    const name = component.createAccountForm.controls.username;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });
  it('password field invalid if invalid', () => {
    const name = component.createAccountForm.controls.password;
    name.setValue('notvalid');
    expect(name.valid).toBeFalsy();
  });
  it('email field invalid if invalid', () => {
    const name = component.createAccountForm.controls.email;
    name.setValue('asdakm');
    expect(name.valid).toBeFalsy();
  });
  it('phoneNumber field invalid if empty', () => {
    const name = component.createAccountForm.controls.phoneNumber;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });
  it('nif field invalid if empty', () => {
    const name = component.createAccountForm.controls.nif;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });
  it('address field invalid if empty', () => {
    const name = component.createAccountForm.controls.address;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });
  it('age field invalid if empty', () => {
    const name = component.createAccountForm.controls.age;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });
  it('firstName field invalid if empty', () => {
    const name = component.createAccountForm.controls.firstName;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });
  it('lastName field invalid if empty', () => {
    const name = component.createAccountForm.controls.lastName;
    name.setValue('');
    expect(name.valid).toBeFalsy();
  });

});

