import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientService} from '../../../services/client.service';
import {AlertService} from '../../../services/alert.service';
import {Client} from '../../../entities/client';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})

export class CreateAccountComponent implements OnInit {
  createAccountForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9]+$/)]),
    password: new FormControl('',
      [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    address: new FormControl('', Validators.required),
    nif: new FormControl('', Validators.required),
    age: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    check: new FormControl('', Validators.requiredTrue),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private alertService: AlertService,
    private service: ClientService) {
  }

  ngOnInit() {
  }

  private add(username, password, email, phoneNumber, nif, age, address, firstName, lastName: string):
    void {
    username = username.trim();
    password = password.trim();
    email = email.trim();
    phoneNumber = phoneNumber.trim();
    nif = nif.trim();
    age = age.trim();
    address = address.trim();
    firstName = firstName.trim();
    lastName = lastName.trim();

    if (!username || !password || !email || !phoneNumber || !nif || !age || !address || !firstName || !lastName) {
      return;
    }

    this.service.addClient(new Client(username, password, email, phoneNumber, nif, age, address, firstName, lastName)).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.createAccountForm.reset();
      alert('Account was created! You may now login.');
    });
  }

  onSubmit() {
    const result: Client = Object.assign({}, this.createAccountForm.value);
    this.add(result.username, result.password, result.email, result.phoneNumber, result.nif,
      result.age, result.address, result.firstName, result.lastName);
  }

}
