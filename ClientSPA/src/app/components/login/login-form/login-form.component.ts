import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {AlertService} from '../../../services/alert.service';
import {AppComponent} from '../../../app.component';
import {logger} from 'codelyzer/util/logger';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(private service: AuthService, private router: Router, private alertService: AlertService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    const result = Object.assign({}, this.loginForm.value);
    this.service.login(result.username, result.password).then(_ => {
      this.router.navigateByUrl('/home').then().catch(err => {
        logger.error(err);
      });
    }, error => {
      this.alertService.error(error);
    });
  }
}
