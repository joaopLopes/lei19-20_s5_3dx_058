import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../../entities/product';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../services/product.service';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  @Input() product: Product;
  access: boolean;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private auth: AuthService,
    private alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    if (this.auth.isAdmin()) {
      this.getProduct();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  getProduct(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.productService.getProduct(name)
      .subscribe(product => this.product = product);
    this.productService.getProductsFabricationPlan(name).subscribe(plan => this.product.fabricationPlan = plan);
  }

  /* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
  onClick(): void {
    document.getElementById('myDropdown').classList.toggle('show');
    const doc = document.getElementById('toggleButton');
    if (doc.innerText === 'Show') {
      doc.innerText = 'Hide';
    } else {
      doc.innerText = 'Show';
    }
  }

}
