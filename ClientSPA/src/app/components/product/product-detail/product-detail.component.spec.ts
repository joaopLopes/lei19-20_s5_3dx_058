import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductDetailComponent} from './product-detail.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductService} from '../../../services/product.service';
import {MockProductService} from '../../../services/mocks/mock-product.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('ProductDetailComponent', () => {
  let component: ProductDetailComponent;
  let fixture: ComponentFixture<ProductDetailComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDetailComponent],
      imports: [RouterTestingModule],
      providers: [{provide: ProductService, useClass: MockProductService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ProductDetailComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'Product fork - Details';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });

  it('should have correct Name', () => {
    const items = element.getElementsByClassName('word');
    const result = items.item(0).textContent;
    const expected = 'Name: fork';
    expect(result).toEqual(expected);
  });

  it('should have button to show operations', () => {
    const fabPlan = element.getElementsByClassName('word').item(2);
    const button = fabPlan.getElementsByClassName('dropbtn').item(0).textContent;
    expect(fabPlan.textContent).toEqual('Fabrication Plan: Show');
    expect(button).toEqual('Show');
  });

  it('should have all operations', () => {
    const items = element.getElementsByClassName('dropdown-content');
    const list = items.item(0);
    const op1 = list.getElementsByClassName('word').item(0).textContent;
    const op2 = list.getElementsByClassName('word').item(1).textContent;
    expect(op1).toEqual('press-5mm');
    expect(op2).toEqual('cut-1mm');
  });
});
