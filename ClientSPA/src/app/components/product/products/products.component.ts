import {Component, OnInit} from '@angular/core';
import {Product} from '../../../entities/product';
import {FabricationPlan} from '../../../entities/fabrication-plan';
import {ProductService} from '../../../services/product.service';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  access: boolean;

  constructor(private prodService: ProductService,
              private auth: AuthService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.getProducts();
      this.access = true;
    } else {
      this.alertService.error('Not allowed');
      this.access = false;
    }
  }


  getProducts(): void {
    this.prodService.getProducts()
      .subscribe(products => this.products = products);
  }

  add(name: string, fabricationPlan: FabricationPlan, price: number): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.prodService.addProduct(new Product(name, fabricationPlan, price))
      .subscribe(prod => {
        this.products.push(prod);
      });
  }
}
