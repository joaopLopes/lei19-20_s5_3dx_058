import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductsComponent} from './products.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductService} from '../../../services/product.service';
import {MockProductService} from '../../../services/mocks/mock-product.service';
import {Product} from '../../../entities/product';
import {FabricationPlan} from '../../../entities/fabrication-plan';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let fixture: ComponentFixture<ProductsComponent>;
  let element: HTMLElement;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductsComponent],
      imports: [RouterTestingModule],
      providers: [{provide: ProductService, useClass: MockProductService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should list all products', () => {
    const products = element.getElementsByClassName('product');
    const prod1 = new Product('spoon', new FabricationPlan([
      'press-5mm',
      'cut-1mm'
    ]), 100);
    const prod2 = new Product('fork', new FabricationPlan([
      'press-5mm',
      'cut-1mm'
    ]), 100);
    const expectedProducts: Product[] = [prod1, prod2];
    expect(products.item(0).textContent).toBe(prod1.name);
    expect(products.item(1).textContent).toBe(prod2.name);
    expect(component.products).toEqual(expectedProducts);
  });
  it('should have title products', () => {
    const expected = 'Products';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
  it('should have new product button', () => {
    const expected = '+ New Product';
    const result = element.getElementsByClassName('new').item(0).textContent;
    expect(expected).toBe(result);
  });
});
