import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {OperationService} from '../../../services/operation.service';
import {Operation} from '../../../entities/operation';
import {FabricationPlan} from '../../../entities/fabrication-plan';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {Product} from '../../../entities/product';
import {ProductService} from '../../../services/product.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  name = new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9|\-|_]*$/)]);
  price = new FormControl('', [Validators.required, Validators.min(0)]);
  chosenOperations: string[] = Array();
  operations: Operation[];
  access: boolean;

  constructor(private operationService: OperationService,
              private prodService: ProductService,
              private alertService: AlertService,
              private authService: AuthService) {
  }

  ngOnInit() {
    if (this.authService.isAdmin()) {
      this.getOperations();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  getOperations(): void {
    this.operationService.getOperations()
      .subscribe(operations => this.operations = operations);
  }

  /**
   * Shows and hides the operations
   */
  showOperations(): void {
    document.getElementById('myDropdown').classList.toggle('show');
  }

  /**
   * Filters the user input when looking for operations
   */
  filterOperations() {
    let input;
    let filter;
    let div;
    let a;
    let i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    div = document.getElementById('myDropdown');
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      input.txtValue = a[i].textContent || a[i].innerText;
      if (input.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  chooseOperation(operation: Operation) {
    this.chosenOperations.push(operation.description);
  }

  removeOp(operation: string) {
    const index = this.chosenOperations.indexOf(operation);
    if (index > -1) {
      this.chosenOperations.splice(index, 1);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.chosenOperations, event.previousIndex, event.currentIndex);
  }

  onSubmit() {
    const fabricationPlan = new FabricationPlan(this.chosenOperations);
    const newProduct = new Product(this.name.value, fabricationPlan, this.price.value);
    this.prodService.addProduct(newProduct).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Product has been created.');
    });
    // clean data
    this.name.reset();
    // deletes all operations from list
    this.chosenOperations.splice(0, this.chosenOperations.length);
  }
}
