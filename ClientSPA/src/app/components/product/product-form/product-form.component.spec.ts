import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductFormComponent} from './product-form.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductService} from '../../../services/product.service';
import {MockProductService} from '../../../services/mocks/mock-product.service';
import {ReactiveFormsModule} from '@angular/forms';
import {OperationService} from '../../../services/operation.service';
import {MockOperationService} from '../../../services/mocks/mock-operation.service';
import {Operation} from '../../../entities/operation';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('ProductFormComponent', () => {
  let component: ProductFormComponent;
  let fixture: ComponentFixture<ProductFormComponent>;
  let element: HTMLElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductFormComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [{provide: OperationService, useClass: MockOperationService},
        {provide: ProductService, useClass: MockProductService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    }).compileComponents();
    fixture = TestBed.createComponent(ProductFormComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'New Product';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have name invalid when empty', () => {
    expect(component.name.valid).toBeFalsy();
  });
  it('name field to be invalid if name is empty', () => {
    const form = component.name;
    form.setValue('');
    expect(form.valid).toBeFalsy();
  });
  it('name field to be invalid if name has spaces', () => {
    const form = component.name;
    form.setValue('knife master super cut 1000');
    expect(form.valid).toBeFalsy();
  });
  it('chosen list should be empty when page loads', () => {
    expect(component.chosenOperations.length).toBe(0);
  });
  it('operations list should have all operations in system when page loads', () => {
    expect(component.operations.length).toBe(2);
  });
  it('should list all operations in the system', () => {
    const operations = element.getElementsByClassName('operation');
    const op1 = new Operation('press-5mm', 'hammer', 10, 5);
    const op2 = new Operation('cut-1mm', 'drill', 20, 10);
    const expectedOps: Operation[] = [op1, op2];
    expect(operations.item(0).textContent).toBe(op1.description);
    expect(operations.item(1).textContent).toBe(op2.description);
    expect(component.operations).toEqual(expectedOps);
  });
  it('should have a button to list all operations', () => {
    const button = element.getElementsByClassName('dropbtn').item(0).textContent;
    expect(button).toEqual('Operations');
  });
  it('should have Fabrication Plan label', () => {
    const label = element.getElementsByClassName('word').item(2).textContent;
    expect(label).toEqual('Fabrication Plan:');
  });
  it('should have price label', () => {
    const label = element.getElementsByClassName('word').item(1).textContent;
    expect(label).toEqual(' Price: ');
  });
});
