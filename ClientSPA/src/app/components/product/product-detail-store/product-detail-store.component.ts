import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../../entities/product';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../services/product.service';
import {StoreService} from '../../../services/store.service';
import {FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-product-detail-store',
  templateUrl: './product-detail-store.component.html',
  styleUrls: ['./product-detail-store.component.css']
})
export class ProductDetailStoreComponent implements OnInit {
  @Input() product: Product;
  quantity = new FormControl('', [Validators.required, Validators.min(1), Validators.pattern(/^[0-9]/)]);
  @Input() deliveryDate: Date;
  access: boolean;

  constructor(
    private route: ActivatedRoute,
    public store: StoreService,
    private productService: ProductService,
    private auth: AuthService,
    private alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.getProduct();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Please log in');
    }
  }

  getProduct(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.productService.getProductForStoreById(name)
      .subscribe(product => this.product = product);
  }

  onSubmit() {
    if (this.quantity.valid) {
      this.store.addProductToShoppingCart(this.product, Number(this.quantity.value), this.deliveryDate);
      alert('You shopping cart has been updated!');

    } else {
      alert('Invalid quantity.');
    }
  }

}
