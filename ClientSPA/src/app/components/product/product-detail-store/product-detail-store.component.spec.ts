import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductDetailStoreComponent} from './product-detail-store.component';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductService} from '../../../services/product.service';
import {MockProductService} from '../../../services/mocks/mock-product.service';
import {StoreService} from '../../../services/store.service';
import {MockStoreService} from '../../../services/mocks/mock-store.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('ProductDetailStoreComponent', () => {
  let component: ProductDetailStoreComponent;
  let fixture: ComponentFixture<ProductDetailStoreComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDetailStoreComponent],
      imports: [ReactiveFormsModule, MatDatepickerModule, MatNativeDateModule,
        FormsModule, MatFormFieldModule, BrowserAnimationsModule, RouterTestingModule, MatInputModule],
      providers: [{provide: AuthService, useClass: AdminMockAuthService},
        {provide: ProductService, useClass: MockProductService},
        {provide: StoreService, useClass: MockStoreService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'Product fork - Details';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });

  it('should have correct Name', () => {
    const items = element.getElementsByClassName('word');
    const result = items.item(0).textContent;
    const expected = 'Name: fork';
    expect(result).toEqual(expected);
  });
});
