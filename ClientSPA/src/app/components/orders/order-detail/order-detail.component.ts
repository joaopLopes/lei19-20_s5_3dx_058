import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Order, ProductForOrder} from '../../../entities/order';
import {OrderService} from '../../../services/order.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';
import {ProductionPlanningService} from '../../../services/production-planning.service';
import {ProductProductionPlanning} from '../../../entities/product-production-planning';
import {PredictDate} from '../../../entities/predict-date';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css'],
})
export class OrderDetailComponent implements OnInit {
  @Input() order: Order;
  access: boolean;
  private sameUser: boolean;

  constructor(
    private route: ActivatedRoute,
    private orderService: OrderService,
    private alertService: AlertService,
    private auth: AuthService,
    private productionPlanningService: ProductionPlanningService
  ) {
  }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.access = false;
      const reference = this.route.snapshot.paramMap.get('reference');
      this.orderService.getOrder(reference)
        .subscribe(order => {
          this.order = order;
          this.sameUser = order.client === this.auth.getLoggedUser();
          this.access = this.auth.hasPermission(PermissionType.CONSULT_ORDER, this.sameUser, false);
          if (this.access) {
            this.getPredictedDeliveryDates();
          }
        });
    } else {
      this.access = false;
      this.alertService.error('You must be logged in to access this area!');
    }
  }

  /* When the user clicks on the button,
toggle between hiding and showing the dropdown content */

  myFunction(): void {
    document.getElementById('myDropdown').classList.toggle('show');
    const doc = document.getElementById('toggleButton');
    if (doc.innerText === 'Show') {
      doc.innerText = 'Hide';
    } else {
      doc.innerText = 'Show';
    }
  }

  cancelOrder() {
    this.orderService.cancelOrder(this.order.reference).subscribe(_ => {
      alert('Order was cancelled.');
      window.location.reload();
    }, error => {
      this.alertService.error(error.toString());
    });
  }

  canCancelOrder(): boolean {
    return this.auth.hasPermission(PermissionType.CANCEL_ORDER, this.sameUser, false);
  }

  canUpdateOrder(): boolean {
    return this.auth.hasPermission(PermissionType.UPDATE_ORDER, this.sameUser, false);
  }

  getPredictedDeliveryDates(): void {
    this.order.products.forEach(prod => {
      const productPlanning = new ProductProductionPlanning(this.order.reference, prod.name);
      this.productionPlanningService.getProductsPredictedDeliveryDate(productPlanning).subscribe(
        auxDate => prod.predictedDeliveryDate = new Date(auxDate.ano, auxDate.mes - 1 , auxDate.dia)
        , err => {
          console.log(err);
        });
    });
  }
}
