import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderDetailComponent} from './order-detail.component';
import {CookieService} from 'ngx-cookie-service';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../../services/order.service';
import {MockOrderService} from '../../../services/mocks/mock-order.service';
import {AuthService} from '../../../services/auth.service';
import {ClientMockAuthService} from '../../../services/mocks/client-mock-auth.service';
import {HttpClientModule} from '@angular/common/http';
import {ProductionPlanningService} from '../../../services/production-planning.service';
import {MockProductionPlanningService} from '../../../services/mocks/mock-production-planning.service';

describe('OrderDetailComponent', () => {
  let component: OrderDetailComponent;
  let fixture: ComponentFixture<OrderDetailComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderDetailComponent],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [{provide: OrderService, useClass: MockOrderService},
        {provide: AuthService, useClass: ClientMockAuthService},
        {provide: ProductionPlanningService, useClass: MockProductionPlanningService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(OrderDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'Order # test-ref-ord2';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have correct information', () => {
    const items = element.getElementsByClassName('word');
    const user = items.item(0).textContent;
    const status = items.item(1).textContent;
    const shippingAddress = items.item(2).textContent;
    const price = items.item(3).textContent;
    const paymentMethod = items.item(4).textContent;
    const products = items.item(6).textContent;
    expect(user).toBe('User: joao');
    expect(status).toBe('Status: ONGOING');
    expect(shippingAddress).toBe('Shipping Address: Rua dos Martelos');
    expect(price).toBe('Price Paid: 600 Euros');
    expect(paymentMethod).toBe('Payment Method: PT501234123412344321');
    expect(products).toBe('Products Ordered Show');
  });
  it('should have all products', () => {
    const items = element.getElementsByClassName('word');
    const prod1 = items.item(7).textContent;
    expect(prod1).toBe(' Product: spoon  Price: 100  Quantity: 60  Delivery Date: 10/10/2030  Predicted Delivery Date: 10/10/2042 ');
  });
  it('should have cancel button', () => {
    const items = element.getElementsByClassName('submit');
    const but = items.item(0).textContent;
    expect(but).toBe(' Cancel ');
  });
  it('should have update button', () => {
    const items = element.getElementsByClassName('submit');
    const but = items.item(1).textContent;
    expect(but).toBe(' Update ');
  });

});
