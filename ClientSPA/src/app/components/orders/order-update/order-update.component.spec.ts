import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrderUpdateComponent} from './order-update.component';
import {CookieService} from 'ngx-cookie-service';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../../services/order.service';
import {MockOrderService} from '../../../services/mocks/mock-order.service';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {ClientMockAuthService} from '../../../services/mocks/client-mock-auth.service';

describe('OrderUpdateComponent', () => {
  let component: OrderUpdateComponent;
  let fixture: ComponentFixture<OrderUpdateComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderUpdateComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [{provide: OrderService, useClass: MockOrderService},
        {provide: AuthService, useClass: ClientMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(OrderUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'Updating Order # ';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('address invalid when empty', () => {
    expect(component.deliveryAddress.valid).toBeFalsy();
  });
  it('address field to be invalid if less than 5 chars', () => {
    component.deliveryAddress.setValue('rua');
    expect(component.deliveryAddress.valid).toBeFalsy();
  });
});
