import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {AlertService} from '../../../services/alert.service';
import {OrderService} from '../../../services/order.service';
import {Order} from '../../../entities/order';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';

@Component({
  selector: 'app-order-update',
  templateUrl: './order-update.component.html',
  styleUrls: ['./order-update.component.css']
})
export class OrderUpdateComponent implements OnInit {

  deliveryAddress = new FormControl('', [Validators.required, Validators.minLength(5)]);
  reference: string;
  access: boolean;
  private order: Order;

  constructor(private orderService: OrderService,
              private alertService: AlertService,
              private route: ActivatedRoute,
              private auth: AuthService
  ) {
  }

  ngOnInit() {
    this.reference = this.route.snapshot.paramMap.get('reference');
    if (!this.auth.isLoggedIn()) {
      this.access = false;
      this.alertService.error('You must be logged in to access this area!');
    } else {
      this.orderService.getOrder(this.reference).subscribe(ord => {
        this.order = ord;
        const sameUser: boolean = this.auth.getLoggedUser() === this.order.client;
        if (!(this.auth.hasPermission(PermissionType.UPDATE_ORDER, sameUser, false))) {
          this.access = false;
          this.alertService.error('Not allowed to update order.');
        } else {
          this.access = true;
        }
      });
    }
  }

  onSubmit() {
    this.order.reference = this.reference;
    this.order.shippingAddress = this.deliveryAddress.value;
    this.orderService.updateOrder(this.reference, this.order).subscribe(_ => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Order updated successfully');
      this.deliveryAddress.reset();
    });
  }
}
