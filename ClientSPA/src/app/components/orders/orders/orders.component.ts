import {Component, OnInit} from '@angular/core';
import {Order} from '../../../entities/order';
import {OrderService} from '../../../services/order.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders: Order[];
  access: boolean;

  constructor(private orderService: OrderService,
              private alertService: AlertService,
              private auth: AuthService) {
  }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.access = false;
      this.alertService.error('Please log in.');
    } else {
      if (this.auth.hasPermission(PermissionType.LIST_ORDERS, false, true)) {
        this.getOrders();
        this.access = true;
      } else {
        this.access = false;
        this.alertService.error('Not allowed.');
      }
    }
  }

  getOrders(): void {
    this.orderService.getOrders()
      .subscribe(orders => this.orders = orders);
  }

}
