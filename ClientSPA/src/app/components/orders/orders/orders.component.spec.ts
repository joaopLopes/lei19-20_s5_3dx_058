import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrdersComponent} from './orders.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../../services/order.service';
import {MockOrderService} from '../../../services/mocks/mock-order.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('OrdersComponent', () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrdersComponent],
      imports: [RouterTestingModule],
      providers: [{provide: OrderService, useClass: MockOrderService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should list all orders', () => {
    const orders = element.getElementsByClassName('order');
    expect(orders.item(0).textContent).toBe('Ref # test-ref-ord1 Placement Date: 12/12/2019');
    expect(orders.item(1).textContent).toBe('Ref # test-ref-ord2 Placement Date: 12/12/2019');
  });
  it('should have title Orders', () => {
    const expected = 'Orders';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
});
