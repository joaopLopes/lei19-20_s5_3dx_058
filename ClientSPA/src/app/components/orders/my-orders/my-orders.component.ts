import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';
import {Order} from '../../../entities/order';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';
import {PermissionType} from '../../../authentication/PermissionType';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {
  orders: Order[];
  client: string;
  access: boolean;

  constructor(private alertService: AlertService,
              private auth: AuthService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.alertService.info('Please log in');
      this.access = false;
    } else {
      if (this.auth.isClient() && this.auth.hasPermission(PermissionType.LIST_ORDERS, true, true)) {
        this.client = this.auth.getLoggedUser();
        this.getOrders();
        this.access = true;
      } else {
        this.alertService.error('Not allowed');
        this.access = false;
      }
    }
  }

  getOrders(): void {
    this.orderService.getOrdersByClient(this.client)
      .subscribe(orders => this.orders = orders);
  }

}
