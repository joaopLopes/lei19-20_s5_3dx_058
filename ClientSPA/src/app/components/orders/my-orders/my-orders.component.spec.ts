import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MyOrdersComponent} from './my-orders.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../../services/order.service';
import {MockOrderService} from '../../../services/mocks/mock-order.service';
import {AuthService} from '../../../services/auth.service';
import {ClientMockAuthService} from '../../../services/mocks/client-mock-auth.service';

describe('MyOrdersComponent', () => {
  let component: MyOrdersComponent;
  let fixture: ComponentFixture<MyOrdersComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyOrdersComponent],
      imports: [RouterTestingModule],
      providers: [{provide: OrderService, useClass: MockOrderService},
        {provide: AuthService, useClass: ClientMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MyOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should list all orders for pedro', () => {
    const orders = element.getElementsByClassName('order');
    expect(component.client).toEqual('pedro');
    expect(orders.item(0).textContent).toBe('Ref # test-ref-ord1 Placement Date: 12/12/2019');
  });
  it('should have title My Orders', () => {
    const expected = 'My Orders';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
});
