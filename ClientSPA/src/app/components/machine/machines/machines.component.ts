import {Component, OnInit} from '@angular/core';
import {Machine} from '../../../entities/machine';
import {MachineService} from '../../../services/machine.service';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.css']
})
export class MachinesComponent implements OnInit {

  machines: Machine[];
  machineTypes: MachineType[];
  selectedMachineType: string;
  access: boolean;

  constructor(private machineService: MachineService,
              private machineTypeService: MachineTypeService,
              private alertService: AlertService,
              private authService: AuthService) {
  }

  /**
   * Initializes the UI with the production lines in database.
   */
  ngOnInit() {
    if (this.authService.isAdmin()) {
      this.getMachineTypes();
      this.getMachines();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not Allowed.');
    }
  }

  /**
   * Gets the machines from database.
   */
  getMachines(): void {
    this.machineService.getMachines().subscribe(machines => this.machines = machines);
  }

  /**
   * Gets the machine types from the server.
   */

  getMachineTypes(): void {
    this.machineTypeService.getMachineTypes().subscribe(machineTypes => this.machineTypes = machineTypes);
  }

  getMachinesByType(): void {
    // tslint:disable-next-line:triple-equals
    if (this.selectedMachineType == 'None') {
      this.getMachines();
    } else {
      this.machineService.getMachineByMachineType(this.selectedMachineType).subscribe(machines => this.machines = machines, error => {
        this.alertService.error(error);
      });
    }
  }

}
