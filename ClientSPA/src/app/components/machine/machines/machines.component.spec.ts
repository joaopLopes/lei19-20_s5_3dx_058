import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MachinesComponent} from './machines.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MachineService} from '../../../services/machine.service';
import {MockMachineService} from '../../../services/mocks/mock-machine.service';
import {Machine} from '../../../entities/machine';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MachineTypeService} from '../../../services/machine-type.service';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';


describe('MachinesComponent', () => {
  let component: MachinesComponent;
  let fixture: ComponentFixture<MachinesComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachinesComponent],
      imports: [RouterTestingModule, MatSelectModule, ReactiveFormsModule, HttpClientModule],
      providers: [{provide: MachineService, useClass: MockMachineService},
        {provide: MachineTypeService, useClass: MockMachinetypeService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MachinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should list all machines', () => {
    const machines = element.getElementsByClassName('machine');
    const machine1 = new Machine('CS23', 'Cutter and Saw M3', 'Johnson Machinery', 'type1');
    const machine2 = new Machine('D11', 'Driller M1', 'Javier Machinery', 'type2');
    const expectedMachines: Machine[] = [machine1, machine2];
    expect(machines.item(0).textContent).toBe(machine1.serialNumber);
    expect(machines.item(1).textContent).toBe(machine2.serialNumber);
    expect(component.machines).toEqual(expectedMachines);
  });
  it('should have title machines', () => {
    const expected = 'Machines';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
  it('should have new machine button', () => {
    const expected = '+ New Machine';
    const result = element.getElementsByClassName('new').item(0).textContent;
    expect(expected).toBe(result);
  });

});
