import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MachineService} from '../../../services/machine.service';
import {Machine} from '../../../entities/machine';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-machine-detail',
  templateUrl: './machine-detail.component.html',
  styleUrls: ['./machine-detail.component.css']
})
export class MachineDetailComponent implements OnInit {
  @Input() machine: Machine;
  @Input() otherMachineTypes: MachineType[];
  access: boolean;

  constructor(
    private route: ActivatedRoute,
    private alertService: AlertService,
    private machineService: MachineService,
    private machineTypeService: MachineTypeService,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
    if (this.auth.isAdmin()) {
      this.access = true;
      this.getMachine();
      this.getOtherMachineTypes();
    } else {
      this.access = false;
      this.alertService.error('No Access.');
    }
  }

  getMachine(): void {
    const name = this.route.snapshot.paramMap.get('serialNumber');
    this.machineService.getMachine(name)
      .subscribe(machine => this.machine = machine);
  }

  showMachineTypes() {
    document.getElementById('myDropdown').classList.toggle('show');
  }

  updateMachineType(machineType: MachineType) {
    this.machine.machineType = machineType.name;
    this.machineService.putMachine(this.machine).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Machine Type has been updated!.');
    });
  }

  private getOtherMachineTypes() {
    this.machineTypeService.getMachineTypes().subscribe(list => this.otherMachineTypes = list);
    this.deleteCurrentMachineTypeFromList();
  }

  private deleteCurrentMachineTypeFromList() {
    let machineTypeAux: MachineType;
    this.machineTypeService.getMachineType(this.machine.machineType).subscribe(type => machineTypeAux = type);
    const index = this.otherMachineTypes.indexOf(machineTypeAux);
    if (index >= 0) {
      this.otherMachineTypes.splice(index, 1);
    }
  }

}
