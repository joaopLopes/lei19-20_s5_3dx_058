import {ComponentFixture, TestBed} from '@angular/core/testing';

import {MachineDetailComponent} from './machine-detail.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MachineService} from '../../../services/machine.service';
import {MockMachineService} from '../../../services/mocks/mock-machine.service';
import {HttpClientModule} from '@angular/common/http';
import {MachineTypeService} from '../../../services/machine-type.service';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {MachineType} from '../../../entities/machine-type';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';


describe('MachineDetailComponent', () => {
  let component: MachineDetailComponent;
  let fixture: ComponentFixture<MachineDetailComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MachineDetailComponent],
      imports: [RouterTestingModule, HttpClientModule, ReactiveFormsModule],
      providers: [{provide: MachineService, useClass: MockMachineService}, {
        provide: MachineTypeService,
        useClass: MockMachinetypeService
      },
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MachineDetailComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'Machine D11 - Details';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have correct Model', () => {
    const items = element.getElementsByClassName('word');
    const result = items.item(0).textContent;
    const expected = 'Model: Driller M1';
    expect(result).toEqual(expected);
  });
  it('should have correct Brand', () => {
    const items = element.getElementsByClassName('word');
    const result = items.item(1).textContent;
    const expected = 'Brand: Javier Machinery';
    expect(result).toEqual(expected);
  });
  it('should have correct Machine Type', () => {
    const items = element.getElementsByClassName('word')[4];
    const result = items.textContent;
    const expected = ' type2';
    expect(result).toEqual(expected);
  });
  it('should have change button', () => {
    const expected = 'Change ▼';
    const result = element.getElementsByClassName('dropbtn').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('machine types list should have all machine types in system when page loads', () => {
    expect(component.otherMachineTypes.length).toBe(1);
  });
  it('should list all machine types in the system', () => {
    const type1 = new MachineType('type1', ['cut', 'saw']);
    const expectedTypes: MachineType[] = [type1];
    expect(component.otherMachineTypes).toEqual(expectedTypes);
  });
});
