import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MachineType} from '../../../entities/machine-type';
import {MachineTypeService} from '../../../services/machine-type.service';
import {Machine} from '../../../entities/machine';
import {MachineService} from '../../../services/machine.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './machine-form.component.html',
  styleUrls: ['./machine-form.component.css']
})
export class MachineFormComponent implements OnInit {

  serialNumber = new FormControl('', Validators.required);
  model = new FormControl('', Validators.required);
  brand = new FormControl('', Validators.required);
  chosenMachineType: string;
  machineTypes: MachineType[];
  access: boolean;


  constructor(private machineService: MachineService,
              private machineTypeService: MachineTypeService,
              private alertService: AlertService,
              private auth: AuthService) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.getMachineTypes();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not allowed.');
    }
  }

  getMachineTypes(): void {
    this.machineTypeService.getMachineTypes()
      .subscribe(machineTypes => this.machineTypes = machineTypes);
  }


  /**
   * Shows and hides the selectable machine types
   */
  showMachineTypes(): void {
    document.getElementById('myDropdown').classList.toggle('show');
  }


  /**
   * Filters the user input when looking for machine types
   */
  filterMachineTypes() {
    let input;
    let filter;
    let div;
    let a;
    let i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    div = document.getElementById('myDropdown');
    a = div.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
      input.txtValue = a[i].textContent || a[i].innerText;
      if (input.txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = '';
      } else {
        a[i].style.display = 'none';
      }
    }
  }

  chooseMachineType(machineType: MachineType) {
    this.chosenMachineType = machineType.name;
    document.getElementById('machine-button').textContent = machineType.name;
  }


  onSubmit() {
    const newMachine = new Machine(this.serialNumber.value, this.model.value, this.brand.value, this.chosenMachineType);
    this.machineService.addMachine(newMachine).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Machine has been created.');
    });
    // clean data
    this.serialNumber.reset();
    this.model.reset();
    this.brand.reset();
  }
}
