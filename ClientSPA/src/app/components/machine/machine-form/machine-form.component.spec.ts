import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RouterTestingModule} from '@angular/router/testing';
import {MachineService} from '../../../services/machine.service';
import {MockMachineService} from '../../../services/mocks/mock-machine.service';
import {MachineTypeService} from '../../../services/machine-type.service';
import {MockMachinetypeService} from '../../../services/mocks/mock-machinetype.service';
import {ReactiveFormsModule} from '@angular/forms';
import {MachineFormComponent} from './machine-form.component';
import {MachineType} from '../../../entities/machine-type';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('MachineFormComponent', () => {
  let component: MachineFormComponent;
  let fixture: ComponentFixture<MachineFormComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MachineFormComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [{provide: MachineService, useClass: MockMachineService},
        {provide: MachineTypeService, useClass: MockMachinetypeService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(MachineFormComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'New Machine';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have serial number invalid when empty', () => {
    expect(component.serialNumber.valid).toBeFalsy();
  });
  it('serial number field to be invalid if serial number is empty', () => {
    const form = component.serialNumber;
    form.setValue('');
    expect(form.valid).toBeFalsy();
  });
  it('serial number field to be invalid if serial number does not respect regex', () => {
    const form = component.serialNumber;
    form.setValue('lowercase letters and special characters !$#$');
    expect(form.valid).toBeFalsy();
  });
  it('should have brand invalid when empty', () => {
    expect(component.brand.valid).toBeFalsy();
  });
  it('brand field to be invalid if brand is empty', () => {
    const form = component.brand;
    form.setValue('');
    expect(form.valid).toBeFalsy();
  });
  it('should have model invalid when empty', () => {
    expect(component.model.valid).toBeFalsy();
  });
  it('model field to be invalid if model is empty', () => {
    const form = component.model;
    form.setValue('');
    expect(form.valid).toBeFalsy();
  });
  it('chosen list should be null when page loads', () => {
    expect(component.chosenMachineType).toBe(undefined);
  });
  it('machine types list should have all machine types in system when page loads', () => {
    expect(component.machineTypes.length).toBe(2);
  });
  it('should list all machine types in the system', () => {
    const type1 = new MachineType('type1', ['cut', 'saw']);
    const type2 = new MachineType('type2', ['drill']);
    const expectedTypes: MachineType[] = [type1, type2];
    expect(component.machineTypes).toEqual(expectedTypes);
  });
  it('should have a button to list all machine types', () => {
    const button = element.getElementsByClassName('dropbtn').item(0).textContent;
    expect(button).toEqual('Machine Type');
  });
});
