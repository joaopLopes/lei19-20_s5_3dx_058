import {Component, Input, OnInit} from '@angular/core';
import {AlertService} from '../../services/alert.service';
import {ProductionPlanningService} from '../../services/production-planning.service';
import {OrderMachines} from '../../entities/order-machines';
import {Line} from '../../entities/line';
import {ProductionLineService} from '../../services/production-line.service';
import {ProductionLine} from '../../entities/production-line';

@Component({
  selector: 'app-production-planning',
  templateUrl: './production-planning.component.html',
  styleUrls: ['./production-planning.component.css']
})
export class ProductionPlanningComponent implements OnInit {
  @Input() startDate: Date;
  @Input() endDate: Date;
  access: boolean;
  orderMachines: OrderMachines; // Order Machines that is going to be subscribed by PROLOG method.
  lines: ProductionLine[];

  constructor(private alertService: AlertService,
              private prologService: ProductionPlanningService,
              private prodLineService: ProductionLineService) {
  }

  ngOnInit() {
    this.getProductionLines();
  }

  /**
   * Method that is going to get JSON to Prolog and build the updated database.
   */
  async onSubmit() {
    if (this.startDate == null || this.endDate == null) {
      this.alertService.error('Please insert the dates !');
    } else {
      const schedule: any = {};
      schedule.productionPlans = [];
      await this.prologService.generatePrologDatabase(this.startDate, this.endDate).subscribe(async _ => {
        await this.prologService.balanceLines().subscribe(async __ => {
          const promises = this.lines.map(async (line) => {
            const aux = new Line(line.productionLineIdentifier);
            await this.prologService.bringsOrderJSON(aux).toPromise().then(orderMachine => {
              this.orderMachines = orderMachine;
              const jsonInString = this.makeJsonOutOfOrderString(this.orderMachines.orders.trim());
              schedule.productionPlans.push(JSON.parse(jsonInString));
            });
          });
          Promise.all(promises).then(() => {
            const scheduleStr = JSON.stringify(schedule);
            localStorage.setItem('schedule', scheduleStr);
          }).catch(err => {
            console.error(err);
          });
        }, err => {
          console.error(err);
        });
      }, err => {
        console.error(err);
      });
    }
  }

  /**
   *
   */
  getProductionLines() {
    this.prodLineService.getProductionLines().subscribe(productionLines => this.lines = productionLines);
  }


  makeJsonOutOfOrderString(content: string): string {
    const l = content.split(','); // The variable containing the list of strings
    let json = '{';
    l.forEach(element => {
      const elementTrimmed = element.trim();
      switch (elementTrimmed) {
        case 'startLines:':
          break;
        case 'startFabrications:':
          break;
        case 'startMachine:':
          break;
        case 'startOcupation:':
          break;
        case 'porBarra':
          json = json + ']';
          break;
        case 'porCurvaBarra':
          json = json + '}';
          break;
        case 'porVirgula':
          json = json + ',';
          break;
        default :
          const arrayOfImportantThing = elementTrimmed.split(':');
          if (arrayOfImportantThing[0] === 'timeSlot') {
            const parenthesis = '"';
            const productionlineInParenthesisLeft = arrayOfImportantThing[0].concat('"');
            const productionLineBetweenParenthesis = parenthesis.concat(productionlineInParenthesisLeft);
            const productionLineWithBars = productionLineBetweenParenthesis.concat(':');
            const withTimeSlot = productionLineWithBars.concat(arrayOfImportantThing[1]);
            const finishing = withTimeSlot.concat('}');
            json = json + finishing;
          } else {
            arrayOfImportantThing.forEach(elementImportant => {
              switch (elementImportant) {
                case 'productionLine' || 'processingTime' || 'setupTime':
                  const parenthesis = '"';
                  const productionlineInParenthesisLeft = elementImportant.concat('"');
                  const productionLineBetweenParenthesis = parenthesis.concat(productionlineInParenthesisLeft);
                  const productionLineWithBars = productionLineBetweenParenthesis.concat(':');
                  json = json + productionLineWithBars;
                  break;
                case 'processingTime':
                  const parenthesisAA = '"';
                  const productionlineInParenthesisLeftB = elementImportant.concat('"');
                  const productionLineBetweenParenthesisB = parenthesisAA.concat(productionlineInParenthesisLeftB);
                  const productionLineWithBarsB = productionLineBetweenParenthesisB.concat(':');
                  json = json + productionLineWithBarsB;
                  break;
                case 'setupTime':
                  const parenthesisBB = '"';
                  const productionlineInParenthesisLeftBBB = elementImportant.concat('"');
                  const productionLineBetweenParenthesisBBB = parenthesisBB.concat(productionlineInParenthesisLeftBBB);
                  const productionLineWithBarsBBB = productionLineBetweenParenthesisBBB.concat(':');
                  json = json + productionLineWithBarsBBB;
                  break;
                case 'operation':
                  const withBrackets = '{';
                  const parenthesisLLL = withBrackets.concat('"');
                  const productionlineInParenthesisLeftLLL = elementImportant.concat('"');
                  const productionLineBetweenParenthesisLLL = parenthesisLLL.concat(productionlineInParenthesisLeftLLL);
                  const productionLineWithBarsLLL = productionLineBetweenParenthesisLLL.concat(':');
                  json = json + productionLineWithBarsLLL;
                  break;
                case 'product':
                  const parenthesisDDD = '{ "';
                  const productionlineInParenthesisLeftDDD = elementImportant.concat('"');
                  const productionLineBetweenParenthesisDDD = parenthesisDDD.concat(productionlineInParenthesisLeftDDD);
                  const productionLineWithBarsDDD = productionLineBetweenParenthesisDDD.concat(':');
                  json = json + productionLineWithBarsDDD;
                  break;
                case 'machine':
                  const parenthesisEEE = '{ "';
                  const productionlineInParenthesisLeftEEE = elementImportant.concat('"');
                  const productionLineBetweenParenthesisEEE = parenthesisEEE.concat(productionlineInParenthesisLeftEEE);
                  const productionLineWithBarsEEE = productionLineBetweenParenthesisEEE.concat(':');
                  json = json + productionLineWithBarsEEE;
                  break;
                case 'fabricationsOrders' || 'machines' || 'ocupations':
                  const parenthesisB = '"';
                  const bLeftParenthesis = elementImportant.concat('"');
                  const bBetweenParenthesis = parenthesisB.concat(bLeftParenthesis);
                  const bBars = bBetweenParenthesis.concat(':');
                  const openBracket = bBars.concat('[');
                  json = json + openBracket;
                  break;
                case 'machines':
                  const parenthesisGGG = '"';
                  const bLeftParenthesisGGG = elementImportant.concat('"');
                  const bBetweenParenthesisGGG = parenthesisGGG.concat(bLeftParenthesisGGG);
                  const bBarsGGG = bBetweenParenthesisGGG.concat(':');
                  const openBracketGGG = bBarsGGG.concat('[');
                  json = json + openBracketGGG;
                  break;
                case 'ocupations':
                  const parenthesisHHH = '"';
                  const bLeftParenthesisHHH = elementImportant.concat('"');
                  const bBetweenParenthesisHHH = parenthesisHHH.concat(bLeftParenthesisHHH);
                  const bBarsHHH = bBetweenParenthesisHHH.concat(':');
                  const openBracketHHH = bBarsHHH.concat('[');
                  json = json + openBracketHHH;
                  break;
                case '':
                  break;
                case 'porCurvaBarra':
                  break;
                default:
                  const elementImportantTrimmed = elementImportant.trim();
                  const parenthesisUnknown = '"';
                  const elementInParenthesisInLeft = elementImportantTrimmed.concat('"');
                  const elementBetweenParenthesis = parenthesisUnknown.concat(elementInParenthesisInLeft);
                  const elementWithComma = elementBetweenParenthesis.concat(', ');
                  json = json + elementWithComma;
                  break;
              }
            });
          }
          break;
      }
    });
    const regex = /,}|, }/;
    const regex2 = /}]/g;
    const regex3 = /}}{|}{/g;
    const json2 = json.replace(regex, '}');
    const json3 = json2.replace(regex2, ']');
    return json3.replace(regex3, '},{');
  }

}
