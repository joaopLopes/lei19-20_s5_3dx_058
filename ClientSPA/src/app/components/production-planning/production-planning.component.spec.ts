import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductionPlanningComponent} from './production-planning.component';
import {MatDatepickerModule, MatFormFieldModule, MatInputModule, MatNativeDateModule} from '@angular/material';
import {MockProductionPlanningService} from '../../services/mocks/mock-production-planning.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ProductionLine} from '../../entities/production-line';
import {ProductionLineService} from '../../services/production-line.service';
import {MockProductionLineService} from '../../services/mocks/mock-production-line.service';
import {ProductionPlanningService} from '../../services/production-planning.service';

describe('ProductionPlanningComponent', () => {
  let component: ProductionPlanningComponent;
  let fixture: ComponentFixture<ProductionPlanningComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductionPlanningComponent],
      imports: [ReactiveFormsModule, RouterTestingModule, MatDatepickerModule, MatNativeDateModule,
        FormsModule, MatFormFieldModule, MatInputModule, BrowserAnimationsModule],
      providers: [{provide: ProductionPlanningService, useClass: MockProductionPlanningService},
        {provide: ProductionLineService, useClass: MockProductionLineService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ProductionPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
