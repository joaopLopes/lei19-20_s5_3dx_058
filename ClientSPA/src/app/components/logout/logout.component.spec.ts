import {ComponentFixture, TestBed} from '@angular/core/testing';

import {LogoutComponent} from './logout.component';
import {CookieService} from 'ngx-cookie-service';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../services/order.service';
import {MockOrderService} from '../../services/mocks/mock-order.service';
import {AuthService} from '../../services/auth.service';
import {ClientMockAuthService} from '../../services/mocks/client-mock-auth.service';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let element: HTMLElement;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LogoutComponent],
      imports: [RouterTestingModule],
      providers: [{provide: AuthService, useClass: ClientMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
