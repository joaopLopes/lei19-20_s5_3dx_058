import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OperationFormComponent} from './operation-form.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OperationService} from '../../../services/operation.service';
import {MockOperationService} from '../../../services/mocks/mock-operation.service';
import {ReactiveFormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('OperationFormComponent', () => {
  let component: OperationFormComponent;
  let fixture: ComponentFixture<OperationFormComponent>;
  let element: HTMLElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OperationFormComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [{provide: OperationService, useClass: MockOperationService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(OperationFormComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'New Operation';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('form invalid when empty', () => {
    expect(component.operationForm.valid).toBeFalsy();
  });
  it('description field to be invalid if description has spaces', () => {
    const descriptionKey = 'description';
    const descriptionForm = component.operationForm.controls[descriptionKey];
    descriptionForm.setValue('super operation');
    expect(descriptionForm.valid).toBeFalsy();
  });
  it('description field to be invalid if description is empty', () => {
    const descriptionKey = 'description';
    const descriptionForm = component.operationForm.controls[descriptionKey];
    descriptionForm.setValue('');
    expect(descriptionForm.valid).toBeFalsy();
  });
  it('tool field to be invalid if tool is empty', () => {
    const toolKey = 'tool';
    const toolForm = component.operationForm.controls[toolKey];
    toolForm.setValue('');
    expect(toolForm.valid).toBeFalsy();
  });
  it('execution time field to be invalid if negative', () => {
    const executionTimeKey = 'executionTime';
    const executionTimeForm = component.operationForm.controls[executionTimeKey];
    executionTimeForm.setValue(-1);
    expect(executionTimeForm.valid).toBeFalsy();
  });
  it('setup time field to be invalid if negative', () => {
    const setupTimeKey = 'setupTime';
    const setupTimeForm = component.operationForm.controls[setupTimeKey];
    setupTimeForm.setValue(-1);
    expect(setupTimeForm.valid).toBeFalsy();
  });
  it('description field to be valid if description is no space string', () => {
    const descriptionKey = 'description';
    const descriptionForm = component.operationForm.controls[descriptionKey];
    descriptionForm.setValue('drill-sharp-screw');
    expect(descriptionForm.valid).toBeTruthy();
  });
  it('tool field to be valid if tool is a string', () => {
    const toolKey = 'tool';
    const toolForm = component.operationForm.controls[toolKey];
    toolForm.setValue('sharp-screw');
    expect(toolForm.valid).toBeTruthy();
  });
  it('execution time field to be valid if 0 or positive', () => {
    const executionTimeKey = 'executionTime';
    const executionTimeForm = component.operationForm.controls[executionTimeKey];
    executionTimeForm.setValue(0);
    expect(executionTimeForm.valid).toBeTruthy();
    executionTimeForm.setValue(10);
    expect(executionTimeForm.valid).toBeTruthy();
  });
  it('setup time field to be valid if 0 or positive', () => {
    const setupTimeKey = 'setupTime';
    const setupTimeForm = component.operationForm.controls[setupTimeKey];
    setupTimeForm.setValue(0);
    expect(setupTimeForm.valid).toBeTruthy();
    setupTimeForm.setValue(12);
    expect(setupTimeForm.valid).toBeTruthy();
  });
  it('form should be valid when all data is valid', () => {
    const descriptionKey = 'description';
    const descriptionForm = component.operationForm.controls[descriptionKey];
    descriptionForm.setValue('drill-sharp-screw');
    const toolKey = 'tool';
    const toolForm = component.operationForm.controls[toolKey];
    toolForm.setValue('sharp-screw');
    const executionTimeKey = 'executionTime';
    const executionTimeForm = component.operationForm.controls[executionTimeKey];
    executionTimeForm.setValue(0);
    const setupTimeKey = 'setupTime';
    const setupTimeForm = component.operationForm.controls[setupTimeKey];
    setupTimeForm.setValue(0);
    expect(component.operationForm.valid).toBeTruthy();
  });
});
