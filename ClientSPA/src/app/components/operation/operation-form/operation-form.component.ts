import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Operation} from '../../../entities/operation';
import {OperationService} from '../../../services/operation.service';
import {AlertService} from '../../../services/alert.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-operation-form',
  templateUrl: './operation-form.component.html',
  styleUrls: ['./operation-form.component.css']
})
export class OperationFormComponent implements OnInit {
  operationForm = new FormGroup({
    description: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9|\-|_]*$/)]),
    tool: new FormControl('', [Validators.required]),
    executionTime: new FormControl('', Validators.min(0)),
    setupTime: new FormControl('', Validators.min(0)),
  });
  access: boolean;

  constructor(private operationService: OperationService,
              private alertService: AlertService,
              private auth: AuthService
  ) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.access = true;
    } else {
      this.alertService.error('Not allowed');
      this.access = false;
    }
  }

  private add(description: string, tool: string, executionTime: number, setupTime: number):
    void {
    description = description.trim();
    tool = tool.trim();

    if (!tool || !description) {
      return;
    }
    this.operationService.addOperation(new Operation(description, tool, executionTime, setupTime)).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.alertService.info('Operation has been created.');
      this.operationForm.reset();
    });
  }

  onSubmit() {
    const result: Operation = Object.assign({}, this.operationForm.value);
    this.add(result.description, result.tool, result.executionTime, result.setupTime);
  }

}
