import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OperationService} from '../../../services/operation.service';
import {Operation} from '../../../entities/operation';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-operation-detail',
  templateUrl: './operation-detail.component.html',
  styleUrls: ['./operation-detail.component.css']
})
export class OperationDetailComponent implements OnInit {
  @Input() operation: Operation;
  access: boolean;

  constructor(
    private route: ActivatedRoute,
    private operationService: OperationService,
    private auth: AuthService,
    private alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    if (this.auth.isAdmin()) {
      this.access = true;
      this.getOperation();
    } else {
      this.alertService.error('Not allowed.');
      this.access = false;
    }
  }

  getOperation(): void {
    const name = this.route.snapshot.paramMap.get('description');
    this.operationService.getOperation(name)
      .subscribe(operation => this.operation = operation);
  }
}
