import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OperationDetailComponent} from './operation-detail.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OperationService} from '../../../services/operation.service';
import {MockOperationService} from '../../../services/mocks/mock-operation.service';
import {Operation} from '../../../entities/operation';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('OperationDetailComponent', () => {
  let component: OperationDetailComponent;
  let fixture: ComponentFixture<OperationDetailComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OperationDetailComponent],
      imports: [RouterTestingModule],
      providers: [{provide: OperationService, useClass: MockOperationService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    }).compileComponents();
    fixture = TestBed.createComponent(OperationDetailComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'Operation cut-1mm - Details';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have correct information', () => {
    const items = element.getElementsByClassName('word');
    const description = items.item(0).textContent;
    const tool = items.item(1).textContent;
    const executionTime = items.item(2).textContent;
    const setupTime = items.item(3).textContent;
    expect(description).toBe('Description: cut-1mm');
    expect(tool).toBe('Tool: drill');
    expect(executionTime).toBe('Execution Time: 20 seconds');
    expect(setupTime).toBe('Setup Time: 10 seconds');
  });
  it('should have operation in ts', () => {
    expect(component.operation).toEqual(new Operation('cut-1mm', 'drill', 20, 10));
  });
});
