import {Component, OnInit} from '@angular/core';
import {Operation} from '../../../entities/operation';
import {OperationService} from '../../../services/operation.service';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {

  operations: Operation[];
  access: boolean;

  constructor(private operationService: OperationService,
              private auth: AuthService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.getOperations();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('No access.');
    }
  }

  getOperations(): void {
    this.operationService.getOperations()
      .subscribe(operations => this.operations = operations);
  }

}
