import {ComponentFixture, TestBed} from '@angular/core/testing';
import {OperationsComponent} from './operations.component';
import {OperationService} from '../../../services/operation.service';
import {MockOperationService} from '../../../services/mocks/mock-operation.service';
import {RouterTestingModule} from '@angular/router/testing';
import {Operation} from '../../../entities/operation';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';


describe('OperationsComponent', () => {
  let component: OperationsComponent;
  let fixture: ComponentFixture<OperationsComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OperationsComponent],
      imports: [RouterTestingModule],
      providers: [{provide: OperationService, useClass: MockOperationService},
        {provide: AuthService, useClass: AdminMockAuthService}
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(OperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should list all operations', () => {
    const operations = element.getElementsByClassName('operation');
    const op1 = new Operation('press-5mm', 'hammer', 10, 5);
    const op2 = new Operation('cut-1mm', 'drill', 20, 10);
    const expectedOps: Operation[] = [op1, op2];
    expect(operations.item(0).textContent).toBe(op1.description);
    expect(operations.item(1).textContent).toBe(op2.description);
    expect(component.operations).toEqual(expectedOps);
  });
  it('should have title operations', () => {
    const expected = 'Operations';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
  it('should have new operation button', () => {
    const expected = '+ New Operation';
    const result = element.getElementsByClassName('new').item(0).textContent;
    expect(expected).toBe(result);
  });
});
