import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {AuthService} from '../../services/auth.service';
import {PermissionType} from '../../authentication/PermissionType';

@Component({
  selector: 'app-admin-area',
  templateUrl: './admin-area.component.html',
  styleUrls: ['./admin-area.component.css']
})
export class AdminAreaComponent implements OnInit {

  access: boolean;

  constructor(public auth: AuthService, private route: Router, private alert: AlertService) {
  }

  ngOnInit() {
    if (!this.auth.isAdmin()) {
      this.alert.error('Only admins can access this area!');
      this.access = false;
    } else {
      this.access = true;
    }
  }

  canSeeOrders(): boolean {
    return this.auth.hasPermission(PermissionType.LIST_ORDERS, false);
  }

  canSeeClients() {
    return this.auth.hasPermission(PermissionType.LIST_CLIENTS, false, true);
  }
}
