import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminAreaComponent} from './admin-area.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../services/order.service';
import {MockOrderService} from '../../services/mocks/mock-order.service';
import {AuthService} from '../../services/auth.service';
import {AdminMockAuthService} from '../../services/mocks/admin-mock-auth.service';

describe('AdminAreaComponent', () => {
  let component: AdminAreaComponent;
  let fixture: ComponentFixture<AdminAreaComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdminAreaComponent],
      imports: [RouterTestingModule],
      providers: [{provide: OrderService, useClass: MockOrderService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(AdminAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have products button', () => {
    const button = element.getElementsByClassName('button').item(0).textContent;
    expect(button).toEqual('Products Menu');
  });
  it('should have operations button', () => {
    const button = element.getElementsByClassName('button').item(1).textContent;
    expect(button).toEqual('Operations Menu');
  });
  it('should have machine types button', () => {
    const button = element.getElementsByClassName('button').item(2).textContent;
    expect(button).toEqual('Machine Types Menu');
  });
  it('should have machines button', () => {
    const button = element.getElementsByClassName('button').item(3).textContent;
    expect(button).toEqual('Machines Menu');
  });
  it('should have production lines button', () => {
    const button = element.getElementsByClassName('button').item(4).textContent;
    expect(button).toEqual('Production Lines Menu');
  });
  it('should have Execution Visualization button', () => {
    const button = element.getElementsByClassName('button').item(5).textContent;
    expect(button).toEqual('Execution Visualization');
  });
  it('should have orders button', () => {
    const button = element.getElementsByClassName('button').item(6).textContent;
    expect(button).toEqual('Production Planning');
  });
  it('should have clients button', () => {
    const button = element.getElementsByClassName('button').item(7).textContent;
    expect(button).toEqual('Orders Menu');
  });
  it('should have clients button', () => {
    const button = element.getElementsByClassName('button').item(8).textContent;
    expect(button).toEqual('Clients Menu');
  });
});
