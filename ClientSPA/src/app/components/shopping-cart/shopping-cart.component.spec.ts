import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ShoppingCartComponent} from './shopping-cart.component';
import {AuthService} from '../../services/auth.service';
import {AdminMockAuthService} from '../../services/mocks/admin-mock-auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StoreService} from '../../services/store.service';
import {MockStoreService} from '../../services/mocks/mock-store.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('ShoppingCartComponent', () => {
  let component: ShoppingCartComponent;
  let fixture: ComponentFixture<ShoppingCartComponent>;
  let element: HTMLElement;

  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ShoppingCartComponent],
      imports: [FormsModule, ReactiveFormsModule, RouterTestingModule],
      providers: [{provide: AuthService, useClass: AdminMockAuthService},
        {provide: StoreService, useClass: MockStoreService}]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'My Shopping Cart';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });

  it('should have all items', () => {
    const expected = 2;
    const num = element.getElementsByClassName('order').length;
    expect(expected).toBe(num);
  });

  it('should have correct total price', () => {
    const priceTotal = 'Total Price = 975';
    const num = element.getElementsByClassName('totalPrice').item(0).textContent;
    expect(priceTotal).toBe(num);
  });

});
