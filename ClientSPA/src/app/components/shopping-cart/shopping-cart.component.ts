import {Component, OnInit} from '@angular/core';
import {StoreService} from '../../services/store.service';
import {LogService} from '../../services/log.service';
import {ProductForOrder} from '../../entities/order';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {PermissionType} from '../../authentication/PermissionType';
import {AlertService} from '../../services/alert.service';
import {logger} from 'codelyzer/util/logger';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  products: ProductForOrder[];
  totalPrice: number;
  creditCard = new FormControl('', [Validators.required, Validators.pattern(/^PT50[0-9]{16}$/)]);
  shippingAddress = new FormControl('', [Validators.required, Validators.minLength(5)]);
  access: boolean;

  constructor(private store: StoreService,
              private logService: LogService,
              private route: Router,
              private auth: AuthService,
              private alert: AlertService) {
  }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.access = false;
      this.alert.error('You must be logged in.');
    } else {
      if (!this.auth.hasPermission(PermissionType.CREATE_ORDER, false, false)) {
        this.access = false;
        this.alert.error('Not allowed');
      } else {
        this.products = this.store.getProductsToBeOrdered();
        this.totalPrice = this.calculateTotalPrice();
        this.access = true;
      }
    }
  }

  removeProduct(name: string) {
    this.store.removeProductFromOrder(name);
    this.ngOnInit();
  }

  calculateTotalPrice() {
    let value = 0;

    for (const product of this.products) {
      value = value + product.quantity * product.price;
    }
    return value;
  }

  onSubmit() {
    this.store.fillOrderDetails(this.shippingAddress.value, this.creditCard.value);
    this.store.executeOrder().subscribe(value => {
      this.store.clearShoppingCart();
      this.shippingAddress.reset();
      this.creditCard.reset();
      this.route.navigateByUrl('/store').then().catch(err => {
        logger.error(err);
      });
      alert('Your order has been placed, and can now be consulted on My Orders!');
    }, error => {
      alert('Your order has not been placed, please try again.');
    });
  }

  private log(message: string) {
    this.logService.log(`Shopping Cart component: ${message}`);
  }


}
