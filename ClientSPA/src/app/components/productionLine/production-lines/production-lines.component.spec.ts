import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductionLinesComponent} from './production-lines.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductionLineService} from '../../../services/production-line.service';
import {MockProductionLineService} from '../../../services/mocks/mock-production-line.service';
import {MachineAssociated, ProductionLine} from '../../../entities/production-line';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';

describe('ProductionLinesComponent', () => {
  let component: ProductionLinesComponent;
  let fixture: ComponentFixture<ProductionLinesComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductionLinesComponent],
      imports: [RouterTestingModule],
      providers: [{provide: ProductionLineService, useClass: MockProductionLineService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ProductionLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should list all production lines', () => {
    const productionLines = element.getElementsByClassName('production-line');
    const productionLine1 = new ProductionLine('123', [new MachineAssociated('machine80', 1),
      new MachineAssociated('machine30', 2)]);
    const productionLine2 = new ProductionLine('321', [new MachineAssociated('machine21', 1),
      new MachineAssociated('machine22', 2)]);
    const expectedProductionLines: ProductionLine[] = [productionLine1, productionLine2];
    expect(productionLines.item(0).textContent).toBe(productionLine1.productionLineIdentifier);
    expect(productionLines.item(1).textContent).toBe(productionLine2.productionLineIdentifier);
    expect(component.productionLines).toEqual(expectedProductionLines);
  });
  it('should have title production lines', () => {
    const expected = 'Production Lines';
    const result = element.getElementsByClassName('h2').item(0).textContent;
    expect(expected).toBe(result);
  });
  it('should have new production line button', () => {
    const expected = '+ New Production Line';
    const result = element.getElementsByClassName('new').item(0).textContent;
    expect(expected).toBe(result);
  });
});
