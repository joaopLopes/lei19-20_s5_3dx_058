import {Component, OnInit} from '@angular/core';
import {ProductionLine} from '../../../entities/production-line';
import {ProductionLineService} from '../../../services/production-line.service';
import {AuthService} from '../../../services/auth.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-production-lines',
  templateUrl: './production-lines.component.html',
  styleUrls: ['./production-lines.component.css']
})

export class ProductionLinesComponent implements OnInit {

  productionLines: ProductionLine[];
  access: boolean;

  constructor(private prodLineService: ProductionLineService,
              private auth: AuthService,
              private alertService: AlertService) {
  }

  /**
   * Initializes the UI with the production lines in database.
   */
  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.access = true;
      this.getProductionLines();
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  /**
   * Gets the production lines from database.
   */
  getProductionLines(): void {
    this.prodLineService.getProductionLines().subscribe(productionLines => this.productionLines = productionLines);
  }

}
