import {Component, OnInit} from '@angular/core';
import {MachineAssociated, ProductionLine} from '../../../entities/production-line';
import {ProductionLineService} from '../../../services/production-line.service';
import {AlertService} from '../../../services/alert.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-production-line-form',
  templateUrl: './production-line-form.component.html',
  styleUrls: ['./production-line-form.component.css']
})
export class ProductionLineFormComponent implements OnInit {

  chosenMachines: string[] = Array();
  machines: string[];
  productionLineIdentifier = new FormControl('', [Validators.required, Validators.pattern(/[0-9][0-9][0-9]/)]);
  access: boolean;

  constructor(private prodLineService: ProductionLineService,
              private alertService: AlertService,
              private auth: AuthService) {
  }

  ngOnInit() {
    if (this.auth.isAdmin()) {
      this.getAvailableMachines();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  /**
   * Fills on real time the check drop list with the available machines to add to a production line
   */
  getAvailableMachines(): void {
    this.prodLineService.getAvailableMachines().subscribe(machines => this.machines = machines);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  onSubmit() {
    const associatedMachines: MachineAssociated[] = [];
    let slot = 0;
    this.chosenMachines.forEach((machineId) => {
      associatedMachines.push(new MachineAssociated(machineId, slot));
      slot++;
    });
    const newProductionLine = new ProductionLine(this.productionLineIdentifier.value, associatedMachines);
    this.prodLineService.addProductionLine(newProductionLine).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
      this.getAvailableMachines();
    }, () => {
      this.alertService.info('Production Line has been created.');
    });
    // clean data
    this.productionLineIdentifier.reset();
    // deletes all operations from list
    this.chosenMachines.splice(0, this.chosenMachines.length);
  }

}
