import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductionLineFormComponent} from './production-line-form.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductionLineService} from '../../../services/production-line.service';
import {MockProductionLineService} from '../../../services/mocks/mock-production-line.service';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';


describe('ProductionLineFormComponent', () => {
  let component: ProductionLineFormComponent;
  let fixture: ComponentFixture<ProductionLineFormComponent>;
  let element: HTMLElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductionLineFormComponent],
      imports: [RouterTestingModule, DragDropModule, ReactiveFormsModule],
      providers: [{provide: ProductionLineService, useClass: MockProductionLineService},
        {provide: AuthService, useClass: AdminMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ProductionLineFormComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionLineFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have correct Title', () => {
    const expected = 'New Production Line';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have name invalid when empty', () => {
    expect(component.productionLineIdentifier.valid).toBeFalsy();
  });
  it('name field to be invalid if name is empty', () => {
    const form = component.productionLineIdentifier;
    form.setValue('');
    expect(form.valid).toBeFalsy();
  });
  it('name field to be invalid if name has spaces', () => {
    const form = component.productionLineIdentifier;
    form.setValue('A A A');
    expect(form.valid).toBeFalsy();
  });
  it('name field to be valid if is within rules', () => {
    const form = component.productionLineIdentifier;
    form.setValue('123');
    expect(form.valid).toBeTruthy();
  });
  it('name field to be valid if is within rules', () => {
    const form = component.productionLineIdentifier;
    form.setValue('111');
    expect(form.valid).toBeTruthy();
  });
  it('should list all available machines in the system', () => {
    const machines = element.getElementsByClassName('example-box');
    const machine1 = 'machine10';
    const machine2 = 'machine23';
    const machine3 = 'machine59';
    const expectedMachines: string[] = ['machine10', 'machine23', 'machine59'];
    expect(machines.item(0).textContent).toBe(machine1);
    expect(machines.item(1).textContent).toBe(machine2);
    expect(machines.item(2).textContent).toBe(machine3);
    expect(component.machines).toEqual(expectedMachines);
  });
  it('chosen list should be empty when page loads', () => {
    expect(component.chosenMachines.length).toBe(0);
  });
  it('operations list should have all available operations when page loads', () => {
    expect(component.machines.length).toBe(3);
  });

});
