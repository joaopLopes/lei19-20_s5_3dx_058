import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductionLineDetailComponent} from './production-line-detail.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ProductionLineService} from '../../../services/production-line.service';
import {MockProductionLineService} from '../../../services/mocks/mock-production-line.service';
import {MockMachineService} from '../../../services/mocks/mock-machine.service';
import {MachineService} from '../../../services/machine.service';
import {AuthService} from '../../../services/auth.service';
import {AdminMockAuthService} from '../../../services/mocks/admin-mock-auth.service';
import {ProductionPlanningService} from '../../../services/production-planning.service';
import {MockProductionPlanningService} from '../../../services/mocks/mock-production-planning.service';

describe('ProductionLineDetailComponent', () => {
  let component: ProductionLineDetailComponent;
  let fixture: ComponentFixture<ProductionLineDetailComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductionLineDetailComponent],
      imports: [RouterTestingModule],
      providers: [{provide: ProductionLineService, useClass: MockProductionLineService}, {
        provide: MachineService,
        useClass: MockMachineService
      },
        {provide: AuthService, useClass: AdminMockAuthService},
        {provide: ProductionPlanningService, useClass: MockProductionPlanningService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ProductionLineDetailComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionLineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct Title', () => {
    const expected = 'Production Line 321 - Details';
    const result = element.getElementsByClassName('title').item(0).textContent;
    expect(result).toBe(expected);
  });
  it('should have button to show machines', () => {
    const machines = element.getElementsByClassName('word').item(0);
    expect(machines.textContent).toEqual('Associated Machines: Show');
  });
  it('should have all machines', () => {
    const items = element.getElementsByClassName('dropdown-content');
    const list = items.item(0);
    const machine1 = list.getElementsByClassName('word').item(0).textContent;
    const machine2 = list.getElementsByClassName('word').item(1).textContent;
    expect(machine1).toEqual(' machine21 ');
    expect(machine2).toEqual(' machine22 ');
  });
});
