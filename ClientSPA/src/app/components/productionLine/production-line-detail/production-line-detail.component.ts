import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductionLineService} from '../../../services/production-line.service';
import {ProductionLine} from '../../../entities/production-line';
import {AlertService} from '../../../services/alert.service';
import {MachineService} from '../../../services/machine.service';
import {AuthService} from '../../../services/auth.service';
import {ProductionPlanningService} from '../../../services/production-planning.service';

@Component({
  selector: 'app-production-line-detail',
  templateUrl: './production-line-detail.component.html',
  styleUrls: ['./production-line-detail.component.css']
})
export class ProductionLineDetailComponent implements OnInit {
  @Input() productionLine: ProductionLine;
  access: boolean;

  constructor(private route: ActivatedRoute,
              private productionLineService: ProductionLineService,
              private alertService: AlertService,
              private machineService: MachineService,
              private auth: AuthService,
              private prologService: ProductionPlanningService) {

  }

  ngOnInit(): void {
    if (this.auth.isAdmin()) {
      this.getProductionLine();
      this.access = true;
    } else {
      this.access = false;
      this.alertService.error('Not allowed');
    }
  }

  getProductionLine(): void {
    const name = this.route.snapshot.paramMap.get('productionLineIdentifier');
    this.productionLineService.getProductionLine(name)
      .subscribe(productionLine => this.productionLine = productionLine);
  }

  /* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
  myFunction(): void {
    document.getElementById('myDropdown').classList.toggle('show');
    const doc = document.getElementById('toggleButton');
    if (doc.innerText === 'Show') {
      doc.innerText = 'Hide';
    } else {
      doc.innerText = 'Show';
    }
  }


  /**
   * Method that allows the user to activate a machine.
   */
  activateMachine(machineSerialNumber: string) {
    this.machineService.activateMachine(machineSerialNumber).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.prologService.activateMachine(machineSerialNumber);
      this.alertService.info('Machine has been activated.');
    });

  }


  /**
   * Method that allows the user to deactivate a machine.
   */
  deactivateMachine(machineSerialNumber: string) {
    this.machineService.deactivateMachine(machineSerialNumber).subscribe(next => {
      // do nothing
    }, error => {
      this.alertService.error(error);
    }, () => {
      this.prologService.deactivateMachine(machineSerialNumber);
      this.alertService.info('Machine has been deactivated.');
    });

  }

}
