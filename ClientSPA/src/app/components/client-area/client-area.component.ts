import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {AuthService} from '../../services/auth.service';
import {PermissionType} from '../../authentication/PermissionType';

@Component({
  selector: 'app-client-area',
  templateUrl: './client-area.component.html',
  styleUrls: ['./client-area.component.css']
})
export class ClientAreaComponent implements OnInit {
  username: any;
  access: boolean;

  constructor(private auth: AuthService,
              private route: Router,
              private alert: AlertService) {
  }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.access = false;
      this.alert.error('Please Log In first.');
    } else {
      if (!this.auth.isClient()) {
        this.access = false;
        this.alert.error('You must be a regular client to access this area!');
      } else {
        this.access = true;
        this.username = this.auth.getLoggedUser();
      }
    }
  }

  canVisitProfile(): boolean {
    return this.auth.hasPermission(PermissionType.CONSULT_CLIENT, true, false);
  }

  canSeeOrders(): boolean {
    return this.auth.hasPermission(PermissionType.LIST_ORDERS, true, false);
  }
}
