import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ClientAreaComponent} from './client-area.component';
import {CookieService} from 'ngx-cookie-service';
import {RouterTestingModule} from '@angular/router/testing';
import {OrderService} from '../../services/order.service';
import {MockOrderService} from '../../services/mocks/mock-order.service';
import {AuthService} from '../../services/auth.service';
import {ClientMockAuthService} from '../../services/mocks/client-mock-auth.service';

describe('ClientAreaComponent', () => {
  let component: ClientAreaComponent;
  let fixture: ComponentFixture<ClientAreaComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClientAreaComponent],
      imports: [RouterTestingModule],
      providers: [{provide: OrderService, useClass: MockOrderService},
        {provide: AuthService, useClass: ClientMockAuthService}]
    })
      .compileComponents();
    fixture = TestBed.createComponent(ClientAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have profile button', () => {
    const button = element.getElementsByClassName('button').item(0).textContent;
    expect(button).toEqual('My Profile');
  });
  it('should have orders button', () => {
    const button = element.getElementsByClassName('button').item(1).textContent;
    expect(button).toEqual('My Orders');
  });
});
