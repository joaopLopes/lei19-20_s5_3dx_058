export const environment = {
  production: true,
  MDF: 'https://master-data-factory-3dx1.azurewebsites.net/mdf',
  MDP: 'https://master-data-production-3dx1.azurewebsites.net/mdp',
  GE: 'https://order-management-58.herokuapp.com',
  PROLOG: 'https://production-planning.herokuapp.com',
  GE_DOMAIN: 'order-management-58.herokuapp.com'
};
