import * as THREE from 'three';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {
  scene,
  machineRotationY,
  machineSeparationDistance,
  machineStartingX,
  machineStartingZ,
  machineTextOffsetX,
  machineTextOffsetY,
  machineTextOffsetZ,
  productionLineSeparationDistance,
  factoryFloorModel,
  machineModel,
  conveyorBeltModel,
  spoonModel,
  fontLoader,
  machines,
  conveyorBelts,
  products,
  conveyorBeltOffsetX,
  conveyorBeltOffsetZ,
  productOnMachineLeftSlotOffsetX,
  productOnMachineOffsetZ,
  productOnMachineOffsetY,
  instantiateMachineModel,
  instantiateConveyorBeltModel,
  instantiateSpoonModel,
  instantiateFactoryFloorModel,
  addMachineToMachinesArray,
  addMachinePosition,
  addConveyorBeltToConveyorBeltsArray,
  addProductToProductsArray,
  findModelChildrenIndexByName,
  productionLines,
  instantiateFontLoader,
  checkMachinePositionIsAvailable,
  boxModel,
  instantiateBoxModel,
  addBoxToBoxesArray,
  boxOffsetZ,
  instantiateSphereModel,
  sphereModel, machineOcupationNameOffsetY
} from './globalVariables';
//import { manageProductThroughProductionLine} from './animations';


//imports the 3d factory floor model
export function factoryFloorModelImporter() {

  if (factoryFloorModel === null) {

    // Instantiate a loader
    var loader = new GLTFLoader();

    loader.load('assets/3d/models/factory_floor_model_simple.glb',

      // called when the resource is loaded
      function handle_gltf_load(gltf) {

        let factoryFloor = gltf.scene;

        factoryFloor.name = 'factoryFloor';

        instantiateFactoryFloorModel(factoryFloor);

        console.log('The factory floor model has been loaded');

      },
      // called while loading is progressing
      function (xhr) {
      },
      // called when loading has errors
      function (error) {

        console.log('An error happened loading the factory floor model');

      }
    );
  } else {
    let factoryFloor = factoryFloorModel.clone();

    factoryFloor.traverse(function (node) {
      if (node instanceof THREE.Mesh || node instanceof THREE.Object3D) {
        node.receiveShadow = true;
      }
    });

    factoryFloor.receiveShadow = true;
    factoryFloor.castShadow = true;
    scene.add(factoryFloor);
  }
}

//creates a 2d sprite of a machine's name with a border around it
export function machineSerialNumberDisplayLoader(machineScene, machineSerialNumber, productionLineIndex, machineIndex) {

  if (fontLoader === null) {

    var loader = new THREE.FontLoader();
    loader.load('assets/3d/fonts/helvetiker_regular.typeface.json',

      function (font) {

        instantiateFontLoader(font);
      });
  } else {

    var text;
    var fontColor = new THREE.Color(0xffffff);
    var borderColor = new THREE.Color(0x000000);

    var matLite = new THREE.MeshBasicMaterial({
      color: fontColor,
      transparent: false,
      opacity: 1,
      side: THREE.DoubleSide
    });

    var shapes = fontLoader.generateShapes(machineSerialNumber, 0.5, 0);
    var geometry = new THREE.ShapeBufferGeometry(shapes);

    text = new THREE.Mesh(geometry, matLite);

    //text.rotation.y = machineRotationY;

    text.position.x -= machineTextOffsetX;
    // text.position.z = machineStartingZ - (machineSeparationDistance * machineIndex) + machineTextOffsetZ;
    text.position.y = machineTextOffsetY;
    machineScene.add(text);

    let machineSerialNumberBorderBox = new THREE.BoxHelper(text, borderColor);
    machineSerialNumberBorderBox.name = machineSerialNumber;
    //scene.add(machineSerialNumberBorderBox);
    machineScene.add(machineSerialNumberBorderBox);
  }


}

//creates a 2d sprite of a machine's ongoing ocupation
export function machineOcupationNameDisplayLoader(machineScene, machineSerialNumber, isSettingUp, isProcessing, operationName, productName) {

  if (fontLoader === null) {

    var loader = new THREE.FontLoader();
    loader.load('assets/3d/fonts/helvetiker_regular.typeface.json',

      function (font) {

        instantiateFontLoader(font);
      });
  } else {

    var text;
    var fontColor = new THREE.Color(0xffffff);
    var borderColor = new THREE.Color(0x000000);

    var matLite = new THREE.MeshBasicMaterial({
      color: fontColor,
      transparent: false,
      opacity: 1,
      side: THREE.DoubleSide
    });

    var message;

    if(isProcessing){
      message = 'Machine ' + machineSerialNumber + ' is processing \n operation ' + operationName + ' for product ' + productName;
    }
    else{
      if(isSettingUp){
        message = 'Machine ' + machineSerialNumber + ' is setting up \n operation ' + operationName + ' for product ' + productName;
      }
    }

    var shapes = fontLoader.generateShapes(message, 0.3, 0);
    var geometry = new THREE.ShapeBufferGeometry(shapes);

    text = new THREE.Mesh(geometry, matLite);

    //text.rotation.y = machineRotationY;

    text.position.x -= machineTextOffsetX;
    // text.position.z = machineStartingZ - (machineSeparationDistance * machineIndex) + machineTextOffsetZ;
    text.position.y = machineTextOffsetY + machineOcupationNameOffsetY;
    text.name = productName + ';' + operationName;
    machineScene.add(text);
  }


}

//imports the 3d machine model
export function machineModelImporter(productionLineIndex, machineIndex, machineInfo) {

  if (machineModel === null) {

    // Instantiate a loader
    var machineLoader = new GLTFLoader();


    machineLoader.load('assets/3d/models/machine_model_with_textures.glb',

      // called when the resource is loaded
      function handle_gltf_load(gltf) {

        let machine = gltf.scene; // THREE.Scene

        machine.traverse(function (node) {
          if (node instanceof THREE.Mesh || node instanceof THREE.Object3D) {
            node.castShadow = true;
          }
        });
        //
        //  machine.rotation.y = machineRotationY;
        //  machine.position.x = machineStartingX + (productionLineSeparationDistance * productionLineIndex);
        //  machine.position.z = machineStartingZ - (machineSeparationDistance * machineIndex);
        //
        //  machine.name = 'machine' + productionLineIndex + '.' + machineIndex;
        //
        //  scene.add(machine);
        //
        // // machines.add(machine);

        // machineModel = machine;

        instantiateMachineModel(machine);

        console.log('Machine model has been loaded');


        // console.log(dumpObject(machine).join('\n'));
        //
        // function dumpObject(obj, lines = [], isLast = true, prefix = '') {
        //   const localPrefix = isLast ? '└─' : '├─';
        //   lines.push(`${prefix}${prefix ? localPrefix : ''}${obj.name || '*no-name*'} [${obj.type}]`);
        //   const newPrefix = prefix + (isLast ? '  ' : '│ ');
        //   const lastNdx = obj.children.length - 1;
        //   obj.children.forEach((child, ndx) => {
        //     const isLast = ndx === lastNdx;
        //     dumpObject(child, lines, isLast, newPrefix);
        //   });
        //   return lines;
        // }

      },
      // called while loading is progressing
      function (xhr) {

      },
      // called when loading has errors
      function (error) {

        console.log('An error happened loading the machine model');

      }
    );
  } else {

    let machine = machineModel.clone();

    let aux = findModelChildrenIndexByName(machine.children, 'Button_Off');
    machine.children[aux].material = machineModel.children[aux].material.clone();
    //machine.children[aux].material.color = new THREE.Color(0.21404114365577698, 0.21404114365577698, 0.21404114365577698);

    aux = findModelChildrenIndexByName(machine.children, 'Button_On');
    machine.children[aux].material = machineModel.children[aux].material.clone();
    //machine.children[aux].material.color = new THREE.Color(0.21404114365577698, 0.21404114365577698, 0.21404114365577698);

    machine.rotation.y = machineRotationY;
    machine.position.x = machineStartingX + (productionLineSeparationDistance * productionLineIndex);
    machine.position.z = machineStartingZ - (machineSeparationDistance * machineIndex);


    machine.castShadow = true;
    machine.receiveShadow = true;

    let index = findModelChildrenIndexByName(machine.children, 'Plane');

    machine.children[index].visible = false;

    let machineAux = 'machine' + productionLineIndex + '.' + machineIndex;
    machineInfo.slot = machineIndex;

    machine.name = machineAux;
    machine.productionLineIndex = productionLineIndex;
    machine.slot = machineInfo.slot;
    machine.serialNumber = machineInfo.machineSerialNumber;

    if (!checkMachinePositionIsAvailable(productionLineIndex, machineIndex)) {
      throw 'The requested machine position is already taken';
    }

    //machineNameDisplayLoader(machineAux, productionLines.indexOf(productionLine), productionLine.listAssociatedMachinesDto.length - 1);
    machineSerialNumberDisplayLoader(machine, machineInfo.machineSerialNumber, productionLineIndex, machineIndex);

    scene.add(machine);

    addMachineToMachinesArray(machine);
    addMachinePosition(productionLineIndex, machineIndex);
  }
}


//imports the 3d conveyor belt model
export function conveyorBeltModelImporter(productionLineIndex, machineIndex) {

  if (conveyorBeltModel === null) {


    // Instantiate a loader
    var conveyorBeltLoader = new GLTFLoader();


    conveyorBeltLoader.load('assets/3d/models/conv-belt_final.glb',

      // called when the resource is loaded
      function handle_gltf_load(gltf) {

        let convBelt = gltf.scene; // THREE.Scene

        convBelt.traverse(function (node) {
          if (node instanceof THREE.Mesh || node instanceof THREE.Object3D) {
            node.castShadow = true;
          }
        });

        // convBelt.rotation.y = machineRotationY;
        //
        // convBelt.position.x = machineStartingX + (productionLineSeparationDistance * productionLineIndex) + conveyorBeltOffsetX;
        // convBelt.position.z = machineStartingZ - (machineSeparationDistance * machineIndex) - conveyorBeltOffsetZ;
        //
        // convBelt.name = 'convBelt' + productionLineIndex + '.' + machineIndex;

        //scene.add(convBelt);

        //conveyorBelts.add(convBelt);

        //conveyorBeltModel = convBelt;

        instantiateConveyorBeltModel(convBelt);

        console.log('Conveyor belt model has been loaded');


        // console.log(dumpObject(convBelt).join('\n'));
        //
        // function dumpObject(obj, lines = [], isLast = true, prefix = '') {
        //   const localPrefix = isLast ? '└─' : '├─';
        //   lines.push(`${prefix}${prefix ? localPrefix : ''}${obj.name || '*no-name*'} [${obj.type}]`);
        //   const newPrefix = prefix + (isLast ? '  ' : '│ ');
        //   const lastNdx = obj.children.length - 1;
        //   obj.children.forEach((child, ndx) => {
        //     const isLast = ndx === lastNdx;
        //     dumpObject(child, lines, isLast, newPrefix);
        //   });
        //   return lines;
        // }

      },
      // called while loading is progressing
      function (xhr) {


      },
      // called when loading has errors
      function (error) {

        console.log('An error happened loading the conveyor belt model');

      }
    );
  } else {

    let convBelt = conveyorBeltModel.clone();
    convBelt.rotation.y = machineRotationY;

    convBelt.position.x = machineStartingX + (productionLineSeparationDistance * productionLineIndex) + conveyorBeltOffsetX;
    convBelt.position.z = machineStartingZ - (machineSeparationDistance * machineIndex) - conveyorBeltOffsetZ;


    convBelt.castShadow = true;
    convBelt.receiveShadow = true;

    convBelt.name = 'convBelt' + productionLineIndex + '.' + machineIndex;
    convBelt.productionLineIndex = productionLineIndex;
    convBelt.machineIndex = machineIndex;

    scene.add(convBelt);

    addConveyorBeltToConveyorBeltsArray(convBelt);

    return convBelt;

  }

}


//imports the 3d sphere model
export function sphereModelImporter(machine, animationInfo) {

  if (sphereModel === null) {

    // Instantiate a loader
    var sphereLoader = new GLTFLoader();

    sphereLoader.load('assets/3d/models/sphere.glb',

      // called when the resource is loaded
      function handle_gltf_load(gltf) {

        let sphere = gltf.scene; // THREE.Scene

        instantiateSphereModel(sphere);
        console.log('Sphere model has been loaded');

      },
      // called while loading is progressing
      function (xhr) {

      },
      // called when loading has errors
      function (error) {

        console.log('An error happened loading the sphere model');

      }
    );
  } else {

    let sphere = sphereModel.clone();

    // spoon.position.x = machineStartingX + productionLineSeparationDistance * productionLineIndex + productOnMachineLeftSlotOffsetX;
    // spoon.position.z = machineStartingZ + productOnMachineOffsetZ;

    // spoon.position.x = machineStartingX + productionLineSeparationDistance * animationInfo.productionLineIndex + productOnMachineLeftSlotOffsetX;
    // spoon.position.z = machineStartingZ * machine.slot + productOnMachineOffsetZ;
    // spoon.position.y = productOnMachineOffsetY;

    sphere.name = animationInfo.product;
    sphere.productionLineIdentifier = animationInfo.productionLineIdentifier;

    //scene.add(spoon);

    addProductToProductsArray(sphere);

    // if (machinesProductionLineCount === 1) {
    //   manageProductThroughProductionLine(spoon, productionLineIndex, 0, machinesProductionLineCount, true, true);
    // } else {
    //   manageProductThroughProductionLine(spoon, productionLineIndex, 0, machinesProductionLineCount, false, true);
    // }
    return sphere;

  }
}

//imports the 3d spoon model
export function spoonModelImporter(animationInfo) {

  if (spoonModel === null) {

    // Instantiate a loader
    var spoonLoader = new GLTFLoader();

    spoonLoader.load('assets/3d/models/spoon_as_one_object.glb',

      // called when the resource is loaded
      function handle_gltf_load(gltf) {

        let spoon = gltf.scene; // THREE.Scene

        instantiateSpoonModel(spoon);
        console.log('Spoon model has been loaded');

      },
      // called while loading is progressing
      function (xhr) {

      },
      // called when loading has errors
      function (error) {

        console.log('An error happened loading the spoon model');

      }
    );
  } else {

    let spoon = spoonModel.clone();

    spoon.name = animationInfo.product;
    spoon.productionLineIdentifier = animationInfo.productionLineIdentifier;

    //scene.add(spoon);

    return spoon;
  }
}

//imports the 3d box model and places it in the last conveyor belt of a production line
export function boxModelImporter(conveyorBelt) {

  if (boxModel === null) {


    // Instantiate a loader
    var boxLoader = new GLTFLoader();

    boxLoader.load('assets/3d/models/box.glb',

      // called when the resource is loaded
      function handle_gltf_load(gltf) {

        let box = gltf.scene; // THREE.Scene

        box.traverse(function (node) {
          if (node instanceof THREE.Mesh || node instanceof THREE.Object3D) {
            node.castShadow = true;
          }
        });
        instantiateBoxModel(box);

        console.log('Box model has been loaded');

      },
      // called while loading is progressing
      function (xhr) {

      },
      // called when loading has errors
      function (error) {

        console.log('An error happened loading the box model');

      }
    );
  } else {

    let box = boxModel.clone();

    box.position.x = conveyorBelt.position.x;
    box.position.z = conveyorBelt.position.z - boxOffsetZ;
    box.position.y = conveyorBelt.position.y;

    box.name = 'box ' + conveyorBelt.productionLineIndex;
    box.productionLineIndex = conveyorBelt.productionLineIndex;

    scene.add(box);

    addBoxToBoxesArray(box);

  }

}
