let camera, renderer, cameraControls, dragControls, allMachines, gui, productionLineService;

let updateProductionLineOperationInProgress = false;
let canUpdateNextProductionLine = false;
let canDeleteNextProductionLine = false;
let canCreateNextProductionLine = false;

let machinesHaveBeenShiftedToTheLeft = false;

let unassociatedMachines = [];
let initialProductionLines = [];

let productionLinesToAdd = [];
let productionLinesToUpdate = [];
let productionLinesToDelete = [];

let tempMachineStartPosX;
let tempMachineStartPosZ;

let productionLineRegex = /^([0-9][0-9][0-9])$/;

let currentProductionLinesCount = 0;
let currentProductionLineIndex = 0;
let currentMachineIndex = 0;
let machineProductionLineCount = 0;

import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls.js';
import {DragControls} from "./MyDragControls";
import {GUI} from 'three/examples/jsm/libs/dat.gui.module.js';
import TWEEN from '@tweenjs/tween.js'

import {
  ProductionLine
} from '../../../app/entities/production-line';


import {
  scene,
  initialImportingIsDone,
  indicateInitialImportingIsDone,
  maximumMachinePerProductionLineNumber,
  maximumProductionLineNumber,
  factoryFloorModel,
  machineModel,
  conveyorBeltModel,
  spoonModel,
  addProductionLineToProductionLinesArray,
  productionLines,
  machines,
  products,
  machineStartingZ,
  machineStartingX,
  addMachinePosition,
  productionLineSeparationDistance,
  machineSeparationDistance,
  machinesPositionMatrix,
  deleteMachineFromProductionLineArray,
  addMachineToProductionLineArray,
  findModelChildrenIndexByName,
  fontLoader,
  checkMachinePositionIsAvailable,
  removeMachinePosition,
  instantiateMachinePosition,
  removeConveyorBeltFromScene,
  spliceOutMachineFromProductionLineArray,
  spliceOutProductionLineFromProductionLineArray,
  spliceInMachineIntoProductionLineArray,
  shiftMachineSlotInProductionLinesArrayToTheLeft,
  findProductionLineByIdentifier,
  boxModel,
  removeProductsFromScene,
  sphereModel,
  findBoxByProductionLineIndex,
  findLastConveyorBeltInProductionLine, boxOffsetZ, removeBoxFromScene, findProductionLineByIndex
} from './globalVariables';

import {
  boxModelImporter,
  conveyorBeltModelImporter,
  factoryFloorModelImporter,
  machineModelImporter, machineSerialNumberDisplayLoader,
  // machineNameDisplayLoader,
  sphereModelImporter, spoonModelImporter
} from "./loaders";
import {setupAnimateProductionPlan} from "./animations";

let factoryMenu, productionLineMenu;

function init() {

  var canvas = document.getElementById('canvas');

  camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.5, 500);

  renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});

  const loader = new THREE.CubeTextureLoader();
  scene.background = loader.load([
    'assets/3d/sky/purplenebula_lf.png',
    'assets/3d/sky/purplenebula_rt.png',
    'assets/3d/sky/purplenebula_up.png',
    'assets/3d/sky/purplenebula_dn.png',
    'assets/3d/sky/purplenebula_ft.png',
    'assets/3d/sky/purplenebula_bk.png',
  ]);

  renderer.setSize(window.innerWidth, window.innerHeight);

  renderer.shadowMap.enabled = true;

  //renderer configuration for texture displaying
  renderer.gammaOutput = true;
  renderer.gammaFactor = 2.2;
  // document.body.appendChild(renderer.domElement);

  //scene.background = new THREE.Color('skyblue');

  //lights configuration
  const directionalLight1 = new THREE.DirectionalLight(0xffffff, 1);
  directionalLight1.position.set(50, 10, 10);
  // directionalLight1.target.position.set(-18, 5, 0);
  directionalLight1.castShadow = true;
  directionalLight1.shadow.camera = new THREE.OrthographicCamera(-28, 28, 30, 0, 1, 100);
  scene.add(directionalLight1);
  const cameraHelper = new THREE.CameraHelper(directionalLight1.shadow.camera);
  //scene.add(cameraHelper);

  const directionalLight2 = new THREE.DirectionalLight(0xffffff, 0.6);
  directionalLight2.position.set(40, 40, 10);
  scene.add(directionalLight2);

  const directionalLight3 = new THREE.DirectionalLight(0xffffff, 0.6);
  directionalLight3.position.set(-40, -40, 10);
  scene.add(directionalLight3);

  // const directionalLight4 = new THREE.DirectionalLight(0xffffff, 0.6);
  // directionalLight4.position.set(-40, 40, 10);
  // scene.add(directionalLight4);

  //
  // const directionalLight5 = new THREE.DirectionalLight(0xffffff, 0.6);
  // directionalLight5.position.set(0, 0, 10);
  // scene.add(directionalLight5);

  //camera position + controls
  camera.position.set(30, 35, 0);
  cameraControls = new OrbitControls(camera, renderer.domElement);
  cameraControls.target.set(0, -0.2, -0.2);

  dragControls = new DragControls(machines, camera, renderer.domElement);

  dragControls.addEventListener('dragstart', function (event) {
    cameraControls.enabled = false;
    tempMachineStartPosX = event.object.position.x;
    tempMachineStartPosZ = event.object.position.z;
  });

  dragControls.addEventListener('drag', function (event) {
    event.object.position.y = 0;
    let index = findModelChildrenIndexByName(event.object.children, 'Plane');
    event.object.children[index].visible = true;
  });


  dragControls.addEventListener('dragend', async function (event) {

    let index, newProductionLineIndex, newMachineIndex, emptySpaceAux;

    let tempMachineFinalPosX = event.object.position.x;
    let tempMachineFinalPosZ = event.object.position.z;

    let oldProductionLineIndex = event.object.productionLineIndex;
    let oldMachineIndex = event.object.slot;

    if (tempMachineFinalPosX > tempMachineStartPosX) {
      newProductionLineIndex = oldProductionLineIndex + Math.round((tempMachineFinalPosX - tempMachineStartPosX) / productionLineSeparationDistance);
    } else {
      newProductionLineIndex = oldProductionLineIndex - Math.round((tempMachineStartPosX - tempMachineFinalPosX) / productionLineSeparationDistance);
    }

    if (tempMachineFinalPosZ > tempMachineStartPosZ) {
      newMachineIndex = oldMachineIndex - Math.floor((tempMachineFinalPosZ - tempMachineStartPosZ) / machineSeparationDistance);
    } else {
      newMachineIndex = oldMachineIndex + Math.floor((tempMachineStartPosZ - tempMachineFinalPosZ) / machineSeparationDistance);
    }

    //if the user tries to drag the machine to an non-existing production line, the machine will rest to its original position
    if (productionLines[newProductionLineIndex] === undefined) {
      resetMovedMachine(event.object);
      return;
    }

    let firstAvailableSlot = productionLines[newProductionLineIndex].listAssociatedMachinesDto.length;
    // will check if there are any empty spaces in the production line and in case there is, slides the machine to the production line's next available slot
    if (newMachineIndex > firstAvailableSlot || (newMachineIndex === firstAvailableSlot && oldMachineIndex === firstAvailableSlot - 1)) {

      //if the machine hasn't switched production lines and is dragged to the next available slot, all other machines will slide to the left, in order for the machine that is being edited to be at the most
      //far right position of the production line
      if (oldProductionLineIndex === newProductionLineIndex) {

        //checks if the machine isn't already at the right most position
        if (oldMachineIndex !== firstAvailableSlot - 1) {

          // spliceOutMachineFromProductionLineArray(oldProductionLineIndex, oldMachineIndex);

          //  shiftMachinesInProductionLineToTheLeft(oldProductionLineIndex, oldMachineIndex, firstAvailableSlot, false);

          //will place the machine in the right most position of the production line
          //let aux = firstAvailableSlot-1;
          event.object.position.z += (newMachineIndex - firstAvailableSlot) * machineSeparationDistance;
          newMachineIndex = firstAvailableSlot;

          // updateMachineSceneInfo(event.object, oldProductionLineIndex, newMachineIndex);
          //
          // addMachineToProductionLineArray(event.object.serialNumber, oldProductionLineIndex, newMachineIndex);
          //
          // let firstProductionLine = productionLines[oldProductionLineIndex];
          //
          // saveAnUpdatedProductionLine(firstProductionLine, false);
          //
          // cameraControls.enabled = true;
          // index = findModelChildrenIndexByName(event.object.children, 'Plane');
          // event.object.children[index].visible = false;
          // return;

        }
        //in this case, the selected machine is already the right most machine in the production line, so it will reset to its original position, causing no changes
        else {
          resetMovedMachine(event.object);
          return;
        }
      }
      //the machine has switched production lines and will be positioned in the right most position of its new production line
      else {
        //firstAvailableSlot = productionLines[newProductionLineIndex].listAssociatedMachinesDto.length;
        //emptySpaceAux = firstAvailableSlot;
        event.object.position.z += (newMachineIndex - firstAvailableSlot) * machineSeparationDistance;
        newMachineIndex = firstAvailableSlot;
      }
    }


    //checks the machine has indeed changed position
    if (newProductionLineIndex !== oldProductionLineIndex || newMachineIndex !== oldMachineIndex) {

      //checks if the new machine's position is available
      if (!checkMachinePositionIsAvailable(newProductionLineIndex, newMachineIndex)) {

        if (confirm("The new machine's position is already in use. Do you wish to switch one machine's position with the other?")) {

          let secondMachine = findMachineByProductionLineAndMachineIndexes(newProductionLineIndex, newMachineIndex);

          let firstProductionLine = productionLines[oldProductionLineIndex];
          let secondProductionLine = productionLines[newProductionLineIndex];


          // first it is necessary to remove both machines form their respective production lines

          spliceOutMachineFromProductionLineArray(oldProductionLineIndex, oldMachineIndex);
          spliceOutMachineFromProductionLineArray(newProductionLineIndex, newMachineIndex);

          //the machine hasn't switched production lines
          if (oldProductionLineIndex === newProductionLineIndex) {
            let listAux = copyProductionLineMachineList(firstProductionLine);
            let productionLineAux = buildProductionLineContainer(firstProductionLine.productionLineIdentifier, listAux);
            productionLinesToUpdate.push(productionLineAux);
            saveAnUpdatedProductionLine(false, false, false);
          }
          //the machine has switched production lines
          else {
            let listAux = copyProductionLineMachineList(firstProductionLine);
            let productionLineAux = buildProductionLineContainer(firstProductionLine.productionLineIdentifier, listAux);
            productionLinesToUpdate.push(productionLineAux);

            let listAux2 = copyProductionLineMachineList(secondProductionLine);
            let productionLineAux2 = buildProductionLineContainer(secondProductionLine.productionLineIdentifier, listAux2);
            productionLinesToUpdate.push(productionLineAux2);
            saveAnUpdatedProductionLine(true, false, false);
            checkCanUpdateNextProductionLine(false, false);
          }

          spliceInMachineIntoProductionLineArray(event.object.serialNumber, newProductionLineIndex, newMachineIndex);
          spliceInMachineIntoProductionLineArray(secondMachine.serialNumber, oldProductionLineIndex, oldMachineIndex);

          secondMachine.position.x = tempMachineStartPosX;
          secondMachine.position.z = tempMachineStartPosZ;

          updateMachineSceneInfo(event.object, newProductionLineIndex, newMachineIndex);
          updateMachineSceneInfo(secondMachine, oldProductionLineIndex, oldMachineIndex);

          firstProductionLine = productionLines[oldProductionLineIndex];


          //now the production line(s) will be updated with the machines's new positions

          //the machine hasn't switched production lines
          if (oldProductionLineIndex === newProductionLineIndex) {
            let listAux = copyProductionLineMachineList(firstProductionLine);
            let productionLineAux = buildProductionLineContainer(firstProductionLine.productionLineIdentifier, listAux);
            productionLinesToUpdate.push(productionLineAux);
            checkCanUpdateNextProductionLine(false, false);
          }
          //the machine has switched production lines
          else {
            let listAux = copyProductionLineMachineList(firstProductionLine);
            let productionLineAux = buildProductionLineContainer(firstProductionLine.productionLineIdentifier, listAux);
            productionLinesToUpdate.push(productionLineAux);
            checkCanUpdateNextProductionLine(false, true);

            let listAux2 = copyProductionLineMachineList(secondProductionLine);
            let productionLineAux2 = buildProductionLineContainer(secondProductionLine.productionLineIdentifier, listAux2);
            productionLinesToUpdate.push(productionLineAux2);
            checkCanUpdateNextProductionLine(false, false);
          }


        }
        //if the user cancels the switch machines operation, the machine returns to its original position
        else {
          //event.object.position.x = tempMachineStartPosX;
          //event.object.position.z = tempMachineStartPosZ;
          resetMovedMachine(event.object);
        }
      }

      //the machine's new position is available
      else {
        removeMachinePosition(oldProductionLineIndex, oldMachineIndex);
        addMachinePosition(newProductionLineIndex, newMachineIndex);

        let oldProductionLineMachinesSize = productionLines[oldProductionLineIndex].listAssociatedMachinesDto.length;

        //the machine wasn't in the right most position in its original production line, and so, the following machines must be shifted to the left
        if (oldMachineIndex !== oldProductionLineMachinesSize - 1) {
          spliceOutMachineFromProductionLineArray(oldProductionLineIndex, oldMachineIndex);


          if (oldProductionLineIndex === newProductionLineIndex) {
            removeMachinePosition(newProductionLineIndex, newMachineIndex);
            newMachineIndex--;
            removeConveyorBeltFromScene(newProductionLineIndex, newMachineIndex);
            shiftMachinesInProductionLineToTheLeft(oldProductionLineIndex, oldMachineIndex, oldProductionLineMachinesSize, false);
            event.object.position.z += machineSeparationDistance;
            machinesHaveBeenShiftedToTheLeft = true;
          } else {
            shiftMachinesInProductionLineToTheLeft(oldProductionLineIndex, oldMachineIndex, oldProductionLineMachinesSize, true);
            machinesHaveBeenShiftedToTheLeft = true;
          }

          //the machine was in the right most position in its original production line, so no machines need to be shifted
        } else {
          spliceOutMachineFromProductionLineArray(oldProductionLineIndex, oldMachineIndex);
          // oldProductionLineMachinesSize = productionLines[oldProductionLineIndex].listAssociatedMachinesDto.length;
          // //if the production line is left with no machines, the production line is deleted from the database
          // if (oldProductionLineMachinesSize === 0) {
          //   let secondProductionLine = productionLines[newProductionLineIndex];
          //   await deleteProductionLine(secondProductionLine);
          // }
        }

        updateMachineSceneInfo(event.object, newProductionLineIndex, newMachineIndex);
        addMachineToProductionLineArray(event.object.serialNumber, newProductionLineIndex, newMachineIndex);

        let firstProductionLine = productionLines[oldProductionLineIndex];
        let secondProductionLine = productionLines[newProductionLineIndex];

        if (!productionLinesToAdd.includes(secondProductionLine)) {
          conveyorBeltModelImporter(newProductionLineIndex, newMachineIndex);
        }


        //the machine hasn't switched production lines
        if (oldProductionLineIndex === newProductionLineIndex) {
          productionLinesToUpdate.push(firstProductionLine);
          saveAnUpdatedProductionLine(false, false);
        }

        //the machine has switched production lines
        else {

          let secondProductionLine = productionLines[newProductionLineIndex];

          //the second production line is a newly created one in the execution visualizer and hasn't had new machines added to it yet
          if (productionLinesToAdd.includes(secondProductionLine)) {
            productionLinesToUpdate.push(firstProductionLine);
            //productionLinesToAdd.push(secondProductionLine);
            saveAnUpdatedProductionLine(false, false, true);
            checkCanCreateNextProductionLine();
          } else {

            oldProductionLineMachinesSize = productionLines[oldProductionLineIndex].listAssociatedMachinesDto.length;

            //if the machine's original production line is left with no machines, the production line is deleted from the database
            if (oldProductionLineMachinesSize === 0) {
              productionLinesToUpdate.push(firstProductionLine);
              productionLinesToDelete.push(firstProductionLine);
              productionLinesToUpdate.push(secondProductionLine);
              saveAnUpdatedProductionLine(false, true, false);
              checkCanDeleteNextProductionLine(true);
              spliceOutProductionLineFromProductionLineArray(oldProductionLineIndex);
              removeBoxFromScene(oldProductionLineIndex);
              currentProductionLinesCount = productionLines.length;
              updateGUIProductionLines();
            }

            //normal update procedures to second production line
            else {
              productionLinesToUpdate.push(firstProductionLine);
              productionLinesToUpdate.push(secondProductionLine);
              saveAnUpdatedProductionLine(true, false, false);
              checkCanUpdateNextProductionLine(false, false);
            }
          }

          if (machinesHaveBeenShiftedToTheLeft === false) {
            removeConveyorBeltFromScene(oldProductionLineIndex, oldMachineIndex);
          }
          //if the machine's original production line is left with no machines, the production line is deleted from the database
          // if (oldProductionLineMachinesSize === 0) {
          //   checkCanDeleteNextProductionLine(firstProductionLine);
          //   spliceOutProductionLineFromProductionLineArray(oldProductionLineIndex);
          //   //deleteProductionLine(secondProductionLine);
          // }
        }
      }
    }
    machinesHaveBeenShiftedToTheLeft = false;
    cameraControls.enabled = true;
    index = findModelChildrenIndexByName(event.object.children, 'Plane');
    event.object.children[index].visible = false;
  });


  factoryFloorModelImporter();
  machineModelImporter();
  conveyorBeltModelImporter();
  sphereModelImporter();
  spoonModelImporter();
  boxModelImporter();
  machineSerialNumberDisplayLoader();
  instantiateMachinePosition();
}


function resetMovedMachine(machine) {
  machine.position.x = tempMachineStartPosX;
  machine.position.z = tempMachineStartPosZ;

  cameraControls.enabled = true;
  let index = findModelChildrenIndexByName(machine.children, 'Plane');
  machine.children[index].visible = false;
}

function shiftMachinesInProductionLineToTheLeft(productionLineIndex, machineIndex, productionLineMachineSize, machineChangedProductionLineFlag) {

  let machineToBeShifted, index;

  addMachinePosition(productionLineIndex, machineIndex);

  for (index = machineIndex + 1; index < productionLineMachineSize; index++) {

    machineToBeShifted = findMachineByProductionLineAndMachineIndexes(productionLineIndex, index);

    // spliceOutMachineFromProductionLineArray(productionLineIndex, index);
    // addMachineToProductionLineArray(machineToBeShifted.serialNumber, productionLineIndex, index - 1);
    updateMachineSceneInfo(machineToBeShifted, productionLineIndex, index - 1);
    shiftMachineSlotInProductionLinesArrayToTheLeft(productionLineIndex, index);

    if (index === productionLineMachineSize - 1 && machineChangedProductionLineFlag === true) {
      removeMachinePosition(productionLineIndex, index);
      //addMachinePosition(productionLineIndex, index - 1);
      removeConveyorBeltFromScene(productionLineIndex, index);
    }
    machineToBeShifted.position.z += machineSeparationDistance;

    //machinesHaveBeenShiftedToTheLeft = false;
  }
}

//gui configuration
function initiateGUI() {

  gui = new GUI({autoPlace: false, width: 350});
  document.getElementById('gui-container').appendChild(gui.domElement);

  const factoryMenuParams = {
    addOneProductionLine: function () {

      if (currentProductionLinesCount === maximumProductionLineNumber) {
        alert("ERROR: The maximum limit permitted for production lines has already been reached");
        return
      }

      loop:
        while (true) {
          const productionLineIdentifier = prompt("Please input the desired production line identifier", "(it must consist of one or more combinations of three numbered sequences)");

          if (productionLineIdentifier === null) {
            break loop;
          } else {

            if (validateProductionLineIdentifier(productionLineIdentifier)) {

              // currentProductionLineIndex++;
              conveyorBeltModelImporter(productionLines.length, 0);

              let listAssociatedMachinesDto = [];

              let productionLineToAdd = buildProductionLineContainer(productionLineIdentifier, listAssociatedMachinesDto);

              // let productionLineToAdd = new Object();
              //
              // //let machineInfo = {machinesSerialNumber: undefined, slot: undefined};
              // //listAssociatedMachinesDto[0] = machineInfo;
              //
              // productionLineToAdd.productionLineIdentifier = productionLineIdentifier;
              // productionLineToAdd.listAssociatedMachinesDto = listAssociatedMachinesDto;

              //let productionLineToAdd = new ProductionLine(productionLineIdentifier, []);

              productionLinesToAdd.push(productionLineToAdd);
              addProductionLineToProductionLinesArray(productionLineToAdd);
              currentProductionLinesCount = productionLines.length;
              alert("Please don't forget to add a machine to the production line");
              updateGUIProductionLines();
              break loop;
            }
          }
        }
    }
  }

  factoryMenu = gui.addFolder('Factory Menu');
  factoryMenu.add(factoryMenuParams, 'addOneProductionLine').name("Add one new production line");
  factoryMenu.open();

  productionLineMenu = gui.addFolder('Production Line Menu');

  window.addEventListener('resize', onWindowsResize, false);

  let productionLineNames = [];

  productionLines.forEach(function (item) {
    productionLineNames.push(item.productionLineIdentifier);
  });

  let tempLine = {};

  productionLineNames.forEach(elem => {
    tempLine[elem] = elem;
  });

  let selectedProductionLineParams = {
    selectedProductionLine: ""
  };

  productionLineMenu.add(selectedProductionLineParams, 'selectedProductionLine', tempLine).name('Selected production line');

  let tempMachine = {};

  unassociatedMachines.forEach(machineElem => {
    tempMachine[machineElem] = machineElem;
  });

  let selectedMachineParams = {
    machineToAdd: ""
  };

  productionLineMenu.add(selectedMachineParams, 'machineToAdd', tempMachine).name("Machine to add");

  let addUnassociatedMachineParams = {

    addMachine: function () {

      let selectedProductionLineControllerIndex = findGUIControllerIndex(productionLineMenu, 'selectedProductionLine');
      let machineToAddControllerIndex = findGUIControllerIndex(productionLineMenu, 'machineToAdd');

      if (productionLineMenu.__controllers[selectedProductionLineControllerIndex].__select.selectedOptions[0] === undefined || productionLineMenu.__controllers[machineToAddControllerIndex].__select.selectedOptions[0] === undefined) {
        alert("ERROR: please select a valid production line and machine to add first");
        return;
      }

      let productionLineAux = productionLineMenu.__controllers[selectedProductionLineControllerIndex].__select.selectedOptions[0].value;
      let machineAux = productionLineMenu.__controllers[machineToAddControllerIndex].__select.selectedOptions[0].value;

      let prodLineAux = findProductionLineByIdentifier(productionLineAux);

      if (prodLineAux.productionLine.listAssociatedMachinesDto.length === maximumMachinePerProductionLineNumber) {
        alert("ERROR: this production line already has the maximum number of machines permitted");
        return;
      }

      // // it adds the new machine to the production line's machine list
      // prodLineAux.productionLine.listAssociatedMachinesDto.push(machineAux);

      //checks if the production line is a newly created one in the execution visualizer
      if (productionLinesToAdd.includes(prodLineAux.productionLine)) {


        addOneMachineToAProductionLine(machineAux, prodLineAux.productionLine, prodLineAux.index, false);
        saveANewProductionLine();
        //productionLinesToAdd.splice(productionLinesToAdd.indexOf(prodLineAux.productionLine),1);


        //machineModelImporter(productionLines.indexOf(productionLine), productionLine.listAssociatedMachinesDto.length - 1, machineAux);

      }

      // the selected production line was one of the initially imported from the database
      else {

        // checks if the production line the user is editing was initially imported from the database hasn't had a new machine added to it yet; if it's that the case
        // it adds the production line to the productionLinesToUpdate array
        // if (!productionLinesToUpdate.includes(prodLineAux.productionLine)) {
        //   productionLinesToUpdate.push(prodLineAux.productionLine);
        // }

        addOneMachineToAProductionLine(machineAux, prodLineAux.productionLine, prodLineAux.index, true);
        productionLinesToUpdate.push(prodLineAux.productionLine);
        saveAnUpdatedProductionLine(prodLineAux.productionLine);


        //machineModelImporter(productionLines.indexOf(productionLine), productionLine.listAssociatedMachinesDto.length - 1, machineAux);
      }


      // let conveyorBelt = findLastConveyorBeltInProductionLine(prodLineAux.productionLine.productionLineIdentifier);
      // let box = findBoxByProductionLineIndex(prodLineAux.index);
      // box.position.z = conveyorBelt.position.z - boxOffsetZ;

      unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
      updateGUIMachines();
    }
  };

  productionLineMenu.add(addUnassociatedMachineParams, 'addMachine').name('Add unassociated machine to production line');

  productionLineMenu.open();

  let saveChangesParams = {

    saveChanges: function () {

      if (productionLinesToAdd.length > 0) {

        productionLinesToAdd.forEach(function (productionLine) {

          productionLineService.addProductionLine(productionLine).subscribe(next => {
            // do nothing
          }, error => {
            alert('An error occurred when creating the new production line with the selected machine');
            // productionLine.listAssociatedMachinesDto.splice(productionLine.listAssociatedMachinesDto.indexOf(machineAux), 1);
            // productionLinesToAdd.splice(productionLinesToAdd.indexOf(productionLine), 1);
          }, () => {
            alert('Production Line ' + productionLine.productionLineIdentifier + ' was created successfully');
            //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
            // unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
            // updateGUIMachines();
          });
        })
      }

      if (productionLinesToUpdate.length > 0) {

        productionLinesToUpdate.forEach(function (productionLine) {

          productionLineService.updateProductionLine(productionLine).subscribe(next => {
            // do nothing
          }, error => {
            //alert('An error occurred when updating the production line with the selected machine');
            //productionLine.listAssociatedMachinesDto.splice(productionLine.listAssociatedMachinesDto.indexOf(machineAux), 1);
            //productionLinesToUpdate.splice(productionLinesToAdd.indexOf(productionLine), 1);
          }, () => {
            alert('Production Line ' + productionLine.productionLineIdentifier + ' was updated successfully');


            //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
            //unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
            //updateGUIMachines();
          });
        })
      }
    }
  };

  let animateProductionPlanParams = {

    animateProductionPlan: function () {

      removeProductsFromScene();

      const scheduleStr = localStorage.getItem('schedule');
      if (!scheduleStr) {
        // TODO error message
        console.error('No scheduling planned.');
      }
      const schedule = JSON.parse(scheduleStr);
      setupAnimateProductionPlan(schedule);
    }
  };

  gui.add(animateProductionPlanParams, 'animateProductionPlan').name("Animate production plan");


  //gui.add(saveChangesParams, 'saveChanges').name("Save all changes");

  // let saveChangesParams = {
  //
  //   saveChanges: function () {
  //
  //     let i;
  //     let prodAux;
  //
  //     //checks if there is at least one production line with no machines
  //     for (i = 0; i < productionLines.length; i++) {
  //       prodAux = productionLines[i];
  //       if (prodAux.listAssociatedMachinesDto.length === 0) {
  //         alert('ERROR: A production line must have at least one machine in order to be saved! Please add a machine to the Production Line ' + prodAux.productionLineIdentifier);
  //         return;
  //       }
  //     }
  //
  //     if(productionLinesToUpdate.length === 0 && productionLinesToAdd.length === 0){
  //       alert('ERROR: No changes to the production lines were detected. Please make some changes before saving the current factory state to the database');
  //     }
  //
  //     if (productionLinesToAdd.length > 0) {
  //
  //       productionLinesToAdd.forEach(async function (productionLine) {
  //
  //         await productionLineService.addProductionLine(productionLine).subscribe(next => {
  //           // do nothing
  //         }, error => {
  //           alert('An error occurred when creating the new production line with the selected machine');
  //           // productionLine.listAssociatedMachinesDto.splice(productionLine.listAssociatedMachinesDto.indexOf(machineAux), 1);
  //           // productionLinesToAdd.splice(productionLinesToAdd.indexOf(productionLine), 1);
  //         }, () => {
  //           alert('Production Line ' + productionLine.productionLineIdentifier + ' was created successfully');
  //           productionLinesToAdd.splice(productionLinesToAdd.indexOf(productionLine),1);
  //
  //
  //           //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
  //           // unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
  //           // updateGUIMachines();
  //         })
  //       })
  //     }
  //
  //     if (productionLinesToUpdate.length > 0) {
  //
  //       productionLinesToUpdate.forEach(async function (productionLine) {
  //
  //         await productionLineService.updateProductionLine(productionLine).subscribe(next => {
  //           // do nothing
  //         }, error => {
  //           //alert('An error occurred when updating the production line with the selected machine');
  //           //productionLine.listAssociatedMachinesDto.splice(productionLine.listAssociatedMachinesDto.indexOf(machineAux), 1);
  //           //productionLinesToUpdate.splice(productionLinesToAdd.indexOf(productionLine), 1);
  //         }, () => {
  //           alert('Production Line ' + productionLine.productionLineIdentifier + ' was updated successfully');
  //           productionLinesToUpdate.splice(productionLinesToUpdate.indexOf(productionLine),1);
  //
  //
  //           //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
  //           //unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
  //           //updateGUIMachines();
  //         });
  //       })
  //     }
  //   }
  // }
  //
  // gui.add(saveChangesParams, 'saveChanges').name("Save all changes");
}

function validateProductionLineIdentifier(productionLineIdentifier) {

  if (!productionLineRegex.test(productionLineIdentifier) || productionLineIdentifier === '') {
    alert('ERROR: the production line identifier is not valid!');
    return false;
  }

  for (let index = 0; index < productionLines.length; index++) {

    if (productionLines[index].productionLineIdentifier === productionLineIdentifier) {
      alert('ERROR: the production line identifier is already in use!');
      return false;
    }
  }

  return true;
}

function updateGUIProductionLines() {

  let productionLineNames = [];

  productionLines.forEach(function (item) {
    productionLineNames.push(item.productionLineIdentifier);
  });

  let tempLine = {};

  productionLineNames.forEach(elem => {
    tempLine[elem] = elem;
  });

  let controllerIndex = findGUIControllerIndex(productionLineMenu, 'selectedProductionLine');

  productionLineMenu.__controllers[controllerIndex].options(tempLine).name("Selected production line");
}

function updateGUIMachines() {

  let tempMachine = {};

  unassociatedMachines.forEach(machineElem => {
    tempMachine[machineElem] = machineElem;
  });

  let controllerIndex = findGUIControllerIndex(productionLineMenu, 'machineToAdd');

  productionLineMenu.__controllers[controllerIndex].options(tempMachine).name("Machine to add");
}

function findGUIControllerIndex(menu, controllerName) {

  for (let i = 0; i < menu.__controllers.length; i++) {

    if (menu.__controllers[i].property === controllerName) {
      return i;
    }
  }

  throw 'GUI controller not found';
}


// function findProductionLineByIdentifier(identifier) {
//
//   let result = {};
//
//   for (let index = 0; index < productionLines.length; index++) {
//
//     if (productionLines[index].productionLineIdentifier === identifier) {
//       result.productionLine = productionLines[index];
//       result.index = index;
//     }
//   }
//
//   if (result === null && result === undefined) {
//     throw 'Production line with specified identifier not found';
//   }
//
//   return result;
// }

function findMachineByProductionLineAndMachineIndexes(productionLineIndex, machineIndex) {

  let machineToFind = 'machine' + productionLineIndex + '.' + machineIndex;
  let machineAux;

  for (let i = 0; i < machines.length; i++) {

    machineAux = machines[i];

    if (machineAux.name === machineToFind) {
      return machineAux;
    }

  }

  throw 'Unable to find machine with specified production line and machine positions.'
}

function updateMachineSceneInfo(machineScene, newProductionLineIndex, newMachineIndex) {

  machineScene.productionLineIndex = newProductionLineIndex;
  machineScene.slot = newMachineIndex;
  machineScene.name = 'machine' + newProductionLineIndex + '.' + newMachineIndex;

}

function copyProductionLineMachineList(productionLine) {

  let aux = [];
  let aux2 = {};

  productionLine.listAssociatedMachinesDto.forEach(function (machine) {
    aux2 = {machineSerialNumber: machine.machineSerialNumber, slot: machine.slot};
    aux.push(aux2);
  });

  return aux;
}

function buildProductionLineContainer(productionLineIdentifier, listAssociatedMachinesDto) {

  let newProductionLine = {};

  newProductionLine.productionLineIdentifier = productionLineIdentifier;
  newProductionLine.listAssociatedMachinesDto = listAssociatedMachinesDto;

  return newProductionLine;
}


//handles windows resize event
function onWindowsResize() {
  if (camera) {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }
}

window.addEventListener('resize', onWindowsResize, false);


//adds the initial production lines to the scene
function addInitialProductionLines(productionLinesArray) {

  if (typeof (productionLinesArray == "object")) {

    if (productionLinesArray.length > maximumProductionLineNumber) {
      throw "This number of production lines exceeds the maximum limit permitted (" + maximumProductionLineNumber + ")";
    }

    productionLinesArray.forEach(function (item) {
      currentProductionLineIndex = productionLinesArray.indexOf(item);
      addMachinesOfAProductionLine(item.listAssociatedMachinesDto, currentProductionLineIndex);
      //sphereModelImporter(currentProductionLineIndex, item.listAssociatedMachinesDto.length);
      addProductionLineToProductionLinesArray(item);
      currentProductionLinesCount = productionLines.length;
    });

    indicateInitialImportingIsDone();
  } else {

    throw "Invalid production lines array";
  }

}

//adds the machines of a production line to the scene
function addMachinesOfAProductionLine(productionLineMachinesArray, productionLineIndex) {

  let conveyorBeltAux;

  if (typeof (productionLineMachinesArray == "object")) {

    if (productionLineMachinesArray.length > maximumMachinePerProductionLineNumber) {
      throw "This production line exceeds the maximum number of machines permitted (" + maximumMachinePerProductionLineNumber + ")";
    }

    //currentMachineIndex = 0;
    productionLineMachinesArray.forEach(function (item) {

      //currentMachineIndex = productionLineMachinesArray.indexOf(item);

      currentMachineIndex = item.slot;

      machineModelImporter(productionLineIndex, currentMachineIndex, item);
      // machineNameDisplayLoader(item, productionLineIndex, currentMachineIndex);

      conveyorBeltAux = conveyorBeltModelImporter(productionLineIndex, currentMachineIndex);

      if (item.slot === productionLineMachinesArray.length - 1) {
        boxModelImporter(conveyorBeltAux);
      }

      unassociatedMachines.forEach(function (unassociatedAux) {

        let aux = unassociatedAux.serialNumber;

        if (item === aux) {
          const index = unassociatedMachines.indexOf(unassociatedAux);
          unassociatedMachines.splice(index, 1);
        }
      });


      if (productionLineMachinesArray.length > 1) {
        machineProductionLineCount = productionLineMachinesArray.length;
      }

      //currentMachineIndex++;
    });

  } else {
    throw "Invalid machines array";
  }
}

//adds one unassociated machine to a production line to the scene
function addOneMachineToAProductionLine(machineSerialNumber, productionLine, productionLineIndex, includeConveyorBeltModelFlag) {

  if (productionLine.listAssociatedMachinesDto.length === maximumMachinePerProductionLineNumber) {
    throw "This production line has already reached the maximum number of machines permitted (" + maximumMachinePerProductionLineNumber + ")";
  }

  currentMachineIndex = productionLine.listAssociatedMachinesDto.length;

  let slot = currentMachineIndex;
  let machineInfo = {machineSerialNumber, slot};

  machineModelImporter(productionLineIndex, slot, machineInfo);
  // machineNameDisplayLoader(item, productionLineIndex, currentMachineIndex);

  if (includeConveyorBeltModelFlag === true) {
    conveyorBeltModelImporter(productionLineIndex, slot);
  }

  addMachinePosition(productionLineIndex, slot);
  addMachineToProductionLineArray(machineSerialNumber, productionLineIndex, slot);

  unassociatedMachines.forEach(function (unassociatedAux) {

    let aux = unassociatedAux.serialNumber;

    if (machineSerialNumber === aux) {
      const index = unassociatedMachines.indexOf(unassociatedAux);
      unassociatedMachines.splice(index, 1);
    }
  });
}

function saveAnUpdatedProductionLine(updateSecondProductionLineFlag, deleteSecondProductionLineFlag, createSecondProductionLineFlag) {

  if (productionLinesToUpdate.length > 0) {

    let productionLineToUpdate = productionLinesToUpdate[0];

    updateProductionLineOperationInProgress = true;
    productionLineService.updateProductionLine(productionLineToUpdate).subscribe(next => {
      // do nothing
    }, error => {
      //alert('An error occurred when updating the production line with the selected machine');
      //productionLine.listAssociatedMachinesDto.splice(productionLine.listAssociatedMachinesDto.indexOf(machineAux), 1);
      //productionLinesToUpdate.splice(productionLinesToAdd.indexOf(productionLine), 1);
    }, () => {
      alert('Production Line ' + productionLineToUpdate.productionLineIdentifier + ' was updated successfully');

      if (!deleteSecondProductionLineFlag || deleteSecondProductionLineFlag === false) {
        let prodLine = findProductionLineByIdentifier(productionLineToUpdate.productionLineIdentifier);
        removeBoxFromScene(prodLine.index);
        let conveyorBelt = findLastConveyorBeltInProductionLine(prodLine.productionLine.productionLineIdentifier);
        boxModelImporter(conveyorBelt);
      }
      // if (updateSecondProductionLineFlag === true) {
      //   canUpdateNextProductionLine = true;
      // } else {
      //   canUpdateNextProductionLine = false;
      // }

      if (deleteSecondProductionLineFlag === true) {
        canDeleteNextProductionLine = true;
      }
      if (createSecondProductionLineFlag === true) {
        canCreateNextProductionLine = true
      }
      updateProductionLineOperationInProgress = false;

      productionLinesToUpdate.splice(productionLinesToUpdate.indexOf(productionLineToUpdate), 1);
      //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
      //unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
      //updateGUIMachines();
    });


  } else {
    throw('There are no production lines to update');
  }
}

function saveANewProductionLine() {

  if (productionLinesToAdd.length > 0) {

    let productionLineToAdd = productionLinesToAdd[0];

    productionLineService.addProductionLine(productionLineToAdd).subscribe(next => {
      // do nothing
    }, error => {
      alert('An error occurred when creating the new production line with the selected machine');
      // productionLine.listAssociatedMachinesDto.splice(productionLine.listAssociatedMachinesDto.indexOf(machineAux), 1);
      // productionLinesToAdd.splice(productionLinesToAdd.indexOf(productionLine), 1);
    }, () => {
      alert('Production Line ' + productionLineToAdd.productionLineIdentifier + ' was created successfully');
      productionLinesToAdd.splice(productionLinesToAdd.indexOf(productionLineToAdd), 1);
      canCreateNextProductionLine = false;
      let conveyorBelt = findLastConveyorBeltInProductionLine(productionLineToAdd.productionLineIdentifier);
      boxModelImporter(conveyorBelt);

      //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
      // unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
      // updateGUIMachines();
    })
  } else {
    throw 'No new production lines to create!';
  }
}

function deleteProductionLine(updateNextProductionLineFlag, productionLineToUpdate) {

  if (productionLinesToDelete.length > 0) {

    let productionLineToDelete = productionLinesToDelete[0];

    productionLineService.deleteProductionLine(productionLineToDelete).subscribe(next => {
      // do nothing
    }, error => {
      alert('An error occurred when deleting the production line');
    }, () => {
      alert('Production Line ' + productionLineToDelete.productionLineIdentifier + ' was deleted successfully');
      //if (updateNextProductionLineFlag === true && productionLineToUpdate != undefined) {
      if (updateNextProductionLineFlag === true) {
        canUpdateNextProductionLine = true;
        checkCanUpdateNextProductionLine(productionLineToUpdate, false);
      }
      productionLinesToDelete.splice(productionLinesToDelete.indexOf(productionLineToDelete), 1);
      canDeleteNextProductionLine = false;
      //productionLineService.getAvailableMachines().subscribe(availableMachines => unassociatedMachines = availableMachines);
      // unassociatedMachines.splice(unassociatedMachines.indexOf(machineAux), 1);
      // updateGUIMachines();
    })
  } else {
    throw 'No production lines to delete!';
  }
}


function onMouseMove(event) {

  let mouse = new THREE.Vector2();
  let raycaster = new THREE.Raycaster();
  let rect = renderer.domElement.getBoundingClientRect();

  mouse.x = ((event.clientX - rect.left) / rect.width) * 2 - 1;
  mouse.y = -((event.clientY - rect.top) / rect.height) * 2 + 1;

  if (camera) raycaster.setFromCamera(mouse, camera);
  var intersects = raycaster.intersectObjects(machines, true);
  var tooltip = document.getElementById('tooltip');
  if (intersects[0]) {
    for (var i = 0; i < intersects.length; i++) {
      console.log(intersects[i].object.name);
      allMachines.forEach(function (machine) {
        if (intersects[i].object.name == machine.serialNumber) {
          //console.log("Serial Number: " + machine.serialNumber + " Model: " + machine.model + " Brand: " + machine.brand + " Machine Type: " + machine.machineType);
          tooltip.innerHTML = "Serial Number: " + machine.serialNumber + "<br/>" + "Model: " + machine.model + "<br/>" + "Brand: " + machine.brand + "<br/>" + "Machine Type: " + machine.machineType;
          tooltip.style.visibility = 'visible';
          tooltip.style.top = event.clientY + 'px';
          tooltip.style.left = event.clientX + 'px';

        } else {
        }
      });
    }
  } else {
    if (tooltip)
      tooltip.style.visibility = 'hidden';
  }
}


function animate() {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
  cameraControls.update();
  TWEEN.update();
}


function checkLoaders(productionLines) {

  if (factoryFloorModel != null && machineModel != null && conveyorBeltModel != null && sphereModel != null && spoonModel != null && fontLoader != null) {
    factoryFloorModelImporter();
    addInitialProductionLines(productionLines);
    window.addEventListener('mousemove', onMouseMove);
  } else {
    setTimeout(function () {
      checkLoaders(productionLines)
    }, 100);
  }
}

function checkInitialImportingIsDone() {

  if (initialImportingIsDone === true) {
    initiateGUI();
    //setupAnimateProductionPlan();
  } else {
    setTimeout(function () {
      checkInitialImportingIsDone()
    }, 100);
  }
}

function checkCanUpdateNextProductionLine(deleteSecondProductionLineFlag, updateSecondProductionLineFlag) {

  if (updateProductionLineOperationInProgress === false) {

    saveAnUpdatedProductionLine(updateSecondProductionLineFlag, deleteSecondProductionLineFlag);

    // if (canUpdateNextProductionLine === true) {
    //   saveAnUpdatedProductionLine(updateSecondProductionLineFlag, deleteSecondProductionLineFlag);
    // } else {
    //   setTimeout(function () {
    //     checkCanUpdateNextProductionLine(deleteSecondProductionLineFlag, updateSecondProductionLineFlag)
    //   }, 100);
    // }

  } else {
    setTimeout(function () {
      checkCanUpdateNextProductionLine(deleteSecondProductionLineFlag, updateSecondProductionLineFlag)
    }, 100);
  }
}

function checkCanDeleteNextProductionLine(updateNextProductionLineFlag) {

  if (canDeleteNextProductionLine === true) {
    deleteProductionLine(updateNextProductionLineFlag);
  } else {
    setTimeout(function () {
      checkCanDeleteNextProductionLine(updateNextProductionLineFlag)
    }, 100);
  }
}

function checkCanCreateNextProductionLine() {

  if (canCreateNextProductionLine === true) {
    saveANewProductionLine();
  } else {
    setTimeout(function () {
      checkCanCreateNextProductionLine()
    }, 100);
  }
}

export function main(productionLines, machines, availableMachines, prodLineServiceAux) {

  allMachines = machines;
  initialProductionLines = productionLines;
  unassociatedMachines = availableMachines;
  productionLineService = prodLineServiceAux;

  init();
  checkLoaders(productionLines);
  checkInitialImportingIsDone();
  animate();
}
