import {  EventDispatcher,
  Matrix4,
  Plane,
  Raycaster,
  Vector2,
  Vector3} from "three";

import {productionLineSeparationDistance, machineSeparationDistance, machinePositionMinX, machinePositionMaxX, machinePositionMinZ, machinePositionMaxZ} from "./globalVariables";

let DragControls = function ( _objects, _camera, _domElement ) {

  let _plane = new Plane();
  let _raycaster = new Raycaster();

  let _mouse = new Vector2();
  let _offset = new Vector3();
  let _intersection = new Vector3();
  let _worldPosition = new Vector3();
  let _inverseMatrix = new Matrix4();

  let _selected = null, _hovered = null;


  let scope = this;

  function activate() {

    _domElement.addEventListener( 'mousemove', onDocumentMouseMove, false );
    _domElement.addEventListener( 'mousedown', onDocumentMouseDown, false );
    _domElement.addEventListener( 'mouseup', onDocumentMouseCancel, false );
    _domElement.addEventListener( 'mouseleave', onDocumentMouseCancel, false );
    _domElement.addEventListener( 'touchmove', onDocumentTouchMove, false );
    _domElement.addEventListener( 'touchstart', onDocumentTouchStart, false );
    _domElement.addEventListener( 'touchend', onDocumentTouchEnd, false );

  }

  function deactivate() {

    _domElement.removeEventListener( 'mousemove', onDocumentMouseMove, false );
    _domElement.removeEventListener( 'mousedown', onDocumentMouseDown, false );
    _domElement.removeEventListener( 'mouseup', onDocumentMouseCancel, false );
    _domElement.removeEventListener( 'mouseleave', onDocumentMouseCancel, false );
    _domElement.removeEventListener( 'touchmove', onDocumentTouchMove, false );
    _domElement.removeEventListener( 'touchstart', onDocumentTouchStart, false );
    _domElement.removeEventListener( 'touchend', onDocumentTouchEnd, false );

  }

  function dispose() {

    deactivate();

  }

  function onDocumentMouseMove( event ) {

    event.preventDefault();

    let rect = _domElement.getBoundingClientRect();

    _mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
    _mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

    _raycaster.setFromCamera( _mouse, _camera );

    if ( _selected && scope.enabled ) {

      if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

        _selected.position.copy( _intersection.sub( _offset ).applyMatrix4( _inverseMatrix ) );

        _selected.position.x = Math.floor(_selected.position.x /productionLineSeparationDistance )*productionLineSeparationDistance;
        //_selected.position.y = event.object.position.y;
        _selected.position.z = Math.floor(_selected.position.z /machineSeparationDistance+0.5 )*machineSeparationDistance+0.5;

        if(_selected.position.x <= machinePositionMinX){
          _selected.position.x = machinePositionMinX;
        }

        if(_selected.position.x > machinePositionMaxX){
          _selected.position.x = machinePositionMaxX;
        }

        if(_selected.position.z >= machinePositionMinZ){
          _selected.position.z = machinePositionMinZ;
        }

        if(_selected.position.z < machinePositionMaxZ){
          _selected.position.z = machinePositionMaxZ;
        }
      }

      scope.dispatchEvent( { type: 'drag', object: _selected } );

      return;

    }

    _raycaster.setFromCamera( _mouse, _camera );

    let intersects = _raycaster.intersectObjects( _objects, true );

    if ( intersects.length > 0 ) {

      let object = intersects[ 0 ].object;

      _plane.setFromNormalAndCoplanarPoint( _camera.getWorldDirection( _plane.normal ), _worldPosition.setFromMatrixPosition( object.matrixWorld ) );

      if ( _hovered !== object ) {

        scope.dispatchEvent( { type: 'hoveron', object: object } );

        _domElement.style.cursor = 'pointer';
        _hovered = object;

      }

    } else {

      if ( _hovered !== null ) {

        scope.dispatchEvent( { type: 'hoveroff', object: _hovered } );

        _domElement.style.cursor = 'auto';
        _hovered = null;

      }

    }

  }

  function onDocumentMouseDown( event ) {

    event.preventDefault();

    _raycaster.setFromCamera( _mouse, _camera );

    var intersects = _raycaster.intersectObjects( _objects, true );

    if ( intersects.length > 0 ) {

      _selected = intersects[ 0 ].object.parent;

      if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

        _inverseMatrix.getInverse( _selected.parent.matrixWorld );
        _offset.copy( _intersection ).sub( _worldPosition.setFromMatrixPosition( _selected.matrixWorld ) );

      }

      _domElement.style.cursor = 'move';

      scope.dispatchEvent( { type: 'dragstart', object: _selected } );

    }


  }

  function onDocumentMouseCancel( event ) {

    event.preventDefault();

    if ( _selected ) {

      scope.dispatchEvent( { type: 'dragend', object: _selected } );

      _selected = null;

    }

    _domElement.style.cursor = _hovered ? 'pointer' : 'auto';

  }

  function onDocumentTouchMove( event ) {

    event.preventDefault();
    event = event.changedTouches[ 0 ];

    let rect = _domElement.getBoundingClientRect();

    _mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
    _mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

    _raycaster.setFromCamera( _mouse, _camera );

    if ( _selected && scope.enabled ) {

      if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

        _selected.position.copy( _intersection.sub( _offset ).applyMatrix4( _inverseMatrix ) );

      }

      scope.dispatchEvent( { type: 'drag', object: _selected } );

      return;

    }

  }

  function onDocumentTouchStart( event ) {

    event.preventDefault();
    event = event.changedTouches[ 0 ];

    let rect = _domElement.getBoundingClientRect();

    _mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
    _mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

    _raycaster.setFromCamera( _mouse, _camera );

    let intersects = _raycaster.intersectObjects( _objects, true );

    if ( intersects.length > 0 ) {

      _selected = intersects[ 0 ].object;

      _plane.setFromNormalAndCoplanarPoint( _camera.getWorldDirection( _plane.normal ), _worldPosition.setFromMatrixPosition( _selected.matrixWorld ) );

      if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

        _inverseMatrix.getInverse( _selected.parent.matrixWorld );
        _offset.copy( _intersection ).sub( _worldPosition.setFromMatrixPosition( _selected.matrixWorld ) );

      }

      _domElement.style.cursor = 'move';

      scope.dispatchEvent( { type: 'dragstart', object: _selected } );

    }


  }

  function onDocumentTouchEnd( event ) {

    event.preventDefault();

    if ( _selected ) {

      scope.dispatchEvent( { type: 'dragend', object: _selected } );

      _selected = null;

    }

    _domElement.style.cursor = 'auto';

  }

  activate();

  // API

  this.enabled = true;

  this.activate = activate;
  this.deactivate = deactivate;
  this.dispose = dispose;

};

DragControls.prototype = Object.create( EventDispatcher.prototype );
DragControls.prototype.constructor = DragControls;

export { DragControls };
