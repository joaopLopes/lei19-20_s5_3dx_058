import * as THREE from "three";

export let scene = new THREE.Scene();

export let initialImportingIsDone = false;

export let machinesPositionMatrix;
export let machines = [];
export let conveyorBelts = [];
export let products = [];
export let productionLines = [];
export let boxes = [];

export let factoryFloorModel = null;
export let machineModel = null;
export let conveyorBeltModel = null;
export let sphereModel = null;
export let spoonModel = null;
export let fontLoader = null;
export let boxModel = null;

export let machineStartingX = -15;
export let machineRotationY = 1.57;
export let machineStartingZ = 20;

export let productionLineSeparationDistance = 5;
export let machineSeparationDistance = 6.5;

export let machineTextOffsetX = 1.5;
export let machineTextOffsetY = 3.5;
export let machineTextOffsetZ = 2;

export let machineOcupationNameOffsetY = 2;

export let conveyorBeltOffsetX = 0.62;
export let conveyorBeltOffsetZ = 3.2;

export let productOnConveyorBeltOffsetX = 0.65;
export let productOnConveyorBeltOffsetY = 1.37;
export let spoonOnConveyorBeltOffsetY = 1.15;
export let productConvBeltStartPosOffsetZ = 1.5;
export let spoonConvBeltEndPosOffsetZ = 2.55;
export let productConvBeltEndPosOffsetZ = 3;

export let productOnMachineOffsetY = 1.75;
export let productOnMachineLeftSlotOffsetX = 0.28;
export let productOnMachineOffsetZ = 1;

export let supportBeamAnimationOffsetY = 0.15;

export let boxOffsetZ = 2.8;

export let spoonFromConveyorBeltToBoxOffsetZ = 2.1;
export let spoonFromConveyorBeltToBoxOffsetY = 1.2;

export let maximumProductionLineNumber = 7;
export let maximumMachinePerProductionLineNumber = 6;

export let machinePositionMinZ = 20;
export let machinePositionMaxZ = -12.5;

export let machinePositionMinX = -15;
export let machinePositionMaxX =15;



export function instantiateFactoryFloorModel(model){
  factoryFloorModel = model;
}

export function instantiateMachineModel(model){
  machineModel = model;
}

export function instantiateConveyorBeltModel(model){
  conveyorBeltModel = model;
}

export function instantiateSphereModel(model) {
  sphereModel = model;
}

export function instantiateSpoonModel(model){
  spoonModel = model;
}

export function instantiateBoxModel(box){
  boxModel = box;
}

export function instantiateFontLoader(font){
  fontLoader = font;
}

export function instantiateMachinePosition(){

  machinesPositionMatrix = [
    [false,false,false,false,false,false],
    [false,false,false,false,false,false],
    [false,false,false,false,false,false],
    [false,false,false,false,false,false],
    [false,false,false,false,false,false],
    [false,false,false,false,false,false],
    [false,false,false,false,false,false]
  ];
}


export function addProductionLineToProductionLinesArray (productionLine){
  productionLines.push(productionLine);
}

export function addMachineToMachinesArray(machine){
  machines.push(machine);
}

export function addConveyorBeltToConveyorBeltsArray(conveyorBelt){
  conveyorBelts.push(conveyorBelt);
}

export function addProductToProductsArray(product){
  products.push(product);
}

export function addBoxToBoxesArray(box){
  boxes.push(box);
}


export function indicateInitialImportingIsDone(){
  initialImportingIsDone = true;
}

export function checkMachinePositionIsAvailable(productionLineIndex, machineIndex){

  if(machinesPositionMatrix[productionLineIndex][machineIndex] === false){
    return true;
  }

  else{
    if(machinesPositionMatrix[productionLineIndex][machineIndex] === true){
      return false;
    }
  }

  alert('Error searching for machines positions');
}

export function addMachinePosition(productionLineIndex, machineIndex) {

  machinesPositionMatrix[productionLineIndex][machineIndex] = true;
}

export function removeMachinePosition(productionLineIndex, machineIndex){

  machinesPositionMatrix[productionLineIndex][machineIndex] = false;
}

export function removeConveyorBeltFromScene(productionLineIndex, machineIndex) {

  let result = findConveyorBeltSceneByProductionLineAndMachineIndexes(productionLineIndex, machineIndex);
  scene.children.splice(result.index,1);
  conveyorBelts.splice(conveyorBelts.indexOf(result.scene),1);

}

export function deleteMachineFromProductionLineArray(productionLineIndex, machineIndex){

  productionLines[productionLineIndex].listAssociatedMachinesDto.forEach(function (machine) {
    if(machine.slot === machineIndex){
      delete productionLines[productionLineIndex].listAssociatedMachinesDto[productionLines[productionLineIndex].listAssociatedMachinesDto.indexOf(machine)];
    }
  });
}

export function spliceOutMachineFromProductionLineArray(productionLineIndex, machineIndex) {

  productionLines[productionLineIndex].listAssociatedMachinesDto.forEach(function (machine) {
    if(machine.slot == machineIndex){
      productionLines[productionLineIndex].listAssociatedMachinesDto.splice(productionLines[productionLineIndex].listAssociatedMachinesDto.indexOf(machine),1);
    }
  })
}

export function spliceInMachineIntoProductionLineArray(machineSerialNumber, productionLineIndex,machineIndex){

  let machineInfo = {machineSerialNumber: machineSerialNumber, slot: machineIndex};
  productionLines[productionLineIndex].listAssociatedMachinesDto.splice(machineIndex,0,machineInfo);
}

export function addMachineToProductionLineArray(machineSerialNumber, productionLineIndex, machineIndex) {

  let machineInfo = {machineSerialNumber: machineSerialNumber, slot: machineIndex};
  productionLines[productionLineIndex].listAssociatedMachinesDto[machineIndex] = machineInfo;
}

export function shiftMachineSlotInProductionLinesArrayToTheLeft(productionLineIndex,oldMachineIndex) {

  productionLines[productionLineIndex].listAssociatedMachinesDto.forEach(function (machine) {
    if(machine.slot == oldMachineIndex){
      productionLines[productionLineIndex].listAssociatedMachinesDto[productionLines[productionLineIndex].listAssociatedMachinesDto.indexOf(machine)].slot--;
    }
  });

  //productionLines[productionLineIndex].listAssociatedMachinesDto[oldMachineIndex].slot--;
  //productionLines[productionLineIndex].listAssociatedMachinesDto[oldMachineIndex].slot = oldMachineIndex-1;
}

export function spliceOutProductionLineFromProductionLineArray(productionLineIndex){

  productionLines.splice(productionLineIndex,1);
}

export function findModelChildrenIndexByName(modelChildren, childrenName){

  for(let i=0;i<modelChildren.length;i++){

    if(modelChildren[i].name === childrenName){
      return i;
    }
  }
  throw 'Unable to find index of the specified children ' + childrenName;
}

function findConveyorBeltSceneByProductionLineAndMachineIndexes(productionLineIndex,machineIndex){

  let index;
  let childAux;
  let nameAux = 'convBelt' + productionLineIndex + '.' + machineIndex;
  let result = {};

  for(index=0;index<scene.children.length;index++){

    childAux = scene.children[index];

    if(childAux.name === nameAux){
      result.scene = childAux;
      result.index = index;
      return result;
    }
  }
  throw 'Could not find specified conveyor belt scene';
}

export function findProductionLineByIdentifier(identifier) {

  let result = {};

  for (let index = 0; index < productionLines.length; index++) {

    if (productionLines[index].productionLineIdentifier === identifier) {
      result.productionLine = productionLines[index];
      result.index = index;
    }
  }

  if (result === null && result === undefined) {
    throw 'Production line with specified identifier not found';
  }

  return result;
}

export function findProductionLineByIndex(index) {

  let result = {};

  for (let i = 0; i < productionLines.length; i++) {

    if (productionLines[i].productionLineIndex === index) {
      return productionLines[i];
    }
  }

  if (result === null && result === undefined) {
    throw 'Production line with specified identifier not found';
  }

  return result;
}

export function findMachineSlotBySerialNumberAndProductionLineIndex(machineSerialNumber, productionLineIndex){

  for (let index = 0; index < productionLines[productionLineIndex].listAssociatedMachinesDto.length; index++) {

    if (productionLines[productionLineIndex].listAssociatedMachinesDto[index].machineSerialNumber === machineSerialNumber) {
      return productionLines[productionLineIndex].listAssociatedMachinesDto[index].slot;
    }
  }
}

export function findMachineBySerialNumber(machineSerialNumber){

  for(let i=0;i<machines.length;i++){

    let aux = machines[i];

    if(aux.serialNumber === machineSerialNumber){
      return aux;
    }
  }
}

export function findProductByNameAndProductionLineIdentifier(productName, productionLineIdentifier){

  let aux;

  for(let i=0;i<products.length;i++){
    aux = products[i];

    if (aux.name === productName && aux.productionLineIdentifier === productionLineIdentifier){
      return aux;
    }
  }
  throw 'Could not find product with specified name in production line identifier';
}

export function removeProductsFromScene() {

  if(products.length>0) {

    products.forEach(function (product) {
      scene.children.splice(scene.children.indexOf(product), 1);
    });

    products.splice(0, products.length);
  }
}

export function findBoxByProductionLineIndex(productionLineIndex){

  let aux;

  for(let i=0;i<boxes.length;i++){

    aux=boxes[i];

    if(aux.productionLineIndex === productionLineIndex){
      return aux;
    }
  }
}

export function findLastConveyorBeltInProductionLine(productionLineIdentifier){

  let aux, index, productionLine,lastSlot, convToFind;

  for(let i=0;i<productionLines.length;i++){

    aux = productionLines[i];

    if(aux.productionLineIdentifier === productionLineIdentifier){
      productionLine = aux;
      index = i;
    }
  }

  lastSlot = productionLine.listAssociatedMachinesDto.length-1;

  convToFind = 'convBelt' + index + '.' + lastSlot;

  for(let i=0;i<conveyorBelts.length;i++){

    aux = conveyorBelts[i];

    if(aux.name === convToFind){
      return aux;
    }
  }
}

export function removeBoxFromScene(productionLineIndex) {

  let box;

  if (box = findBoxByProductionLineIndex(productionLineIndex))
  {

    boxes.splice(boxes.indexOf(box), 1);
    scene.children.splice(scene.children.indexOf(box), 1);
  }
}
