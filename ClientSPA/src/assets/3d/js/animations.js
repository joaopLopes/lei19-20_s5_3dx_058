import TWEEN from "@tweenjs/tween.js";
import * as THREE from 'three';
import {
  findMachineSlotBySerialNumberAndProductionLineIndex,
  findModelChildrenIndexByName,
  findProductionLineByIdentifier,
  machineSeparationDistance,
  machineStartingX,
  machineStartingZ,
  productConvBeltEndPosOffsetZ,
  productConvBeltStartPosOffsetZ,
  productionLineSeparationDistance,
  productOnConveyorBeltOffsetX,
  productOnConveyorBeltOffsetY,
  productOnMachineLeftSlotOffsetX,
  productOnMachineOffsetY,
  productOnMachineOffsetZ,
  scene,
  machines,
  productionLines,
  supportBeamAnimationOffsetY,
  findMachineBySerialNumber,
  findProductByNameAndProductionLineIdentifier,
  spoonOnConveyorBeltOffsetY,
  spoonConvBeltEndPosOffsetZ,
  findLastConveyorBeltInProductionLine,
  findProductionLineBox,
  boxOffsetZ, spoonFromConveyorBeltToBoxOffsetZ, spoonFromConveyorBeltToBoxOffsetY
} from "./globalVariables";
import {machineOcupationNameDisplayLoader, sphereModelImporter, spoonModelImporter} from "./loaders";
import {animation} from "@angular/animations";


let machinesAnimationsByProductionLine;
let productsAnimations;


//reads the information provided by the production plan json and organizes it in order to animate the production plan
export function setupAnimateProductionPlan(productionPlan) {

  machinesAnimationsByProductionLine = new Array(productionLines.length);
  productsAnimations = [];

  for (let i = 0; i < machinesAnimationsByProductionLine.length; i++) {
    machinesAnimationsByProductionLine[i] = [];
  }

  machines.forEach(function (machine) {

    let machineAnimationsInfo = {timeSlotCount: 0, pendingAnimationsArray: []};
    machineAnimationsInfo.machineSerialNumber = machine.serialNumber;
    machineAnimationsInfo.isAvailable = true;
    machinesAnimationsByProductionLine[machine.productionLineIndex].push(machineAnimationsInfo);
  });


  //goes through one production line at a time
  productionPlan.productionPlans.forEach(function (productionLineAnimationPlan) {

    let productionLineIdentifier = productionLineAnimationPlan.productionLine;

    //goes through one product at a time that has operations on this production line
    productionLineAnimationPlan.fabricationsOrders.forEach(function (fabricationOrder) {

      let productToAnimate = fabricationOrder.product;
      let machinesToBeUsed = fabricationOrder.machines;

      let productAnimationInfo = [];

      machinesToBeUsed.forEach(function (machineWithOcupationsInfo) {

        let machineSerialNumber = machineWithOcupationsInfo.machine;
        let machineOcupations = machineWithOcupationsInfo.ocupations;

        machineOcupations.forEach(function (ocupation) {

          //checks if ocupation isn't empty
          if (ocupation.hasOwnProperty('operation')) {

            ocupation.isProductFirstOperation = false;

            let indexAux = findProductionLineByIdentifier(productionLineIdentifier).index;
            ocupation.productionLineIndex = indexAux;
            ocupation.productionLineIdentifier = productionLineIdentifier;

            ocupation.product = productToAnimate;

            ocupation.machineSerialNumber = machineSerialNumber;
            let machineSlot = findMachineSlotBySerialNumberAndProductionLineIndex(machineSerialNumber, indexAux);
            ocupation.machineSlot = machineSlot;



            //gathers all the operations (one at a time)  of one product
            productAnimationInfo.push(ocupation);

            addAnimationToMachineAnimationsArray(ocupation, machineSerialNumber);
          }
        })
      });
      //saves all operations of a product in array form to a global array and puts them in correct order
      if (productAnimationInfo.length > 0) {
        productAnimationInfo.reverse();
        productAnimationInfo[0].isProductFirstOperation = true;
        productAnimationInfo.productIsAvailable = true;
        productsAnimations.push(productAnimationInfo);
      }
    });
  });
  animateProductionPlan();
}

function addAnimationToMachineAnimationsArray(operationToAnimate) {

  for (let index = 0; index < machinesAnimationsByProductionLine[operationToAnimate.productionLineIndex].length; index++) {

    let aux = machinesAnimationsByProductionLine[operationToAnimate.productionLineIndex][index];

    if (aux.machineSerialNumber === operationToAnimate.machineSerialNumber) {
      aux.pendingAnimationsArray.push(operationToAnimate);
      return;
    }
  }
}

// function animateProductionPlan() {
//
//   machinesAnimationsByProductionLine.forEach(function (productionLine) {
//
//     productionLine.forEach(function (machine) {
//
//       if (machine.pendingAnimationsArray.length > 0) {
//         processAnimation(machine.pendingAnimationsArray[0]);
//         console.log("Machine " + machine.machineSerialNumber + " has no more animations to process!");
//       }
//     })
//   })
// }


//é preciso atualizar machinesAnimationsByProductionLine quando se termina uma animaçao

function animateProductionPlan() {

  //analyses each product to be animated
  productsAnimations.forEach(function (productAnimations) {

    //analyses each product's operation throughout the production line
    for(let i=0;i<productAnimations.length;i++){

      let operationAnimationInfo = productAnimations[i];

      //checks if this is the product's last operation
      if(i===productAnimations.length-1){
        animateOperation(operationAnimationInfo,false, true);
      }
      else{
        //checks if product's next operation will be in the same machine
        if(operationAnimationInfo.machineSerialNumber === productAnimations[i+1].machineSerialNumber){
          animateOperation(operationAnimationInfo, true, false);
        }
        else{
          animateOperation(operationAnimationInfo,false, false);
        }
      }
    }
  });
}


function animateOperation(animationInfo, nextMachineIsSameFlag, lastOperationFlag) {

  let machine = findMachineBySerialNumber(animationInfo.machineSerialNumber);


  //adds the product to the scene if this is its first operation
  if (animationInfo.isProductFirstOperation) {
    //addProductToScene(animationInfo.machineSerialNumber);
    sphereModelImporter(machine, animationInfo);
    animateSetupMachine(animationInfo, machine,nextMachineIsSameFlag, lastOperationFlag);
  } else {
    //product = findProductByNameAndProductionLineIdentifier(animationInfo.product, animationInfo.productionLineIdentifier);
    animateSetupMachine(animationInfo, machine,nextMachineIsSameFlag,lastOperationFlag);
  }

}


// function addProductToScene(machineSerialNumber) {
//
//   let machine = findMachineBySerialNumber(machineSerialNumber);
//   sphereModelImporter(machine.productionLineIndex);
//
//
// }

// export function manageProductThroughProductionLine(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, lastMachine, animateMachineCondition) {
//
//   if (animateMachineCondition === true) {
//     animateMachine(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, lastMachine);
//     return;
//   }
//
//   if (lastMachine === false) {
//
//     //  animateMachine(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, lastMachine);
//     moveProductFromMachineToNextConveyorBelt(spoon, productionLineIndex, machineIndex);
//     animateProductInConveyorBelt(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, false);
//     //moveProductFromConveyorBeltToNextMachine(spoon, productionLineIndex, machineIndex);
//   } else {
//     if (lastMachine === true) {
//       moveProductFromMachineToNextConveyorBelt(spoon, productionLineIndex, machineIndex);
//       animateProductInConveyorBelt(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, true);
//     }
//   }
//
// }

function findProductInAnimationsArray(productName){

  let aux;

  for(let i=0;i<productsAnimations.length;i++){

    aux = productsAnimations[i];

    if(aux[0].product === productName){
      return aux;
    }
  }
}


function animateSetupMachine(animationInfo, machine, nextMachineIsNextFlag,lastOperationFlag) {

  let productInAnimationsArray = findProductInAnimationsArray(animationInfo.product);

  if (machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].isAvailable && machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].timeSlotCount === animationInfo.timeSlot && productInAnimationsArray.productIsAvailable === true) {

    machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].isAvailable = false;
    productInAnimationsArray.productIsAvailable = false;

    let product = findProductByNameAndProductionLineIdentifier(animationInfo.product, animationInfo.productionLineIdentifier);

    scene.add(product);

    machineOcupationNameDisplayLoader(machine, machine.serialNumber,true,false,animationInfo.operation,animationInfo.product);

    moveProductToNextMachine(product, machine);

    let index;

    let setupTime = parseInt(animationInfo.setupTime);
    let processingTime = parseInt(animationInfo.processingTime);

    if (setupTime > 0) {


      index = findModelChildrenIndexByName(machine.children, 'Button_Off');
      let buttonOff = machine.children[index];
      let originalColor = buttonOff.material.color;

      //changes the colour of the off button to red
      buttonOff.material.color = new THREE.Color(0xff0000);

      setTimeout(function () {
        //reverts the off button color to its original color after setup time is concluded
        let index = findModelChildrenIndexByName(machine.children, 'Button_Off');
        let buttonOff = machine.children[index];
        buttonOff.material.color = originalColor;
        deleteOperationMessageFromScene(machine,animationInfo.operation,animationInfo.product);
        animateProcessingMachine(animationInfo, product, machine, processingTime, nextMachineIsNextFlag, lastOperationFlag);
      }, setupTime * 1000)
    } else {
      animateProcessingMachine(animationInfo, product, machine, processingTime, nextMachineIsNextFlag, lastOperationFlag);
    }
  } else {
    //calls the function again in order to wait for an opportunity to process this animation
    setTimeout(function () {
      animateSetupMachine(animationInfo, machine, nextMachineIsNextFlag,lastOperationFlag);
    }, 1000)
  }
}

function animateProcessingMachine(animationInfo, product, machine, processingTime, nextMachineIsSameFlag, lastOperationFlag) {

  let index;

  machineOcupationNameDisplayLoader(machine, machine.serialNumber,false,true,animationInfo.operation,animationInfo.product);

  index = findModelChildrenIndexByName(machine.children, 'Button_On');
  let buttonOn = machine.children[index];
  let originalColor = buttonOn.material.color;

  //changes the colour of the on button to green
  buttonOn.material.color = new THREE.Color(0x00FF00);

  index = findModelChildrenIndexByName(machine.children, 'Apoio_Solda');

  const supportBeam = machine.children[index];
  const supportBeamInitialPos = {
    x: supportBeam.position.x,
    y: supportBeam.position.y,
    z: supportBeam.position.z,
    rotation: 0
  };
  const supportBeamTarget = supportBeam;


  var supportBeamTween = new TWEEN.Tween(supportBeamInitialPos)
    .to({
      x: supportBeamInitialPos.x,
      y: (supportBeamInitialPos.y - supportBeamAnimationOffsetY),
      z: supportBeamInitialPos.z,
      rotation: 0
    }, (processingTime*1000) / 3)
    .onUpdate(function () {
      supportBeamTarget.position.y = supportBeamInitialPos.y;
    })
    .yoyo(true)
    .repeat(3)
    .start();

  index = findModelChildrenIndexByName(machine.children, 'Prensador001');

  const presser = machine.children[index];
  const presserInitialPos = {x: presser.position.x, y: presser.position.y, z: presser.position.z, rotation: 0};
  const presserTarget = presser;

  var presserTween = new TWEEN.Tween(presserInitialPos)
    .to({
      x: presserInitialPos.x,
      y: presserInitialPos.y - supportBeamAnimationOffsetY,
      z: presserInitialPos.z,
      rotation: 0
    }, (processingTime*1000) / 3)
    .onUpdate(function () {
      presserTarget.position.y = presserInitialPos.y;
    })
    .yoyo(true)
    .repeat(3)
    .onComplete(function () {
      //manageProductThroughProductionLine(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, lastMachine, false);

      //changes the on button back to its original color
      //let originalColor = new THREE.Color(0.21404114365577698, 0.21404114365577698, 0.21404114365577698);
      buttonOn.material.color = originalColor;

      deleteOperationMessageFromScene(machine,animationInfo.operation,animationInfo.product);

      if(lastOperationFlag){
        let spoon = swapSphereModelToSpoonModelIntoFinalConveyorBelt(product,animationInfo);
        animateProductInConveyorBelt(animationInfo, spoon, machine, lastOperationFlag);
      }

      else {

        if (!nextMachineIsSameFlag) {
          moveProductFromMachineToNextConveyorBelt(product, machine.productionLineIndex, machine.slot);
          animateProductInConveyorBelt(animationInfo, product, machine, lastOperationFlag);

        } else {

          let aux = machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].pendingAnimationsArray;

          //updates the machine time slot, removes the completed animation form the machine's pending animation array and frees up the machine for another animation
          machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].timeSlotCount++;
          aux.splice([aux.indexOf(animationInfo)], 1);
          machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].isAvailable = true;
          findProductInAnimationsArray(animationInfo.product).productIsAvailable = true;
        }
      }
    })
    .start();
}


function deleteOperationMessageFromScene(machineScene,operationName,productName) {

  machineScene.children.forEach(function (child) {

    if(child.name === productName + ';' + operationName){
      machineScene.children.splice(machineScene.children.indexOf(child),1);
    }

  })

}

// function animateMachine(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, lastMachine, setupTime, processingTime) {
//
//   const machine = scene.getObjectByName('machine' + productionLineIndex + '.' + machineIndex);
//
//   //animateSmoke(machine);
//
//   let index = findModelChildrenIndexByName(machine.children, 'Apoio_Solda');
//
//   const supportBeam = machine.children[index];
//   const supportBeamInitialPos = {
//     x: supportBeam.position.x,
//     y: supportBeam.position.y,
//     z: supportBeam.position.z,
//     rotation: 0
//   };
//   const supportBeamTarget = supportBeam;
//
//
//   var supportBeamTween = new TWEEN.Tween(supportBeamInitialPos)
//     .to({
//       x: supportBeamInitialPos.x,
//       y: (supportBeamInitialPos.y - supportBeamAnimationOffsetY),
//       z: supportBeamInitialPos.z,
//       rotation: 0
//     }, 3000)
//     .onUpdate(function () {
//       supportBeamTarget.position.y = supportBeamInitialPos.y;
//     })
//     .yoyo(true)
//     .repeat(1)
//     .start();
//
//   index = findModelChildrenIndexByName(machine.children, 'Prensador001');
//
//   const presser = machine.children[index];
//   const presserInitialPos = {x: presser.position.x, y: presser.position.y, z: presser.position.z, rotation: 0};
//   const presserTarget = presser;
//
//   var presserTween = new TWEEN.Tween(presserInitialPos)
//     .to({
//       x: presserInitialPos.x,
//       y: presserInitialPos.y - supportBeamAnimationOffsetY,
//       z: presserInitialPos.z,
//       rotation: 0
//     }, 3000)
//     .onUpdate(function () {
//       presserTarget.position.y = presserInitialPos.y;
//     })
//     .yoyo(true)
//     .repeat(1)
//     .onComplete(function () {
//       manageProductThroughProductionLine(spoon, productionLineIndex, machineIndex, machinesProductionLineCount, lastMachine, false);
//     })
//     .start();
// }

function moveProductFromMachineToNextConveyorBelt(sphere, productionLineIndex, machineIndex) {

  sphere.position.x = machineStartingX + productionLineSeparationDistance * productionLineIndex + productOnConveyorBeltOffsetX;
  sphere.position.y = productOnConveyorBeltOffsetY;
  sphere.position.z = machineStartingZ - (machineSeparationDistance * machineIndex) - productConvBeltStartPosOffsetZ;

  //spoon.position.x = spoon.position.x + productOnConveyorBeltOffsetX;
  //spoon.position.y = productOnConveyorBeltOffsetY;
  //spoon.position.z = spoon.position.z - productConvBeltStartPosOffsetZ;

}

function moveProductToNextMachine(product, machine) {

  product.position.x = machineStartingX + productionLineSeparationDistance * machine.productionLineIndex + productOnMachineLeftSlotOffsetX;
  product.position.y = productOnMachineOffsetY;
  product.position.z = machineStartingZ  - (machineSeparationDistance * machine.slot) + productOnMachineOffsetZ;


}

function swapSphereModelToSpoonModelIntoFinalConveyorBelt(sphereModel, animationInfo){

  let spoonModel = spoonModelImporter(animationInfo);

  spoonModel.position.x = machineStartingX + productionLineSeparationDistance * animationInfo.productionLineIndex + productOnConveyorBeltOffsetX;
  spoonModel.position.y = spoonOnConveyorBeltOffsetY;
  spoonModel.position.z = machineStartingZ - (machineSeparationDistance * animationInfo.machineSlot) - productConvBeltStartPosOffsetZ;

  scene.children.splice(scene.children.indexOf(sphereModel),1,spoonModel);

  return spoonModel;

}

function moveProductFromConveyorBeltToNextMachine(spoon, productionLineIndex, machineIndex) {

  spoon.position.x = machineStartingX + productionLineSeparationDistance * productionLineIndex + productOnMachineLeftSlotOffsetX;
  spoon.position.y = productOnMachineOffsetY;
  spoon.position.z = machineStartingZ - machineSeparationDistance * (machineIndex) + productOnMachineOffsetZ;

  //spoon.position.x = spoon.position.x - productOnConveyorBeltOffsetX;
  //spoon.position.y = productOnMachineOffsetY;
  //spoon.position.z = spoon.position.z - productFromConvBeltToMachineOffsetZ;

}

function animateProductInConveyorBelt(animationInfo, product, machine, lastAnimation) {

  let auxEndZ;

  if(lastAnimation){
    auxEndZ = spoonConvBeltEndPosOffsetZ ;
  }
  else{
    auxEndZ = productConvBeltEndPosOffsetZ;
  }

  const productInitialPos = {x: product.position.x, y: product.position.y, z: product.position.z, rotation: 0};
  const productTarget = product;
  var productTween = new TWEEN.Tween(productInitialPos)
    .to({
      x: productInitialPos.x,
      y: productInitialPos.y,
      z: (product.position.z - auxEndZ),
      rotation: 0
    }, 4000)
    .onUpdate(function () {
      productTarget.position.z = productInitialPos.z;
    })
    .onComplete(function () {
      if (lastAnimation === true) {
        animateSpoonIntoBox(product);
      }

      let aux = machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].pendingAnimationsArray;

      //updates the machine time slot, removes the completed animation form the machine's pending animation array and frees up the machine for another animation
      machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].timeSlotCount++;
      aux.splice([aux.indexOf(animationInfo)], 1);
      machinesAnimationsByProductionLine[machine.productionLineIndex][machine.slot].isAvailable = true;
      findProductInAnimationsArray(animationInfo.product).productIsAvailable = true;

    })
    .start();
}

function animateSpoonIntoBox(product){

  const productInitialPos = {x: product.position.x, y: product.position.y, z: product.position.z, rotation: 0};
  const productTarget = product;
  var productTween = new TWEEN.Tween(productInitialPos)
    .to({
      x: productInitialPos.x,
      y: productInitialPos.y,
      z: (product.position.z - spoonFromConveyorBeltToBoxOffsetZ),
      rotation: 0
    }, 2500)
    .onUpdate(function () {
      productTarget.position.z = productInitialPos.z;
    })
    .onComplete(function () {
      const productInitialPos = {x: 0, y: 0, z: 0};
      const productTarget = product;
      var productTween = new TWEEN.Tween(productInitialPos)
        .to({
          x: productInitialPos.x,
          y: 90,
          z: productInitialPos.z,
        }, 3000)
        .onUpdate(function () {
          productTarget.rotation.y = productInitialPos.y;
        })
        .repeat(0)
        .onComplete(function () {
          const productInitialPos = {x: product.position.x, y: product.position.y, z: product.position.z, rotation: 0};
          const productTarget = product;
          var productTween = new TWEEN.Tween(productInitialPos)
            .to({
              x: productInitialPos.x,
              y: productInitialPos.y - spoonFromConveyorBeltToBoxOffsetY,
              z: productInitialPos.z,
              rotation: 0
            }, 2500)
            .onUpdate(function () {
              productTarget.position.y = productInitialPos.y;
            })
            .start()
        })
        .start()
      })
    .start();

}


// function animateProductInConveyorBelt(product, productionLineIndex, machineIndex, machinesProductionLineCount, lastAnimation) {
//
//   const productInitialPos = {x: product.position.x, y: product.position.y, z: product.position.z, rotation: 0};
//   const productTarget = product;
//   var productTween = new TWEEN.Tween(productInitialPos)
//     .to({
//       x: productInitialPos.x,
//       y: productInitialPos.y,
//       z: (product.position.z - productConvBeltEndPosOffsetZ),
//       rotation: 0
//     }, 4000)
//     //.repeat( Infinity)
//     .onUpdate(function () {
//       productTarget.position.z = productInitialPos.z;
//     })
//     .onComplete(function () {
//       if (lastAnimation === true) {
//         return;
//       }
//       if (machineIndex === machinesProductionLineCount - 2) {
//         moveProductFromConveyorBeltToNextMachine(product, productionLineIndex, machineIndex + 1);
//         manageProductThroughProductionLine(product, productionLineIndex, machineIndex + 1, machinesProductionLineCount, true, true);
//       } else {
//         moveProductFromConveyorBeltToNextMachine(product, productionLineIndex, machineIndex + 1);
//         manageProductThroughProductionLine(product, productionLineIndex, machineIndex + 1, machinesProductionLineCount, false, true);
//       }
//
//     })
//     .start();
// }
