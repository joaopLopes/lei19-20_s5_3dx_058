using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using MasterDataProduction.Core.Dtos.Product;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProduction.Web.Controllers
{
    [Route("mdp/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IRepositoryFactory _repositoryFactory;
        private IProductBuilder _builder;

        public ProductsController(IRepositoryFactory repositoryFactory, IProductBuilder builder)
        {
            _repositoryFactory = repositoryFactory;
            _builder = builder;
        }

        // GET: mdf/products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductSimplifiedDto>>> GetProducts()
        {
            try
            {
                List<ProductSimplifiedDto> dtos = new List<ProductSimplifiedDto>();
                IEnumerable<Product> products = await _repositoryFactory.Product.FindAll().AsTracking().ToListAsync();
                foreach (Product product in products)
                {
                    dtos.Add(product.ToSimplifiedDto());
                }

                return dtos;
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        // GET: mdf/products/spoon
        [HttpGet("{name}")]
        public async Task<ActionResult<ProductDto>> GetProduct(string name)
        {
            try
            {
                var product = await _repositoryFactory.Product.FindById(name);

                return product.ToDto();
            }
            catch (Exception)
            {
                return NotFound("Product " + name + " does not exist.");
            }
        }

        // GET: mdf/products/spoon/fabrication-plan
        [HttpGet("{name}/fabrication-plan")]
        public async Task<ActionResult<FabricationPlanDto>> GetProductsFabricationPlan(string name)
        {
            try
            {
                var product = await _repositoryFactory.Product.FindById(name);


                return product.FabricationPlan.ToDto();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        // POST: mdf/products
        [HttpPost]
        public async Task<ActionResult<ProductSimplifiedDto>> PostProduct(ProductDto productDto)
        {
            try
            {
                _builder.WithDataFromDto(productDto);
                Product product = _builder.Create();

                _repositoryFactory.Product.Create(product);
                await _repositoryFactory.Product.SaveASync();

                return CreatedAtAction(nameof(GetProduct), new {name = product.ProductName.ToString()},
                    product.ToSimplifiedDto());
            }
            catch (Exception e)
            {
                if (e is SqlException || e is DbUpdateException)
                {
                    return BadRequest("Product " + productDto.Name + " already exists.");
                }

                if (productDto.FabricationPlan == null)
                {
                    return BadRequest("No Fabrication Plan defined.");
                }
                return BadRequest(e.Message);
            }
        }
        
        //DELETE mdf/productionlines/1
        [HttpDelete("{productIdentifier}")]
        public async Task<IActionResult> DeleteProduct(string productIdentifier)
        {

            var product = await _repositoryFactory.Product.FindById(productIdentifier);

                if (product == null)
                {
                    return NotFound();
                }

                //Integrity of the database guarantees that only production lines without machines can be deleted, otherwise DB throws exception.
                _repositoryFactory.Product.Delete(product);
                await _repositoryFactory.Product.SaveASync();
                return NoContent();
            }
           
        }
    }
