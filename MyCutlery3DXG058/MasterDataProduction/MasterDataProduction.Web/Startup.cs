﻿using System.Diagnostics.CodeAnalysis;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Persistence;
using MasterDataProduction.Core.Services.Adapters;
using MasterDataProduction.Core.Services.Authorization;
using MasterDataProduction.Core.Services.Builders.Impl;
using MasterDataProduction.Infrastructure.Persistence.Impl;
using MasterDataProduction.Infrastructure.Services.Adapters.Impl;
using MasterDataProduction.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MasterDataProduction.Web
{
   [ExcludeFromCodeCoverage]
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200", "https://localhost:4200",
                                "https://masterdatafactory3dxg058.firebaseapp.com",
                                "https://masterdatafactory3dxg058.web.app").AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
            var connectionString = Configuration.GetConnectionString("Database");
            ;
            services.AddDbContext<RepositoryContext>(opt => opt.UseLazyLoadingProxies().UseSqlServer(connectionString));
            services.AddScoped<IRepositoryFactory, RepositoryFactory>();
            services.AddScoped<IProductBuilder, ProductBuilder>();
            services.AddScoped<IMasterDataFactoryAdapter, MasterDataFactoryAdapter>();
            services.AddScoped<IAuthService, AuthService>();    
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(MyAllowSpecificOrigins);
            app.UseAuthorization();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}