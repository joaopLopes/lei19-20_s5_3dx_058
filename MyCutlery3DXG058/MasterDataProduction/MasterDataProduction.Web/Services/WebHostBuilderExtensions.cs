using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Hosting;

namespace MasterDataProduction.Web.Services
{
   [ExcludeFromCodeCoverage]
    public static class WebHostBuilderExtensions
    {
        //needed to expose port in development
        public static IWebHostBuilder UsePort(this IWebHostBuilder builder)
        {
            var port = Environment.GetEnvironmentVariable("PORT");
            if (!string.IsNullOrEmpty(port))
            {
                return builder;
            }

            return builder.UseUrls($"http://+:{port}");
        }
    }
}