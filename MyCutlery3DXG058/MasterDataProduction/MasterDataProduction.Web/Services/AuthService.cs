using System;
using System.Diagnostics.CodeAnalysis;
using MasterDataProduction.Core.Services.Authorization;

namespace MasterDataProduction.Web.Services
{
   [ExcludeFromCodeCoverage]
    public class AuthService : IAuthService
    {
        private string SessionToken { get; set; }

        public string GetSessionToken()
        {
            if (this.SessionToken == null) throw new Exception("Token not set.");
            return this.SessionToken;
        }

        public void SetSessionToken(string token)
        {
            this.SessionToken = token;
        }
    }
}