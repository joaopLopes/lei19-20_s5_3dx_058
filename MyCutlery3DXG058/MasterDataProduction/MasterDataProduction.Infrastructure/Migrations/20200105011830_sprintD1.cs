﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataProduction.Infrastructure.Migrations
{
    
    public partial class sprintD1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductName_Name = table.Column<string>(nullable: true),
                    Price_Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FabricationPlans",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<long>(nullable: false),
                    ProductionTime = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FabricationPlans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FabricationPlans_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Operation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    ExecutionTime = table.Column<long>(nullable: false),
                    SetupTime = table.Column<long>(nullable: false),
                    Tool = table.Column<string>(nullable: true),
                    FabricationPlanId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Operation_FabricationPlans_FabricationPlanId",
                        column: x => x.FabricationPlanId,
                        principalTable: "FabricationPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FabricationPlans_ProductId",
                table: "FabricationPlans",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Operation_FabricationPlanId",
                table: "Operation",
                column: "FabricationPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_Price_Value",
                table: "Products",
                column: "Price_Value");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductName_Name",
                table: "Products",
                column: "ProductName_Name",
                unique: true,
                filter: "[ProductName_Name] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Operation");

            migrationBuilder.DropTable(
                name: "FabricationPlans");

            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
