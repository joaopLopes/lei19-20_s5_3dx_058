using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using MasterDataProduction.Infrastructure.Persistence.Impl;

namespace MasterDataProduction.Infrastructure.Bootstraps
{
    [ExcludeFromCodeCoverage]
    public static class ProductBootstrap
    {
        public static void Seed(RepositoryContext dbContext)
        {
            List<Operation> operations1 = new List<Operation>();
            List<Operation> operations2 = new List<Operation>();
            operations1.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });
            operations1.Add(new Operation
            {
                Description = "presser",
                Tool = "tool2",
                SetupTime = 2,
                ExecutionTime = 5
            });
            operations2.Add(new Operation
            {
                Description = "molder",
                Tool = "tool3",
                SetupTime = 2,
                ExecutionTime = 5
            });
            operations2.Add(new Operation
            {
                Description = "cutter-2mm",
                Tool = "tool4",
                SetupTime = 2,
                ExecutionTime = 5
            });
            dbContext.Products.Add(
                new Product("Fork", 10, operations1));
            dbContext.Products.Add(
                new Product("Knife", 10, operations2));

            dbContext.SaveChanges();
        }
    }
}