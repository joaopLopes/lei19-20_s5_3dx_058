using System.Diagnostics.CodeAnalysis;
using MasterDataProduction.Infrastructure.Persistence.Impl;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MasterDataProduction.Infrastructure.Bootstraps
{
    [ExcludeFromCodeCoverage]
    public static class RepositoryContextMocker
    {
        public static RepositoryContext GetTodoContext()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            var options = new DbContextOptionsBuilder<RepositoryContext>()
                .UseInMemoryDatabase("MasterDataProductionTestDB").UseInternalServiceProvider(serviceProvider).Options;
            RepositoryContext dbContext = new RepositoryContext(options);
            return dbContext;
        }
    }
}