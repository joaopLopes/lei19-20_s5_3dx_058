using System;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProduction.Infrastructure.Persistence.Impl
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public Task<Product> FindById(string name)
        {
            IQueryable<Product> result =
                this.RepositoryContext.Products.Where(product => product.ProductName.IsEqualTo(name));

            return result.SingleAsync();
        }
    }
}