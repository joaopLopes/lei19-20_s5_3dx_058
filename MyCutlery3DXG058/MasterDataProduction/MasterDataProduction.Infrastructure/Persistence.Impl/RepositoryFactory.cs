using MasterDataProduction.Core.Persistence;

namespace MasterDataProduction.Infrastructure.Persistence.Impl
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private RepositoryContext _repoContext;
        private IProductRepository _product;

        public RepositoryFactory(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public IProductRepository Product
        {
            get
            {
                if (_product == null)
                {
                    _product = new ProductRepository(_repoContext);
                }

                return _product;
            }
        }
    }
}