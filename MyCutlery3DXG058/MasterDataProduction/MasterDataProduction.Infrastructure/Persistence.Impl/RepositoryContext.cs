using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using Microsoft.EntityFrameworkCore;


namespace MasterDataProduction.Infrastructure.Persistence.Impl
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<FabricationPlan> FabricationPlans { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>(new Product.ProductConfiguration().Configure);
            modelBuilder.Entity<FabricationPlan>(new FabricationPlan.FabricationPlanConfiguration().Configure);
        }
    }
}