using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using MasterDataProduction.Core.Services.Adapters;
using MasterDataProduction.Core.Services.Authorization;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace MasterDataProduction.Infrastructure.Services.Adapters.Impl
{
    [ExcludeFromCodeCoverage]
    public class MasterDataFactoryAdapter : IMasterDataFactoryAdapter
    {
        
        private HttpClient _client;
        private IConfiguration config;

        public MasterDataFactoryAdapter(IConfiguration configuration, IAuthService authService)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
            {
                return true;
            };
            this._client = new HttpClient(clientHandler);

            this._client.DefaultRequestHeaders.Add("token", authService.GetSessionToken());
            this.config = configuration;
        }

        public async Task<Operation> GetOperationById(string id)
        {
            string mdf = this.config.GetConnectionString("mdf") + "mdf/operations/";
            HttpResponseMessage response = await _client.GetAsync(mdf + id);

            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new ArgumentException("Operation " + id + " does not exist.");
            }

            var content = await response.Content.ReadAsStringAsync();
            Operation operation = JsonConvert.DeserializeObject<Operation>(content);


            return operation;
        }
    }
}