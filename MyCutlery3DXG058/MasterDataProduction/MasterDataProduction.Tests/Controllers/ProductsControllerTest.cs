using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using FakeItEasy;
using MasterDataProduction.Core.Dtos.Product;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using MasterDataProduction.Core.Services.Adapters;
using MasterDataProduction.Core.Services.Builders.Impl;
using MasterDataProduction.Infrastructure.Bootstraps;
using MasterDataProduction.Infrastructure.Persistence.Impl;
using MasterDataProduction.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace MasterDataProduction.Tests.Controllers
{

    public class ProductsControllerTest
    {
        [Fact]
        public void EnsureCreateProductCreatesProductSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange

                ProductSimplifiedDto expected = new ProductSimplifiedDto
                {
                    Name = "Spoon",
                    Price = 0,
                    ProductionTime = 0
                };
                List<string> operations = new List<string>();
                operations.Add("cutter");
                ProductDto input = new ProductDto
                {
                    Name = "Spoon",
                    FabricationPlan = new FabricationPlanDto
                    {
                        Operations = operations
                    }
                };
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                A.CallTo(() => adapter.GetOperationById("cutter")).Returns(new Operation
                {
                    Description = "cutter",
                    Tool = "tool1",
                    SetupTime = 2,
                    ExecutionTime = 5
                });
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                controllerTest.PostProduct(input);
                ProductDto result = controllerTest.GetProduct("Spoon").Result.Value;
                //Assert
                Assert.Equal(input, result);
            }
        }

        [Fact]
        public void EnsureGetProductRetrievesProductSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                List<string> operations = new List<string>();
                operations.Add("molder");
                operations.Add("cutter-2mm");
                ProductDto input = new ProductDto
                {
                    Name = "Knife",
                    Price = 10,
                    FabricationPlan = new FabricationPlanDto
                    {
                        Operations = operations
                    }
                };
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                ProductDto result = controllerTest.GetProduct("Knife").Result.Value;
                //Assert
                Assert.Equal(input, result);
            }
        }

        [Fact]
        public void EnsureGetProductsRetrievesAllProductsSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange

                ProductSimplifiedDto dto1 = new ProductSimplifiedDto
                {
                    Name = "Fork",
                    Price = 10
                };
                ProductSimplifiedDto dto2 = new ProductSimplifiedDto
                {
                    Name = "Knife",
                    Price = 10
                };

                List<ProductSimplifiedDto> expected = new List<ProductSimplifiedDto>();
                expected.Add(dto1);
                expected.Add(dto2);
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                IEnumerable<ProductSimplifiedDto> result = controllerTest.GetProducts().Result.Value;
                //Assert
                Assert.Equal(expected, result);
            }
        }

        [Fact]
        public void EnsureGetProductsProductionPlanRetrievesProductionPlanSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                List<string> operations = new List<string>();
                operations.Add("cutter");
                operations.Add("presser");
                FabricationPlanDto expected = new FabricationPlanDto
                {
                    Operations = operations
                };

                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                FabricationPlanDto result = controllerTest.GetProductsFabricationPlan("Fork").Result.Value;
                //Assert
                Assert.Equal(expected, result);
            }
        }

        [Fact]
        public void EnsureGetProductsProductionPlanThrowsExceptionWhenProductDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange

                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                FabricationPlanDto result = controllerTest.GetProductsFabricationPlan("Fake").Result.Value;
                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public void EnsureGetProductIsNullWhenProductDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange

                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                ProductDto result = controllerTest.GetProduct("Fake").Result.Value;
                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public void EnsureGetProductsIsNullWhenProductsDoNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                IEnumerable<ProductSimplifiedDto> result = controllerTest.GetProducts().Result.Value;
                //Assert
                Assert.Empty(result);
            }
        }

        [Fact]
        public void EnsureDeleteProductCanNotDeleteWhenProductDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                //Act
                Task<IActionResult> result = controllerTest.DeleteProduct("productDoesNotExist");

               
                //Assert
                Assert.Equal(result.Status, TaskStatus.Faulted);
            }
            
        }

        [Fact]
        public void EnsurePostProductThrowsExceptionWhenIdAlreadyExists()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                List<string> operations = new List<string>();
                operations.Add("cutter");
                
                ProductDto input = new ProductDto
                {
                    Name = "Knife",
                    Price = 10,
                    FabricationPlan = new FabricationPlanDto
                    {
                        Operations = operations
                    }
                };
                //Act
                ProductSimplifiedDto result = controllerTest.PostProduct(input).Result.Value;
                //Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public void EnsurePostProductThrowsExceptionWhenIdIsInvalid()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                //Create Mock Adapter
                IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
                ProductBootstrap.Seed(context);
                ProductsController controllerTest =
                    new ProductsController(new RepositoryFactory(context), new ProductBuilder(adapter));
                List<string> operations = new List<string>();
                operations.Add("cutter");
                ProductDto input = new ProductDto
                {
                    Name = "Smart Spoon Master One Thousand",
                    Price = 10,
                    FabricationPlan = new FabricationPlanDto
                    {
                        Operations = operations
                    }
                };
                //Act
                ProductSimplifiedDto result = controllerTest.PostProduct(input).Result.Value;
                //Assert
                Assert.Null(result);
            }
        }
    }
}