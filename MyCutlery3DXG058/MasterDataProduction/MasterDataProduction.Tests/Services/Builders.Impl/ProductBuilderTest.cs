using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using FakeItEasy;
using MasterDataProduction.Core.Dtos.Product;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using MasterDataProduction.Core.Services.Adapters;
using MasterDataProduction.Core.Services.Builders.Impl;
using Xunit;

namespace MasterDataProduction.Tests.Services.Builders.Impl
{

    public class ProductBuilderTest
    {
        [Fact]
        public void EnsureBuildingProductFromDtoUsesCorrectData()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });
            List<string> operationIds = new List<string>();
            operationIds.Add("cutter");

            ProductDto dto = new ProductDto
            {
                Name = "Spoon",
                FabricationPlan = new FabricationPlanDto
                {
                    Operations = operationIds
                }
            };

            //Create Mock Adapter
            IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
            A.CallTo(() => adapter.GetOperationById("cutter")).Returns(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product expected = new Product("Spoon", 10, operations);
            //Act
            IProductBuilder builder = new ProductBuilder(adapter);
            builder.WithDataFromDto(dto);
            Product result = builder.Create();

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void EnsureBuildingProductFromDtoThrowsErrorWhenOperationDoesNotExistInMdf()
        {
            //Arrange
            List<string> operationIds = new List<string>();
            operationIds.Add("Spoon");
            ProductDto dto = new ProductDto
            {
                Name = "Spoon",
                FabricationPlan = new FabricationPlanDto
                {
                    Operations = operationIds
                }
            };

            //Create Mock Adapter
            IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
            A.CallTo(() => adapter.GetOperationById("Spoon"))
                .Throws(new ArgumentException("Operation Spoon does not exist."));
            //Act
            IProductBuilder builder = new ProductBuilder(adapter);

            //Assert
            string message = Assert.Throws<ArgumentException>(() => builder.WithDataFromDto(dto)).Message;
            Assert.Equal("Operation Spoon does not exist.", message);
        }

        [Fact]
        public void EnsureBuildingProductFromDtoThrowsErrorWhenListHasNoOperations()
        {
            //Arrange
            List<string> operationIds = new List<string>();
            ProductDto dto = new ProductDto
            {
                Name = "Spoon",
                FabricationPlan = new FabricationPlanDto
                {
                    Operations = operationIds
                }
            };

            //Create Mock Adapter
            IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
            A.CallTo(() => adapter.GetOperationById("Spoon")).Returns(new Operation
            {
                Description = "Spoon"
            });
            //Act
            IProductBuilder builder = new ProductBuilder(adapter);

            //Assert
            string message = Assert.Throws<ArgumentException>(() => builder.WithDataFromDto(dto)).Message;
            Assert.Equal("Fabrication Plan must have atleast one operation.", message);
        }


        [Fact]
        public void EnsureBuildingProductFromDtoThrowsErrorWhenOperationDoesNotExistInMdf2()
        {
            //Arrange
            List<string> operationIds = new List<string>();
            operationIds.Add("Spoon");
            ProductDto dto = new ProductDto
            {
                Name = "Spoon",
                FabricationPlan = new FabricationPlanDto
                {
                    Operations = operationIds
                }
            };
            Operation result = null;
            //Create Mock Adapter
            IMasterDataFactoryAdapter adapter = A.Fake<IMasterDataFactoryAdapter>();
            A.CallTo(() => adapter.GetOperationById("Spoon"))
                .Returns(result);
            //Act
            IProductBuilder builder = new ProductBuilder(adapter);

            //Assert
            string message = Assert.Throws<ArgumentException>(() => builder.WithDataFromDto(dto)).Message;
            Assert.Equal("Operation Spoon does not exist.", message);
        }
    }
}