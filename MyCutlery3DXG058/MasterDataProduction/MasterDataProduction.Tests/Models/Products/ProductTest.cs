using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using MasterDataProduction.Core.Dtos.Product;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using Xunit;

namespace MasterDataProduction.Tests.Models.Products
{
   
    public class ProductTest
    {
        [Fact]
        public void EnsureCreatingProductWithNullNameThrowsException()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter"
            });
            //Act
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Product(null, 10, operations)).Message;
            Assert.Equal("Product Name must not be empty.", message);
        }

        [Fact]
        public void EnsureCreatingProductWithNameWithWhitrSpacesThrowsException()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter 1000"
            });
            //Act
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Product("cutter 1000", 10, operations)).Message;
            Assert.Equal("Product name must not contain spaces.", message);
        }

        [Fact]
        public void EnsureCreatingProductWithNullOperationsThrowsException()
        {
            //Arrange
            //Act
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Product("Spoon", 10, null)).Message;
            Assert.Equal("Fabrication Plan must have atleast one operation.", message);
        }

        [Fact]
        public void EnsureCreatingProductWithEmptyOperationsThrowsException()
        {
            //Arrange
            //Act
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Product("Spoon", 10, new List<Operation>()))
                .Message;
            Assert.Equal("Fabrication Plan must have atleast one operation.", message);
        }

        [Fact]
        public void EnsureToDtoCreatesCorrectDto()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });
            List<string> operationIds = new List<string>();
            operationIds.Add("cutter");

            ProductDto expected = new ProductDto
            {
                Name = "Spoon",
                Price = 10,
                FabricationPlan = new FabricationPlanDto
                {
                    Operations = operationIds
                }
            };
            //Act
            Product product = new Product("Spoon", 10, operations);
            ProductDto result = product.ToDto();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void EnsureEqualsReturnsTrueWhenObjectsAreEqual()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            Product prod2 = new Product("Spoon", 10, operations);
            //Act
            bool result = prod1.Equals(prod2);
            //Assert
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsReturnsTrueWhenNamesAreEqualButOperationsAreNot()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });
            List<Operation> operations2 = new List<Operation>();
            operations2.Add(new Operation
            {
                Description = "presser",
                Tool = "tool2",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            Product prod2 = new Product("Spoon", 10, operations2);
            //Act
            bool result = prod1.Equals(prod2);
            //Assert
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseWhenNamesAreNotEqual()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            Product prod2 = new Product("Fork", 10, operations);
            //Act
            bool result = prod1.Equals(prod2);
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseWhenOtherIsNull()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            Product prod2 = null;
            //Act
            bool result = prod1.Equals(prod2);
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseWhenOtherIsDifferentObject()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            string prod2 = "test";
            //Act
            bool result = prod1.Equals(prod2);
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureHashCodeReturnsSameValueForSameObject()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            Product prod2 = new Product("Spoon", 10, operations);
            //Act
            int hash1 = prod1.GetHashCode();
            int hash2 = prod2.GetHashCode();
            //Assert
            Assert.Equal(hash1, hash2);
        }

        [Fact]
        public void EnsureHashCodeReturnsDifferentValueForDifferentObject()
        {
            //Arrange
            List<Operation> operations = new List<Operation>();
            operations.Add(new Operation
            {
                Description = "cutter",
                Tool = "tool1",
                SetupTime = 2,
                ExecutionTime = 5
            });

            Product prod1 = new Product("Spoon", 10, operations);
            Product prod2 = new Product("Fork", 10, operations);
            //Act
            int hash1 = prod1.GetHashCode();
            int hash2 = prod2.GetHashCode();
            //Assert
            Assert.NotEqual(hash1, hash2);
        }
        
        [Fact]
        public void EnsureProductDtoEqualAreSameHashCode()
        {
            ICollection<string> operations = new List<string>();
            operations.Add("cut");
            operations.Add("press");
            FabricationPlanDto fabricationPlanDto = new FabricationPlanDto();
            fabricationPlanDto.Operations = operations;
            ProductDto productDto = new ProductDto();
            productDto.Name = "knife";
            productDto.Price = 10;
            productDto.FabricationPlan = fabricationPlanDto;
            ProductDto productDto2 = new ProductDto();
            productDto2.Name = "knife";
            productDto2.Price = 10;
            productDto2.FabricationPlan = fabricationPlanDto;
            Assert.Equal(productDto.GetHashCode(),productDto2.GetHashCode());

        }
        
        [Fact]
        public void EnsureProductDtoNotEqualAreNotSameHashCode()
        {
            ICollection<string> operations = new List<string>();
            operations.Add("cut");
            operations.Add("press");
            FabricationPlanDto fabricationPlanDto = new FabricationPlanDto();
            fabricationPlanDto.Operations = operations;
            ProductDto productDto = new ProductDto();
            productDto.Name = "spoon";
            productDto.Price = 10;
            productDto.FabricationPlan = fabricationPlanDto;
            ProductDto productDto2 = new ProductDto();
            productDto2.Name = "knife";
            productDto2.Price = 12;
            productDto2.FabricationPlan = fabricationPlanDto;
            Assert.NotEqual(productDto.GetHashCode(),productDto2.GetHashCode());

        }

        [Fact]
        public void EnsureProductSimplifiedDtoEqualAreSameHashCode()
        {
            ProductSimplifiedDto productSimplifiedDto = new ProductSimplifiedDto();
            productSimplifiedDto.Name = "knife";
            productSimplifiedDto.Price = 10;
            productSimplifiedDto.ProductionTime = 10;
            ProductSimplifiedDto productSimplifiedDto2 = new ProductSimplifiedDto();
            productSimplifiedDto2.Name = "knife";
            productSimplifiedDto2.Price = 10;
            productSimplifiedDto.ProductionTime = 10;
            Assert.Equal(productSimplifiedDto.GetHashCode(),productSimplifiedDto.GetHashCode());
        }

        [Fact]
        public void EnsureProductSimplifiedDtoDifferentAreDifferentHashCode()
        {
            ProductSimplifiedDto productSimplifiedDto = new ProductSimplifiedDto();
            productSimplifiedDto.Name = "knife";
            productSimplifiedDto.Price = 10;
            productSimplifiedDto.ProductionTime = 10;
            ProductSimplifiedDto productSimplifiedDto2 = new ProductSimplifiedDto();
            productSimplifiedDto2.Name = "spoon";
            productSimplifiedDto2.Price = 11;
            productSimplifiedDto.ProductionTime = 11;
            Assert.NotEqual(productSimplifiedDto.GetHashCode(),productSimplifiedDto2.GetHashCode());
        }

        [Fact]
        public void EnsureGetProductSimplifiedDTOIsGottenWithSuccess()
        {
            ProductSimplifiedDto productSimplifiedDto = new ProductSimplifiedDto();
            productSimplifiedDto.Name = "knife";
            productSimplifiedDto.Price = 10;
            productSimplifiedDto.ProductionTime = 10;
            long expected = 10;
            long result = productSimplifiedDto.ProductionTime;
            Assert.Equal(result,expected);

        }
        
    }
    
    
}