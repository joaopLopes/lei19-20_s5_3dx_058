namespace MasterDataProduction.Core.Persistence
{
    public interface IRepositoryFactory
    {
        IProductRepository Product { get; }
    }
}