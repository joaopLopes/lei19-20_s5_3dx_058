using System.Threading.Tasks;
using MasterDataProduction.Core.Models.Products;

namespace MasterDataProduction.Core.Persistence
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        Task<Product> FindById(string name);
    }
}