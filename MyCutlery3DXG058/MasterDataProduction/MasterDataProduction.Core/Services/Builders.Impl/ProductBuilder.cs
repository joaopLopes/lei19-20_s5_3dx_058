using System;
using System.Collections.Generic;
using MasterDataProduction.Core.Dtos.Product;
using MasterDataProduction.Core.Models.Products;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using MasterDataProduction.Core.Services.Adapters;

namespace MasterDataProduction.Core.Services.Builders.Impl
{
    public class ProductBuilder : IProductBuilder
    {
        private Product _product;
        private IMasterDataFactoryAdapter _adapter;

        public ProductBuilder(IMasterDataFactoryAdapter adapter)
        {
            this._adapter = adapter;
        }

        public void WithDataFromDto(ProductDto dto)
        {
            ICollection<Operation> operations = new List<Operation>();
            foreach (string operationId in dto.FabricationPlan.Operations)
            {
                Operation aux = _adapter.GetOperationById(operationId).Result;
                if (aux == null)
                {
                    throw new ArgumentException("Operation " + operationId + " does not exist.");
                }

                operations.Add(aux);
            }

            this._product = new Product(dto.Name, dto.Price, operations);
        }

        public Product Create()
        {
            return _product;
        }
    }
}