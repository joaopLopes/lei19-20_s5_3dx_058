using System.Threading.Tasks;
using MasterDataProduction.Core.Models.Products.FabricationPlans;

namespace MasterDataProduction.Core.Services.Adapters
{
    public interface IMasterDataFactoryAdapter
    {
        Task<Operation> GetOperationById(string id);
    }
}