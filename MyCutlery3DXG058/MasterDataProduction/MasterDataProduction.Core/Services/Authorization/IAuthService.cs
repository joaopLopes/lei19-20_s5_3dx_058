using System;

namespace MasterDataProduction.Core.Services.Authorization
{
    public interface IAuthService
    {
        string GetSessionToken();

        void SetSessionToken(string token);
    }
}