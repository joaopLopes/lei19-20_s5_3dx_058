using System.Collections.Generic;
using System.Linq;

namespace MasterDataProduction.Core.Dtos.Product
{
    public class FabricationPlanDto
    {
        public ICollection<string> Operations { get; set; }

        protected bool Equals(FabricationPlanDto other)
        {
            return this.Operations.SequenceEqual(other.Operations);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((FabricationPlanDto) obj);
        }

        public override int GetHashCode()
        {
            return (Operations != null ? Operations.GetHashCode() : 0);
        }
    }
}