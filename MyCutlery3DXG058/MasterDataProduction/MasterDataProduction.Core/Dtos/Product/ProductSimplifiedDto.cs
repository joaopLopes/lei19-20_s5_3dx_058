using System;

namespace MasterDataProduction.Core.Dtos.Product
{
    public class ProductSimplifiedDto
    {
        public string Name { get; set; }
        public ulong Price { get; set; }
        
        public long ProductionTime { get; set; }

        protected bool Equals(ProductSimplifiedDto other)
        {
            return Name == other.Name && Price == other.Price;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProductSimplifiedDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ Price.GetHashCode();
            }
        }
    }
}