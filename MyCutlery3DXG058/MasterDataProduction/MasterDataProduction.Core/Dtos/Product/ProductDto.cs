using System;

namespace MasterDataProduction.Core.Dtos.Product
{
    public class ProductDto
    {
        public string Name { get; set; }
        public ulong Price { get; set; }
        public FabricationPlanDto FabricationPlan { get; set; }


        protected bool Equals(ProductDto other)
        {
            return string.Equals(Name, other.Name, StringComparison.OrdinalIgnoreCase) &&
                   FabricationPlan.Equals(other.FabricationPlan) && other.Price.Equals(this.Price);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProductDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Price.GetHashCode();
                hashCode = (hashCode * 397) ^ (FabricationPlan != null ? FabricationPlan.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}