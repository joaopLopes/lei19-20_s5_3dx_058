using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace MasterDataProduction.Core.Models
{
    [ExcludeFromCodeCoverage]
    public abstract class Entity
    {
        public long Id { get; protected set; }
    }
}