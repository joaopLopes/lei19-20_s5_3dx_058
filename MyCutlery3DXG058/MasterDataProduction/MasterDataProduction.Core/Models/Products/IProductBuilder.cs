using MasterDataProduction.Core.Dtos.Product;

namespace MasterDataProduction.Core.Models.Products
{
    public interface IProductBuilder
    {
        void WithDataFromDto(ProductDto dto);
        Products.Product Create();
    }
}