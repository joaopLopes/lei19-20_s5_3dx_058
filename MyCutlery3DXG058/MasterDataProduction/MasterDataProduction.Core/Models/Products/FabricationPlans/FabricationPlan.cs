using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using MasterDataProduction.Core.Dtos.Product;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataProduction.Core.Models.Products.FabricationPlans
{
    public class FabricationPlan : Entity
    {
        internal virtual ICollection<Operation> Operations { get; set; }

        protected internal virtual Product Product { get; protected set; }

        protected internal long ProductId { get; protected set; }

        protected internal virtual long ProductionTime { get; protected set; }

        public FabricationPlan(Product product, ICollection<Operation> operations)
        {
            if (operations == null || operations.Count == 0)
            {
                throw new ArgumentException("Fabrication Plan must have atleast one operation.");
            }

            this.Operations = new List<Operation>();
            this.Product = product;
            this.ProductId = product.Id;
            foreach (Operation op in operations)
            {
                AddOperation(op);
            }

            this.SetProductionTime();
        }

        [ExcludeFromCodeCoverage]
        protected FabricationPlan()
        {
            //for ef only
        }

        /**
         * Adds an Operation to the Fabrication Plan
         */
        private void AddOperation(Operation operation)
        {
            this.Operations.Add(operation);
        }

        public class FabricationPlanConfiguration : IEntityTypeConfiguration<FabricationPlan>
        {
            public void Configure(EntityTypeBuilder<FabricationPlan> builder)
            {
                builder.HasKey(product => product.Id);
                builder.OwnsMany(p => p.Operations, op =>
                {
                    op.HasKey(operation => operation.Id);
                    op.Property(operation => operation.Description);
                    op.Property(operation => operation.ExecutionTime);
                    op.Property(operation => operation.SetupTime);
                    op.Property(operation => operation.Tool);
                });
                builder.Property(p => p.ProductionTime);
            }
        }

        private void SetProductionTime()
        {
            string previousTool = null;
            long ret = 0;
            foreach (var vOperation in Operations)
            {
                if (!vOperation.Tool.Equals(previousTool))
                {
                    ret += (vOperation.SetupTime + vOperation.ExecutionTime);
                    previousTool = vOperation.Tool;
                }
                else
                {
                    ret += vOperation.ExecutionTime;
                    previousTool = vOperation.Tool;
                }
            }

            this.ProductionTime = ret;
        }

        public FabricationPlanDto ToDto()
        {
            List<string> operations = new List<string>();
            foreach (Operation op in this.Operations)
            {
                operations.Add(op.Description);
            }

            FabricationPlanDto fabPlan = new FabricationPlanDto
            {
                Operations = operations
            };
            return fabPlan;
        }
    }
}