using System.Diagnostics.CodeAnalysis;

namespace MasterDataProduction.Core.Models.Products.FabricationPlans
{
    /**
     * Acts as a DTO
     */
    public class Operation
    {
        public string Description { get; set; }
        
        public long ExecutionTime { get; set; }
        
        public long SetupTime { get; set; }
        
        public string Tool { get; set; }

        [ExcludeFromCodeCoverage] public long Id { get; private set; }

        [ExcludeFromCodeCoverage]
        public Operation()
        {
            //for ef
        }
    }
}