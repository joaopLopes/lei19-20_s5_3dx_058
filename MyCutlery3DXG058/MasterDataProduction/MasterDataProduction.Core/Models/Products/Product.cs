using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using MasterDataProduction.Core.Dtos.Product;
using MasterDataProduction.Core.Models.Products.FabricationPlans;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace MasterDataProduction.Core.Models.Products
{
    public class Product : Entity
    {
        public virtual ProductName ProductName { get; protected set; }
        protected virtual Price Price { get; set; }
        public virtual FabricationPlan FabricationPlan { get; protected set; }
        
        public Product(string name, ulong price, ICollection<Operation> operations)
        {
            this.ProductName = new ProductName(name);
            this.Price = new Price(price);
            this.FabricationPlan = new FabricationPlan(this, operations);
        }

        [ExcludeFromCodeCoverage]
        protected Product()
        {
            //for ef only
        }

        /**
         * Dto with all data
         */
        public ProductDto ToDto()
        {
            return new ProductDto
            {
                FabricationPlan = this.FabricationPlan.ToDto(),
                Name = this.ProductName.Name,
                Price = this.Price.Value
            };
        }

        /**
         * Dto with Id and Name only.
         */
        public ProductSimplifiedDto ToSimplifiedDto()
        {
            return new ProductSimplifiedDto()
            {
                Name = this.ProductName.Name,
                Price = this.Price.Value,
                ProductionTime =  this.FabricationPlan.ProductionTime
            };
        }

        protected bool Equals(Product other)
        {
            return Equals(ProductName, other.ProductName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Product) obj);
        }

        public override int GetHashCode()
        {
            return (ProductName != null ? ProductName.GetHashCode() : 0);
        }

        public class ProductConfiguration : IEntityTypeConfiguration<Product>
        {
            public void Configure(EntityTypeBuilder<Product> builder)
            {
                builder.HasKey(product => product.Id);
                builder.OwnsOne(product => product.ProductName).HasIndex(name => name.Name).IsUnique();
                builder.OwnsOne(product => product.Price).HasIndex(price => price.Value);
                builder.HasOne(product => product.FabricationPlan).WithOne(plan => plan.Product)
                    .HasForeignKey<FabricationPlan>(plan => plan.ProductId);
            }
        }
    }
}