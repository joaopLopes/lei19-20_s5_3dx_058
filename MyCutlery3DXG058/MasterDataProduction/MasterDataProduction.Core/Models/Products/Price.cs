using System;

namespace MasterDataProduction.Core.Models.Products
{
    public class Price
    {
        /**
         * Value in cents.
         */
        internal ulong Value { get; set; }

        /**
         * Should receive the value in cents.
         */
        public Price(ulong value)
        {
            this.Value = value;
        }
    }
}