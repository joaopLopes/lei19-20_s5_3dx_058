using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MasterDataProduction.Core.Models.Products
{
    public class ProductName : ValueObject
    {
        internal string Name { get; set; }

        internal ProductName(string name)
        {
            if (!String.IsNullOrEmpty(name))
            {
                if (name.Trim().Contains(" "))
                {
                    throw new ArgumentException("Product name must not contain spaces.");
                }

                Name = name.Trim();
            }
            else
            {
                throw new ArgumentException("Product Name must not be empty.");
            }
        }

        [ExcludeFromCodeCoverage]
        protected ProductName()
        {
            //for ef only
        }


        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
        }

        public bool IsEqualTo(string name)
        {
            return this.Name.Equals(name);
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}