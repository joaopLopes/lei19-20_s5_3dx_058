using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Web.Controllers
{
    [Route("mdf/[controller]")]
    [ApiController]
    public class OperationsController : ControllerBase
    {
        private IRepositoryFactory _repositoryFactory;
        private IOperationBuilder _builder;

        public OperationsController(IRepositoryFactory repositoryFactory, IOperationBuilder builder)
        {
            _repositoryFactory = repositoryFactory;
            _builder = builder;
        }

        // GET: mdf/operations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OperationDto>>> GetOperations()
        {
            try
            {
                List<OperationDto> dtos = new List<OperationDto>();
                IEnumerable<Operation> operations = await _repositoryFactory.Operation.FindAll().ToListAsync();
                foreach (Operation op in operations)
                {
                    dtos.Add(op.ToDto());
                }

                return dtos;
            }
            catch (Exception exception)
            {
                return NotFound(exception.Message);
            }
        }

        // GET: mdf/operations/cutter-5mm
        [HttpGet("{id}")]
        public async Task<ActionResult<OperationDto>> GetOperation(string id)
        {
            try
            {
                var operation = await _repositoryFactory.Operation.FindById(id);

                if (operation == null)
                {
                    return NotFound();
                }

                return operation.ToDto();
            }
            catch (Exception exception)
            {
                return NotFound(exception.Message);
            }
        }

        // POST: mdf/operations
        [HttpPost]
        public async Task<ActionResult<OperationDto>> PostOperation(OperationDto operationDto)
        {
            try
            {
                _builder.WithDataFromDto(operationDto);
                Operation operation = _builder.Create();

                _repositoryFactory.Operation.Create(operation);
                await _repositoryFactory.Operation.SaveASync();

                return CreatedAtAction(nameof(GetOperation), new {id = operation.Id}, operation.ToDto());
            }
            catch (ArgumentException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (Exception)
            {
                return BadRequest("An operation with that description already exists.");
            }
        }
        
        
        //DELETE mdf/operations/1
        [HttpDelete("{description}")]
        public async Task<IActionResult> DeleteOperation(string description)
        {
            var operation = await _repositoryFactory.Operation.FindById(description);
    
            if (operation == null)
            {
                return NotFound();
            }
    
            _repositoryFactory.Operation.Delete(operation);
            await _repositoryFactory.Operation.SaveASync();
    
            return NoContent();
        }
        
    }
}