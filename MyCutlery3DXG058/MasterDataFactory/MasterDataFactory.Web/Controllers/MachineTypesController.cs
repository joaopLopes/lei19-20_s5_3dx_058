﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Web.Controllers
{
    [Route("mdf/[controller]")]
    [ApiController]
    public class MachineTypesController : ControllerBase
    {
        private IRepositoryFactory _repositoryFactory;
        private IMachineTypeBuilder _builder;

        public MachineTypesController(IRepositoryFactory repositoryFactory, IMachineTypeBuilder builder)
        {
            _repositoryFactory = repositoryFactory;
            _builder = builder;
        }

        // GET: mdf/machinetypes
        [HttpGet]
        public async Task<ActionResult<List<MachineTypeDto>>> getMachineTypes()
        {
            List<MachineType> list = (await _repositoryFactory.MachineType.FindAll().AsTracking().ToListAsync());
            List<MachineTypeDto> returnList = new List<MachineTypeDto>();
            for (int i = 0; i < list.Count; i++)
            {
                returnList.Add(list[i].ToDto());
            }

            return returnList;
        }

        // GET: mdf/machinetypes/1
        [HttpGet("{name}")]
        public async Task<ActionResult<MachineTypeDto>> GetMachineType(string name)
        {
            try
            {
                var todoItem = await _repositoryFactory.MachineType.FindByStringId(name);

                return todoItem.ToDto();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: mdf/machinetypes
        [HttpPost]
        public async Task<ActionResult<MachineTypeDto>> PostMachineType(MachineTypeDto mactypedto)
        {
            try
            {
                MachineType mactype = _builder.BuildMachineType(mactypedto);

                _repositoryFactory.MachineType.Create(mactype);
                await _repositoryFactory.MachineType.SaveASync();

                MachineTypeDto to = mactype.ToDto();

                return CreatedAtAction("GetMachineType", new {name = to.Name}, to);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        //Put: mdf/machineTypes
        [HttpPut]
        [Route("{name}")]
        public async Task<ActionResult<MachineTypeChangeOperationReturnDTO>> ChangeOperationsOfMachineType(
            MachineTypeChangeOperationDto updatedMachineTypeDto, string name)
        {
            try
            {
                MachineType foundMachineType =
                    this._repositoryFactory.MachineType.FindByStringId(name).Result;
                if (foundMachineType == null)
                {
                    return NotFound();
                }

                List<string> listIdsOperations = new List<string>();

                foreach (string operationId in updatedMachineTypeDto.ListMacTypeOperations)
                {
                    listIdsOperations.Add(operationId);
                }

                ICollection<MachineTypeOperation> updateMachineTypeOperations =
                    _builder.BuildListOperations(listIdsOperations, foundMachineType);
                await _repositoryFactory.MachineType.UpdateMachineTypesOperations(updatedMachineTypeDto.Name,
                    updateMachineTypeOperations);
                await _repositoryFactory.MachineType.SaveASync();
                MachineTypeChangeOperationReturnDTO dto = foundMachineType.ToChangeOperationReturnDto();
                return dto;
            }
            catch (ArgumentException)
            {
                return BadRequest("Invalid operation list, the operation list can not contain duplicated operations!");
            }
        }

        [HttpGet]
        [Route("{machineTypeId}/operations")]
        public async Task<ActionResult<List<OperationDto>>> ConsultOperationsOfMachineType(string machineTypeId)
        {
            try
            {
                MachineType machineType = await this._repositoryFactory.MachineType.FindByStringId(machineTypeId);
                if (machineType == null)
                {
                    return NotFound();
                }

                return machineType.OperationDtos();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }


        //DELETE mdf/machinetypes/presser f.e
        [HttpDelete("{name}")]
        public async Task<IActionResult> DeleteMachineType(string name)
        {
            try
            {
                var operation = await _repositoryFactory.MachineType.FindByStringId(name);

                if (operation == null)
                {
                    return NotFound();
                }

                _repositoryFactory.MachineType.Delete(operation);
                await _repositoryFactory.MachineType.SaveASync();

                return NoContent();
            }
            catch (DbUpdateException)
            {
                return BadRequest(
                    "This machine type is being used in a machine! Please delete the machines using this machine type before deleting this machine type.");
            }
        }

    }
}