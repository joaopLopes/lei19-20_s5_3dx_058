﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Castle.Core.Internal;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Web.Controllers
{
    [Route("mdf/[controller]")]
    [ApiController]
    public class MachinesController : ControllerBase
    {
        private readonly IRepositoryFactory _repositoryFactory;
        private readonly IMachineBuilder _machineBuilder;

        public MachinesController(IRepositoryFactory repositoryFactory, IMachineBuilder machineBuilder)
        {
            _repositoryFactory = repositoryFactory;
            _machineBuilder = machineBuilder;
        }

        // GET: mdf/machines
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MachineDto>>> GetMachines()
        {
            try
            {
                List<MachineDto> dtoList = new List<MachineDto>();
                List<Machine> machineList = await _repositoryFactory.Machine.FindAll().ToListAsync();

                foreach (Machine machine in machineList)
                {
                    dtoList.Add(machine.ToDto());
                }

                return dtoList;
            }

            catch (Exception exp)
            {
                return NotFound(exp.Message);
            }

        }

        // GET: mdf/machines/1
        [HttpGet("{serialNumber}")]
        public async Task<ActionResult<MachineDto>> GetMachine(string serialNumber)
        {

            try
            {
                Machine machine = await _repositoryFactory.Machine.FindBySerialNumber(serialNumber);

                return machine.ToDto();
            }

            catch (Exception exp)
            {
                return NotFound(exp.Message);
            }
        }

        // GET: mdf/machines/2
//        [HttpGet("{model}")]
//        public async Task<ActionResult<IEnumerable<Machine>>> GetMachinesByModel(string model)
//        {
//            var todoItems = await _repositoryFactory.Machine.FindByModel(model);
//            if (todoItems == null)
//            {
//                return NotFound();
//            }
//
//            return todoItems;
//        }

        // GET: mdf/machines/3
//        [HttpGet("{brand}")]
//        public async Task<ActionResult<IEnumerable<Machine>>> GetMachinesByBrand(string brand)
//        {
//            var todoItems = await _repositoryFactory.Machine.FindByBrand(brand);
//            if (todoItems == null)
//            {
//                return NotFound();
//            }
//            
//            return todoItems;
//        }

        // GET: mdf/machines/4
        [HttpGet("{by-type}/{mactype}")]
        public async Task<ActionResult<List<MachineDto>>> GetMachinesByMachineType(string mactype)
        {
            var todoItems = await _repositoryFactory.Machine.FindByMachineType(mactype);
            if (todoItems.IsNullOrEmpty())
            {
                return BadRequest("There are no machines with that Machine Type!");
            }

            List<MachineDto> returnList = new List<MachineDto>();

            for (int i = 0; i < todoItems.Count; i++)
            {
                returnList.Add(todoItems[i].ToDto());
            }

            return returnList;
        }

        // POST: mdf/machines
        [HttpPost]
        public async Task<ActionResult<MachineDto>> PostMachine(MachineDto machineDto)

        {
            try
            {
                Machine machine = await _machineBuilder.BuildMachine(machineDto);
                 _repositoryFactory.Machine.Create(machine);
                await _repositoryFactory.Machine.SaveASync();

                return Created("Machine Created.", machine.ToDto());
            }
            catch (Exception exp)
            {
                return BadRequest(exp);
            }
        }

        // PUT: mdf/machines/1
        [HttpPut("{serialNumber}")]
        public async Task<ActionResult<MachineDto>> PutMachine(string serialNumber, MachineDto machineDto)
        {
            try{
                if (serialNumber != machineDto.SerialNumber)
                {
                    return BadRequest("Serial numbers do not match.");
                }

                Machine machineToUpdate = _repositoryFactory.Machine.FindBySerialNumber(serialNumber).Result;
                if (machineToUpdate == null)
                {
                    return BadRequest();
                }

                MachineType newMachineType =
                    _repositoryFactory.MachineType.FindByStringId(machineDto.MachineType).Result;
                if (newMachineType == null)
                {
                    return BadRequest();
                }

                machineToUpdate.ChangeMachineType(newMachineType);

                _repositoryFactory.Machine.Update(machineToUpdate);
                await _repositoryFactory.Operation.SaveASync();

                Machine machineToUpdateResult = await _repositoryFactory.Machine.FindBySerialNumber(serialNumber);

                return machineToUpdateResult.ToDto();
            }
            
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        /**
         * Method that allows the user to activate the machine.
         */
        [HttpPut("activate/{serialNumber}")]
        public async Task<ActionResult<MachineDto>> ActivateMachine(string serialNumber)
        {
            try
            {
                Machine fetchedMachine = this._repositoryFactory.Machine.FindBySerialNumber(serialNumber).Result;

                if (fetchedMachine == null)
                {
                    return NotFound("The machine does not exist!");
                }

                fetchedMachine.ActivateMachine();

                this._repositoryFactory.Machine.Update(fetchedMachine);
                await _repositoryFactory.Machine.SaveASync();

                return fetchedMachine.ToDto();

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /**
         * Method that allows to deactivate a machine.
         */

        [HttpPut("deactivate/{serialNumber}")]
        public async Task<ActionResult<MachineDto>> DeactivateMachine(string serialNumber)
        {
            try
            {
                Machine fetchedMachine = this._repositoryFactory.Machine.FindBySerialNumber(serialNumber).Result;

                if (fetchedMachine == null)
                {
                    return NotFound("The machine does not exist.");
                }

                fetchedMachine.DeactivateMachine();

                this._repositoryFactory.Machine.Update(fetchedMachine);
                await this._repositoryFactory.Machine.SaveASync();

                return fetchedMachine.ToDto();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);   
            }
        }
        
       
            //DELETE mdf/machines/1
            [HttpDelete("{serialnumber}")]
            public async Task<IActionResult> DeleteMachine(string serialnumber)
            {
                var machine = await _repositoryFactory.Machine.FindBySerialNumber(serialnumber);
    
                if (machine == null)
                {
                    return NotFound();
                }
              
                _repositoryFactory.Machine.Delete(machine);
                await _repositoryFactory.Operation.SaveASync();
    
                return NoContent();
            }
    
            
    }
}