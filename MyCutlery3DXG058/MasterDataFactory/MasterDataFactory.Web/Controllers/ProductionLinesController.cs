﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using Microsoft.AspNetCore.Mvc;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Services;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.EntityFrameworkCore;
using Machine = MasterDataFactory.Core.Models.Machines.Machine;

namespace MasterDataFactory.Web.Controllers
{
    /**
     * Controller for Production Lines that extends the methods existent in the interface ControllerBase.
     */
    [Route("mdf/[controller]")]
    [ApiController]
    public class ProductionLinesController : ControllerBase
    {
        private IRepositoryFactory _repositoryFactory;
        private IProductionLineBuilder _productionLineBuilder;
        private IProductionLineService _productionLineService;


        public ProductionLinesController(IRepositoryFactory repositoryFactory, IProductionLineBuilder productionLineBuilder)
        {

            _repositoryFactory = repositoryFactory;
            _productionLineBuilder = productionLineBuilder;
            _productionLineService = new ProductionLineService(_repositoryFactory.Machine, _repositoryFactory.ProductionLine);
        }
        

        /**
         * Controller method that returns all the production lines.
         * 
         */

        //GET mdf/productionlines
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductionLineDto>>> GetProductionsLines()
        {
            List<ProductionLineDto> listProductionLinesDTO = new List<ProductionLineDto>();
            IEnumerable<ProductionLine> productionLines =  await _repositoryFactory.ProductionLine.FindAll().ToListAsync();

            foreach (ProductionLine productionLine in productionLines)
            {
                listProductionLinesDTO.Add(productionLine.ToDto());
                
            }
            return listProductionLinesDTO;
        }

        [HttpGet("availableMachines")]
        public async Task<ActionResult<IEnumerable<String>>> GetNonAssociatedMachines()
        {
            List<String> listOfNonAssociatedMachinesDTO = new List<String>();
            IEnumerable<Machine> machines = await _repositoryFactory.Machine.FindNonAssociatedMachines();

            foreach (Machine machine in  machines )
            {
                listOfNonAssociatedMachinesDTO.Add(machine.ToDto().SerialNumber);
            }

            return listOfNonAssociatedMachinesDTO;
        }

        /**
         * Gets a production line according to the id passed as a parameter.
         */

        // GET: mdf/productionline/1
        [HttpGet("{domainId}")]
        public async Task<ActionResult<ProductionLineDto>> GetProductionLineByDomainId(string domainId)
        {
            try
            {
                var fetchedProductionLine = await this._repositoryFactory.ProductionLine.FindByDomainId(domainId);

                if (fetchedProductionLine == null)
                {
                    return NotFound();
                }

                return fetchedProductionLine.ToDto();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }

        }
        
        public async Task<ActionResult<ProductionLineDto>> GetProductionLineById(long id)
        {
            var fetchedProductionLine = await this._repositoryFactory.ProductionLine.FindById(id);

            if (fetchedProductionLine == null)
            {
                return NotFound();
            }


            return  fetchedProductionLine.ToDto();

        }

        /**
         * Posts a production line in the database according to the value of the attributes passed as a parameter.
         */
        // POST: mdf/productionline/1
        [HttpPost]
        public async Task<ActionResult<ProductionLineDto>> PostProductionLine(ProductionLineDto productionLineDto)
        {
            try
            {
                this._productionLineBuilder.WithNonAssociatedMachines(productionLineDto);
                ProductionLine productionLine =
                    this._productionLineBuilder.Build(productionLineDto.ProductionLineIdentifier);
                _repositoryFactory.ProductionLine.Create(productionLine);
                await _repositoryFactory.ProductionLine.SaveASync();
                return CreatedAtAction(nameof(GetProductionLineById), new {id = productionLine.Id}, productionLine);
            }
            catch (InvalidDataException)
            {
                return BadRequest("Invalid Production Line ");
            }
            catch (SqlException)
            {
                return BadRequest("Production Line already Exists! ");
            }
            catch (DbUpdateException)
            {
                return BadRequest("Production Line already Exists! ");
            }
        }
        
        /**
         * Updates a production line in the database according to the value of the attributes passed as a parameter.
         */
        // PUT: mdf/productionLines/1
        [HttpPut]
        [Route("{productionLineIdentifier}")]
        public async Task<ActionResult<ProductionLineDto>> PutProductionLine(string productionLineIdentifier, ProductionLineUpdateDto productionLineUpdateDto)
        {
            try
            {

                _productionLineService.UpdateProductionLineMachines(productionLineIdentifier, productionLineUpdateDto);
                await _repositoryFactory.ProductionLine.SaveASync();

                return NoContent();

            }
            
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        //DELETE mdf/productionlines/1
       [HttpDelete("{productionLineIdentifier}")]
        public async Task<IActionResult> DeleteProductionLine(string productionLineIdentifier)
        {
            try
            {
                var productionLine = await _repositoryFactory.ProductionLine.FindByDomainId(productionLineIdentifier);

                if (productionLine == null)
                {
                    return NotFound();
                }

                //Integrity of the database guarantees that only production lines without machines can be deleted, otherwise DB throws exception.
                _repositoryFactory.ProductionLine.Delete(productionLine);
                await _repositoryFactory.ProductionLine.SaveASync();
                return NoContent();
            }
            catch (DbUpdateException)
            {
                return BadRequest(
                    "This Production Line still has associated machines! Please disassociate the machines!");
            }
        }
    }
}