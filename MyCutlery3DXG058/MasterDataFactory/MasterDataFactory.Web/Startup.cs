using System.Diagnostics.CodeAnalysis;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using MasterDataFactory.Web.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MasterDataFactory.Web
{
   [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200", "https://localhost:4200",
                            "https://masterdatafactory3dxg058.firebaseapp.com",
                            "https://masterdatafactory3dxg058.web.app").AllowAnyMethod().AllowAnyHeader();
                    });
            });
            string connectionString = Configuration.GetConnectionString("Database");
            services.AddDbContext<RepositoryContext>(opt => opt.UseLazyLoadingProxies().UseSqlServer(connectionString),
                ServiceLifetime.Transient);
            services.AddScoped<IRepositoryFactory, RepositoryFactory>();
            services.AddScoped<IMachineTypeBuilder, MachineTypeBuilder>();
            services.AddScoped<IMachineBuilder, MachineBuilder>();
            services.AddScoped<IOperationBuilder, OperationBuilder>();
            services.AddScoped<IProductionLineBuilder, ProductionLineBuilder>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(MyAllowSpecificOrigins);
            app.UseAuthorization();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}