using System;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MasterDataFactory.Web.Services
{
  [ExcludeFromCodeCoverage]
    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthorizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            //Reading the AuthHeader which is signed with JWT
            string jwt = context.Request.Headers["token"];
            if (jwt != null)
            {
                //Reading the JWT middle part     
                var handler = new JwtSecurityTokenHandler();
                if (!handler.CanReadToken(jwt))
                    throw new UnauthorizedAccessException("token is not readable");
                var token = handler.ReadJwtToken(jwt);
                var role = token.Payload["role"].ToString();
                if (role == "admin")
                {
                    try
                    {
                        await _next(context);
                    }
                    catch (Exception)
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An error has occurred");
                    }
                }
                else
                {
                    context.Response.StatusCode = 401;
                    await context.Response.WriteAsync("Not Allowed");
                    return;
                }
            }
            else
            {
                context.Response.StatusCode = 401;
                await context.Response.WriteAsync("Not Allowed");
            }
        }
    }

   [ExcludeFromCodeCoverage]
    public static class MyMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthorization(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthorizationMiddleware>();
        }
    }
}