﻿using System.Diagnostics.CodeAnalysis;
using MasterDataFactory.Web.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace MasterDataFactory.Web
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UsePort()
                .UseStartup<Startup>();
    }
}