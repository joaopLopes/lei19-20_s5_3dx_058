﻿using System;
using System.Collections.Generic;
using System.IO;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using Xunit;

namespace MasterDataFactory.Tests.Services.Builders.Impl
{
    public class ProductionLinesBuilderTest
    {
        [Fact]
        public void EnsureBuildProductionLineWithNullListThrowsException()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            //Basically trying to create a line with no machines at all.
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
            string expectedMessage = "Associate the respective machines to the Production Line!";
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            string message = Assert.Throws<InvalidDataException>(() => builder.Build("123")).Message;
            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void EnsureBuildProductionLineWithEmptyListThrowsException()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            string expectedMessage = "The production line can not be created with no machines!";
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            builder.WithNonAssociatedMachines(productionLineDto);
            string message = Assert.Throws<InvalidDataException>(() => builder.Build("123")).Message;
            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void EnsureBuildProductionLineWithNonExistingMachineThrowsException()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
            string expectedMessage = "One or more errors occurred. (Sequence contains no elements)";
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            idsList.Add(new MachineAssociateToLineDto("A4", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            string message = Assert
                .Throws<AggregateException>(() => builder.WithNonAssociatedMachines(productionLineDto))
                .Message;
            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void EnsureBuildProductionLineWithAlreadyAssociatedMachineThrowsException()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A1", machineModel, machineBrand, machineType);
            repositoryFactory.Machine.Create(machine1);
            repositoryFactory.Machine.SaveASync();
            string expected =
                "Machine trying to add in the production line is already associated to other production line";
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            idsList.Add(new MachineAssociateToLineDto("A1", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            builder.WithNonAssociatedMachines(productionLineDto);
            builder.Build("123");

            //Should throw exception now.
            string message = Assert
                .Throws<InvalidDataException>(() => builder.WithNonAssociatedMachines(productionLineDto)).Message;
            Assert.Equal(expected, message);
        }

        [Fact]
        public void EnsureBuildProductionLineWhenArgumentsAreValidIsSuccessful()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);

            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A2", machineModel, machineBrand, machineType);
            repositoryFactory.Machine.Create(machine1);
            repositoryFactory.Machine.SaveASync();
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            idsList.Add(new MachineAssociateToLineDto("A2", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            builder.WithNonAssociatedMachines(productionLineDto);
            ProductionLine productionLine = builder.Build("123");

            Assert.Equal(0, productionLine.Id);
        }

        [Fact]
        public void EnsureMachineThatIsInProductionLineHasActiveDefaultWorkingStatusByDefault()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);

            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A2", machineModel, machineBrand, machineType);
            repositoryFactory.Machine.Create(machine1);
            repositoryFactory.Machine.SaveASync();
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            idsList.Add(new MachineAssociateToLineDto("A2", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            builder.WithNonAssociatedMachines(productionLineDto);
            ProductionLine productionLine = builder.Build("123");

            string expected = "Activated";
            string result = machine1.MachineWorkingState.WorkingState;

            Assert.Equal(expected, result);
        }

        [Fact]
        public void EnsureMachineThatIsValidCantHaveAMachineThatIsNotActive()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);

            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A2", machineModel, machineBrand, machineType);
            repositoryFactory.Machine.Create(machine1);
            repositoryFactory.Machine.SaveASync();
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            idsList.Add(new MachineAssociateToLineDto("A2", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            builder.WithNonAssociatedMachines(productionLineDto);
            ProductionLine productionLine = builder.Build("123");

            machine1.DeactivateMachine();

            string expected = "Deactivated";
            string result = machine1.MachineWorkingState.WorkingState;

            Assert.Equal(expected, result);
        }

        [Fact]
        public void EnsureMachineThatIsValidCanHaveAMachineThatSwitchesFromActiveToActiveBecauseItIsIrrelevant()
        {
            var context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);

            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A2", machineModel, machineBrand, machineType);
            repositoryFactory.Machine.Create(machine1);
            repositoryFactory.Machine.SaveASync();
            ICollection<MachineAssociateToLineDto> idsList = new List<MachineAssociateToLineDto>();
            idsList.Add(new MachineAssociateToLineDto("A2", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idsList, "123");
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            builder.WithNonAssociatedMachines(productionLineDto);
            ProductionLine productionLine = builder.Build("123");


            string expected = "You can not activate a machine that is already active.";
            string result = Assert.Throws<InvalidDataException>(() => machine1.ActivateMachine())
                .Message;

            Assert.Equal(expected, result);
        }
    }
}