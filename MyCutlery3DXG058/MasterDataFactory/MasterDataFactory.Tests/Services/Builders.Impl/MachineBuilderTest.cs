﻿using System;
using System.Collections.Generic;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using MasterDataFactory.Web.Controllers;
using Xunit;

namespace MasterDataFactory.Tests.Services.Builders.Impl
{
    public class MachineBuilderTest
    {
        //Operations
        OperationDto op5kgPresserDto = new OperationDto()
        {
            Tool = "TestTool",
            SetupTime = 1,
            Description = "Presser",
            ExecutionTime = 10
        };

        OperationDto op10kgPresserDto = new OperationDto()
        {
            Tool = "TestTool",
            SetupTime = 1,
            Description = "Presser",
            ExecutionTime = 7
        };

        OperationDto _op5KgCutterdDto = new OperationDto()
        {
            Description = "Cutter",
            ExecutionTime = 10
        };

        [Fact]
        public void EnsureBuildMachineSuccessfullyBuildsAValidMachine()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);

                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto expectedDto = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = machineTypePresserDto.Name,
                    MachineWorkingState = "Deactivated"
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser-Machine-Type"
                };

                //Act
                machineController.PostMachine(input);
                MachineDto resultDto = machineController.GetMachine(expectedDto.SerialNumber).Result.Value;

                //Assert
                Assert.True(expectedDto.Equals(resultDto));
            }
        }


        [Fact]
        public void EnsureBuildMachineWithANonExistingMachineTypeThrowsException()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);


                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachineBuilder machineBuilder = new MachineBuilder(repoFac);

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Non-Existing-Machine-Type"
                };

                //Act
                string expected = "ERROR - MachineType not instantiated";
                string result = Assert.ThrowsAsync<ArgumentException>(() => machineBuilder.BuildMachine(input))
                    .Result.Message;


                //Assert
                Assert.Equal(expected, result);
            }
        }

        [Fact]
        public void EnsureBuildMachineWithNullBrandNameThrowsException()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);

                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser-Machine-Type"
                };

                //Act


                //Assert
                Assert.True(machineController.PostMachine(input).Result.Value == null);
            }
        }

        [Fact]
        public void EnsureBuildMachineWithEmptyBrandNameThrowsException()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);

                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto expectedDto = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "",
                    Model = "Fork Presser",
                    MachineType = machineTypePresserDto.Name
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser Machine Type"
                };
                //Act


                //Assert
                Assert.True(machineController.PostMachine(input).Result.Value == null);
            }
        }

        [Fact]
        public void EnsureBuildMachineWithNullModelNameThrowsException()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);

                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto expectedDto = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = null,

                    MachineType = machineTypePresserDto.Name
                };
                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser Machine Type"
                };


                //Act


                //Assert
                Assert.True(machineController.PostMachine(input).Result.Value == null);
            }
        }

        [Fact]
        public void EnsureBuildMachineWithEmptyModelNameThrowsException()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);

                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto expectedDto = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "",
                    MachineType = machineTypePresserDto.Name
                };

                //Act
                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser Machine Type"
                };

                //Assert
                Assert.True(machineController.PostMachine(input).Result.Value == null);
            }
        }
    }
}