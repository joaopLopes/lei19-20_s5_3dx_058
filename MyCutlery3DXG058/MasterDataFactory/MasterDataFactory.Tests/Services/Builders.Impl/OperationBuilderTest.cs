using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Services.Builders.Impl;
using Xunit;

namespace MasterDataFactory.Tests.Services.Builders.Impl
{
    public class OperationBuilderTest
    {
        [Fact]
        public void EnsureBuildingOperationFromDtoUsesCorrectData()
        {
            //Arrange
            OperationDto dto = new OperationDto
            {
                Tool = "TestTool",
                SetupTime = 1,
                Description = "Sawmill",
                ExecutionTime = 10
            };

            Operation expected = new Operation("TestTool", 1, "Sawmill", 10);
            //Act
            IOperationBuilder builder = new OperationBuilder();
            builder.WithDataFromDto(dto);
            Operation result = builder.Create();

            //Assert
            Assert.Equal(expected, result);
        }
    }
}