﻿using System.IO;
using MasterDataFactory.Core.Models.ProductionLines;
using Xunit;

namespace MasterDataFactory.Tests.Models.ProductionLines
{
    public class ProductionLineIdentifierTest
    {
        [Fact]
        public void EnsureCreateProductionLineIdentifierNullReturnsException()
        {
            string test = null;
            string expectedMessage = "The identifier of Production line can not be empty!";

            string message = Assert.Throws<InvalidDataException>(() => new ProductionLineIdentifier(test))
                .Message;
            Assert.Equal(expectedMessage,message);

        }

        [Fact]
        public void EnsureCreateProductionLineIdentifierEmptyReturnsException()
        {
            string test = "";
            string expectedMessage = "The identifier of Production line can not be empty!";
            
            string message =  Assert.Throws<InvalidDataException>(() => new ProductionLineIdentifier(test))
                .Message;
            
            Assert.Equal(expectedMessage,message);

        }

        [Fact]
        public void EnsureCreateProductionLineIdentifierWithLessThan3DigitsThrowsException()
        {
            string test = "12";
            string expectedMessage = "The Production Line Identifier is invalid!";
            
            string message = Assert.Throws<InvalidDataException>(() => new ProductionLineIdentifier(test))
                .Message;
            
            Assert.Equal(expectedMessage,message);

        }

        [Fact]
        public void EnsureCreateProductionLineWithMoreThan3DigitsThrowsExceptions()
        {
            string test = "1234123";
            string expectedMessage = "The Production Line Identifier is invalid!";
            
            string message = Assert.Throws<InvalidDataException>(() => new ProductionLineIdentifier(test))
                .Message;
            
            
            Assert.Equal(expectedMessage,message);
        }
        
        [Fact]
        public void EnsureCreateProductionLineWithLettersThrowsException()
        {
            string test = "AAA";
            string expectedMessage = "The Production Line Identifier is invalid!";
            
            string message = Assert.Throws<InvalidDataException>(() => new ProductionLineIdentifier(test))
                .Message;
            
            Assert.Equal(expectedMessage,message);
        }
        

        [Fact]
        public void EnsureCreateProductionLineWithSpecialCharacteresThrowsException()
        {
            string test = "?!#";
            string expectedMessage = "The Production Line Identifier is invalid!";
            
            string message = Assert.Throws<InvalidDataException>(() => new ProductionLineIdentifier(test))
                .Message;
            
            Assert.Equal(expectedMessage,message);

        }

        [Fact]
        public void EnsureCreateProductionLineIdentifierCanCreateSuccessfully()
        {
            string test = "123";

            ProductionLineIdentifier pId = new ProductionLineIdentifier(test);

            bool result = pId.IsEqualToo(test);
            Assert.True(result);
            
        }
    }
}