﻿using System.Collections.Generic;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.ProductionLines;
using Xunit;

namespace MasterDataFactory.Tests.Models.ProductionLines

{
    public class ProductionLinesTest
    {

        [Fact]
        public void EnsureProductionLineEqualsComparedWithNullReturnsFalse()
        {
            ICollection<Machine> machineList = new List<Machine>();
            ProductionLine productionLine = new ProductionLine(machineList,"123");
            bool result = productionLine.Equals(null);
            Assert.False(result);
        }

        [Fact]
        public void EnsureProductionLineEqualsWithDifferentClassReturnsFalse()
        {
            ICollection<Machine> machineList = new List<Machine>();
            ProductionLine productionLine = new ProductionLine(machineList,"123");
            string test = "test";
            bool result = productionLine.Equals(test);
            Assert.False(result);
        }

        [Fact]
        public void EnsureProductionLineEqualsWithTheSameMemoryReferenceProductionLineReturnsTrue()
        {
            ICollection<Machine> machineList = new List<Machine>();
            ProductionLine productionLine = new ProductionLine(machineList,"123");
            bool result = productionLine.Equals(productionLine);
            Assert.True(result);
        }

     

        [Fact]
        public void EnsureEqualsProductionLinesWithDIfferentArgumentsReturnsFalse()
        {
            ICollection<Machine> machineList = new List<Machine>();
            ICollection<Machine> machineListOfOtherProductionLine = new List<Machine>();
            
            machineList.Add(new Machine("A1","FactoryModel","FactoryBrand", new MachineType("Presser")));
            machineListOfOtherProductionLine.Add(new Machine("A2","OtherFactoryModel","OtherFactoryBrand",new MachineType("Cutter")));
            
            ProductionLine productionLine = new ProductionLine(machineList,"123");
            ProductionLine productionLine2 = new ProductionLine(machineListOfOtherProductionLine,"123");

            bool result = productionLine.Equals(productionLine2);
            Assert.False(result);
        }

        [Fact]
        public void EnsureHashCodesAreDifferentWhenProductionLinesAreDifferent()
        {
            {
                ICollection<Machine> machineList = new List<Machine>();
                ICollection<Machine> machineListOfOtherProductionLine = new List<Machine>();
            
                machineList.Add(new Machine("A1","FactoryModel","FactoryBrand", new MachineType("Presser")));
                machineListOfOtherProductionLine.Add(new Machine("A2","OtherFactoryModel","OtherFactoryBrand",new MachineType("Cutter")));
            
                ProductionLine productionLine = new ProductionLine(machineList,"123");
                ProductionLine productionLine2 = new ProductionLine(machineListOfOtherProductionLine,"123");
                
                Assert.NotEqual(productionLine.GetHashCode(),productionLine2.GetHashCode());
            }
        }
        
}
}