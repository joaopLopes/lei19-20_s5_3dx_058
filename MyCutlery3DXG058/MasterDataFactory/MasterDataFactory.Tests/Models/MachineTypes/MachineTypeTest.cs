﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using Xunit;

namespace MasterDataFactory.Tests.Models.MachineTypes
{
    public class MachineTypeTest
    {
        [Fact]
        public void EnsureEqualsReturnsTrueIfObjectsAreEqual()
        {
            var m1 = new MachineType("Test1");
            var m2 = new MachineType("Test1");
            var result = m1.Equals(m2);
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsReturnsTrueIfReferencesAreTheSame()
        {
            var m1 = new MachineType("Test1");
            var m2 = m1;
            var result = m1.Equals(m2);
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseIfObjectsHaveDifferentName()
        {
            var m1 = new MachineType("Test1");
            var m2 = new MachineType("Test2");
            var result = m1.Equals(m2);
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseIfObjectsHaveDifferentList()
        {
            var m1 = new MachineType("Test1");
            m1.AddOperation(new MachineTypeOperation(m1, new Operation("TestTool", 1, "Cutter", 0)));
            var m2 = new MachineType("Test1");
            m2.AddOperation(new MachineTypeOperation(m1, new Operation("TestTool", 1, "Presser", 58)));
            var result = m1.Equals(m2);
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseIfObjectIsNull()
        {
            var m1 = new MachineType("Test1");
            MachineType m2 = null;
            var result = m1.Equals(m2);
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseIfObjectIsOfDifferentType()
        {
            var m1 = new MachineType("Test1");
            var m2 = new Operation("TestTool", 1, "Test", 5);
            var result = m1.Equals(m2);
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsReturnsFalseIfListsAreOfDifferentSize()
        {
            var m1 = new MachineType("Test1");
            m1.AddOperation(new MachineTypeOperation(m1, new Operation("TestTool", 1, "asdasd", 0)));
            var m2 = new MachineType("Test1");
            m2.AddOperation(new MachineTypeOperation(m1, new Operation("TestTool", 1, "asdasd", 0)));
            m2.AddOperation(new MachineTypeOperation(m1, new Operation("TestTool", 1, "QWESSA", 58)));
            var result = m1.Equals(m2);
            Assert.False(result);
        }

        [Fact]
        public void EnsureMachineTypeIsNotCreatedIfNameIsNullAndExceptionIsThrown()
        {
            var ex = Assert.Throws<ArgumentException>(() => new MachineType(null));
            Assert.Equal("Error: Name is empty - MachineType was not created", ex.Message);
        }

        [Fact]
        public void EnsureMachineTypeIsNotCreatedIfNameIsEmptyAndExceptionIsThrown()
        {
            var ex = Assert.Throws<ArgumentException>(() => new MachineType(""));
            Assert.Equal("Error: Name is empty - MachineType was not created", ex.Message);
        }

        [Fact]
        public void EnsureGetHashCodeReturnsCorrectValue()
        {
            var m1 = new MachineType("Test1");
            var m2 = new MachineType("Test1");

            Assert.Equal(m1.GetHashCode(), m2.GetHashCode());
        }

        [Fact]
        public void EnsureGetHashCodeIsEqual()
        {
            List<string> listOperations = new List<string>();
            listOperations.Add("op1");
            MachineTypeDto machineTypeDto = new MachineTypeDto();
            machineTypeDto.Name = "machineType87";
            machineTypeDto.ListMacTypeOperations = listOperations;
            MachineTypeDto machineTypeDto2 = new MachineTypeDto();
            machineTypeDto2.Name = "machineType87";
            machineTypeDto2.ListMacTypeOperations = listOperations;
            Assert.Equal(machineTypeDto.GetHashCode(),machineTypeDto2.GetHashCode());
        }

        [Fact]
        public void EnsureDifferentHashCodesIfMachineTypesDifferent()
        {
            List<string> listOperations = new List<string>();
            listOperations.Add("op1");
            List<string> listOperations2 = new List<string>();
            listOperations2.Add("op2");
            MachineTypeDto machineTypeDto = new MachineTypeDto();
            machineTypeDto.Name = "machineType87";
            machineTypeDto.ListMacTypeOperations = listOperations;
            MachineTypeDto machineTypeDto2 = new MachineTypeDto();
            machineTypeDto2.Name = "machineType88";
            machineTypeDto2.ListMacTypeOperations = listOperations2;
            Assert.NotEqual(machineTypeDto.GetHashCode(),machineTypeDto2.GetHashCode());
            
            
        }

        /*
        [Fact]
        public void EnsureToDtoReturnsCorrectObject()
        {
            var m1 = new MachineType("Test1");
            var operation = new Operation("Test", 5);
            m1.AddOperation(new MachineTypeOperation(m1, operation));
            m1.MachineTypeId = 1;
            OperationDto dto2 = operation.ToDto();

            List<OperationDto> list = new List<OperationDto>();
            list.Add(dto2);

            MachineTypeReturnDto dto = new MachineTypeReturnDto
                {Id = 1, Name = "Test1", ListMacTypeOperationDtos = list};

            MachineTypeReturnDto dtoResult = m1.toDTO();

            Assert.True(dto.Equals(dtoResult));
        }
<<<<<<< HEAD:MyCutlery3DXG058/MasterDataFactory/MasterDataFactory.Tests/Models/MachineTypes/MachineTypeTest.cs

        [Fact]
        public void EnsureChangeMachineTypeOperationsPerformsASuccessfullOperation()
        {
            MachineType machineType = new MachineType("Test1");
            machineType.AddOperation(new MachineTypeOperation(machineType, new Operation("hammer", 100)));
            ICollection<MachineTypeOperation> machineTypeOperationList = new List<MachineTypeOperation>();
            machineTypeOperationList.Add(new MachineTypeOperation(machineType, new Operation("screwdriver", 100)));
            machineType.ChangeMachineTypesOperations(machineTypeOperationList);
            Assert.Equal(1, machineType.ListOp.Count);
            MachineTypeOperation expected = new MachineTypeOperation(machineType, new Operation("screwdriver", 100));
            //Assert.Equal(expected.Op, machineType.ListOp.First().Op);
        }

        [Fact]
        public void EnsureToDtoOperationsOfAMachineTypeWorksForWhenMachineTypeDoesNotHaveOperations()
        {
            MachineType machineType = new MachineType("Test1");
            List<OperationDto> expected = new List<OperationDto>();
            Assert.Equal(expected, machineType.OperationDtos());
        }

        [Fact]
        public void EnsureToDtoOperationsOfMachineTypeWorksSuccessfully()
        {
            MachineType machineType = new MachineType("Test1");
            List<OperationDto> expected = new List<OperationDto>();
            Operation operation = new Operation("hammer", 100);
            expected.Add(operation.ToDto());
            machineType.AddOperation(new MachineTypeOperation(machineType, new Operation("hammer", 100)));
            List<OperationDto> result = machineType.OperationDtos();
            Assert.Equal(expected, result);
        }
=======
        */
    }
}