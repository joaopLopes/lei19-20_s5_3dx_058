﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using Xunit;

namespace MasterDataFactory.Tests.Models.Machines
{
    public class MachineNonAssociatedStateTest
    {
        [Fact]
        public void EnsureNonAssociateMachineGetsTheCorrectCurrentStateCorrect()
        {
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            Machine machine1 = new Machine("A1", machineModel, machineBrand, machineType);

            Assert.Equal(MachineStateEnum.NonAssociated, machine1.State());
        }

        [Fact]
        public void EnsureAssociateProductionLineToNonAssociatedMachineWorksSuccessfully()
        {
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A2", machineModel, machineBrand, machineType);
            RepositoryContext context = RepositoryContextMocker.GetTodoContext();
            IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
            ProductionLineBuilder builder = new ProductionLineBuilder(repositoryFactory);
            MachineRepository machineRepository = new MachineRepository(context);
            machineRepository.Create(machine1);
            machineRepository.SaveASync();
            List<MachineAssociateToLineDto> idList = new List<MachineAssociateToLineDto>();
            idList.Add(new MachineAssociateToLineDto("A2", 1));
            ProductionLineDto productionLineDto = new ProductionLineDto(idList, "123");
            builder.WithNonAssociatedMachines(productionLineDto);
            ProductionLine pLine = builder.Build("123");
            Assert.Equal(MachineStateEnum.Associated, machine1.State());
        }
    }
}