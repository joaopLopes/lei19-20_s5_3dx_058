﻿using System;
using System.IO;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Xunit;

namespace MasterDataFactory.Tests.Models.Machines
{
    public class MachineTest
    {
         
        
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSamesModelAndBrandAndMachineTypeAndSateReturnsTrue()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);
            Machine machine2 = new Machine("A2",machineModel, machineBrand, machineType);
            
            MachineState machineState = new MachineNonAssociatedState(machine1);
            machine1.SetState(machineState);
            machine2.SetState(machineState);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsWithSameReferenceReturnsTrue()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);
            
            //Act
            var result = machine1.Equals(machine1);

            //Assert
            Assert.True(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSameModelAndBrandButDifferentMachineTypesReturnsFalse()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType1 = new MachineType("Test-Machine-Type");
            MachineType machineType2 = new MachineType("Test-Machine-Type-2");

            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType1);
            Machine machine2 = new Machine("A2",machineModel, machineBrand, machineType2);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSameModelButDifferentBrandsAndDifferentMachineTypesReturnsFalse()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand1 = "Test Brand";
            string machineBrand2 = "Test Brand 2";
            MachineType machineType1 = new MachineType("Test-Machine-Type");
            MachineType machineType2 = new MachineType("Test-Machine-Type-2");

            Machine machine1 = new Machine("A1",machineModel, machineBrand1, machineType1);
            Machine machine2 = new Machine("A2",machineModel, machineBrand2, machineType2);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSameBrandButDifferentModelsAndDifferentMachineTypesReturnsFalse()
        {
            //Arrange
            string machineBrand = "Test Brand";
            string machineModel1 = "Test Model";
            string machineModel2 = "Test Model 2";
            MachineType machineType1 = new MachineType("Test-Machine-Type");
            MachineType machineType2 = new MachineType("Test-Machine-Type-2");

            Machine machine1 = new Machine("A1",machineModel1, machineBrand, machineType1);
            Machine machine2 = new Machine("A2",machineModel2, machineBrand, machineType2);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSameBrandAndMachineTypeButDifferentModelsReturnsFalse()
        {
            //Arrange
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            string machineModel1 = "Test Model";
            string machineModel2 = "Test Model 2";

            Machine machine1 = new Machine("A1",machineModel1, machineBrand, machineType);
            Machine machine2 = new Machine("A2",machineModel2, machineBrand, machineType);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSameMachineTypeAndModelButDifferentBrandsReturnsFalse()
        {
            //Arrange
            MachineType machineType = new MachineType("Test-Machine-Type");
            string machineModel = "Test Model";
            string machineBrand1 = "Test Brand";
            string machineBrand2 = "Test Brand 2";

            Machine machine1 = new Machine("A1",machineModel, machineBrand1, machineType);
            Machine machine2 = new Machine("A2",machineModel, machineBrand2, machineType);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithSameMachineTypeButDifferentModelsAndBrandsReturnsFalse()
        {
            //Arrange
            MachineType machineType = new MachineType("Test-Machine-Type");
            string machineModel1 = "Test Model";
            string machineModel2 = "Test Model 2";
            string machineBrand1 = "Test Brand";
            string machineBrand2 = "Test Brand 2";

            Machine machine1 = new Machine("A1",machineModel1, machineBrand1, machineType);
            Machine machine2 = new Machine("A2",machineModel2, machineBrand2, machineType);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithTwoMachinesWithDifferentMachineTypesAndModelsAndBrandsReturnsFalse()
        {
            //Arrange
            MachineType machineType1 = new MachineType("Test-Machine-Type");
            MachineType machineType2 = new MachineType("Test-Machine-Type");
            string machineModel1 = "Test Model";
            string machineModel2 = "Test Model 2";
            string machineBrand1 = "Test Brand";
            string machineBrand2 = "Test Brand 2";

            Machine machine1 = new Machine("A1",machineModel1, machineBrand1, machineType1);
            Machine machine2 = new Machine("A2",machineModel2, machineBrand2, machineType2);

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsWithOneMachineAndNullReturnsFalse()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);
            Machine machine2 = null;

            //Act
            var result = machine1.Equals(machine2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithOneMachineAndAndDifferentClassReturnsFalse()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);
            string otherClass = "Other class test";

            //Act
            var result = machine1.Equals(otherClass);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureGetHashCodeWithEqualMachinesReturnsTrue()
        {
            //Arrange
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");

            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);
            Machine machine2 = new Machine("A2",machineModel, machineBrand, machineType);

            MachineState machineState = new MachineNonAssociatedState(machine1);
            machine1.SetState(machineState);
            machine2.SetState(machineState);

            //Act
            var result = machine1.GetHashCode().Equals(machine2.GetHashCode());

            //Assert
            Assert.True(result);
        }
        
        [Fact]
        public void EnsureValidateMachineSerialNumberThrowsExceptionWithEmptyMachineSerialNumber()
        {
            //Arrange
            string machineSerialNumber = "";
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            //Act
            string expected = "ERROR: The machine serial number can not be empty - Machine was not created";
            string result = Assert.Throws<ArgumentException>(() => new Machine(machineSerialNumber,machineModel,machineBrand,machineType)).Message;
            
            
            //Assert
            Assert.Equal(expected,result);
        }
        
        [Fact]
        public void EnsureValidateMachineSerialNumberThrowsExceptionWithNullMachineSerialNumber()
        {
            //Arrange
            string machineSerialNumber = null;
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            //Act
            string expected = "ERROR: The machine serial number can not be empty - Machine was not created";
            string result = Assert.Throws<ArgumentException>(() => new Machine(machineSerialNumber,machineModel,machineBrand,machineType)).Message;
            
            
            //Assert
            Assert.Equal(expected ,result);
        }
        
        [Fact]
        public void EnsureValidateMachineSerialNumberThrowsExceptionWithWhiteSpaceMachineSerialNumber()
        {
            //Arrange
            string machineSerialNumber = "   ";
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            //Act
            string expected = "ERROR: The machine serial number can not be empty - Machine was not created";
            string result = Assert.Throws<ArgumentException>(() => new Machine(machineSerialNumber,machineModel,machineBrand,machineType)).Message;
            
            
            //Assert
            Assert.Equal(expected,result);
        }
        
        [Fact]
        public void EnsureValidateMachineSerialNumberThrowsExceptionWithInvalidMachineSerialNumber()
        {
            //Arrange
            string machineSerialNumber = "-d2%$";
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            //Act
            string expected = "ERROR: The machine serial number can only be constituted by uppercase letters and/or numbers - Machine was not created";
            string result = Assert.Throws<ArgumentException>(() => new Machine(machineSerialNumber,machineModel,machineBrand,machineType)).Message;
            
            
            //Assert
            Assert.Equal(expected,result);
        }

        [Fact]
        public void EnsureMachineThatWasCreatedAndIsNotAssociatedToAnyProductionLineHasWorkingStateDeactivated()
        {
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);

            string expected = "Deactivated";
            string result = machine1.MachineWorkingState.WorkingState;
            
            Assert.Equal(expected,result);
        }

        [Fact]
        public void EnsureMachineThatHasNoProductionLineCantBeActivated()
        {
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);

            string expectedMessage = "Can't activate a machine that is not associated to a production line";
            
            string message =  Assert.Throws<InvalidDataException>(() => machine1.ActivateMachine())
                .Message;
            
            Assert.Equal(expectedMessage,message);
        }

        [Fact]
        public void EnsureMachineThatIsNotAssociatedCantBeDeactivated()
        {
            string machineModel = "Test Model";
            string machineBrand = "Test Brand";
            MachineType machineType = new MachineType("Test-Machine-Type");
            
            Machine machine1 = new Machine("A1",machineModel, machineBrand, machineType);

            string expectedMessage = "Can't deactivate a machine that is not associated to a production line";
            
            string message =  Assert.Throws<InvalidDataException>(() => machine1.DeactivateMachine())
                .Message;
            
            Assert.Equal(expectedMessage,message);
            
        }

        [Fact]
        public void EnsureMachineDTOEqualAreSameHashCode()
        {
            MachineDto machineDto = new MachineDto();
            machineDto.Brand = "Lancia";
            machineDto.Model = "Lenovo";
            machineDto.Slot = 1;
            machineDto.MachineType = "Cutter";
            machineDto.SerialNumber = "222";
            machineDto.MachineWorkingState = "Activated";
            MachineDto machineDto2 = new MachineDto();
            machineDto2.Brand = "Lancia";
            machineDto2.Model = "Lenovo";
            machineDto2.Slot = 1;
            machineDto2.MachineType = "Cutter";
            machineDto2.SerialNumber = "222";
            machineDto2.MachineWorkingState = "Activated";
            Assert.Equal(machineDto.GetHashCode(),machineDto2.GetHashCode());
        }

        [Fact]
        public void EnsureMachineDTOHashCodesAreDifferentIfDifferent()
        {
            MachineDto machineDto = new MachineDto();
            machineDto.Brand = "Lancia";
            machineDto.Model = "Lenovo";
            machineDto.Slot = 1;
            machineDto.MachineType = "Cutter";
            machineDto.SerialNumber = "222";
            machineDto.MachineWorkingState = "Activated";
            MachineDto machineDto2 = new MachineDto();
            machineDto2.Brand = "Lancia2";
            machineDto2.Model = "Lenovo2";
            machineDto2.Slot = 12;
            machineDto2.MachineType = "Cutter2";
            machineDto2.SerialNumber = "2222";
            machineDto2.MachineWorkingState = "Activat2ed";
            Assert.NotEqual(machineDto.GetHashCode(),machineDto2.GetHashCode());
        }
    }
}