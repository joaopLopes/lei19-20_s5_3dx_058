﻿using System;
using MasterDataFactory.Core.Models.Machines;
using Xunit;

namespace MasterDataFactory.Tests.Models.Machines
{
    public class ModelTest
    {
/*
        private string modelName = "Test Model";
        
        
        [Fact]
        public void EnsureConstructorWithNullModelNameThrowsException()
        {
            //Arrange

            //Act
            var result = Assert.Throws<ArgumentException>(() => new Model(null)).Message;
            var expected = "Error: MACHINE NOT CREATED - Machine model cannot be null or empty!";

            //Assert
            Assert.True(result.Equals(expected));
        }
        
        [Fact]
        public void EnsureConstructorWithEmptyModelNameThrowsException()
        {
            //Arrange

            //Act
            var result = Assert.Throws<ArgumentException>(() => new Model("")).Message;
            var expected = "Error: MACHINE NOT CREATED - Machine model cannot be null or empty!";

            //Assert
            Assert.True(result.Equals(expected));
        }

        [Fact]
        public void EnsureEqualsTwoModelsWithSameNameReturnsTrue()
        {
            //Arrange
            Model model1 = new Model(modelName);
            Model model2 = new Model(modelName);

            //Act
            var result = model1.Equals(model2);
            //Assert
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsSameMReferenceReturnsTrue()
        {
            //Arrange
            Model model1 = new Model(modelName);

            //Act
            var result = model1.Equals(model1);
            
            //Assert
            Assert.True(result);
        }
        
        [Fact]
        public void EnsureEqualsTwoModelsWithDifferentNamesReturnsFalse()
        {
            //Arrange
            Model model1 = new Model(modelName);
            Model model2 = new Model("Different Model");

            //Act
            var result = model1.Equals(model2);
            
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsOneModelAndNullReturnsFalse()
        {
            //Arrange
            Model model1 = new Model(modelName);
            Model model2 = null;

            //Act
            var result = model1.Equals(model2);
            
            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsOneModelAndOneDifferentClassReturnsFalse()
        {
            //Arrange
            Model model1 = new Model(modelName);
            string differentClass = "Different Class";

            //Act
            var result = model1.Equals(differentClass);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureGetHashCodeWithTwoModelsWithSameNameReturnsTrue()
        {
            //Arrange
            Model model1 = new Model(modelName);
            Model model2 = new Model(modelName);

            //Act
            var result = model1.GetHashCode().Equals(model2.GetHashCode());
            
            //Assert
            Assert.True(result);
        }
        
        [Fact]
        public void EnsureGetHashCodeWithTwoModelsWithDifferentNamesReturnsFalse()
        {
            //Arrange
            Model model1 = new Model(modelName);
            Model model2 = new Model("Different Name");

            //Act
            var result = model1.GetHashCode().Equals(model2.GetHashCode());
            //Assert
            Assert.False(result);
        }
         */
    }
   
}