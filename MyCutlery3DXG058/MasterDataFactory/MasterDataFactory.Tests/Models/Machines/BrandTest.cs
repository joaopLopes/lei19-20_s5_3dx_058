﻿using System;
using MasterDataFactory.Core.Models.Machines;
using Xunit;

namespace MasterDataFactory.Tests.Models.Machines
{
    public class BrandTest
    {
        /*
        private string brandName = "Test Brand";

        [Fact]
        public void EnsureConstructorWithNullBrandNameThrowsException()
        {
            //Arrange

            //Act
            var result = Assert.Throws<ArgumentException>(() => new Brand(null)).Message;
            var expected = "Error: MACHINE NOT CREATED - Machine brand cannot be null or empty!";

            //Assert
            Assert.True(result.Equals(expected));
        }
        
        [Fact]
        public void EnsureConstructorWithEmptyBrandNameThrowsException()
        {
            //Arrange

            //Act
            var result = Assert.Throws<ArgumentException>(() => new Brand("")).Message;
            var expected = "Error: MACHINE NOT CREATED - Machine brand cannot be null or empty!";

            //Assert
            Assert.True(result.Equals(expected));
        }
        
        [Fact]
        public void EnsureEqualsWithTwoBrandsWithSameNameReturnsTrue()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);
            Brand brand2 = new Brand(brandName);

            //Act
            var result = brand1.Equals(brand2);

            //Assert
            Assert.True(result);
        }
        
        [Fact]
        public void EnsureEqualsWithSameReferenceReturnsTrue()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);

            //Act
            var result = brand1.Equals(brand1);

            //Assert
            Assert.True(result);
        }
        
        
        
        [Fact]
        public void EnsureEqualsWithTwoBrandsWithDifferentNamesReturnsFalse()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);
            Brand brand2 = new Brand("Different Brand");

            //Act
            var result = brand1.Equals(brand2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithOneBrandAndNullReturnsFalse()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);
            Brand brand2 = null;

            //Act
            var result = brand1.Equals(brand2);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureEqualsWithOneBrandAndOneDifferentClassReturnsFalse()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);
            string differentClass = "Different Class";

            //Act
            var result = brand1.Equals(differentClass);

            //Assert
            Assert.False(result);
        }
        
        [Fact]
        public void EnsureGetHashCodeWithTwoBrandsWithSameNameReturnsTrue()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);
            Brand brand2 = new Brand(brandName);

            //Act
            var result = brand1.GetHashCode().Equals(brand2.GetHashCode());
            
            //Assert
            Assert.True(result);
        }
        
        [Fact]
        public void EnsureGetHashCodeWithTwoBrandsWithDifferentNamesReturnsFalse()
        {
            //Arrange
            Brand brand1 = new Brand(brandName);
            Brand brand2 = new Brand("Different Name");

            //Act
            var result = brand1.GetHashCode().Equals(brand2.GetHashCode());
            //Assert
            Assert.False(result);
        }
        */
    }
}