using System;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models.Operations;
using Xunit;

namespace MasterDataFactory.Tests.Models.Operations
{
    public class OperationTest
    {
        [Fact]
        public void EnsureCreatingOperationWithNullDescriptionNameThrowsException()
        {
            //Arrange
            string expectedMessage = "Description must not be empty.";
            //Act
            //nothing to do 
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Operation("TestTool", 1, null, 10)).Message;
            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void EnsureCreatingOperationWithEmptyDescriptionNameThrowsException()
        {
            //Arrange
            string expectedMessage = "Description must not be empty.";
            //Act
            //nothing to do 
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Operation("TestTool", 1, "", 10)).Message;
            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void EnsureCreatingOperationWithNegativeDurationThrowsException()
        {
            //Arrange
            string expectedMessage = "Time must be positive.";
            //Act
            //nothing to do 
            //Assert
            string message = Assert.Throws<ArgumentException>(() => new Operation("TestTool", 1, "TestDescription", -5))
                .Message;
            Assert.Equal(expectedMessage, message);
        }

        [Fact]
        public void EnsureEqualsMethodReturnsTrueWhenOperationsHaveSameData()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Sawmill", 5);
            Operation second = new Operation("TestTool", 1, "Sawmill", 5);

            //Act
            bool result = first.Equals(second);
            //Assert
            Assert.True(result);
        }


        [Fact]
        public void EnsureEqualsMethodReturnsTrueWhenOtherOperationIsTheSameEntity()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Sawmill", 5);

            //Act
            bool result = first.Equals(first);
            //Assert
            Assert.True(result);
        }

        [Fact]
        public void EnsureEqualsMethodReturnsFalseWhenDescriptionIsDifferent()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Presser", 5);
            Operation second = new Operation("TestTool", 1, "Sawmill", 5);

            //Act
            bool result = first.Equals(second);
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsMethodReturnsFalseWhenOtherOperationIsNull()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Sawmill", 5);

            //Act
            bool result = first.Equals(null);
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureEqualsMethodReturnsFalseWhenOtherOperationIsDifferentObject()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Sawmill", 5);
            string test = "for testing";
            //Act
            bool result = first.Equals(test);
            //Assert
            Assert.False(result);
        }

        [Fact]
        public void EnsureHashcodeReturnsSameCodeForEqualOperations()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Sawmill", 5);
            Operation second = new Operation("TestTool", 1, "Sawmill", 5);

            //Act
            int hashCode = first.GetHashCode();
            int hashCode2 = second.GetHashCode();
            //Assert
            Assert.Equal(hashCode, hashCode2);
        }

        [Fact]
        public void EnsureToStringReturnsCorrectMessage()
        {
            //Arrange
            Operation first = new Operation("TestTool", 1, "Sawmill", 5);
            string expected = "Tool: TestTool\nSetup Time: 00:00:01\nDescription: Sawmill\nExecution Time: 00:00:05";
            //Act
            string result = first.ToString();

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void EnsureHashCodeIsSameForEqualOperationsDTO()
        {
            OperationDto operationDto = new OperationDto();
            operationDto.Description = "test";
            operationDto.Tool = "hammer";
            operationDto.ExecutionTime = 10;
            operationDto.SetupTime = 10;
            OperationDto operationDto2 = new OperationDto();
            operationDto2.Description = "test";
            operationDto2.Tool = "hammer";
            operationDto2.ExecutionTime = 10;
            operationDto2.SetupTime = 10;
            Assert.Equal(operationDto.GetHashCode(),operationDto2.GetHashCode());
        }

        [Fact]
        public void EnsureHashCodesAreDifferentWhenOperationsDTOAlsoAre()
        {
            OperationDto operationDto = new OperationDto();
            operationDto.Description = "test";
            operationDto.Tool = "hammer";
            operationDto.ExecutionTime = 10;
            operationDto.SetupTime = 10;
            OperationDto operationDto2 = new OperationDto();
            operationDto2.Description = "tes2t";
            operationDto2.Tool = "hammer2";
            operationDto2.ExecutionTime = 120;
            operationDto2.SetupTime = 102;
            Assert.NotEqual(operationDto.GetHashCode(),operationDto2.GetHashCode());

        }
    }
}