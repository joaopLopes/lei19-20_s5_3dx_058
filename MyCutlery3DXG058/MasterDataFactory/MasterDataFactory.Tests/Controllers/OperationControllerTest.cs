using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using MasterDataFactory.Web.Controllers;
using Xunit;

namespace MasterDataFactory.Tests.Controllers
{
    public class OperationControllerTest
    {
        [Fact]
        public void EnsureGetOperationReturnsExpectedObject()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                OperationDto expected = new OperationDto
                {
                    Tool = "TestTool",
                    SetupTime = 1,
                    ExecutionTime = 10,
                    Description = "Presser",
                };
                OperationBootstrap.Seed(context);
                OperationsController controller =
                    new OperationsController(new RepositoryFactory(context), new OperationBuilder());
                //Act

                OperationDto result = controller.GetOperation("Presser").Result.Value;
                //Assert
                Assert.Equal(expected, result);
            }
        }

        [Fact]
        public void EnsureGetOperationThrowsErrorWhenOperationNotFound()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 
                //Nothing to do
                OperationBootstrap.Seed(context);
                OperationsController controller =
                    new OperationsController(new RepositoryFactory(context), new OperationBuilder());
                //Act
                //nothing to do
                //Assert

                var response = controller.GetOperation("FakeOperation").Result.Value;
                Assert.Null(response);
            }
        }


        [Fact]
        public void EnsurePostOperationCreatesObjectSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                OperationDto expected = new OperationDto
                {
                    Tool = "TestTool",
                    SetupTime = 1,
                    ExecutionTime = 7,
                    Description = "Cutter",
                };

                OperationBootstrap.Seed(context);
                OperationsController controller =
                    new OperationsController(new RepositoryFactory(context), new OperationBuilder());
                controller.PostOperation(expected);

                //Act
                OperationDto result = controller.GetOperation("Cutter").Result.Value;
                //Assert
                Assert.Equal(expected, result);
            }
        }

        [Fact]
        public void EnsureGetAllOperationsReturnsAllOperationsFromDatabase()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                List<OperationDto> expected = new List<OperationDto>();
                expected.Add(new OperationDto
                {
                    Tool = "TestTool",
                    SetupTime = 1,
                    ExecutionTime = 10,
                    Description = "Presser",
                });
                expected.Add(new OperationDto
                {
                    Tool = "TestTool",
                    SetupTime = 1,
                    ExecutionTime = 7,
                    Description = "Cutter",
                });

                OperationBootstrap.Seed(context);
                OperationsController controller =
                    new OperationsController(new RepositoryFactory(context), new OperationBuilder());

                //Act
                IEnumerable<OperationDto> result = controller.GetOperations().Result.Value;
                //Assert
                Assert.Equal(expected, result);
            }
        }
    }
}