﻿using System.Collections.Generic;
using System.Linq;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using MasterDataFactory.Web.Controllers;
using Xunit;

namespace MasterDataFactory.Tests.Controllers
{
    public class MachineControllerTest
    {
        //Operations
        OperationDto op5kgPresserDto = new OperationDto()
        {
            Tool = "TestTool",
            SetupTime = 1,
            Description = "Presser",
            ExecutionTime = 10
        };

        OperationDto op10kgPresserDto = new OperationDto()
        {
            Tool = "TestTool",
            SetupTime = 1,
            Description = "Presser",
            ExecutionTime = 7
        };

        [Fact]
        public void EnsurePostMachineSuccessfullyCreatesAValidMachine()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);


                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto expectedDto = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = machineTypePresserDto.Name,
                    MachineWorkingState = "Deactivated"
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser-Machine-Type",
                    MachineWorkingState = "Deactivated"
                };

                //Act
                machineController.PostMachine(input);
                MachineDto resultDto = machineController.GetMachine(expectedDto.SerialNumber).Result.Value;

                //Assert
                Assert.True(expectedDto.Equals(resultDto));
            }
        }

        [Fact]
        public void EnsureGetMachineSuccessfullyReturnsCorrespondingMachine()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);


                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto expectedDto = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = machineTypePresserDto.Name,
                    MachineWorkingState = "Deactivated"
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser-Machine-Type"
                };

                //Act
                machineController.PostMachine(input);
                MachineDto resultDto = machineController.GetMachine(expectedDto.SerialNumber).Result.Value;

                //Assert
                Assert.Equal(expectedDto, (resultDto));
            }
        }

        [Fact]
        public void EnsureGetMachineOfANonExistingMachinesReturnsBadRequest()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);


                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto nonExistingMachineDto = new MachineDto()
                {
                    SerialNumber = "A50",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = machineTypePresserDto.Name
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "F4K3",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser-Machine-Type"
                };

                //Act
                machineController.PostMachine(input);


                //Assert
                Assert.True(machineController.GetMachine(nonExistingMachineDto.SerialNumber).Result.Value == null);
            }
        }

        [Fact]
        public void EnsureGetMachinesSuccessfullyReturnsAllExistingMachines()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineBootstrap.Seed(context);


                RepositoryFactory repoFac = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(repoFac, new MachineBuilder(repoFac));


                List<string> listPresserOpDto = new List<string>()
                {
                    op5kgPresserDto.Description,
                    op10kgPresserDto.Description
                };

                MachineTypeDto machineTypePresserDto = new MachineTypeDto()
                {
                    Name = "Presser-Machine-Type",
                    ListMacTypeOperations = listPresserOpDto
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "Presser Machine Type"
                };

                //Act
                machineController.PostMachine(input);


                //Assert
                Assert.True(machineController.GetMachine("A99").Result.Value == null);
            }
        }

        [Fact]
        public void EnsurePutMachineUpdatesDataCorrectly()
        {
            //As of the time of the creation of this test, only MachineType can be updated (MDF06)
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                OperationDto opDto = new OperationDto()
                {
                    Tool = "TestTool",
                    SetupTime = 1,
                    Description = "Cutter",
                    ExecutionTime = 5
                };

                MachineTypeDto machineTypeReturnDto = new MachineTypeDto
                {
                    Name = "machine-test2"
                };

                MachineDto dto2 = new MachineDto
                {
                    SerialNumber = "1",
                    Model = "Iphone",
                    Brand = "Apple",
                    MachineType = machineTypeReturnDto.Name,
                    MachineWorkingState =  "Deactivated"
                };

                MachineDto input = new MachineDto()
                {
                    SerialNumber = "1",
                    Brand = "Apple",
                    Model = "Iphone",
                    MachineType = "machine-test"
                };

                MachineDto input2 = new MachineDto()
                {
                    SerialNumber = "1",
                    Brand = "Apple",
                    Model = "Iphone",
                    MachineType = "machine-test2"
                };
                machineTypeReturnDto.ListMacTypeOperations = new List<string>();
                machineTypeReturnDto.ListMacTypeOperations.Add(opDto.Description);

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                var test = machineController.PostMachine(input);
                machineController.PutMachine("1", input2);

                MachineDto result = machineController.GetMachine("1").Result.Value;
                Assert.True(result.Equals(dto2));
            }
        }

        [Fact]
        public void EnsurePutMachineThrowsBadRequestIfMachineTypeDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);

                MachineTypeDto machineTypeReturnDto = new MachineTypeDto
                {
                    Name = "someFakeName"
                };


                MachineDto dto = new MachineDto
                {
                    Model = "Lenovo",
                    Brand = "X999",
                    MachineType = machineTypeReturnDto.Name
                };
                MachineDto dto2 = new MachineDto
                {
                    Model = "Lenovo",
                    Brand = "X999",
                    MachineType = "machine-testjkbhgvyfuyur"
                };
                MachineDto input = new MachineDto()
                {
                    SerialNumber = "A7",
                    Brand = "IKEA",
                    Model = "Fork Presser",
                    MachineType = "machine-test"
                };

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                machineController.PostMachine(input);


                var response = machineController.PutMachine("4", dto2);

                Assert.Null(response.Result.Value);
            }
        }

        [Fact]
        public void EnsurePutMachineThrowsBadRequestIfMachineDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);

                MachineDto dto2 = new MachineDto
                {
                    Model = "Lenovo",
                    Brand = "X999",
                    MachineType = "machine-test"
                };

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                var response = machineController.PutMachine("4568378943278934789", dto2);

                Assert.Null(response.Result.Value);
            }
        }

        [Fact]
        public void EnsureGetMachineByMachineTypeReturnsCorrectObject()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);

                MachineTypeDto machineTypeReturnDto = new MachineTypeDto
                {
                    Name = "Cutter",
                    ListMacTypeOperations = new List<string>()
                };

                MachineDto dtoExpected = new MachineDto
                {
                    Model = "Lenovo",
                    Brand = "X999",
                    MachineType = machineTypeReturnDto.Name,
                    SerialNumber = "A1",
                    MachineWorkingState = "Deactivated"
                };

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                List<MachineDto> result = machineController.GetMachinesByMachineType("Cutter").Result.Value;

                Assert.Contains(dtoExpected, result);
            }
        }

        [Fact]
        public void EnsureGetMachineByMachineTypeReturnsBadRequestIfTypeDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                var result = machineController.GetMachinesByMachineType("Saw-Cutter-Turbo-3000");

                Assert.Null(result.Result.Value);
            }
        }
        
        [Fact]
        public void EnsureGetMachinesWorks()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                var result = machineController.GetMachines();

                Assert.Equal(6, result.Result.Value.Count());
            }
        }
        
        [Fact]
        public void EnsureActivateMachineWorks()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));
                machineController.DeactivateMachine("A1");
                var result = machineController.ActivateMachine("A1");

                Assert.Equal("Activated" ,result.Result.Value.MachineWorkingState);
            }
        }
        
        [Fact]
        public void EnsureActivateMachineThrowsExceptionIfMachineDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));

                var result = machineController.ActivateMachine("F8");

                Assert.Null(result.Result.Value);
            }
        }
        
        [Fact]
        public void EnsureDeactivateMachineWorks()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));
                
                var result = machineController.DeactivateMachine("A1");

                Assert.Equal("Deactivated" ,result.Result.Value.MachineWorkingState);
            }
        }
        
        [Fact]
        public void EnsureDeactivateMachineThrowsExceptionIfMachineDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);

                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachinesController machineController = new MachinesController(auxRepo, new MachineBuilder(auxRepo));
                
                var result = machineController.DeactivateMachine("L1");

                Assert.Null( result.Result.Value);
            }
        }
    }
}