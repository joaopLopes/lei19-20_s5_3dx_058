﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using MasterDataFactory.Web.Controllers;
using Xunit;

namespace MasterDataFactory.Tests.Controllers
{
    public class ProductionLineControllerTest
    {
        [Fact]
        public async System.Threading.Tasks.Task EnsureGetProductionLineThrowsErrorWhenProductionLineNotFound()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 
                //Nothing to do
                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do

                //Assert
                var result = await controller.GetProductionLineByDomainId("349");
                Assert.Null(result.Value);
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsureGetProductionLinesReturnsAllProductionLines()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 
                //Nothing to do
                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do

                //Assert
                var result = controller.GetProductionsLines().Result.Value;
                Assert.Equal(2, result.Count());
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsureGetProductionLineReturnsCorrectProductionLines()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                IProductionLineBuilder productionLineBuilder = new ProductionLineBuilder(repositoryFactory);
                ICollection<MachineAssociateToLineDto> listIds = new List<MachineAssociateToLineDto>();
                listIds.Add(new MachineAssociateToLineDto("A1", 1));
                ProductionLineDto productionLineDtoWithMachine1 = new ProductionLineDto(listIds, "123");
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do

                //Assert
                var result = controller.GetProductionLineByDomainId("123").Result.Value;
                Assert.Equal(productionLineDtoWithMachine1.ProductionLineIdentifier, result.ProductionLineIdentifier);
                Assert.Equal(productionLineDtoWithMachine1.ListAssociatedMachinesDto.Count, result.ListAssociatedMachinesDto.Count);
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsureGetProductionLineReturnsNotFoundIfLineDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                    IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                    MachineBootstrap.Seed(context);
                    ProductionLineBootstrap.Seed(context);
                    ProductionLinesController controller =
                        new ProductionLinesController(new RepositoryFactory(context),
                            new ProductionLineBuilder(repositoryFactory));
                    //Act
                    //nothing to do

                    //Assert
                    var result = controller.GetProductionLineByDomainId("329340985893489435").Result.Value;
                    Assert.Null(result);
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsurePostProductionLineCreatesLineIfInfoIsValid()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do
                ICollection<MachineAssociateToLineDto> machines = new List<MachineAssociateToLineDto>();
                machines.Add(new MachineAssociateToLineDto("A3", 1));
                ProductionLineDto toCreate = new ProductionLineDto(machines, "555");
                await controller.PostProductionLine(toCreate);
                var result = controller.GetProductionLineByDomainId("555");
                //Assert
                Assert.Equal(toCreate.ProductionLineIdentifier, result.Result.Value.ProductionLineIdentifier);
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsurePostProductionLineThrowsExceptionIfLineIsRepeated()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do
                ICollection<MachineAssociateToLineDto> machines = new List<MachineAssociateToLineDto>();
                machines.Add(new MachineAssociateToLineDto("A3", 1));
                ProductionLineDto toCreate = new ProductionLineDto(machines, "555");
                await controller.PostProductionLine(toCreate);
                var result = await controller.PostProductionLine(toCreate);
                //Assert
                Assert.Null(result.Value);
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsureDeleteProductionLineWorks()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do
                ICollection<MachineAssociateToLineDto> machines = new List<MachineAssociateToLineDto>();
                machines.Add(new MachineAssociateToLineDto("A3", 1));
                ProductionLineDto toCreate = new ProductionLineDto(machines, "555");
                await controller.PostProductionLine(toCreate);
                await controller.DeleteProductionLine("555");
                var result = controller.GetProductionLineByDomainId("555");
                //Assert
                Assert.Null(result.Result.Value);
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsureDeleteProductionLineThrowsExceptionIfLineDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do
                ICollection<MachineAssociateToLineDto> machines = new List<MachineAssociateToLineDto>();
                machines.Add(new MachineAssociateToLineDto("A3", 1));
                ProductionLineDto toCreate = new ProductionLineDto(machines, "555");
                try
                {
                    await controller.DeleteProductionLine("555");
                    Assert.False(true);
                }
                catch (Exception)
                {
                    Assert.True(true);
                }
            }
        }

        
        [Fact]
        public async System.Threading.Tasks.Task EnsureGetNonAssociatedMachinesWorks()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do
                var result = controller.GetNonAssociatedMachines();
                //Assert
                Assert.Equal(4, result.Result.Value.Count());
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsurePutProductionLineWorks()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                ICollection<MachineAssociateToLineDto> machines = new List<MachineAssociateToLineDto>();
                var aux = new MachineAssociateToLineDto("B1", 1);
                var aux2 = new MachineAssociateToLineDto("B2", 1);
                machines.Add(aux);
                machines.Add(aux2);
                ProductionLineUpdateDto dto = new ProductionLineUpdateDto();
                dto.ListAssociatedMachinesDto = machines;
                await controller.PutProductionLine("123", dto);
                var result = controller.GetProductionLineByDomainId("123");
                var list = result.Result.Value.ListAssociatedMachinesDto;
                Assert.Equal(2, list.Count());
            }
        }
        
        [Fact]
        public async System.Threading.Tasks.Task EnsurePutProductionLineThrowsExceptionIfLineDoesNotExist()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange 

                IRepositoryFactory repositoryFactory = new RepositoryFactory(context);
                OperationBootstrap.Seed(context);
                MachineTypeBootstrap.Seed(context);
                MachineBootstrap.Seed(context);
                ProductionLineBootstrap.Seed(context);
                ProductionLinesController controller =
                    new ProductionLinesController(new RepositoryFactory(context),
                        new ProductionLineBuilder(repositoryFactory));
                //Act
                //nothing to do
                try
                {
                    await controller.PutProductionLine("23908230948230948", new ProductionLineUpdateDto());
                    Assert.False(true);
                }
                catch (Exception)
                {
                    Assert.True(true);
                }
            }
        }
        
    }
}