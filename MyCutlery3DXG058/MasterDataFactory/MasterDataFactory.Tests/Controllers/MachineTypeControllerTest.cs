﻿using System;
using System.Collections.Generic;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Bootstraps;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using MasterDataFactory.Web.Controllers;
using Xunit;

namespace MasterDataFactory.Tests.Controllers
{
    public class MachineTypeControllerTest

    {
        //Tests regarding POST requests

        [Fact]
        public void EnsureThatMachineTypeIsCreatedIfDataIsValid()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineTypeDto initial = new MachineTypeDto
                {
                    Name = "Maquina-de-Serrar",
                    ListMacTypeOperations = new List<string>()
                };

                MachineTypeDto expected = new MachineTypeDto
                {
                    Name = "Maquina-de-Serrar",
                    ListMacTypeOperations = new List<string>()
                };

                OperationDto opDto = new OperationDto
                {
                    Tool = "TestTool",
                    SetupTime = 1,
                    Description = "Cutter",
                    ExecutionTime = 5
                };
                expected.ListMacTypeOperations.Add("Cutter");
                initial.ListMacTypeOperations.Add("Cutter");
                RepositoryFactory auxRepo = new RepositoryFactory(context);
                OperationsController opCont = new OperationsController(auxRepo, new OperationBuilder());
                MachineTypesController controller =
                    new MachineTypesController(auxRepo, new MachineTypeBuilder(auxRepo));
                opCont.PostOperation(opDto);
                MachineTypeBootstrap.Seed(context);
                //Act
                controller.PostMachineType(initial);
                MachineTypeDto result = controller.GetMachineType("Maquina-de-Serrar").Result.Value;
                //Assert
                Assert.True(expected.Equals(result));
            }
        }

        [Fact]
        public async void EnsureThatPostMachineTypeThrowsExceptionIfOperationIsDuplicated()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                MachineTypeDto initial = new MachineTypeDto
                {
                    Name = "Máquina-de-Serrar",
                    ListMacTypeOperations = new List<string>()
                };

                initial.ListMacTypeOperations.Add("Cutter");
                initial.ListMacTypeOperations.Add("Cutter");
                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachineTypesController controller =
                    new MachineTypesController(auxRepo, new MachineTypeBuilder(auxRepo));
                MachineTypeBootstrap.Seed(context);

                //Act and Assert
                var result = await controller.PostMachineType(initial);
                Assert.Null(result.Value);
            }
        }

        [Fact]
        public void EnsureMachineTypeAlterOperationsWorksSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                IMachineTypeBuilder mBuilder = new MachineTypeBuilder(new RepositoryFactory(context));
                MachineTypesController controller =
                    new MachineTypesController(new RepositoryFactory(context), mBuilder);
                ICollection<string> operationIds = new List<string>();
                operationIds.Add("Presser");
                operationIds.Add("Cutter");
                OperationBootstrap.Seed(context);
                MachineTypeChangeOperationDto changeOperationDto = new MachineTypeChangeOperationDto
                {
                    Name = "cutter",
                    ListMacTypeOperations = operationIds
                };
                context.MachineTypes.Add(new MachineType("cutter"));
                context.SaveChanges();
                List<OperationDto> listOperationsDto = new List<OperationDto>();
                RepositoryFactory factory = new RepositoryFactory(context);
                listOperationsDto.Add(factory.Operation.FindById("Presser").Result.ToDto());
                listOperationsDto.Add(factory.Operation.FindById("Cutter").Result.ToDto());
                MachineTypeChangeOperationReturnDTO expectedDto = new MachineTypeChangeOperationReturnDTO()
                {
                    Name = "cutter",
                    operationDTOList = listOperationsDto
                };
                MachineTypeChangeOperationReturnDTO returnDto =
                    controller.ChangeOperationsOfMachineType(changeOperationDto, changeOperationDto.Name).Result.Value;
                Assert.Equal(expectedDto.Name, returnDto.Name);
                Assert.Equal(expectedDto.operationDTOList, returnDto.operationDTOList);
            }
        }

        [Fact]
        public void ensureGetOperationsOfMachineTypeWorksSuccessfully()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                //Arrange
                List<OperationDto> expected = new List<OperationDto>();
                RepositoryFactory repositoryFactory = new RepositoryFactory(context);
                IMachineTypeBuilder machineTypeBuilder = new MachineTypeBuilder(repositoryFactory);
                MachineTypesController machineTypesController =
                    new MachineTypesController(repositoryFactory, machineTypeBuilder);
                MachineType machineType = new MachineType("washer");
                machineType.AddOperation(new MachineTypeOperation(machineType,
                    new Operation("TestTool", 1, "chemicals", 10)));
                context.MachineTypes.Add(machineType);
                context.SaveChanges();
                List<OperationDto> result = machineTypesController.ConsultOperationsOfMachineType("washer").Result
                    .Value;
                Assert.Equal(result, machineType.OperationDtos());
            }
        }

        [Fact]
        public void EnsureGetMachineTypeReturnsBadRequest()
        {
            using (var context = RepositoryContextMocker.GetTodoContext())
            {
                RepositoryFactory auxRepo = new RepositoryFactory(context);
                MachineTypesController controller =
                    new MachineTypesController(auxRepo, new MachineTypeBuilder(auxRepo));
                var result = controller.GetMachineType("non-existing-type");
                Assert.Null(result.Result.Value);
            }
        }
    }
}