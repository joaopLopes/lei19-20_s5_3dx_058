using MasterDataFactory.Infrastructure.Persistence.Impl;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MasterDataFactory.Infrastructure.Bootstraps
{
    public static class RepositoryContextMocker
    {
        public static RepositoryContext GetTodoContext()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            var options = new DbContextOptionsBuilder<RepositoryContext>()
                .UseInMemoryDatabase("MasterDataFactoryTestDB").UseInternalServiceProvider(serviceProvider).Options;
            RepositoryContext dbContext = new RepositoryContext(options);
            return dbContext;
        }
    }
}