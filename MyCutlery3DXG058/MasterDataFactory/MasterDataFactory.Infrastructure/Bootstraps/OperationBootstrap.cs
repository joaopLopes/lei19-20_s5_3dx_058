using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Infrastructure.Persistence.Impl;

namespace MasterDataFactory.Infrastructure.Bootstraps
{
    public static class OperationBootstrap
    {
        public static void Seed(RepositoryContext dbContext)
        {
            dbContext.Operations.Add(
                new Operation("TestTool", 1, "Presser", 10));
            dbContext.Operations.Add(
                new Operation("TestTool", 1, "Cutter", 7));

            dbContext.SaveChanges();
        }
    }
}