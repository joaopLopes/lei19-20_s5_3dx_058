﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using MasterDataFactory.Core.Services.Builders.Impl;
using MasterDataFactory.Infrastructure.Persistence.Impl;

namespace MasterDataFactory.Infrastructure.Bootstraps
{
    public class ProductionLineBootstrap
    {
        public static void Seed(RepositoryContext dbContext)
        {
            IRepositoryFactory repositoryFactory = new RepositoryFactory(dbContext);
            IProductionLineBuilder productionLineBuilder = new ProductionLineBuilder(repositoryFactory);
            ICollection<MachineAssociateToLineDto> listIds = new List<MachineAssociateToLineDto>();
            listIds.Add(new MachineAssociateToLineDto("A1", 1));
            ICollection<MachineAssociateToLineDto> listIds2 = new List<MachineAssociateToLineDto>();
            listIds2.Add(new MachineAssociateToLineDto("A2", 1));
            ProductionLineDto productionLineDtoWithMachine1 = new ProductionLineDto(listIds, "123");
            productionLineBuilder.WithNonAssociatedMachines(productionLineDtoWithMachine1);
            ProductionLine productionLine = productionLineBuilder.Build("123");
            dbContext.Add(productionLine);
            ProductionLineDto productionLineDtoWithMachine2 = new ProductionLineDto(listIds2, "234");
            productionLineBuilder.WithNonAssociatedMachines(productionLineDtoWithMachine2);
            ProductionLine productionLine2 = productionLineBuilder.Build("234");
            dbContext.Add(productionLine2);
            dbContext.SaveChanges();
        }
    }
}