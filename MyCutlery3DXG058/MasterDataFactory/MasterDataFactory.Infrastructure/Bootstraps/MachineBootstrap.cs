﻿using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Infrastructure.Persistence.Impl;
using Microsoft.EntityFrameworkCore.Query.Expressions;

namespace MasterDataFactory.Infrastructure.Bootstraps
{
    public class MachineBootstrap
    {
        public static void Seed(RepositoryContext dbContext)
        {

            dbContext.Machines.Add(new Machine("A1","Lenovo","X999",new MachineType("Cutter")));
            dbContext.Machines.Add(new Machine("A2","Audi", "X110", new MachineType("Sharpener")));
            dbContext.Machines.Add(new Machine("A3","Toshiba", "X990", new MachineType("Cutter")));
            
            
            //to use for put method tests
            
            //Operations
            Operation op5kgPresser = new Operation( "TestTool", 1, "Presser", 10);
            Operation op10kgPresser = new Operation("TestTool", 1, "Presser", 7);
            Operation op5kgCutter = new Operation("TestTool", 1, "Cutter", 10);
            
            
            dbContext.Operations.Add(op5kgPresser);
            dbContext.Operations.Add(op10kgPresser);
            dbContext.Operations.Add(op5kgCutter);
            
            
            //Machine Type and Machine Type Operations
            MachineType machineTypePresser = new MachineType("Presser-Machine-Type");

            MachineTypeOperation machineOpPresser1 = new MachineTypeOperation(machineTypePresser, op5kgPresser);
            MachineTypeOperation machineOpPresser2 = new MachineTypeOperation(machineTypePresser, op10kgPresser);
            
            machineTypePresser.AddOperation(machineOpPresser1);
            machineTypePresser.AddOperation(machineOpPresser2);
            
            dbContext.MachineTypes.Add(machineTypePresser);

            
            MachineType machineTypeCutter = new MachineType("Cutter-Machine");
            
            MachineTypeOperation machineOpCutter1 = new MachineTypeOperation(machineTypeCutter, op5kgCutter);

            machineTypeCutter.AddOperation(machineOpCutter1);
            
            dbContext.MachineTypes.Add(machineTypeCutter);
            

            MachineType machineTypePresserAndCutter = new MachineType("Presser-and-Cutter-Machine-Type");
            
            MachineTypeOperation machineOpPresserCutter1 = new MachineTypeOperation(machineTypePresserAndCutter, op5kgPresser);
            MachineTypeOperation machineOpPresserCutter2 = new MachineTypeOperation(machineTypePresserAndCutter, op10kgPresser);
            MachineTypeOperation machineOpPresserCutter3 = new MachineTypeOperation(machineTypePresserAndCutter, op5kgCutter);
            
            machineTypePresserAndCutter.AddOperation(machineOpPresserCutter1);
            machineTypePresserAndCutter.AddOperation(machineOpPresserCutter2);
            machineTypePresserAndCutter.AddOperation(machineOpPresserCutter3);

            dbContext.MachineTypes.Add(machineTypePresserAndCutter);



            //Machines
            Machine forkPresserMachine = new Machine("B1","Fork Presser", "IKEA", machineTypePresser);
            Machine forkCutterMachine = new Machine("B2","Fork Cutter", "Herdmar", machineTypeCutter );
            Machine spoonPresserCutterMachine = new Machine("B3","Spoon Cutter and Presser", "IKEA", machineTypePresserAndCutter);

            dbContext.Machines.Add(forkPresserMachine);
            dbContext.Machines.Add(forkCutterMachine);
            dbContext.Machines.Add(spoonPresserCutterMachine);
            
            dbContext.SaveChanges();

        }
    }
}