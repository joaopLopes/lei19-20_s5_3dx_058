﻿using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Infrastructure.Persistence.Impl;

namespace MasterDataFactory.Infrastructure.Bootstraps
{
    public static class MachineTypeBootstrap
    {
        public static void Seed(RepositoryContext dbContext)
        {
            var op1 = new Operation("TestTool", 1, "Cutter", 5);
            var op2 = new Operation("TestTool", 1, "Presser", 2);
            var op3 = new Operation("TestTool", 1, "Cutter", 5); //duplicated
            dbContext.Operations.Add(op1);
            dbContext.Operations.Add(op2);
            dbContext.Operations.Add(op3);

            var mactype = new MachineType("machine-test");
            mactype.AddOperation(new MachineTypeOperation(mactype, op1));
            mactype.AddOperation(new MachineTypeOperation(mactype, op2));
            
            var mactype2 = new MachineType("machine-test2");
            var mactype3 = new MachineType("machine-test3");
            mactype2.AddOperation(new MachineTypeOperation(mactype2, op1));
            dbContext.MachineTypes.Add(mactype);
            dbContext.MachineTypes.Add(mactype2);
            dbContext.MachineTypes.Add(mactype3);
            dbContext.SaveChanges();
        }
    }
}