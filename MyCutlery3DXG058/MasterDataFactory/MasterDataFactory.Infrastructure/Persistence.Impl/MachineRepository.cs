﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Persistence;
using Microsoft.EntityFrameworkCore;
using Machine = MasterDataFactory.Core.Models.Machines.Machine;


namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public class MachineRepository : RepositoryBase<Machine>, IMachineRepository
    {
        public MachineRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
        
        public Task<Machine> FindBySerialNumber(string serialNumber)
            {
                Machine asd =  this.RepositoryContext.Machines.Where(machine => machine.MachineSerialNumber.IsEqualTo(serialNumber)).AsTracking().SingleAsync().Result;

                return this.RepositoryContext.Machines
                    .Where(machine => machine.MachineSerialNumber.IsEqualTo(serialNumber)).AsTracking().SingleAsync();
            }
        
        public Task<IEnumerable<Machine>> FindByModel(string model)
        {
            return this.RepositoryContext.FindAsync<IEnumerable<Machine>>(model);
        }
        
        public Task<IEnumerable<Machine>> FindByBrand(string brand)
        {
            return this.RepositoryContext.FindAsync<IEnumerable<Machine>>(brand);
        }
        
        public Task<List<Machine>> FindByMachineType(string name)
        {
            return RepositoryContext.Machines.Where(machine => machine.MachineType.MachineTypeName.IsEqualTo(name)).ToListAsync();
            
        }

        /**
         * Allows the user to get the available Machines.
         */
        public Task<List<Machine>> FindNonAssociatedMachines()
        {
            return RepositoryContext.Machines.Where(machine => machine.MachineState.GetCurrentState().Equals(MachineStateEnum.NonAssociated))
                .ToListAsync();
        }
    }
}