using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Models.ProductionLines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Operation> Operations { get; set; }
        public DbSet<ProductionLine> ProductionLines { get; set; }
        public DbSet<MachineType> MachineTypes { get; set; }
        public DbSet<Machine> Machines { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductionLine>(new ProductionLine.ProductionLineConfiguration().Configure);
            modelBuilder.Entity<Machine>(new Machine.MachineConfiguration().Configure);
            modelBuilder.Entity<MachineType>(new MachineType.MachineTypeConfiguration().Configure);
            modelBuilder.Entity<Operation>(new Operation.OperationConfiguration().Configure);
            modelBuilder.Entity<MachineTypeOperation>(new MachineTypeOperation.MachineTypeOperationConfiguration()
                .Configure);
            modelBuilder.Entity<MachineAssociatedState>(
                new MachineAssociatedState.MachineAssociatedStateConfiguration().Configure);
            modelBuilder.Entity<MachineState>(
                new MachineState.MachineStateConfiguration().Configure);
            modelBuilder.Entity<MachineNonAssociatedState>(
                new MachineNonAssociatedState.MachineNonAssociatedStateConfiguration().Configure);
        }
    }
}