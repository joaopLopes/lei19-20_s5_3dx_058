﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public class ProductionLineRepository : RepositoryBase<ProductionLine>, IProductionLineRepository
    {
        public ProductionLineRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }


        /**
         * Returns by its production Line.
         */
        public Task<ProductionLine> FindByDomainId(string productionLineIdentifier)
        {
            IQueryable<ProductionLine> result = this.RepositoryContext.ProductionLines
                .Where(productionLine => productionLine.ProductionLineIdentifier.IsEqualToo(productionLineIdentifier));
            if (!result.AnyAsync().Result)
            {
                throw new ArgumentException("The Production Line " + productionLineIdentifier + " does not exist.");
            }

            return result.FirstAsync();
        }
    }
}