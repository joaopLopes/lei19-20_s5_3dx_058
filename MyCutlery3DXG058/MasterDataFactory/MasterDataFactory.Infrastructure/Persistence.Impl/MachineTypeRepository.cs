﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public class MachineTypeRepository : RepositoryBase<MachineType>, IMachineTypeRepository
    {
        public MachineTypeRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public Task<MachineType> FindByStringId(string name)
        {
            IQueryable<MachineType> ret =  this.RepositoryContext.MachineTypes.Where(machinetype => machinetype.MachineTypeName.IsEqualTo(name))
                .AsTracking();
            if (!ret.AnyAsync().Result)
            {
                throw new ArgumentException("ERROR - MachineType not instantiated");
            }

            return ret.SingleAsync();
   
        }
        
        public async Task UpdateMachineTypesOperations(string name, ICollection<MachineTypeOperation> operationsIds)
        {
            MachineType machineType = FindByStringId(name).Result;
            machineType.ChangeMachineTypesOperations(operationsIds);
            await this.RepositoryContext.SaveChangesAsync();
        }
    }
}