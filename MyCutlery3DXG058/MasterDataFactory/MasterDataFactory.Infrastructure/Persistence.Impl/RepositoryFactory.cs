using MasterDataFactory.Core.Persistence;

namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private readonly RepositoryContext _repoContext;
        private IOperationRepository _operation;
        private IMachineTypeRepository _mactype;
        private IProductionLineRepository _productionLine;
        private IMachineRepository _machine;

        public RepositoryFactory(RepositoryContext repositoryContext)
        {
            if (this._repoContext == null)
                _repoContext = repositoryContext;
        }


        public IOperationRepository Operation
        {
            get
            {
                if (_operation == null)
                {
                    _operation = new OperationRepository(_repoContext);
                }

                return _operation;
            }
        }

        public IMachineRepository Machine
        {
            get
            {
                if (_machine == null)
                {
                    _machine = new MachineRepository(_repoContext);
                }

                return _machine;
            }
        }

        public IMachineTypeRepository MachineType
        {
            get
            {
                if (_mactype == null)
                {
                    _mactype = new MachineTypeRepository(_repoContext);
                }

                return _mactype;
            }
        }

        /**
         * Method from the factory that instantiates a Production Line repository.
         */
        public IProductionLineRepository ProductionLine
        {
            get
            {
                if (_productionLine == null)
                {
                    _productionLine = new ProductionLineRepository(_repoContext);
                }

                return _productionLine;
            }
        }
    }
}