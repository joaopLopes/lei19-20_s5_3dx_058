using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MasterDataFactory.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext RepositoryContext { get; set; }

        public RepositoryBase(RepositoryContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.RepositoryContext.Set<T>().AsTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.RepositoryContext.Set<T>().Where(expression).AsTracking();
        }

        public void Create(T entity)
        {
            this.RepositoryContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.RepositoryContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
        }

        public Task<T> FindById(long id)
        {
            return this.RepositoryContext.FindAsync<T>(id);
        }

        public Task SaveASync()
        {
            return this.RepositoryContext.SaveChangesAsync();
        }
    }
}