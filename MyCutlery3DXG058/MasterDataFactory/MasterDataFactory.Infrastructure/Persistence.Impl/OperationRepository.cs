using System;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Persistence;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Infrastructure.Persistence.Impl
{
    public class OperationRepository : RepositoryBase<Operation>, IOperationRepository
    {
        public OperationRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public Task<Operation> FindById(string description)
        {
            IQueryable<Operation> result = this.RepositoryContext.Operations
                .Where(operation => operation.Description.IsEqualTo(description));
            if (!result.AnyAsync().Result)
            {
                throw new ArgumentException("The Operation " + description + " does not exist.");
            }

            return result.FirstAsync();
        }
    }
}