﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataFactory.Infrastructure.Migrations
{
 [ExcludeFromCodeCoverage]
    public partial class migration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MachineTypes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MachineTypeName_Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Operations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExecutionTime_Hours = table.Column<int>(nullable: false),
                    ExecutionTime_Minutes = table.Column<int>(nullable: false),
                    ExecutionTime_Seconds = table.Column<int>(nullable: false),
                    Description_Designation = table.Column<string>(nullable: true),
                    Tool_Designation = table.Column<string>(nullable: true),
                    Tool_SetupTime_Hours = table.Column<int>(nullable: false),
                    Tool_SetupTime_Minutes = table.Column<int>(nullable: false),
                    Tool_SetupTime_Seconds = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLines",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductionLineIdentifier_ProductionLineId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MachineTypeOperation",
                columns: table => new
                {
                    MachineTypeId = table.Column<long>(nullable: false),
                    OperationId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineTypeOperation", x => new { x.MachineTypeId, x.OperationId });
                    table.ForeignKey(
                        name: "FK_MachineTypeOperation_MachineTypes_MachineTypeId",
                        column: x => x.MachineTypeId,
                        principalTable: "MachineTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MachineTypeOperation_Operations_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Machines",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MachineSerialNumber_SerialNumberString = table.Column<string>(nullable: true),
                    MachineModel_MachineModel = table.Column<string>(nullable: true),
                    MachineBrand_MachineBrand = table.Column<string>(nullable: true),
                    MachineTypeId = table.Column<long>(nullable: true),
                    ProductionLineId = table.Column<long>(nullable: true),
                    Slot = table.Column<long>(nullable: false),
                    MachineWorkingState_WorkingState = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Machines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Machines_MachineTypes_MachineTypeId",
                        column: x => x.MachineTypeId,
                        principalTable: "MachineTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Machines_ProductionLines_ProductionLineId",
                        column: x => x.ProductionLineId,
                        principalTable: "ProductionLines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MachineState",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MachineId = table.Column<long>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineState", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MachineState_Machines_MachineId",
                        column: x => x.MachineId,
                        principalTable: "Machines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Machines_MachineTypeId",
                table: "Machines",
                column: "MachineTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Machines_ProductionLineId",
                table: "Machines",
                column: "ProductionLineId");

            migrationBuilder.CreateIndex(
                name: "IX_Machines_MachineSerialNumber_SerialNumberString",
                table: "Machines",
                column: "MachineSerialNumber_SerialNumberString",
                unique: true,
                filter: "[MachineSerialNumber_SerialNumberString] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MachineState_MachineId",
                table: "MachineState",
                column: "MachineId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MachineTypeOperation_OperationId",
                table: "MachineTypeOperation",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_MachineTypes_MachineTypeName_Name",
                table: "MachineTypes",
                column: "MachineTypeName_Name",
                unique: true,
                filter: "[MachineTypeName_Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Operations_Description_Designation",
                table: "Operations",
                column: "Description_Designation",
                unique: true,
                filter: "[Description_Designation] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Operations_Tool_Designation",
                table: "Operations",
                column: "Tool_Designation");

            migrationBuilder.CreateIndex(
                name: "IX_Operations_Tool_SetupTime_Hours",
                table: "Operations",
                column: "Tool_SetupTime_Hours");

            migrationBuilder.CreateIndex(
                name: "IX_Operations_Tool_SetupTime_Minutes",
                table: "Operations",
                column: "Tool_SetupTime_Minutes");

            migrationBuilder.CreateIndex(
                name: "IX_Operations_Tool_SetupTime_Seconds",
                table: "Operations",
                column: "Tool_SetupTime_Seconds");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_ProductionLineIdentifier_ProductionLineId",
                table: "ProductionLines",
                column: "ProductionLineIdentifier_ProductionLineId",
                unique: true,
                filter: "[ProductionLineIdentifier_ProductionLineId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MachineState");

            migrationBuilder.DropTable(
                name: "MachineTypeOperation");

            migrationBuilder.DropTable(
                name: "Machines");

            migrationBuilder.DropTable(
                name: "Operations");

            migrationBuilder.DropTable(
                name: "MachineTypes");

            migrationBuilder.DropTable(
                name: "ProductionLines");
        }
    }
}
