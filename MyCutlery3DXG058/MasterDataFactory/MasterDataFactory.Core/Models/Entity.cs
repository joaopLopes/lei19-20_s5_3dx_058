namespace MasterDataFactory.Core.Models
{
    public abstract class Entity
    {
        public long Id { get; private set; }
    }
}