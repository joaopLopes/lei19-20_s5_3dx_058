﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace MasterDataFactory.Core.Models.ProductionLines
{
    public class ProductionLineIdentifier
    {

        Regex _regex = new Regex(@"^\d{3}$");
        public string ProductionLineId { get; set; }
        
        public ProductionLineIdentifier(string productionLineId)
        {
          
            if (string.IsNullOrEmpty(productionLineId))
            {
                throw  new InvalidDataException("The identifier of Production line can not be empty!");
            }
            if (!_regex.IsMatch(productionLineId))
            {
                throw new InvalidDataException("The Production Line Identifier is invalid!");
            }

            this.ProductionLineId = productionLineId;
        }
         
         

        public bool IsEqualToo(string productionLine)
        {
            return this.ProductionLineId.Equals(productionLine);
        }
    }
}