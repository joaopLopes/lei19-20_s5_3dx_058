﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Core.Models.ProductionLines
{
    /**
     *  This class is referent to the object Production Line. This class, according to our interpretation, has an
     *  ID and a list of associated machines that temporarily is not represented since it isan use case that is
     * *  not yet implemented.
     */
    public class ProductionLine
    {
        public long Id { get; set; } // For the database.
        protected internal virtual ICollection<Machine> ListAssociatedMachines { get; set; }

        public virtual ProductionLineIdentifier ProductionLineIdentifier { get; set; }

        /**
         * This constructor is necessary for EF purposes.
         */
        [ExcludeFromCodeCoverage]
        protected ProductionLine()
        {
            // This constructor is necessary for EF purposes.
        }

        /**
         * Normal constructor for the Production Line. This constructor pretty much associates non-associated machines to this machine.
         */
        public ProductionLine(ICollection<Machine> listOfNonAssociatedMachines, string productionLineIdentifier)
        {
            this.ListAssociatedMachines = listOfNonAssociatedMachines;
            this.ProductionLineIdentifier = new ProductionLineIdentifier(productionLineIdentifier);
        }

        /**
         * Equals method for the production Line.
         */
        protected bool Equals(ProductionLine other)
        {
            return Equals(ListAssociatedMachines, other.ListAssociatedMachines);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ProductionLine) obj);
        }

        public override int GetHashCode()
        {
            return (ListAssociatedMachines != null ? ListAssociatedMachines.GetHashCode() : 0);
        }

        public ProductionLineDto ToDto()
        {
            ICollection<MachineAssociateToLineDto> machines = new List<MachineAssociateToLineDto>();
            foreach (Machine machine in this.ListAssociatedMachines)
            {
                machines.Add(new MachineAssociateToLineDto(machine.MachineSerialNumber.SerialNumberString,
                    machine.Slot));
            }

            return new ProductionLineDto(machines, ProductionLineIdentifier.ProductionLineId)
            {
                ListAssociatedMachinesDto = machines,
                ProductionLineIdentifier = ProductionLineIdentifier.ProductionLineId
            };
        }


        /**
         * Nested class for the mapping
         */
        public class ProductionLineConfiguration : IEntityTypeConfiguration<ProductionLine>
        {
            public void Configure(EntityTypeBuilder<ProductionLine> builder)
            {
                builder.HasKey(productionLine => productionLine.Id);
                builder.HasMany(productionLine => productionLine.ListAssociatedMachines);
                builder.OwnsOne(id => id.ProductionLineIdentifier)
                    .HasIndex(productionLineId => productionLineId.ProductionLineId).IsUnique();
            }
        }

        /**
         * Updates a production line by changing its machine list'
         */
        public void UpdateProductionLineMachineList(ICollection<Machine> machinesList)
        {
            this.ListAssociatedMachines = machinesList;
        }

        /**
         * Checks if one machine is associated to the production line
         */
        public bool ProductionLineIncludesMachine(Machine machine)
        {
            return this.ListAssociatedMachines.Contains(machine);
        }

        /**
         * Disassociates all the current production line's machines, making it empty'
         */
        public void DisassociatedAllMachines()
        {
            foreach (Machine machine in this.ListAssociatedMachines)
            {
                machine.Disassociate();
            }
        }
        
        /**
         * Changes all the current production line's machines working state to deactivated'
         */
        public void DeactivateAllMachines()
        {
            foreach (Machine machine in this.ListAssociatedMachines)
            {
                machine.DeactivateMachine();
            }
        }
    }
}