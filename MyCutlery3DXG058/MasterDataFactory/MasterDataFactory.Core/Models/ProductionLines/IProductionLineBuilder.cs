﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;

namespace MasterDataFactory.Core.Models.ProductionLines
{
    public interface IProductionLineBuilder
    {

        /**
         * Validates the non associated Machines to be then associated to a production line.
         */
        void WithNonAssociatedMachines(ProductionLineDto productionLineDto);
        
        /**
         * Builds a production line.
         */
        ProductionLine Build(string productionLineIdentifier);
    }
}