﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;

namespace MasterDataFactory.Core.Models.ProductionLines
{
    public interface IProductionLineService
    {
        void UpdateProductionLineMachines(string productionLineIdentifier, ProductionLineUpdateDto productionLineUpdateDto);
    }
}