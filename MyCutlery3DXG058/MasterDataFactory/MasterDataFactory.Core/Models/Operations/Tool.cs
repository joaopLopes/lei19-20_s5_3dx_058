using System;
using System.Collections.Generic;

namespace MasterDataFactory.Core.Models.Operations
{
    internal class Tool : ValueObject
    {
        internal string Designation { get; set; }
        internal virtual Time SetupTime { get; set; }

        public Tool(string tool, long setupTime)
        {
            this.SetupTime = new Time(setupTime);
            this.Designation = tool;
        }

        protected Tool()
        {
            //for ef only
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Designation;
            yield return SetupTime;
        }

        public override string ToString()
        {
            return this.Designation + "\nSetup Time: " + this.SetupTime.ToString();
        }
    }
}