using System;
using System.Collections.Generic;
using MasterDataFactory.Core.Models.MachineTypes;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using MasterDataFactory.Core.Dtos.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


// ReSharper disable VirtualMemberCallInConstructor


[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace MasterDataFactory.Core.Models.Operations
{
    public class Operation : Entity
    {
        internal virtual Time ExecutionTime { get; set; }

        /**
         * Operation Id
         */
        public virtual Description Description { get; set; }


        internal virtual Tool Tool { get; set; }

        internal virtual ICollection<MachineTypeOperation> AssociatedMachineTypes { get; private set; }

        public Operation(string tool, long setupTime, string description, long executionTime)
        {
            this.Tool = new Tool(tool, setupTime);
            this.Description = new Description(description);
            this.ExecutionTime = new Time(executionTime);
        }


        [ExcludeFromCodeCoverage]
        protected Operation()
        {
            //required by entity framework
        }

        protected bool Equals(Operation other)
        {
            return Equals(Description, other.Description);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Operation) obj);
        }

        public override int GetHashCode()
        {
            return (Description != null ? Description.GetHashCode() : 0);
        }


        public void AddMachineTypeAssociation(MachineTypeOperation newOp)
        {
            if (AssociatedMachineTypes == null)
            {
                AssociatedMachineTypes = new List<MachineTypeOperation>();
            }

            AssociatedMachineTypes.Add(newOp);
        }

        public override string ToString()
        {
            return "Tool: " + this.Tool.ToString() +
                   "\nDescription: " + this.Description.ToString() +
                   "\nExecution Time: " + this.ExecutionTime.ToString();
        }


        public class OperationConfiguration : IEntityTypeConfiguration<Operation>
        {
            public void Configure(EntityTypeBuilder<Operation> builder)
            {
                builder.HasKey(operation => operation.Id);
                builder.OwnsOne(operation => operation.Tool).HasIndex(tool => tool.Designation);
                builder.OwnsOne(operation => operation.Tool).OwnsOne(tool => tool.SetupTime)
                    .HasIndex(time => time.Seconds);
                builder.OwnsOne(operation => operation.Tool).OwnsOne(tool => tool.SetupTime)
                    .HasIndex(time => time.Minutes);
                builder.OwnsOne(operation => operation.Tool).OwnsOne(tool => tool.SetupTime)
                    .HasIndex(time => time.Hours);
                builder.OwnsOne(id => id.Description).HasIndex(designation => designation.Designation).IsUnique();
                builder.OwnsOne(operation => operation.ExecutionTime).Property(duration => duration.Seconds);
                builder.OwnsOne(operation => operation.ExecutionTime).Property(duration => duration.Minutes);
                builder.OwnsOne(operation => operation.ExecutionTime).Property(duration => duration.Hours);
            }
        }

        public OperationDto ToDto()
        {
            return new OperationDto
            {
                Description = this.Description.Designation,
                ExecutionTime = this.ExecutionTime.ToSeconds(),
                Tool = this.Tool.Designation,
                SetupTime = this.Tool.SetupTime.ToSeconds()
            };
        }
    }
}