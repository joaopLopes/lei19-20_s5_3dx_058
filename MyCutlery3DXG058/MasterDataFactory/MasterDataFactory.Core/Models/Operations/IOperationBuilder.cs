using MasterDataFactory.Core.Dtos.Operations;

namespace MasterDataFactory.Core.Models.Operations
{
    public interface IOperationBuilder
    {
        void WithDataFromDto(OperationDto dto);
        Operation Create();
    }
}