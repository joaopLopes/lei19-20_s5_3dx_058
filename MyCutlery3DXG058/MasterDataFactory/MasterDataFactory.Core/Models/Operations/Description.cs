using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Core.Models.Operations
{
    [Owned]
    public class Description : ValueObject
    {
        internal string Designation { get; private set; }

        [ExcludeFromCodeCoverage]
        protected Description()
        {
            //for EF
        }

        internal Description(string designation)
        {
            if (!string.IsNullOrEmpty(designation))
            {
                if (designation.Trim().Contains(" "))
                {
                    throw new ArgumentException("Description must not have spaces. E.g. \"Cutter-5mm\".");
                }

                this.Designation = designation;
            }
            else
            {
                throw new ArgumentException("Description must not be empty.");
            }
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Designation;
        }

        public bool IsEqualTo(string description)
        {
            return this.Designation.Equals(description);
        }

        public override string ToString()
        {
            return this.Designation;
        }
    }
}