using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MasterDataFactory.Core.Models
{
    public class Time : ValueObject
    {
        public int Hours { get; protected set; }
        public int Minutes { get; protected set; }
        public int Seconds { get; protected set; }

        [ExcludeFromCodeCoverage]
        protected Time()
        {
            //for EF
        }

        public Time(long seconds)
        {
            if (seconds >= 0)
            {
                TimeSpan aux = TimeSpan.FromSeconds(seconds);
                this.Hours = aux.Hours;
                this.Minutes = aux.Minutes;
                this.Seconds = aux.Seconds;
            }
            else
            {
                throw new ArgumentException("Time must be positive.");
            }
        }

        public long ToSeconds()
        {
            return Hours * 3600 + Minutes * 60 + Seconds;
        }

        public override string ToString()
        {
            return String.Format(
                "{0:00}:{1:00}:{2:00}",
                this.Hours, this.Minutes, this.Seconds);
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Hours;
            yield return Minutes;
            yield return Seconds;
        }
    }
}