﻿using System.Diagnostics.CodeAnalysis;
using MasterDataFactory.Core.Models.ProductionLines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Core.Models.Machines
{
    public abstract class MachineState : Entity
    {
        protected internal virtual Machine Machine { get; set; }

        protected internal long MachineId { get; set; }

        protected MachineState(Machine machine)
        {
            this.Machine = machine;
            this.MachineId = machine.Id;
        }

        [ExcludeFromCodeCoverage]
        protected internal MachineState()
        {
            //for ef only
        }

        /**
         * Method that allows the user to associate the machine to the production line.
         */
        public abstract void AssociateMachineToProductionLine(ProductionLine productionLine, long slot);

        /**
         * Allows to get the current state of the machine, useful for the productionLine Builder.
         */
        public abstract MachineStateEnum GetCurrentState();

        /**
         * Allows the user to activate a Machine.
         */
        public abstract void ActivateMachine();

        /**
         * Allows the user to deactivate a machine.
         */
        public abstract void DeactivateMachine();

        public class MachineStateConfiguration : IEntityTypeConfiguration<MachineState>
        {
            public void Configure(EntityTypeBuilder<MachineState> modelBuilder)
            {
                modelBuilder.HasKey(machine => machine.Id);
            }
        }

        public abstract void Disassociate();
    }
}