﻿namespace MasterDataFactory.Core.Models.Machines
{
    public enum MachineStateEnum
    {
        Associated ,
        NonAssociated
    }
}