﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using MasterDataFactory.Core.Models.ProductionLines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Core.Models.Machines
{
    public class MachineAssociatedState : MachineState
    {
        /**
         * Constructor of already associated machine.
         */
        public MachineAssociatedState(Machine machine) : base(machine)
        {
        }

        [ExcludeFromCodeCoverage]
        protected MachineAssociatedState()
        {
            //for ef only
        }

        /**
         * This method is not implemented in machine associate, because since the machine is already associated, it is already there.
         *
         * The concept of disassociate does not exist!
         */
        public override void AssociateMachineToProductionLine(ProductionLine productionLine, long slot)
        {
            throw new InvalidDataException("The machine is already associated to a production line!");
        }

        /**
         * Returns the Machine State of the machine.
         */
        public override MachineStateEnum GetCurrentState()
        {
            return MachineStateEnum.Associated;
        }


        public override void Disassociate()
        {
            this.Machine.ProductionLine = null;
            this.Machine.SetState(new MachineNonAssociatedState(this.Machine));
        }

        /**
         * Activates a Machine in a production line.
         */
        public override void ActivateMachine()
        {
            Machine.MachineWorkingState.ActivateMachine();
        }

        /**
         * Deactivates a Machine in a production line.
         */
        public override void DeactivateMachine()
        {
            Machine.MachineWorkingState.DeactivateMachine();
        }

        public class MachineAssociatedStateConfiguration : IEntityTypeConfiguration<MachineAssociatedState>
        {
            public void Configure(EntityTypeBuilder<MachineAssociatedState> modelBuilder)
            {
                modelBuilder.HasBaseType<MachineState>();
            }
        }
    }
}