﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MasterDataFactory.Core.Models.Machines
{
    class Model : ValueObject
    {
        public string MachineModel { get; private set; }

        [ExcludeFromCodeCoverage]
        protected Model()
        {
            //required by EF
        }

        public Model(string machineModel)
        {
            if (!string.IsNullOrEmpty(machineModel))
            {
                this.MachineModel = machineModel;
            }
            else
            {
                throw new ArgumentException("Error: MACHINE NOT CREATED - Machine model cannot be null or empty!");
            }
        }
        

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return MachineModel;
        }

        protected bool Equals(Model other)

        {
            return base.Equals(other) && MachineModel == other.MachineModel;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Model) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (MachineModel != null ? MachineModel.GetHashCode() : 0);
            }
        }

    }
}