﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace MasterDataFactory.Core.Models.Machines
{
    class Brand : ValueObject
    {
        public string MachineBrand { get; private set; }


        [ExcludeFromCodeCoverage]
        protected Brand()
        {
            //required by EF
        }

        public Brand(string machineBrand)
        {
            if (!string.IsNullOrEmpty(machineBrand))
            {
                this.MachineBrand = machineBrand;
            }
            else
            {
                throw new ArgumentException("Error: MACHINE NOT CREATED - Machine brand cannot be null or empty!");
            }
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return MachineBrand;
        }

        protected bool Equals(Brand other)
        {
            return base.Equals(other) && MachineBrand == other.MachineBrand;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Brand) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (MachineBrand != null ? MachineBrand.GetHashCode() : 0);
            }
        }
    }
}