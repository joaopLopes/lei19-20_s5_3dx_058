﻿using System.Diagnostics.CodeAnalysis;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.ProductionLines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Core.Models.Machines
{
    public class Machine : Entity
    {
        //The machine's serial number
        public virtual SerialNumber MachineSerialNumber { get; protected set; }

        //The machine's model
        internal virtual Model MachineModel { get; set; }

        //The machine's brand
        internal virtual Brand MachineBrand { get; set; }

        //The machine's type (which determines which operations it can perform)
        public virtual MachineType MachineType { get; private set; }

        protected internal long AssociatedProductionLineId { get; set; }
        protected internal virtual ProductionLine ProductionLine { get; set; }

        protected internal long Slot { get; set; }

        public virtual MachineState MachineState { get; set; }

        public virtual MachineWorkingState MachineWorkingState { get; set; }


        [ExcludeFromCodeCoverage]
        protected Machine()
        {
            //required by EF
        }

        public Machine(string machineSerialNumber, string machineModel, string machineBrand, MachineType machineType)
        {
            this.MachineSerialNumber = new SerialNumber(machineSerialNumber);
            this.MachineModel = new Model(machineModel);
            this.MachineBrand = new Brand(machineBrand);
            this.MachineType = machineType;
            this.MachineState = new MachineNonAssociatedState(this);
            this.MachineWorkingState = new MachineWorkingState();
        }


        public MachineDto ToDto()
        {
            MachineDto dto = new MachineDto();
            dto.SerialNumber = MachineSerialNumber.SerialNumberString;
            dto.Brand = MachineBrand.MachineBrand;
            dto.Model = MachineModel.MachineModel;
            dto.MachineType = MachineType.MachineTypeName.Name;
            dto.MachineWorkingState = MachineWorkingState.WorkingState;
            if (this.MachineState.GetCurrentState().Equals(MachineStateEnum.Associated))
                dto.Slot = this.Slot;
            return dto;
        }


        /**
         * Method that allows the machine to associate itself to the production line.
         */
        public void AssociateToProductionLine(ProductionLine productionLine, long slot)
        {
            this.MachineState.AssociateMachineToProductionLine(productionLine, slot);
        }

        public void Disassociate()
        {
            this.MachineState.Disassociate();
        }

        /**
         * The current state of the machine.
         */
        public MachineStateEnum State()
        {
            return this.MachineState.GetCurrentState();
        }

        /**
         * Alters the state of the Machine.
         */
        public void SetState(MachineState newState)
        {
            this.MachineState = newState;
        }

        /**
         *  Alters the working state of the Machine to disassociated.
         */
        public void DeactivateMachine()
        {
            this.MachineState.DeactivateMachine();
        }

        /**
         * Alters the working state of the Machine to associated.
         */
        public void ActivateMachine()
        {
            this.MachineState.ActivateMachine();
        }

        protected bool Equals(Machine other)
        {
            return Equals(MachineModel, other.MachineModel) && Equals(MachineBrand, other.MachineBrand) &&
                   Equals(MachineType, other.MachineType) &&
                   AssociatedProductionLineId == other.AssociatedProductionLineId &&
                   Equals(ProductionLine, other.ProductionLine) && Equals(MachineState, other.MachineState);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Machine) obj);
        }


        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (MachineModel != null ? MachineModel.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MachineBrand != null ? MachineBrand.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MachineType != null ? MachineType.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ AssociatedProductionLineId.GetHashCode();
                hashCode = (hashCode * 397) ^ (ProductionLine != null ? ProductionLine.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MachineState != null ? MachineState.GetHashCode() : 0);
                return hashCode;
            }
        }

        public class MachineConfiguration : IEntityTypeConfiguration<Machine>
        {
            public void Configure(EntityTypeBuilder<Machine> modelBuilder)
            {
                modelBuilder.HasKey(machine => machine.Id);
                modelBuilder.OwnsOne(machine => machine.MachineModel).Property(model => model.MachineModel);
                modelBuilder.OwnsOne(machine => machine.MachineBrand).Property(brand => brand.MachineBrand);
                modelBuilder.OwnsOne(machine => machine.MachineWorkingState)
                    .Property(workingState => workingState.WorkingState);
                modelBuilder.OwnsOne(machine => machine.MachineSerialNumber)
                    .HasIndex(number => number.SerialNumberString).IsUnique();
                modelBuilder.HasOne(machine => machine.MachineType).WithMany(machineType => machineType.ListMachines);
                modelBuilder.HasOne(machine => machine.MachineState).WithOne(state => state.Machine)
                    .HasForeignKey<MachineState>(state => state.MachineId);
                modelBuilder.Property(machine => machine.Slot);
                modelBuilder.HasOne(machine => machine.ProductionLine).WithMany(line => line.ListAssociatedMachines);
            }
        }

        public void ChangeMachineType(MachineType newType)
        {
            //TODO: missing validation
            this.MachineType = newType;
        }
    }
}