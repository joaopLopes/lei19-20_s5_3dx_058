﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using MasterDataFactory.Core.Models.ProductionLines;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Core.Models.Machines
{
    public class MachineNonAssociatedState : MachineState
    {
        /**
         * Constructor that calls the base constructor of Machine State
         */
        public MachineNonAssociatedState(Machine machine) : base(machine)
        {
            //for ef only
        }

        [ExcludeFromCodeCoverage]
        protected MachineNonAssociatedState()
        {
            //for ef 
        }

        /**
         * In this case we have to associate it to a production line.
         */
        public override void AssociateMachineToProductionLine(ProductionLine productionLine, long slot)
        {
            Machine.ProductionLine = productionLine;
            Machine.Slot = slot;
            Machine.SetState(new MachineAssociatedState(Machine));
            Machine.MachineWorkingState.ActivateMachine();
        }

        /**
         * Method that allows the user to get the state of the machine enum retrieved.
         */
        public override MachineStateEnum GetCurrentState()
        {
            return MachineStateEnum.NonAssociated;
        }


        public override void Disassociate()
        {
            throw new Exception("Machine is not associated.");
        }

        /**
         * Throws an error because you can not activate a machine that is not associated to any production line.
         */
        public override void ActivateMachine()
        {
            throw new InvalidDataException("Can't activate a machine that is not associated to a production line");
        }

        /**
         * Throws an error because it makes no sense to deactivate a machine that is not associated to a production line.
         */
        public override void DeactivateMachine()
        {
            throw new InvalidDataException("Can't deactivate a machine that is not associated to a production line");
        }

        public class MachineNonAssociatedStateConfiguration : IEntityTypeConfiguration<MachineNonAssociatedState>
        {
            public void Configure(EntityTypeBuilder<MachineNonAssociatedState> modelBuilder)
            {
                modelBuilder.HasBaseType<MachineState>();
            }
        }
    }
}