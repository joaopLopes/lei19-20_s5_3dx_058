﻿using System.Threading.Tasks;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Models.MachineTypes;

namespace MasterDataFactory.Core.Models.Machines
{
    public interface IMachineBuilder
    {
        Task<Machine> BuildMachine(MachineDto dto);
    }
}