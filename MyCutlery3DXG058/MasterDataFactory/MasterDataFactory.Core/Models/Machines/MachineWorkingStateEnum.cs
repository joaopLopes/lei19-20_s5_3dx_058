﻿using System.IO;

namespace MasterDataFactory.Core.Models.Machines
{
    /**
     * Working status of a machine. 
     */
public enum MachineWorkingStateEnum
    {
        Activated ,
        Deactivated
    }

public class MachineWorkingState
{
    public string WorkingState { get; set; }

    public MachineWorkingState()
    {
        WorkingState = MachineWorkingStateEnum.Deactivated.ToString();
    }

    /**
     * Dissassociates the machine.
     */
    public void DeactivateMachine()
    {
        if (WorkingState == MachineWorkingStateEnum.Deactivated.ToString())
        {
            throw new InvalidDataException("You can not deactivate a machine that is already not active. ");
        }
        
        WorkingState = MachineWorkingStateEnum.Deactivated.ToString();

    }

    /**
     * Associates the Machine.
     */
    public void ActivateMachine()
    {
        if (WorkingState == MachineWorkingStateEnum.Activated.ToString())
        {
            throw new InvalidDataException("You can not activate a machine that is already active.");
        }
        WorkingState = MachineWorkingStateEnum.Activated.ToString();
    }

    /**
     * Returns the working state of the machine.
     */
    public string getWorkingStateOfMachine()
    {
        return this.WorkingState;
    }
}
}