﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MasterDataFactory.Core.Models.Machines
{
    public class SerialNumber : ValueObject
    {
        
        internal string SerialNumberString { get; set; }
        internal SerialNumber(string serialNumberString)
        {
            ValidateMachineSerialNumber(serialNumberString);
            SerialNumberString = serialNumberString;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return SerialNumberString;
        }
        
        private void ValidateMachineSerialNumber(string serialNumber)
        {
            Regex _regex = new Regex("[^A-Z,0-9]+");


            if (string.IsNullOrWhiteSpace(serialNumber))
            {
                throw new ArgumentException("ERROR: The machine serial number can not be empty - Machine was not created");
            }

            if (_regex.Match(serialNumber).Success)
            {
                throw new ArgumentException("ERROR: The machine serial number can only be constituted by uppercase letters and/or numbers - Machine was not created");
            }
        }

        public bool IsEqualTo(string serialNumber)
        {
            return String.Equals(this.SerialNumberString, serialNumber, StringComparison.OrdinalIgnoreCase);
        }
    }
}