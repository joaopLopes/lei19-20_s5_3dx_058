﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFactory.Core.Models.MachineTypes
{
    [Owned]
    public class MachineTypeId : ValueObject
    {
        internal string Name { get; private set; }

        protected MachineTypeId()
        {
            //for ef only
        }

        internal MachineTypeId(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Error: Name is empty - MachineType was not created");
            }
            if (name.Trim().Contains(" "))
            {
                throw new ArgumentException("Name must not have spaces. E.g. \"Cutter-5mm\".");
            }
            this.Name = name;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
        }

        public bool IsEqualTo(string name)
        {
            return this.Name.Equals(name);
        }
    }
}