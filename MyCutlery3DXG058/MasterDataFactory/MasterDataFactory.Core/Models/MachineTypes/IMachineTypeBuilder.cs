using System.Collections.Generic;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.MachineTypes;

namespace MasterDataFactory.Core.Models.MachineTypes
{
    public interface IMachineTypeBuilder
    {
        MachineType BuildMachineType(MachineTypeDto initialDto);

        ICollection<MachineTypeOperation> BuildListOperations(ICollection<string> list, MachineType machinetype);
    }
}