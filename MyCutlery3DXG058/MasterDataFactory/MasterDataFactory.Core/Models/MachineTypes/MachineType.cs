﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace MasterDataFactory.Core.Models.MachineTypes
{
    public class MachineType:Entity
    {
        //Domain ID, the "name" of the MachineType
        public virtual MachineTypeId MachineTypeName { get; protected set; }

        //List of Operations allowed by this MachineType
        public virtual ICollection<MachineTypeOperation> ListOp { get; protected set; }
        
        public virtual ICollection<Machine> ListMachines { get; protected set; }
        
        protected MachineType()
        {
            //for EF
        }

        public MachineType(string name)
        {
            this.MachineTypeName = new MachineTypeId(name);
            this.ListOp = new List<MachineTypeOperation>();
        }

        public void AddOperation(MachineTypeOperation newOp)
        {
            ListOp.Add(newOp);
        }
        
        public class MachineTypeConfiguration : IEntityTypeConfiguration<MachineType>
        {
            public void Configure(EntityTypeBuilder<MachineType> modelBuilder)
            {
                modelBuilder.HasKey(machinetype => machinetype.Id);
                modelBuilder.OwnsOne(machinetype => machinetype.MachineTypeName).HasIndex(name => name.Name).IsUnique();
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MachineType) obj);
        }

        protected bool Equals(MachineType other)
        {
            if (other.ListOp.Count != this.ListOp.Count)
            {
                return false;
            }

            List<MachineTypeOperation> aux1 = other.ListOp.ToList();
            List<MachineTypeOperation> aux2 = ListOp.ToList();
            for (int i = 0; i < aux1.Count; i++)
            {
                if (!aux1.Contains(aux2[i]))
                {
                    return false;
                }
            }

            return Equals(MachineTypeName, other.MachineTypeName);
        }

        public override int GetHashCode()
        {
            var hashCode = (MachineTypeName != null ? MachineTypeName.GetHashCode() : 0);
            return hashCode;
        }

        public MachineTypeDto ToDto()
        {
            var dto = new MachineTypeDto();
            dto.Name = this.MachineTypeName.Name;
            dto.ListMacTypeOperations = new List<string>();

            foreach (MachineTypeOperation machineTypeOperation in this.ListOp)

            {
                dto.ListMacTypeOperations.Add(machineTypeOperation.Op.ToDto().Description);
            }
            return dto;
        }

        /**
         * Method that allows the users to alter the machine types operations.
         */
        public void ChangeMachineTypesOperations(ICollection<MachineTypeOperation> newMachineTypeOperations)
        {
            this.ListOp.Clear();
            this.ListOp = newMachineTypeOperations;
        }

        /**
         * Method that allows to return the dto we want.
         */
        public MachineTypeChangeOperationReturnDTO ToChangeOperationReturnDto()
        {
            MachineTypeChangeOperationReturnDTO returnDto = new MachineTypeChangeOperationReturnDTO();
            returnDto.Name = this.MachineTypeName.Name;

            List<OperationDto> listOperationDto = new List<OperationDto>();
            foreach (MachineTypeOperation machineTypeOperation in this.ListOp)
            {
                OperationDto operationDto = machineTypeOperation.Op.ToDto();
                listOperationDto.Add(operationDto);
            }

            returnDto.operationDTOList = listOperationDto;
            return returnDto;
        }
        /**
         * Returns A List of operations DTOs of this machine type.
         */
        public List<OperationDto> OperationDtos()
        {
            List<OperationDto> operationListDTO = new List<OperationDto>();

            foreach (MachineTypeOperation machineTypeOperation  in this.ListOp)
            {
                operationListDTO.Add(machineTypeOperation.Op.ToDto());
            }
            return operationListDTO;
        }
    }
}