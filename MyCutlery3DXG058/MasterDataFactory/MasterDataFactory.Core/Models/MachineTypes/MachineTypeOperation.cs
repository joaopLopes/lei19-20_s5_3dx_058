﻿using MasterDataFactory.Core.Models.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFactory.Core.Models.MachineTypes
{
    public class MachineTypeOperation
    {
        internal long MachineTypeId { get; private set; }
        internal virtual MachineType Mactype { get; set; }
        internal long OperationId { get; private set; }
        internal virtual Operation Op { get; set; }

        protected MachineTypeOperation()
        {
        }

        public MachineTypeOperation(MachineType mactype, Operation op)
        {
            this.Op = op;
            this.OperationId = op.Id;
            this.Mactype = mactype;
            this.MachineTypeId = mactype.Id;
        }

        public class MachineTypeOperationConfiguration : IEntityTypeConfiguration<MachineTypeOperation>
        {
            public void Configure(EntityTypeBuilder<MachineTypeOperation> builder)
            {
                builder.HasKey(macop => new {macop.MachineTypeId, macop.OperationId});

                builder.HasOne(macop => macop.Mactype).WithMany(mactype => mactype.ListOp)
                    .HasForeignKey(macop => macop.MachineTypeId);
                builder.HasOne(macop => macop.Op).WithMany(op => op.AssociatedMachineTypes)
                    .HasForeignKey(macop => macop.OperationId);
            }
        }
    }
}