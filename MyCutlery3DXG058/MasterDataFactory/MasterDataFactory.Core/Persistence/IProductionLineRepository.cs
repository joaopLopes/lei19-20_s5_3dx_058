﻿using System.Threading.Tasks;
using MasterDataFactory.Core.Models.ProductionLines;

namespace MasterDataFactory.Core.Persistence
{
    public interface IProductionLineRepository : IRepositoryBase<ProductionLine>
    {
        /**
         * Method in the interface that returns a production line according to the id passed as a parameter.
         */
        Task<ProductionLine> FindById(long id);

        /**
         * Returns a production by its domain id (which is its production Line Identifier).
         */
        Task<ProductionLine> FindByDomainId(string productionLineIdentifier);
    }
}