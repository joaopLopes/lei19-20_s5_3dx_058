using System.Threading.Tasks;
using MasterDataFactory.Core.Models.Operations;

namespace MasterDataFactory.Core.Persistence
{
    public interface IOperationRepository : IRepositoryBase<Operation>
    {
        Task<Operation> FindById(string description);
    }
}