﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Machine = MasterDataFactory.Core.Models.Machines.Machine;

namespace MasterDataFactory.Core.Persistence
{
    public interface IMachineRepository : IRepositoryBase<Machine>
    {
        Task<Machine> FindById(long id);
        Task<Machine> FindBySerialNumber(string serialNumber);
        //Task<IEnumerable<Machine>> FindByModel(string model);
        //Task<IEnumerable<Machine>> FindByBrand(string brand);
        Task<List<Machine>> FindByMachineType(string name);

        /**
         * Allows the user to fetch the available machines to be associated to a production line.
         */
        Task<List<Machine>> FindNonAssociatedMachines();
    }
}