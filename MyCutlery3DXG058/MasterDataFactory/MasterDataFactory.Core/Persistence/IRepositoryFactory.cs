namespace MasterDataFactory.Core.Persistence
{
    public interface IRepositoryFactory
    {
        IMachineTypeRepository MachineType { get; }
        IOperationRepository Operation { get; }

        IProductionLineRepository ProductionLine { get; }

        //IProductionLineRepository ProductionLine { get; }
        
        IMachineRepository Machine { get; }
    }
}