﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFactory.Core.Models.MachineTypes;

namespace MasterDataFactory.Core.Persistence
{
    public interface IMachineTypeRepository : IRepositoryBase<MachineType>
    {
        Task<MachineType> FindById(long id);
        Task UpdateMachineTypesOperations(string name, ICollection<MachineTypeOperation> operationsIds);
        Task<MachineType> FindByStringId(string name);

    }
}