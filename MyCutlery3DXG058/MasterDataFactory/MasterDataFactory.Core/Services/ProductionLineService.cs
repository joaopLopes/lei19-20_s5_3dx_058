﻿using System;
using System.Collections.Generic;
using System.IO;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;

namespace MasterDataFactory.Core.Services
{
    public class ProductionLineService : IProductionLineService
    {
        private IMachineRepository _machineRepository;
        private IProductionLineRepository _prodLineRepository;

        public ProductionLineService(IMachineRepository machineRepository,
            IProductionLineRepository productionLineRepository)
        {
            _machineRepository = machineRepository;
            _prodLineRepository = productionLineRepository;
        }

        private ICollection<Machine> ValidateMachineListToUpdate(
            ICollection<MachineAssociateToLineDto> machinesSerialNumberList,
            ProductionLine productionLine)
        {
            ICollection<Machine> newMachinesList = new List<Machine>();

            foreach (MachineAssociateToLineDto machine in machinesSerialNumberList)
            {
                if (_machineRepository.FindBySerialNumber(machine.MachineSerialNumber).Result != null)
                {
                    Machine foundMachine = _machineRepository.FindBySerialNumber(machine.MachineSerialNumber).Result;

                    //Check if the machine is not associated or if it is already associated to the production line. If it fails this if condition, it empties the list and throws error.
                    if (foundMachine.State().Equals(MachineStateEnum.NonAssociated) ||
                        productionLine.ProductionLineIncludesMachine(foundMachine))
                    {
                        newMachinesList.Add(foundMachine);
                    }
                    else
                    {
                        throw new InvalidDataException(
                            "Machine trying to add in the production line is already associated to other production line");
                    }
                }
                else
                {
                    // It means that the machine does not exist, in that case, with a non-existing machine in the repos we should throw an exception.
                    newMachinesList.Clear(); // Clear the list if it does not have a machine known to the system.
                    throw new InvalidDataException(
                        "Machine trying to implement in production Line does not exist.");
                }
            }

            return newMachinesList;
        }

        public void UpdateProductionLineMachines(string productionLineIdentifier,
            ProductionLineUpdateDto productionLineUpdateDto)
        {
            ProductionLine productionLineToUpdate = _prodLineRepository.FindByDomainId(productionLineIdentifier).Result;
            if (productionLineToUpdate == null)
            {
                throw new ArgumentException("The production line to update was not found!");
            }

            ICollection<Machine> newMachineList =
                this.ValidateMachineListToUpdate(productionLineUpdateDto.ListAssociatedMachinesDto,
                    productionLineToUpdate);

            productionLineToUpdate.DeactivateAllMachines();
            productionLineToUpdate.DisassociatedAllMachines();
            productionLineToUpdate.UpdateProductionLineMachineList(newMachineList);
            foreach (Machine machine in newMachineList)
            {
                long slot = this.FindSlot(machine, productionLineUpdateDto);
                machine.AssociateToProductionLine(productionLineToUpdate, slot);
            }
        }

        private long FindSlot(Machine machine, ProductionLineUpdateDto line)
        {
            foreach (MachineAssociateToLineDto dto in line.ListAssociatedMachinesDto)
            {
                if (dto.MachineSerialNumber.Equals(machine.MachineSerialNumber.SerialNumberString))
                {
                    return dto.Slot;
                }
            }

            throw new Exception("An error has occurred.");
        }
    }
}