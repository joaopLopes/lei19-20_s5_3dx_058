﻿using System;
using System.Collections.Generic;
using System.IO;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Dtos.ProductionLines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.ProductionLines;
using MasterDataFactory.Core.Persistence;
using Microsoft.Extensions.Logging;

namespace MasterDataFactory.Core.Services.Builders.Impl
{
    public class ProductionLineBuilder : IProductionLineBuilder
    {
        private ICollection<Machine> _listMachines;

        private IRepositoryFactory _machineRepository;
        private List<long> _listMachineSlots;

        public ProductionLineBuilder(IRepositoryFactory repositoryFactory)
        {
            _machineRepository = repositoryFactory;
        }

        /**
         * Very basically. This a method that validates the most important part of the productions lines. The context of each machine
         * that is going to be added to the production line.
         *
         * A machine is only considered valid to be added to a production line, if it does exist (and by existing we presume the validity of
         * the construction of the machine in the previous use case. Another relevant point for the machine to be valid to be added to a
         * production line is if only and only it is not already associated to another machine.
         *
         * If the machine is okay relatively to the previous points we can add it to the private list of machines that are going to be associated
         * to the list. If any machine in the list, fails any of these requirements we immediately empty the list making this builder
         * available to build another production line and we throw an exception that is to be caught above.
         * 
         */
        public void WithNonAssociatedMachines(ProductionLineDto productionLineDto)
        {
            this._listMachines = new List<Machine>();
            this._listMachineSlots = new List<long>();
            foreach (MachineAssociateToLineDto machine in productionLineDto.ListAssociatedMachinesDto)
            {
                if (_machineRepository.Machine.FindBySerialNumber(machine.MachineSerialNumber).Result != null)
                {
                    Machine foundMachine = _machineRepository.Machine.FindBySerialNumber(machine.MachineSerialNumber)
                        .Result;
                    //Check if the machine is already associated if not, add it to the list, if not just empty the list and throw error.
                    if (foundMachine.State().Equals(MachineStateEnum.NonAssociated))
                    {
                        this._listMachines.Add(foundMachine);
                        this._listMachineSlots.Add(machine.Slot);
                    }
                    else
                    {
                        this._listMachines.Clear();
                        throw new InvalidDataException(
                            "Machine trying to add in the production line is already associated to other production line");
                    }
                }
                else
                {
                    // It means that the machine does not exist, in that case, with a non-existing machine in the repos we should throw an exception.
                    this._listMachines.Clear(); // Clear the list if it does not have a machine known to the system.
                    throw new InvalidDataException(
                        "Machine trying to implement in production Line does not exist.");
                }
            }
        }

        /**
         * Builds the production line.
         */
        public ProductionLine Build(string productionLineIdentifier)
        {
            if (this._listMachines == null)
            {
                throw new InvalidDataException(
                    "Associate the respective machines to the Production Line!");
            }

            if (this._listMachines.Count == 0)
            {
                throw new InvalidDataException(
                    "The production line can not be created with no machines!");
            }

            ProductionLine productionLine = new ProductionLine(this._listMachines, productionLineIdentifier);
            int index = 0;
            foreach (Machine machine in this._listMachines)
            {
                machine.AssociateToProductionLine(productionLine, this._listMachineSlots[index]);
                index++;
            }
            this._listMachines.Clear();
            return productionLine;
        }
    }
}