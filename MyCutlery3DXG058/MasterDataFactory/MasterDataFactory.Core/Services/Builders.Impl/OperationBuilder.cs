using System;
using MasterDataFactory.Core.Dtos.Operations;
using MasterDataFactory.Core.Models;
using MasterDataFactory.Core.Models.Operations;

namespace MasterDataFactory.Core.Services.Builders.Impl
{
    public class OperationBuilder : IOperationBuilder
    {
        private Operation _operation;

        public void WithDataFromDto(OperationDto dto)
        {
            if (dto.Tool == null || dto.Description == null)
            {
                throw new ArgumentException("Fields tool and description must be filled.");
            }

            this._operation = new Operation(dto.Tool, dto.SetupTime, dto.Description, dto.ExecutionTime);
        }

        public Operation Create()
        {
            return _operation;
        }
    }
}