﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasterDataFactory.Core.DTOs;
using MasterDataFactory.Core.Dtos.MachineTypes;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Models.Operations;
using MasterDataFactory.Core.Persistence;

namespace MasterDataFactory.Core.Services.Builders.Impl
{
    public class MachineTypeBuilder : IMachineTypeBuilder
    {
        private IRepositoryFactory _rep;

        public MachineTypeBuilder(IRepositoryFactory newRep)
        {
            _rep = newRep;
        }

        public MachineType BuildMachineType(MachineTypeDto initialDto)
        {
            MachineType typeToReturn = new MachineType(initialDto.Name);
            List<MachineTypeOperation> list = this.BuildListOperations(initialDto.ListMacTypeOperations, typeToReturn).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                typeToReturn.AddOperation(list[i]);
                list[i].Op.AddMachineTypeAssociation(list[i]);
            }

            return typeToReturn;
        }


        public ICollection<MachineTypeOperation> BuildListOperations(ICollection<string> list, MachineType machinetype)
        {
            List<string> aux = list.ToList();
            ICollection<MachineTypeOperation> listToReturn = new List<MachineTypeOperation>();
            Operation op;
            for (var i = 0; i < list.Count; i++)
            {
                op = _rep.Operation.FindById(aux[i]).Result;
                if (op == null)
                {
                    throw new ArgumentException(
                        "Error (MachineType not created) - Operation ID is invalid or does not exist.");
                }

                if (!RelationExistsInList(op, listToReturn.ToList()))
                {
                    listToReturn.Add(new MachineTypeOperation(machinetype, op));
                }
                else
                {
                    throw new ArgumentException("Error (MachineType not created) - Operation is duplicated.");
                }
            }

            return listToReturn;
        }

        private bool RelationExistsInList(Operation op, List<MachineTypeOperation> list)
        {
            bool aux = false;
            for (var i = 0; i < list.Count; i++)
            {
                if (op.Equals(list[i].Op))
                {
                    aux = true;
                }
            }

            return aux;
        }
    }
}