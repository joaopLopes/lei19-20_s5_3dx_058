﻿using System;
using System.Threading.Tasks;
using MasterDataFactory.Core.Dtos.Machines;
using MasterDataFactory.Core.Models.Machines;
using MasterDataFactory.Core.Models.MachineTypes;
using MasterDataFactory.Core.Persistence;

namespace MasterDataFactory.Core.Services.Builders.Impl
{
    public class MachineBuilder : IMachineBuilder
    {
        private IRepositoryFactory __rep;

        public MachineBuilder(IRepositoryFactory newRepo)
        {
            this.__rep = newRepo;
        }



        public async Task<MachineType> VerifiesExistingMachineType(string machineType)
        {
            MachineType returnMachineType = await this.__rep.MachineType.FindByStringId(machineType);
            if (returnMachineType == null)
            {
                throw new ArgumentException("ERROR - MachineType is not specified.");
            }
            return returnMachineType;
        }

        public async Task<Machine> BuildMachine(MachineDto dto)
        {
            MachineType machType = await VerifiesExistingMachineType(dto.MachineType);
            Machine machineToReturn = new Machine(dto.SerialNumber, dto.Model,dto.Brand,machType);
            
            return machineToReturn;
        }
    }
}