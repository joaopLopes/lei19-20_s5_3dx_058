﻿namespace MasterDataFactory.Core.Dtos.Machines
{
    public class MachineDto
    {
        public string SerialNumber { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string MachineType { get; set; }

        public long Slot { get; set; }

        public string MachineWorkingState { get; set; }

        protected bool Equals(MachineDto other)
        {
            return SerialNumber == other.SerialNumber && Model == other.Model && Brand == other.Brand &&
                   MachineType == other.MachineType && MachineWorkingState == other.MachineWorkingState;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MachineDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (SerialNumber != null ? SerialNumber.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Model != null ? Model.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Brand != null ? Brand.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MachineWorkingState != null ? MachineWorkingState.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (MachineType != null ? MachineType.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}