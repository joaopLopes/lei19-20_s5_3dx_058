namespace MasterDataFactory.Core.Dtos.Machines
{
    public class MachineAssociateToLineDto
    {
        public string MachineSerialNumber;
        public long Slot;

        public MachineAssociateToLineDto(string serialNumber, long slot)
        {
            this.MachineSerialNumber = serialNumber;
            this.Slot = slot;
        }
    }
}