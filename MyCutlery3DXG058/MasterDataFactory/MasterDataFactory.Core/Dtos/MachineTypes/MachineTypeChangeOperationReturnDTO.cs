﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Operations;


namespace MasterDataFactory.Core.DTOs
{
    public class MachineTypeChangeOperationReturnDTO
    {
        public string Name { get; set; }

        public List<OperationDto> operationDTOList { get; set; }
    }
    
}