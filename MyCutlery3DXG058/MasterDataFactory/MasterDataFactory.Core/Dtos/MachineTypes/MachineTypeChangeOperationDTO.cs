﻿using System.Collections.Generic;

namespace MasterDataFactory.Core.Dtos.MachineTypes
{
    /**
     * Necessary to alter the machine type operations.
     */
    public class MachineTypeChangeOperationDto
    {
        public string Name { get; set; }

        public ICollection<string> ListMacTypeOperations { get; set; }
    }
}