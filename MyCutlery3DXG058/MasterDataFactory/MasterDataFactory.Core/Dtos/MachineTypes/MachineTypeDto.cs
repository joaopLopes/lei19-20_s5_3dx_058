﻿using System.Collections.Generic;

namespace MasterDataFactory.Core.Dtos.MachineTypes
{
    public class MachineTypeDto
    {
        //DTO class created for the purpose of returning data with the result of a HTTP Request
        public string Name{ get; set; }
        public List<string> ListMacTypeOperations { get; set; }

        protected bool Equals(MachineTypeDto other)
        {
            var aux = this.ListMacTypeOperations.Count;
            
            if (other.ListMacTypeOperations.Count != aux)
            {
                return false;
            }

            for (var i = 0; i < aux; i++)
            {
                if (!this.ListMacTypeOperations.Contains(other.ListMacTypeOperations[i]))
                {
                    return false;
                }
            }
            return Name == other.Name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MachineTypeDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 0;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ListMacTypeOperations != null ? ListMacTypeOperations.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}