using System.Diagnostics.CodeAnalysis;

namespace MasterDataFactory.Core.Dtos.Operations
{
    public class OperationDto
    {
        public string Description { get; set; }
        public string Tool { get; set; }
        public long ExecutionTime { get; set; }
        public long SetupTime { get; set; }

        protected bool Equals(OperationDto other)
        {
            return Description == other.Description && Tool == other.Tool && ExecutionTime == other.ExecutionTime &&
                   SetupTime == other.SetupTime;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OperationDto) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Description != null ? Description.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Tool != null ? Tool.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ ExecutionTime.GetHashCode();
                hashCode = (hashCode * 397) ^ SetupTime.GetHashCode();
                return hashCode;
            }
        }
    }
}