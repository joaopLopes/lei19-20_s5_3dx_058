﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Machines;

namespace MasterDataFactory.Core.Dtos.ProductionLines
{
    public class ProductionLineDto
    {
        public virtual ICollection<MachineAssociateToLineDto> ListAssociatedMachinesDto { get; set; }
        public string ProductionLineIdentifier { get; set; }

        public ProductionLineDto(ICollection<MachineAssociateToLineDto> listAssociatedMachinesDto,
            string productionLineIdentifier)
        {
            ListAssociatedMachinesDto = listAssociatedMachinesDto;
            this.ProductionLineIdentifier = productionLineIdentifier;
        }
    }
}