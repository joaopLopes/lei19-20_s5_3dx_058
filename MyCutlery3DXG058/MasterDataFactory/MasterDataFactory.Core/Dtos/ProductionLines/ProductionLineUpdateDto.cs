﻿using System.Collections.Generic;
using MasterDataFactory.Core.Dtos.Machines;

namespace MasterDataFactory.Core.Dtos.ProductionLines
{
    /**
     * This Dto is needed in order to update a production line's machines list'
     */
    public class ProductionLineUpdateDto
    {
        public ICollection<MachineAssociateToLineDto> ListAssociatedMachinesDto { get; set; }
    }
}