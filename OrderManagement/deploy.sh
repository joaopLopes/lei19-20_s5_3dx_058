#!/usr/bin/env bash
docker login --username=_ --password=$HEROKU_TOKEN registry.heroku.com

docker build --no-cache -t ge .

docker tag ge registry.heroku.com/order-management-58/web 

docker push registry.heroku.com/order-management-58/web

export DOCKER_IMAGE_ID=$(docker inspect ge --format='{{.Id}}')

curl -n -X PATCH https://api.heroku.com/apps/order-management-58/formation \
    -d '{
        "updates": [
            {
                "type": "web",
                "docker_image": "'"$DOCKER_IMAGE_ID"'"
            }
        ]
    }' \
    -H "Content-Type: application/json" \
    -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
    -H "Authorization: Bearer $HEROKU_TOKEN"
