import {User} from "../../src/domain/models/user/User";
import {ClientRole} from "../../src/domain/models/user/client/ClientRole";
import {AdminRole} from "../../src/domain/models/user/admin/AdminRole";
import {ClientDto} from "../../src/dtos/user/ClientDto";
import {ClientUpdateDTO} from "../../src/dtos/user/ClientUpdateDTO";
import {AdminDto} from "../../src/dtos/user/AdminDto";
import {UserMapper} from "../../src/mappers/UserMapper";

describe('User mapper test', () => {

    let mapper: UserMapper;
    let jsonClient: any;
    let jsonUpdateClient: any;
    let jsonAdmin: any;
    let userClientDto: ClientDto;
    let userClientUpdateDto: ClientUpdateDTO;
    let userAdminDto: AdminDto;
    let userClient1: User;
    let userClient1NullId: User;
    let userAdmin1: User;
    let username: string;
    let username2: string;
    let password: string;
    let password2: string;
    let email: string;
    let nif: string;
    let phoneNumber: string;
    let address: string;
    let address2: string;
    let firstName: string;
    let firstName2: string;
    let lastName: string;
    let lastName2: string;
    let age: string;

    beforeEach(() => {
        mapper = new UserMapper();

        username = 'Ana';
        username2 = 'Miguel';
        password = 'secretPassword#99';
        password2 = 'secretPassword#95';

        email = 'client@hotmail.com';
        nif = '202234975';
        phoneNumber = '918499999';
        address = 'Cutlery Street 99, Fork Boulevard, New York';
        address2 = 'Cutlery Street 59, Fork Boulevard, New York';
        firstName = 'John';
        firstName2 = 'Jack';
        lastName = 'Johnson';
        lastName2 = 'Smith';
        age = '45';

        userClient1 = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName),'id');
        userClient1NullId = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName),null);
        userAdmin1 = new User(username2, password2, new AdminRole());

        userClientDto = new ClientDto(username, password, address, nif, phoneNumber, email, age, firstName, lastName, 0);
        userAdminDto = new AdminDto(username2, password2);
        userClientUpdateDto = new ClientUpdateDTO(firstName2, lastName2, address2);

        jsonClient = {};
        jsonClient.username = username;
        jsonClient.password = password;
        jsonClient.address = address;
        jsonClient.nif = nif;
        jsonClient.phoneNumber = phoneNumber;
        jsonClient.age = age;
        jsonClient.email = email;
        jsonClient.firstName = firstName;
        jsonClient.lastName = lastName;

        jsonAdmin = {};
        jsonAdmin.username = username2;
        jsonAdmin.password = password2;

        jsonUpdateClient = {};
        jsonUpdateClient.firstName = firstName2;
        jsonUpdateClient.lastName = lastName2;
        jsonUpdateClient.address = address2;
    });

    it('should convert json to user dto', () => {
        const result = mapper.JSONToClientDTO(jsonClient);

        expect(result.username).toBe(userClientDto.username);
        expect(result.password).toBe(userClientDto.password);
        expect(result.address).toBe(userClientDto.address);
        expect(result.nif).toBe(userClientDto.nif);
        expect(result.email).toBe(userClientDto.email);
        expect(result.address).toBe(userClientDto.address);
        expect(result.firstName).toBe(userClientDto.firstName);
        expect(result.lastName).toBe(userClientDto.lastName);
        expect(result.phoneNumber).toBe(userClientDto.phoneNumber);
    });

    it('should convert json to admin dto', () => {
        const result = mapper.JSONToAdminDTO(jsonAdmin);

        expect(result.username).toBe(userAdminDto.username);
        expect(result.password).toBe(userAdminDto.password);
    });

    it('should convert domain to user dto', () => {
        const result = mapper.DomainToClientDTO(userClient1);

        expect(result.username).toBe(userClient1.username.toString());
        expect(result.password).toBe(userClient1.password._password);
        expect(result.address).toBe(userClient1.role.getAddress().toString());
        expect(result.nif).toBe(userClient1.role.getNif().toString());
        expect(result.email).toBe(userClient1.role.getEmail().toString());
        expect(result.address).toBe(userClient1.role.getAddress().toString());
        expect(result.firstName).toBe(userClient1.role.getFirstName().toString());
        expect(result.lastName).toBe(userClient1.role.getLastName().toString());
        expect(result.phoneNumber).toBe(userClient1.role.getPhoneNumber().toString());
        expect(userClient1.getId()).toBe('id');
        expect(userClient1.toString()).toContain('id');
        expect(userClient1NullId.getId()).toBe('');
    });

    it('should convert domain to admin dto', () => {
        const result = mapper.DomainToAdminDTO(userAdmin1);

        expect(result.username).toBe(userAdmin1.username.toString());
        expect(result.password).toBe(userAdmin1.password._password);
    });

    it('should convert json to user update dto', () => {
        const result = mapper.JSONToClientUpdateDTO(jsonUpdateClient);

        expect(result.firstName).toBe(userClientUpdateDto.firstName);
        expect(result.lastName).toBe(userClientUpdateDto.lastName);
        expect(result.address).toBe(userClientUpdateDto.address);
    });

    it('should convert domain to user limited dto', () => {
        const result = mapper.DomainToClientDTONameOnly(userClient1);

        expect(result.firstName).toBe(userClient1.role.getFirstName().toString());
        expect(result.lastName).toBe(userClient1.role.getLastName().toString());
        expect(result.username).toBe(userClient1.username.toString());

    });
});
