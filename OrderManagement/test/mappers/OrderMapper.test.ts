import {ProductForOrder} from "../../src/domain/models/product/ProductForOrder";
import Order = require("../../src/domain/models/order/Order");
import {OrderDto} from "../../src/dtos/order/OrderDto";
import {OrderUpdateDto} from "../../src/dtos/order/OrderUpdateDto";
import {ProductDto} from "../../src/dtos/product/ProductDto";
import {OrderMapper} from "../../src/mappers/OrderMapper";
import moment = require("moment");

let products: ProductForOrder[];
let productsDto: ProductDto[];
let prod1Dto: ProductDto;
let prod2Dto: ProductDto;
let prod1: ProductForOrder;
let prod2: ProductForOrder;
let client: string;
let price: number;
let today: Date;
let creditCard: string;
let shippingAddress: string;
let order: Order;
let jsonOrder: any;
let orderDto: OrderDto;
let orderUpdateDto: OrderUpdateDto;

beforeEach(() => {
    prod1 = new ProductForOrder('spoon', 500, 10, new Date("10/10/2030"));
    prod2 = new ProductForOrder('fork', 300, 5, new Date("10/10/2030"));
    prod1Dto = new ProductDto('spoon', 0, 10, "10/10/2030");
    prod2Dto = new ProductDto('fork', 0, 5, "10/10/2030");
    products = [prod1, prod2];
    productsDto = [prod1Dto, prod2Dto];
    client = "mike";
    price = 500 * 10 + 300 * 5;
    creditCard = "PT501234123412341234";
    const now = moment();
    now.seconds(0).milliseconds(0);
    today = now.toDate();
    shippingAddress = "Rua das Flores";
    order = new Order(products, client, price, creditCard, today, shippingAddress);
    orderDto = new OrderDto(productsDto, client, creditCard, today, shippingAddress);
    jsonOrder = {};
});

describe('OrderMapper test', function () {
    it('should convert JSON to OrderDTO', () => {
        jsonOrder.client = 'mike';
        jsonOrder.products = productsDto;
        jsonOrder.creditCardUsed = creditCard;
        jsonOrder.shippingAddress = shippingAddress;
        const mapper = new OrderMapper();
        const result = mapper.JSONToOrderDTO(jsonOrder);
        expect(result).toEqual(orderDto);
    });

    it('should convert JSON to OrderUpdateDto', () => {
        let updateJson: any = {};
        updateJson.shippingAddress = 'Rua do Sol';
        orderUpdateDto = new OrderUpdateDto('Rua do Sol');
        const mapper = new OrderMapper();
        const result = mapper.JSONToOrderUpdateDTO(updateJson);
        expect(result).toEqual(orderUpdateDto);
    });

    it('should throw error converting JSON to OrderUpdateDto when no shipping address', () => {
        expect(() => {
            let updateJson: any = {};
            const mapper = new OrderMapper();
            mapper.JSONToOrderUpdateDTO(updateJson);
        }).toThrowError('Shipping address missing');
    });

    it('should convert mongoose schema to domain object Order', () => {
        let schema: any = {};
        let productsSchema: any = [];
        productsSchema[0] = {};
        productsSchema[0]._name = prod1.name;
        productsSchema[0]._deliveryDate = new Date('10/10/2030');
        productsSchema[0]._quantity = prod1.quantity;
        productsSchema[0]._price = 500;
        productsSchema[1] = {};
        productsSchema[1]._name = prod2.name;
        productsSchema[1]._deliveryDate = new Date('10/10/2030');
        productsSchema[1]._quantity = prod2.quantity;
        productsSchema[1]._price = 300;
        schema._client = client;
        schema._products = productsSchema;
        schema._totalPrice = price;
        schema._creditCardUsed = creditCard;
        schema._placementDate = today;
        schema._shippingAddress = shippingAddress;
        schema._state = 'ongoing';
        schema._reference = order.reference;
        schema._id = 1;
        const expected = new Order(products, client, price, creditCard, today, shippingAddress, 'ongoing', order.reference, 1);
        const mapper = new OrderMapper();
        const result: Order = mapper.MongooseModelToDomain(schema);
        var equal = JSON.stringify(result) === JSON.stringify(expected);
        expect(equal).toBeTruthy();
    });
});
