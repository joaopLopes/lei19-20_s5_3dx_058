import Order = require("../../../../src/domain/models/order/Order");
import {ProductForOrder} from "../../../../src/domain/models/product/ProductForOrder";
import {OrderState} from "../../../../src/domain/models/order/OrderState";

let products: ProductForOrder[];
let prod1: ProductForOrder;
let prod2: ProductForOrder;
let client: string;
let price: number;
let today: Date;
let creditCard: string;
let shippingAddress: string;

beforeEach(() => {
    prod1 = new ProductForOrder('spoon', 500, 10, new Date("10/10/2030"));
    prod2 = new ProductForOrder('fork', 300, 5, new Date("10/10/2030"));
    products = [prod1, prod2];
    client = "mike";
    price = 500 * 10 + 300 * 5;
    creditCard = "PT501234123412341234";
    today = new Date();
    shippingAddress = "Rua das Flores";
});
describe('order test', () => {
    it('should create order with expected data when info is correct', () => {
        const order: Order = new Order(products, client, price, creditCard, today, shippingAddress);
        expect(order.shippingAddress).toBe(shippingAddress);
        expect(order.price).toBe(price);
        expect(order.creditCardUsed).toBe(creditCard);
        expect(order.placementDate).toBe(today);
        expect(order.state).toBe(OrderState.Ongoing);
        expect(order.reference).toBeDefined();
        expect(order.client).toBe(client);
        expect(order.products).toBe(products);
    });
    it('should have correct toString', () => {
        const order: Order = new Order(products, client, price, creditCard, today, shippingAddress);
        const result: string = order.toString();
        const expected: string = "Reference: " + order.reference + "\n"
            + "Client: " + client + "\n"
            + "Date: " + today.toDateString() + "\n"
            + "Payment Method: " + creditCard + "\n"
            + "Products: " + products + "\n"
            + "Price: " + price.toString() + '\n'
            + "State: " + OrderState.Ongoing;
        expect(result).toEqual(expected);
    });
    it('should change state from ongoing to cancelled when cancelled', () => {
        const order: Order = new Order(products, client, price, creditCard, today, shippingAddress);
        order.cancel();
        expect(order.state).toBe(OrderState.Cancelled);
    });
    it('should throw error when changing from cancelled to cancelled', () => {
        const order: Order = new Order(products, client, price, creditCard, today, shippingAddress);
        order.cancel();
        expect(() => {
            order.cancel();
        }).toThrowError(Error('Order is not ongoing. Unable to cancel.'));
    });
    it('should throw error when price is invalid', () => {
        expect(() => {
            new Order(products, client, -10, creditCard, today, shippingAddress);
        }).toThrowError(Error('Price must not be lower than 0'));
    });
    it('should throw error when product list is empty', () => {
        const aux: ProductForOrder[] = [];
        expect(() => {
            new Order(aux, client, price, creditCard, today, shippingAddress);
        }).toThrowError(Error("Order must have atleast a product."));
    });
    it('should throw error when products are null', () => {
        expect(() => {
            new Order(null, client, price, creditCard, today, shippingAddress);
        }).toThrowError(Error("Order must have atleast a product."));
    });
    it('should throw error when date is null', () => {
        expect(() => {
            new Order(products, client, price, creditCard, null, shippingAddress);
        }).toThrowError(Error("Date must exist"));
    });
    it('should throw error when client is null', () => {
        expect(() => {
            new Order(products, null, price, creditCard, today, shippingAddress);
        }).toThrowError(Error("There must be a client"));
    });
    it('should throw error when address is null', () => {
        expect(() => {
            new Order(products, client, price, creditCard, today, null);
        }).toThrowError(Error("Address must exist"));
    });
    it('should throw error when credit card is null', () => {
        expect(() => {
            new Order(products, client, price, null, today, shippingAddress);
        }).toThrowError(Error("Credit Card must exist"));
    });
    it('should throw error when price is null', () => {
        expect(() => {
            new Order(products, client, null, creditCard, today, shippingAddress);
        }).toThrowError(Error("Price is not valid."));
    });
});
