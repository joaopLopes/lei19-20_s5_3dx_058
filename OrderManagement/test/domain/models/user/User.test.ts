import {User} from "../../../../src/domain/models/user/User";
import {ClientRole} from "../../../../src/domain/models/user/client/ClientRole";
import {CreditCard} from "../../../../src/domain/models/user/client/CreditCard";
import {Address} from "../../../../src/domain/models/user/client/Address";
import {State} from "../../../../src/domain/models/user/State";

let username: string;
let password: string;
let email: string;
let nif: string;
let phoneNumber: string;
let address: string;
let firstName: string;
let lastName: string;
let age: string;

beforeEach(() => {
    username = 'ourLovelyClient';
    password = 'secretPassword#99';
    email = 'client@hotmail.com';
    nif = '202234975';
    phoneNumber = '918499999';
    address = 'Cutlery Street 99, Fork Boulevard, New York';
    firstName = 'John';
    lastName = 'Johnson';
    age = '45';
});

describe('user test', () => {
    it('should create client with expected data when info is correct', () => {
        const john: User = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address,firstName, lastName));
        expect(john.username.toString()).toBe(username);
        expect(john.password.toString()).toBe(password);
        expect(john.role.getEmail().toString()).toBe(email);
        expect(john.role.getNif().toString()).toBe(nif);
        expect(john.role.getPhoneNumber().toString()).toBe(phoneNumber);
        expect(john.role.getAddress().toString()).toBe(address);
        expect(john.role.getFirstName().toString()).toBe(firstName);
        expect(john.role.getLastName().toString()).toBe(lastName);
    });
    it('should change state of client', () => {
        const john: User = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address,firstName, lastName));
        john.delete();
        expect(john.state.valueOf()).toBe(1);
    });
    it('should change address of client', () => {
        const john: User = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address,firstName, lastName));
        john.updateAddress('New Address');
        expect(john.role.getAddress().toString()).toBe('New Address');
    });
    it('should change address of client', () => {
        const john: User = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address,firstName, lastName));
        john.updateName('Filipe', 'Saraiva');
        expect(john.role.getFirstName().toString()).toBe('Filipe');
        expect(john.role.getLastName().toString()).toBe('Saraiva');
    });
    it('should throw error if NIF is invalid',() => {
        expect(() => {
            new User(username, password, new ClientRole(email, '000000000', phoneNumber, age, address,firstName, lastName));
        }).toThrowError(Error("NIF is invalid"));
    });
    it('should throw error if Username is invalid',() => {
        expect(() => {
            new User('username', password, new ClientRole(email, '000000000', phoneNumber, age, address,firstName, lastName));
        }).toThrowError(Error("NIF is invalid"));
    });
    it('should throw error if Phone Number is invalid',() => {
        expect(() => {
            new User(username, password, new ClientRole(email, nif, '000000000', age, address,firstName, lastName));
        }).toThrowError(Error("Inserted phone number is invalid."));
    });
    it('should throw error if Email is invalid',() => {
        expect(() => {
            new User(username, password, new ClientRole('notvalid-gmail.com', nif, phoneNumber, age, address,firstName, lastName));
        }).toThrowError(Error("Email is invalid."));
    });
    it('should throw error Age is invalid',() => {
        expect(() => {
            new User(username, password, new ClientRole(email, nif, phoneNumber, '15', address,firstName, lastName));
        }).toThrowError(Error("Age is invalid."));
    });
    it('should throw error if Credit Card is invalid',() => {
        expect(() => {
            new CreditCard('PT558304192038167066');
        }).toThrowError(("IBAN only works with portuguese accounts."));

        expect(() => {
            new CreditCard('PT508304194444444444444444444444444203816706671715');
        }).toThrowError(("IBAN is invalid."));
    });
    it('should throw error if Address is invalid',() => {
        expect(() => {
            new Address(null);
        }).toThrowError("Address must exist");
    });
    // Only tested for coverage, since this option is yet to be used
    it('should create user as deleted',() => {
        const user = new User(username, password, new ClientRole(email, nif, phoneNumber, '50', address,firstName, lastName), 'id', 1);
        expect(user.state).toBe(State.deleted);
    });
});
