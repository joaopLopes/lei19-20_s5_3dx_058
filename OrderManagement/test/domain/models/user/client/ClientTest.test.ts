
import {User} from "../../../../../src/domain/models/user/User";
import {ClientRole} from "../../../../../src/domain/models/user/client/ClientRole";
import {ClientLimitedInformationDTO} from "../../../../../src/dtos/user/ClientLimitedInformationDTO";
import useFakeTimers = jest.useFakeTimers;


let username: string;
let password: string;
let email: string;
let nif: string;
let phoneNumber: string;
let address: string;
let firstName: string;
let lastName: string;
let age: string;

beforeEach(() => {
    username = 'ourLovelyClient';
    password = 'secretPassword#99';
    email = 'client@hotmail.com';
    nif = '202234975';
    phoneNumber = '918499999';
    address = 'Cutlery Street 99, Fork Boulevard, New York';
    firstName = 'John';
    lastName = 'Johnson';
    age = '45';
});

describe('user test', () => {
    it('Client Limited Information DTO is successful', () => {
        const john: ClientLimitedInformationDTO = new ClientLimitedInformationDTO(firstName, lastName,username);
        expect(john.firstName).toBe("John");
        expect(john.lastName).toBe("Johnson");
        expect(john.username).toBe("ourLovelyClient");
    });
});



