
import {ClientRole} from "../../../../../src/domain/models/user/client/ClientRole";
import {User} from "../../../../../src/domain/models/user/User";



let username: string;
let password: string;
let address: string;
let firstName: string;
let lastName: string;
let nif: string;
let phoneNumber: string;
let email: string;
let age: string;
let priority: number;

let alternativeAddress: string;
let alternativeFirstName: string;
let alternativeLastName: string;

beforeEach(() => {
   username = "userTestA";
   password = "madmax#99";
   address = "Rua da Tristeza";
   firstName = "Daniel";
   lastName = "Oliveira";
   nif = "812850785";
   phoneNumber = "914334189";
   email = "usertest@gmail.com";
   age = "21";
   priority = 111;

   alternativeAddress = "Rua da Felicidade";
   alternativeFirstName = "Rafael";
   alternativeLastName = "Carvalho";
});

describe('update client test', () => {
    it('should create client with expected data when info is correct', () => {
        const client: User = new User(username,password,new ClientRole(email,nif,phoneNumber,age,address,firstName,lastName));
        expect(client.username.toString()).toBe(username);
        expect(client.password.toString()).toBe(password);
        expect(client.role.getEmail().toString()).toBe(email);
        expect(client.role.getNif().toString()).toBe(nif);
        expect(client.role.getPhoneNumber().toString()).toBe(phoneNumber);
        expect(client.role.getAge().toString()).toBe(age);
        expect(client.role.getAddress().toString()).toBe(address);
        expect(client.role.getFirstName().toString()).toBe(firstName);
        expect(client.role.getLastName().toString()).toBe(lastName);
    })
    it('should be correctly updated', () => {
        const client: User = new User(username,password,new ClientRole(email,nif,phoneNumber,age,address,firstName,lastName));
        client.role.updateAddress(alternativeAddress);
        client.role.updateName(alternativeFirstName,alternativeLastName);
        expect(client.role.getAddress().toString()).toBe(alternativeAddress);
        expect(client.role.getFirstName().toString()).toBe(alternativeFirstName);
        expect(client.role.getLastName().toString()).toBe(alternativeLastName);
    });
});
