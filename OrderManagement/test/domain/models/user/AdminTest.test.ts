import {AdminRole} from "../../../../src/domain/models/user/admin/AdminRole";
import {User} from "../../../../src/domain/models/user/User";
import {ClientRole} from "../../../../src/domain/models/user/client/ClientRole";
import {error} from "winston";



let username: string;
let password: string;


beforeEach(() => {
    username = "admin";
    password = "admin#99";
});

    describe('Admin Creating Test test', () => {
    it('should create admin with expected data when info is correct', () => {
        const john: User = new User(username, password, new AdminRole());
        expect(john.username.toString()).toBe(username);
        expect(john.password.toString()).toBe(password);
        expect(john.role.getAddress).toThrowError(Error);
        expect(john.role.getAge).toThrowError(Error);
        expect(john.role.getEmail).toThrowError(Error);
        expect(john.role.getFirstName).toThrowError(Error);
        expect(john.role.getLastName).toThrowError(Error);
        expect(john.role.getLastUsedCard).toThrowError(Error);
        expect(john.role.getNif).toThrowError(Error);
        expect(john.role.getPhoneNumber).toThrowError(Error);
        expect(john.role.getPriority).toThrowError(Error);
        expect(john.role.updateAddress).toThrowError(Error);
        expect(john.role.getRole()).toBe('Admin');
    });

});
