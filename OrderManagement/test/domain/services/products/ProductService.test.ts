import {IOrderRepository} from "../../../../src/persistence/interfaces/IOrderRepository";
import {ProductForOrder} from "../../../../src/domain/models/product/ProductForOrder";
import Order = require("../../../../src/domain/models/order/Order");
import {IMasterDataProductionAdapter} from "../../../../src/domain/services/interfaces/adapters/IMasterDataProductionAdapter";
import {ProductDto} from "../../../../src/dtos/product/ProductDto";
import {ProductService} from "../../../../src/domain/services/impl/product/ProductService";

let mockOrderRepo: any;
let mockMdpAdapter: any;
let products: ProductForOrder[];
let order1: Order;
let order2: Order;
let order3: Order;
let order4: Order;
let order5: Order;
let orderList1: ProductForOrder[];
let orderList2: ProductForOrder[];
let orderList3: ProductForOrder[];
let orderList4: ProductForOrder[];
let orderList5: ProductForOrder[];
let prod1: ProductForOrder;
let prod2: ProductForOrder;
let prod3: ProductForOrder;
let prod1Dto: ProductDto;
let prod2Dto: ProductDto;
let prod3Dto: ProductDto;

let productService: ProductService;

beforeEach(() => {
    products = [];
    orderList1 = [];
    orderList2= [];
    orderList3= [];
    orderList4= [];
    orderList5= [];

    prod1 = new ProductForOrder('spoon', 40,200,null, 120);
    prod2 = new ProductForOrder('knife', 50,100,null, 65);
    prod3 = new ProductForOrder('fork', 80,6000,null, 15);

    prod1Dto = new ProductDto('spoon', 40);
    prod2Dto = new ProductDto('knife', 50);
    prod3Dto = new ProductDto('fork', 80);

    products.push(prod1);
    products.push(prod2);
    products.push(prod3);

    orderList1.push(prod1);
    orderList2.push(prod1, prod2);
    orderList3.push(prod1, prod2);
    orderList4.push(prod1);
    orderList5.push(prod3);

    order1 = new Order(orderList1, 'pedro', 60, 'PT501928394857463748', new Date(2019,5), 'Rua teste', 'ONGOING', '');
    order2 = new Order(orderList2, 'luis', 60, 'PT501928394857463741', new Date(2019,5), 'Rua teste', 'ONGOING', '');
    order3 = new Order(orderList3, 'maria', 60, 'PT501928394857463742', new Date(2019,5), 'Rua teste', 'ONGOING', '');
    order4 = new Order(orderList4, 'ana', 60, 'PT501928394857463743', new Date(2019,5), 'Rua teste', 'ONGOING', '');
    order5 = new Order(orderList5, 'paulo', 60, 'PT501928394857463744', new Date(2019,5), 'Rua teste', 'ONGOING', '');

    mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
        getProductTimesOrdered: jest.fn(),
        getQuantityOrderedForProduct: jest.fn(),
        update: jest.fn(),
        create: jest.fn(),
        findAll: jest.fn(),
        findById: jest.fn(),
        findByClient: jest.fn(),
        findOngoing: jest.fn()
    }));
    mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
        findProductByName: jest.fn(),
        findAllProducts: jest.fn()
    }));

    productService = new ProductService(mockMdpAdapter, mockOrderRepo);
});
describe('Product Service tests',() => {
    it('should sort products by quantity ordered', async () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts(): Promise<ProductForOrder[]>{
                return new Promise<ProductForOrder[]>((resolve) => {
                    resolve(products);
                });
            }
        }));
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct(product: string): Promise<number> {
                if(product == 'spoon'){
                    return new Promise<number>(((resolve) => {
                        resolve(800);
                    }));
                }else if(product == 'knife'){
                    return new Promise<number>(((resolve) => {
                        resolve(200);
                    }));
                }else{
                    return new Promise<number>(((resolve) => {
                        resolve(6000);
                    }));
                }
            },
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret: ProductDto[] = [];
        await productService.sortBy('quantity').then(value => {
            ret = value;
        }).catch(error => {
            console.log(error.toString());
        });

        let retExpected: ProductDto[] = [];
        retExpected.push(prod3Dto);
        retExpected.push(prod1Dto);
        retExpected.push(prod2Dto);
        expect(ret).toEqual(retExpected);
    });
    it('should sort products by times ordered', async () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts(): Promise<ProductForOrder[]>{
                return new Promise<ProductForOrder[]>((resolve) => {
                    resolve(products);
                });
            }
        }));
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered(product: string): Promise<number> {
                if(product == 'spoon'){
                    return new Promise<number>(((resolve) => {
                        resolve(4);
                    }));
                }else if(product == 'knife'){
                    return new Promise<number>(((resolve) => {
                        resolve(2);
                    }));
                }else{
                    return new Promise<number>(((resolve) => {
                        resolve(1);
                    }));
                }
            },
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret: ProductDto[] = [];
        await productService.sortBy('times').then(value => {
            ret = value;
        }).catch(error => {
            console.log(error.toString());
        });

        let retExpected: ProductDto[] = [];
        retExpected.push(prod1Dto);
        retExpected.push(prod2Dto);
        retExpected.push(prod3Dto);
        expect(ret).toEqual(retExpected);
    });
    it('should sort products by production time', async () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts(): Promise<ProductForOrder[]>{
                return new Promise<ProductForOrder[]>((resolve) => {
                    resolve(products);
                });
            }
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret: ProductDto[] = [];
        await productService.sortBy('production-time').then(value => {
            ret = value;
        }).catch(error => {
            console.log(error.toString());
        });

        let retExpected: ProductDto[] = [];
        retExpected.push(prod3Dto);
        retExpected.push(prod2Dto);
        retExpected.push(prod1Dto);
        expect(ret).toEqual(retExpected);
    });
    it('should throw error if criteria is invalid', async () => {
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret: null;
        await productService.sortBy('fakeCriteria').then(value => {
        }).catch(error => {
            ret = error;
        });

        expect(ret).toEqual(Error('Sort criteria is invalid.'));

    });
    it('should get all products', async () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts(): Promise<ProductForOrder[]>{
                return new Promise<ProductForOrder[]>((resolve) => {
                    resolve(products);
                });
            }
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret: ProductDto[] = [];
        await productService.getAllProducts().then(value => {
            ret = value;
        }).catch(error => {
            console.log(error.toString());
        });

        let retExpected: ProductDto[] = [];
        retExpected.push(prod1Dto);
        retExpected.push(prod2Dto);
        retExpected.push(prod3Dto);
        expect(ret).toEqual(retExpected);
    });
    it('should get a single product', async () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName:string): Promise<ProductForOrder>{
                return new Promise<ProductForOrder>((resolve,reject) => {
                    resolve(prod1);
                });
            },
            findAllProducts: jest.fn()
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret: ProductDto;
        await productService.getProduct('spoon').then(value => {
            ret = value;
        }).catch(error => {
            ret = error;
        });
        expect(ret).toEqual(prod1Dto );
    });
    it('should throw error if product does not exist', async () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts(): Promise<ProductForOrder[]>{
                return new Promise<ProductForOrder[]>((resolve,reject) => {
                    reject('There are no products in the system.');
                });
            }
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret = null;
        await productService.getAllProducts().then(value => {
        }).catch(error => {
            ret = error;
        });
        expect(ret).toEqual('There are no products in the system.');
    });
    it('should throw error if there are no products',async  () => {
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName:string): Promise<ProductForOrder>{
                return new Promise<ProductForOrder>((resolve,reject) => {
                    reject("ProductForOrder " + prodName + " does not exist.");
                });
            },
            findAllProducts: jest.fn()
        }));
        productService = new ProductService(mockMdpAdapter(), mockOrderRepo());
        let ret = null;
        await productService.getProduct('spoon').then(value => {
        }).catch(error => {
            ret = error;
        });
        expect(ret).toEqual("ProductForOrder " + 'spoon' + " does not exist." );
    });
});
