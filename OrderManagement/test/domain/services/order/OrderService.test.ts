import {ProductForOrder} from "../../../../src/domain/models/product/ProductForOrder";
import {OrderService} from "../../../../src/domain/services/impl/order/OrderService";
import OrderFactory = require("../../../../src/domain/services/impl/order/OrderFactory");
import {IOrderRepository} from "../../../../src/persistence/interfaces/IOrderRepository";
import {IUserRepository} from "../../../../src/persistence/interfaces/IUserRepository";
import {IMasterDataProductionAdapter} from "../../../../src/domain/services/interfaces/adapters/IMasterDataProductionAdapter";
import {OrderDto} from "../../../../src/dtos/order/OrderDto";
import {ProductDto} from "../../../../src/dtos/product/ProductDto";
import {OrderMapper} from "../../../../src/mappers/OrderMapper";
import {ProductMapper} from "../../../../src/mappers/ProductMapper";
import {User} from "../../../../src/domain/models/user/User";
import {ClientRole} from "../../../../src/domain/models/user/client/ClientRole";
import Order = require("../../../../src/domain/models/order/Order");
import {OrderState} from "../../../../src/domain/models/order/OrderState";
import {OrderUpdateDto} from "../../../../src/dtos/order/OrderUpdateDto";
import {stringify} from "querystring";

let products: ProductForOrder[];
let productsDto: ProductDto[];
let prod1: ProductForOrder;
let prod2: ProductForOrder;
let prod1Dto;
let prod2Dto;
let client: string;
let price: number;
let today: Date;
let creditCard: string;
let shippingAddress: string;
let mockOrderRepo: any;
let mockUserRepo: any;
let mockMdpAdapter: any;
let orderService: OrderService;
let factory: OrderFactory;
let orderInputDto: OrderDto;
let orderReturnDto: OrderDto;
let orderMapper: OrderMapper;
let productMapper: ProductMapper;
let email: string;
let firstName: string;
let lastName: string;
let nif: string;
let phoneNum: string;
let age: string;
let address: string;

beforeEach(() => {
    orderMapper = new OrderMapper();
    productMapper = new ProductMapper();

    prod1 = new ProductForOrder('spoon', 500, 10, new Date("10/10/2030"));
    prod2 = new ProductForOrder('fork', 300, 5, new Date("10/10/2030"));
    prod1Dto = productMapper.DomainToDtoOrder(prod1);
    prod2Dto = productMapper.DomainToDtoOrder(prod2);
    productsDto = [prod1Dto, prod2Dto];
    products = [prod1, prod2];
    client = "mike";
    email = "mike@gmail.com";
    nif = "123456789";
    phoneNum = "912345678";
    age = "32";
    address = "Rua das Rosas";
    firstName = "Luke";
    lastName = "Skywalker";
    price = 500 * 10 + 300 * 5;
    creditCard = "PT501234123412341234";
    today = new Date();
    shippingAddress = "Rua das Flores";
    orderInputDto = new OrderDto(productsDto, client, creditCard, today, shippingAddress);

    // mocking repository
    mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
        getProductTimesOrdered: jest.fn(),
        getQuantityOrderedForProduct: jest.fn(),
        update: jest.fn(),
        create: jest.fn(),
        findAll: jest.fn(),
        findById: jest.fn(),
        findByClient: jest.fn(),
        findOngoing: jest.fn()
    }));
    mockUserRepo = jest.fn<IUserRepository, []>(() => ({
        create: jest.fn(),
        findAll: jest.fn(),
        findByUsername: jest.fn(),
        update: jest.fn(),
        forgetUser: jest.fn()
    }));
    mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
        findProductByName: jest.fn(),
        findAllProducts: jest.fn()
    }));
    factory = new OrderFactory();
    orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
    //--------------------------------------------------
});


describe('Order Service tests', () => {
    it('should create order', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()

        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            }, update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result: OrderDto = null;
        await orderService.createOrder(orderInputDto).then(order => {
            result = order;
        });
        orderReturnDto = new OrderDto(productsDto, client, creditCard, today, shippingAddress, price, OrderState.Ongoing, "test-ref");
        expect(result).toEqual(orderReturnDto);
    });

    it('should throw error creating when client does not exist', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve, reject) => {
                    reject(Error('Client ' + username + ' does not exist.'));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let resultErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            resultErr = error;
        });
        expect(resultErr).toEqual(Error('Client ' + client + ' does not exist.'));
    });

    it('should throw error creating when client is missing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        orderInputDto.client = null;
        let returnErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            returnErr = error;
        });
        expect(returnErr).toEqual(Error('Missing information.'));
    });

    it('should throw error creating when products list is missing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        orderInputDto.products = null;
        let returnErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            returnErr = error;
        });
        expect(returnErr).toEqual(Error('Missing information.'));
    });

    it('should throw error creating when products list is empty', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        orderInputDto.products = [];
        let returnErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            returnErr = error;
        });
        expect(returnErr).toEqual(Error('Order must have atleast a product.'));
    });

    it('should throw error creating when placement date is missing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        orderInputDto.placementDate = null;
        let returnErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            returnErr = error;
        });
        expect(returnErr).toEqual(Error('Missing information.'));
    });

    it('should throw error creating when shipping address is missing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        orderInputDto.shippingAddress = null;
        let returnErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            returnErr = error;
        });
        expect(returnErr).toEqual(Error('Missing information.'));
    });
    it('should throw error creating when credit card is missing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create(order: Order): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, "somePassword", new ClientRole(email, nif, phoneNum, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName(prodName: string): Promise<ProductForOrder> {
                return new Promise<ProductForOrder>((resolve) => {
                    if (prodName === 'spoon')
                        resolve(prod1);
                    else resolve(prod2);
                });
            },
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        orderInputDto.creditCardUsed = null;
        let returnErr = null;
        await orderService.createOrder(orderInputDto).then().catch(error => {
            returnErr = error;
        });
        expect(returnErr).toEqual(Error('Missing information.'));
    });

    it('should list all orders from repo', async () => {
        const o1 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id");
        const o2 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref2", "fake-db-id");
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll(): Promise<Order[]> {
                return new Promise<Order[]>((resolve => {
                    resolve([o1, o2]);
                }));
            },
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.getAllOrders().then(orders => {
            result = orders;
        });
        const dto1 = orderMapper.DomainToOrderDTO(o1);
        const dto2 = orderMapper.DomainToOrderDTO(o2);
        expect(result).toEqual([dto1, dto2]);
    });

    it('should list all orders from repo of a client', async () => {
        const o1 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id");
        const o2 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref2", "fake-db-id");
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient(): Promise<Order[]> {
            return new Promise<Order[]>((resolve => {
                resolve([o1, o2]);
            }));
            },
            findOngoing: jest.fn()
        }));

        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.getOrdersByClient('mike').then(orders => {
            result = orders;
        });
        const dto1 = orderMapper.DomainToOrderDTO(o1);
        const dto2 = orderMapper.DomainToOrderDTO(o2);
        expect(result).toEqual([dto1, dto2]);
    });

    it('should throw exception if client has no orders', async () => {
        const o1 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id");
        const o2 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref2", "fake-db-id");
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient(): Promise<Order[]> {
                return new Promise<Order[]>(((resolve, reject) => {
                    reject('Client has no orders.');
                }));
            },
            findOngoing: jest.fn()
        }));

        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result: string;
        await orderService.getOrdersByClient('mike').catch(orders => {
            result = orders;
        });

        expect(result).toEqual('Client has no orders.');
    });

    it('should list all orders from repo between two dates', async () => {
        const date1 = new Date(1900, 1);
        const date2 = new Date(2050,1);
        const o1 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id");
        const o2 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref2", "fake-db-id");
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing(date1, date2): Promise<Order[]> {
                return new Promise<Order[]>((resolve => {
                    resolve([o1, o2]);
                }));
            }
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.getOrdersForScheduling(date1, date2).then(orders => {
            result = orders;
        });
        const dto1 = orderMapper.DomainToOrderDTO(o1);
        const dto2 = orderMapper.DomainToOrderDTO(o2);
        expect(result).toEqual([dto1, dto2]);
    });

    it('should throw exception if there are no orders between two dates', async () => {
        const o1 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id");
        const o2 = new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref2", "fake-db-id");
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById: jest.fn(),
            findByClient: jest.fn(),
            findOngoing(startDate: Date, endDate: Date): Promise<Order[]> {
                return new Promise<Order[]>(((resolve, reject) => {
                    reject('No Orders between these two dates.');
                }));
            }
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result: string;
        await orderService.getOrdersForScheduling(new Date(), new Date()).catch(orders => {
            result = orders;
        });

        expect(result).toEqual('No Orders between these two dates.');
    });

    it('should get order by reference from repo', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById(reference: string): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.getOrder('test-ref').then(order => {
            result = order;
        });
        orderReturnDto = new OrderDto(productsDto, client, creditCard, today, shippingAddress, price, OrderState.Ongoing, "test-ref");
        expect(result).toEqual(orderReturnDto);
    });

    it('should cancel order if it exists and is ongoing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update(order: Order): Promise<string> {
                return new Promise<string>(resolve => {
                    resolve('Order updated');
                });
            },
            create: jest.fn(),
            findAll: jest.fn(),
            findById(reference: string): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.cancelOrder('test-ref').then(order => {
            result = order;
        });
        orderReturnDto = new OrderDto(productsDto, client, creditCard, today, shippingAddress, price, OrderState.Cancelled, "test-ref");
        expect(result).toEqual('Order updated');
    });

    it('should throw error in cancel order if order does not exist', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById(reference: string): Promise<Order> {
                return new Promise<Order>(((resolve, reject) => {
                    reject(Error('Order not found'));
                }));
            },
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.cancelOrder('test-ref').then().catch(error => {
            result = error;
        });
        expect(result).toEqual(Error('Order not found'));
    });

    it('should throw error in cancel order if order is finished', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById(reference: string): Promise<Order> {
                return new Promise<Order>((resolve => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Cancelled, "test-ref", "fake-db-id"));
                }));
            },
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.cancelOrder('test-ref').then().catch(error => {
            result = error;
        });
        expect(result).toEqual(Error('Order is not ongoing. Unable to cancel.'));
    });

    it('should throw error in update if order does not exist', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update: jest.fn(),
            create: jest.fn(),
            findAll: jest.fn(),
            findById(reference: string): Promise<Order> {
                return new Promise<Order>(((resolve, reject) => {
                    reject(Error('Order not found'));
                }));
            },
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        await orderService.cancelOrder('test-ref').then().catch(error => {
            result = error;
        });
        expect(result).toEqual(Error('Order not found'));
    });

    it('should update order shipping address if it exists and is ongoing', async () => {
        mockOrderRepo = jest.fn<IOrderRepository, []>(() => ({
            getProductTimesOrdered: jest.fn(),
            getQuantityOrderedForProduct: jest.fn(),
            update(order: Order): Promise<string> {
                return new Promise<string>((resolve) => {
                    resolve('Order was updated.');
                });
            },
            create: jest.fn(),
            findAll: jest.fn(),
            findById(reference: string): Promise<Order> {
                return new Promise<Order>(((resolve) => {
                    resolve(new Order(products, client, price, creditCard, today, shippingAddress, OrderState.Ongoing, "test-ref", "fake-db-id"));
                }));
            },
            findByClient: jest.fn(),
            findOngoing: jest.fn()
        }));
        mockUserRepo = jest.fn<IUserRepository, []>(() =>
            ({
                create: jest.fn(),
                findAll: jest.fn(),
                findByUsername: jest.fn(),
                update: jest.fn(),
                forgetUser: jest.fn()
            })
        );
        mockMdpAdapter = jest.fn<IMasterDataProductionAdapter, []>(() => ({
            findProductByName: jest.fn(),
            findAllProducts: jest.fn()
        }));
        orderService = new OrderService(factory, mockOrderRepo(), mockUserRepo(), mockMdpAdapter());
        let result = null;
        const dto: OrderUpdateDto = new OrderUpdateDto('Rua do Sol');
        await orderService.updateOrder('test-ref', dto).then().then(message => {
            result = message;
        });
        expect(result).toEqual('Order was updated.');
    });


})

