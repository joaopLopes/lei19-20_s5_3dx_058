import {UserMapper} from "../../../../src/mappers/UserMapper";
import {IUserRepository} from "../../../../src/persistence/interfaces/IUserRepository";
import {User} from "../../../../src/domain/models/user/User";
import {AdminRole} from "../../../../src/domain/models/user/admin/AdminRole";
import {AdminDto} from "../../../../src/dtos/user/AdminDto";
import {AdminService} from "../../../../src/domain/services/impl/user/AdminService";
let mockAdminRepo: any;
let adminService: AdminService;

let userMapper: UserMapper;
let username: string;
let password: string;


beforeEach(() => {
    userMapper = new UserMapper();
    username = 'Luis';
    password = "somePassword#99";


    mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
        create: jest.fn(),
        findAll: jest.fn(),
        findByUsername: jest.fn(),
        update: jest.fn(),
        forgetUser: jest.fn()
    }));
    adminService = new AdminService(mockAdminRepo());
    //--------------------------------------------------
});

describe('admin tests', () => {
    it('should create an Admin with right information', async () => {
        mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
            create(user: User): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, password, new AdminRole()));
                }));
            },
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        adminService = new AdminService(mockAdminRepo());
        let dto = new AdminDto(username, password);
        let dto2: AdminDto;
        await adminService.createAdmin(dto).then(admin => {
            dto2 = admin;
        });
        expect(dto2.username).toBe(dto.username);
        expect(dto2.password).toBe(dto.password);
    });
    it('should throw exception Admin with wrong information', async () => {
        mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
            create(user: User): Promise<User> {
                return new Promise<User>(((resolve, reject) => {
                    reject('Data is invalid');
                }));
            },
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        adminService = new AdminService(mockAdminRepo());
        let dto = new AdminDto(username, password);
        let dto2: AdminDto;
        let result: string;
        await adminService.createAdmin(dto).catch(admin => {
            result = admin;
        });
        expect(result).toBe('Data is invalid');
    });
    it('should find Admin', async () => {
        mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(admin: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, password, new AdminRole()));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        adminService = new AdminService(mockAdminRepo());
        let dto = new AdminDto(username, password);
        let dto2: AdminDto;
        await adminService.getAdmin(username).then(user => {
            dto2 = user;
        });
        expect(dto2.username).toBe(dto.username);
        expect(dto2.password).toBe(dto.password);
    });
    it('should throw error if Admin does not exist', async () => {
        mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(admin: string): Promise<User> {
                return new Promise<User>(((resolve, reject) => {
                    reject('Admin ' + username + ' does not exist.');
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        adminService = new AdminService(mockAdminRepo());
        let dto = new AdminDto(username, password);
        let ret: AdminDto;
        await adminService.getAdmin(username).catch(user => {
            ret = user;
        });
        expect(ret).toBe('Admin ' + username + ' does not exist.');
    });
    it('should find all Admins', async () => {
        let dto = new AdminDto(username, password);
        let dto2 = new AdminDto('newUsername', 'newPassword==&11');
        let user = new User(username, password, new AdminRole());
        let user2 = new User('newUsername', 'newPassword==&11', new AdminRole());

        mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll(): Promise<User[]>{
                return new Promise<User[]>(((resolve, reject) => {
                    resolve([user, user2]);
                })); },
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        adminService = new AdminService(mockAdminRepo());
        let result: AdminDto[] = null;
        await adminService.getAllAdmins().then(users => {
            result = users;
        });
        expect(result[0].username).toBe(dto.username);
        expect(result[1].username).toBe(dto2.username);
    });

});
