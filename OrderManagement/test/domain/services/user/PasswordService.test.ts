import {PasswordService} from "../../../../src/domain/services/impl/user/PasswordService";

describe('pass test', () => {
    it('should throw error if password is too short', () => {
            PasswordService.hashPassword('seGS8%', 5).catch(error => {
                expect(error.toString()).toBe('Error: Password must have one number, one letter, and at least 8 characters.');
            })

    });
    it('should throw error if password does not have all the necessary types of characters', () => {
        PasswordService.hashPassword('secret%', 5).catch(error => {
            expect(error.toString()).toBe('Error: Password must have one number, one letter, and at least 8 characters.');
        })
    });
});
