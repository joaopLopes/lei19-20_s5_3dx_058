import {OrderService} from "../../../../src/domain/services/impl/order/OrderService";
import {OrderDto} from "../../../../src/dtos/order/OrderDto";
import {OrderMapper} from "../../../../src/mappers/OrderMapper";
import {ProductMapper} from "../../../../src/mappers/ProductMapper";
import {UserMapper} from "../../../../src/mappers/UserMapper";
import {ClientDto} from "../../../../src/dtos/user/ClientDto";
import {IUserRepository} from "../../../../src/persistence/interfaces/IUserRepository";
import {UserService} from "../../../../src/domain/services/impl/user/UserService";
import {User} from "../../../../src/domain/models/user/User";
import {ClientRole} from "../../../../src/domain/models/user/client/ClientRole";
import {AdminRole} from "../../../../src/domain/models/user/admin/AdminRole";
import {AdminDto} from "../../../../src/dtos/user/AdminDto";
import {AdminService} from "../../../../src/domain/services/impl/user/AdminService";
import {error} from "winston";
import {ClientUpdateDTO} from "../../../../src/dtos/user/ClientUpdateDTO";
import {ClientLimitedInformationDTO} from "../../../../src/dtos/user/ClientLimitedInformationDTO";

let client: string;
let price: number;
let today: Date;
let creditCard: string;
let shippingAddress: string;
let mockOrderRepo: any;
let mockUserRepo: any;
let mockAdminRepo: any;
let adminService: AdminService;
let userService: UserService;

let userInputDto: ClientDto;
let userReturnDto: ClientDto;
let userMapper: UserMapper;
let productMapper: ProductMapper;
let username: string;
let password: string;
let email: string;
let nif: string;
let phoneNumber: string;
let address: string;
let firstName: string;
let lastName: string;
let age: string;


beforeEach(() => {
    userMapper = new UserMapper();
    client = "mike";
    email = "mike@gmail.com";
    nif = "245790985";
    username = 'Luis';
    firstName = 'Luis';
    lastName = 'Sousa';
    password = "somePassword#99";
    phoneNumber = "912345678";
    age = "32";
    address = "Rua das Rosas";
    creditCard = "PT501234123412341234";
    today = new Date();
    shippingAddress = "Rua das Flores";

    mockUserRepo = jest.fn<IUserRepository, []>(() => ({
        create: jest.fn(),
        findAll: jest.fn(),
        findByUsername: jest.fn(),
        update: jest.fn(),
        forgetUser: jest.fn()
    }));
    mockAdminRepo = jest.fn<IUserRepository, []>(() => ({
        create: jest.fn(),
        findAll: jest.fn(),
        findByUsername: jest.fn(),
        update: jest.fn(),
        forgetUser: jest.fn()
    }));
    adminService = new AdminService(mockUserRepo());
    userService = new UserService(mockUserRepo());
    //--------------------------------------------------
});

describe('Order Service tests', () => {
    it('should create a Client with right information', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create(user: User): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName)));
                }));
            },
            findAll: jest.fn(),
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let dto = new ClientDto(username, password, address, nif, phoneNumber,email, age, firstName, lastName, 0);
        let dto2: ClientDto;
        await userService.createClient(dto).then(client => {
            dto2 = client;
        });
        expect(dto2.username).toBe(dto.username);
        expect(dto2.password).toBe(dto.password);
        expect(dto2.email).toBe(dto.email);
        expect(dto2.address).toBe(dto.address);
        expect(dto2.nif).toBe(dto.nif);
        expect(dto2.age).toBe(dto.age);
        expect(dto2.phoneNumber).toBe(dto.phoneNumber);
        expect(dto2.firstName).toBe(dto.firstName);
        expect(dto2.lastName).toBe(dto.lastName);
    });
    it('should find User', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(user: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let dto = new ClientDto(username, password, address, nif, phoneNumber,email, age, firstName, lastName, 0);
        let dto2: ClientDto;
        await userService.getClient(username).then(user => {
           dto2 = user;
        });
        expect(dto2.username).toBe(dto.username);
        expect(dto2.password).toBe(dto.password);
        expect(dto2.email).toBe(dto.email);
        expect(dto2.address).toBe(dto.address);
        expect(dto2.nif).toBe(dto.nif);
        expect(dto2.age).toBe(dto.age);
        expect(dto2.phoneNumber).toBe(dto.phoneNumber);
        expect(dto2.firstName).toBe(dto.firstName);
        expect(dto2.lastName).toBe(dto.lastName);
    });
    it('should throw error if User does not exist', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(user: string): Promise<User> {
                return new Promise<User>(((resolve, reject) => {
                    reject('Client ' + username + ' does not exist.');
                }));
            },
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let errorMessage: string;
        await userService.getClient(username).catch(error => {
            errorMessage = error.toString();
        });
        expect(errorMessage).toBe('Client ' + username + ' does not exist.');
    });
    it('should find all Users', async () => {
        let dto = new ClientDto(username, password, address, nif, phoneNumber,email, age, firstName, lastName, 0);
        let dto2 = new ClientDto('newUsername', 'newPassword==&11', address, nif, phoneNumber,'email@gmail.com', age, firstName, lastName, 0);
        let user = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName));
        let user2 = new User('newUsername', 'newPassword==&11', new ClientRole('email@gmail.com', nif, phoneNumber, age, address, firstName, lastName));

        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll(): Promise<User[]>{
            return new Promise<User[]>(((resolve, reject) => {
                resolve([user, user2]);
            })); },
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let result: ClientDto[] = null;
        await userService.getAllClients().then(users => {
            result = users;
        });
        expect(result[0].username).toBe(dto.username);
        expect(result[1].username).toBe(dto2.username);
    });
    it('should find all Users limited info', async () => {
        let dto = new ClientDto(username, password, address, nif, phoneNumber,email, age, firstName, lastName, 0);
        let dto2 = new ClientDto('newUsername', 'newPassword==&11', address, nif, phoneNumber,'email@gmail.com', age, firstName, lastName, 0);
        let user = new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName));
        let user2 = new User('newUsername', 'newPassword==&11', new ClientRole('email@gmail.com', nif, phoneNumber, age, address, firstName, lastName));

        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll(): Promise<User[]>{
                return new Promise<User[]>(((resolve, reject) => {
                    resolve([user, user2]);
                })); },
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let result: ClientLimitedInformationDTO[] = null;
        await userService.getAllClientsName().then(users => {
            result = users;
        });
        expect(result[0].username).toBe(dto.username);
        expect(result[1].username).toBe(dto2.username);
    });
    it('should throw exception if there are no users', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll(): Promise<User[]>{
                return new Promise<User[]>(((resolve, reject) => {
                    reject('No Client was found.');
                })); },
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let result: string;
        await userService.getAllClients().catch(ret => {
            result = ret;
        });
        expect(result).toBe('No Client was found.');
    });
    it('should throw exception if there are no users 2', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll(): Promise<User[]>{
                return new Promise<User[]>(((resolve, reject) => {
                    reject('No Client was found.');
                })); },
            findByUsername: jest.fn(),
            update: jest.fn(),
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let result: string;
        await userService.getAllClientsName().catch(ret => {
            result = ret;
        });
        expect(result).toBe('No Client was found.');
    });
    it('should forget User', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName)));
                }));
            },
            update: jest.fn(),
            forgetUser(user: User, username: string): Promise<string> {
                return new Promise<string>(((resolve) => {
                    resolve('The Client\'s personal information was deleted!');
                }));
            }
        }));
        userService = new UserService(mockUserRepo());
        let result: string;
        await userService.updateClientDeletion(username).then(ret => {
            result = ret;
        });
        expect(result).toBe('The Client\'s personal information was deleted!');
    });
    it('should update User', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve) => {
                    resolve(new User(username, password, new ClientRole(email, nif, phoneNumber, age, address, firstName, lastName)));
                }));
            },
            update(user: User, username: string): Promise<string> {
                return new Promise<string>(((resolve) => {
                    resolve('Client was updated!');
                }));
            },
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let result: string;
        await userService.updateClient(username, new ClientUpdateDTO('Maria','Santos','Street Nova 101')).then(ret => {
            result = ret;
        });
        expect(result).toBe('Client was updated!');
    });
    it('should throw exception if User to be forgotten does not exist', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve, reject) => {
                    reject('Client does not exist');
                }));
            },
            update: jest.fn(),
            forgetUser(user: User, username: string): Promise<string> {
                return new Promise<string>(((resolve, reject) => {
                    reject('Client does not exist');
                }));
            }
        }));
        userService = new UserService(mockUserRepo());
        let result: string;
        await userService.updateClientDeletion(username).then(ret => {
            result = ret;
        });
        expect(result).toBe('Client does not exist');
    });
    it('should throw exception if User to be updated does not exist', async () => {
        mockUserRepo = jest.fn<IUserRepository, []>(() => ({
            create: jest.fn(),
            findAll: jest.fn(),
            findByUsername(username: string): Promise<User> {
                return new Promise<User>(((resolve, reject) => {
                    reject('Client does not exist');
                }));
            },
            update(user: User, username: string): Promise<string> {
                return new Promise<string>(((resolve, reject) => {
                    reject('Client does not exist');
                }));
            },
            forgetUser: jest.fn()
        }));
        userService = new UserService(mockUserRepo());
        let result: string;
        await userService.updateClient(username, new ClientUpdateDTO('Maria','Santos','Street Nova 101')).then(ret => {
            result = ret;
        });
        expect(result).toBe('Client does not exist');
    });
});
