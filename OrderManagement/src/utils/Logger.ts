import winston from "winston";

const options = {
    file: {
        level: 'info',
        filename: 'logs/order-management.log',
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
        )
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        colorize: true,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.simple()
        )
    },
};

const logger = winston.createLogger({
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

export = logger;
