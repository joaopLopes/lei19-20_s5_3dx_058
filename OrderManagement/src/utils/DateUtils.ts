export function toddMMyyyyDate(date: string) {
    if (!date) {
        throw Error('Date missing');
    }
    const aux = date.split('-');
    if (aux.length != 3) {
        throw Error('Date invalid');
    }
    return new Date(Number.parseInt(aux[2]), Number.parseInt(aux[1]) - 1, Number.parseInt(aux[0]));
}
