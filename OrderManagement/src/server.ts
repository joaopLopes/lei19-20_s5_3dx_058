import app from "./app";
import https from 'https'
import fs from 'fs'
import logger from "./utils/Logger";

const port = process.env.PORT || 5004;
const ssl = process.env.SSL;

app.listen(port, () => {
    logger.info('HTTP/Heroku: Express server listening on port ' + port);
});
https.createServer({
    key: fs.readFileSync(__dirname + '/resources/server.key'),
    cert: fs.readFileSync(__dirname + '/resources/server.cert')
}, app).listen(ssl, () => {
    logger.info('HTTPS: Express server listening on port ' + ssl)
});
