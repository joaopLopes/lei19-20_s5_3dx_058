import {IRepositoryFactory} from "./interfaces/IRepositoryFactory";
import {RepositoryFactory} from "./impl/mongo-db/RepositoryFactory";

export class PersistenceContext {
    // TODO add env variable to create if its setup to use mongo-db
    public static repositoryFactory(): IRepositoryFactory {
        return new RepositoryFactory();
    }
}
