import {IUserRepository} from "../../interfaces/IUserRepository";
import {IUserBootstrapper} from "../../interfaces/IUserBootstrapper";
import {User} from "../../../domain/models/user/User";
import {AdminRole} from "../../../domain/models/user/admin/AdminRole";
import logger from "../../../utils/Logger";
import {PasswordService} from "../../../domain/services/impl/user/PasswordService";
import {ClientRole} from "../../../domain/models/user/client/ClientRole";

export class UserBootstrapper implements IUserBootstrapper {
    private _repo: IUserRepository;
    private _repo_admin: IUserRepository;

    private readonly admin = 'admin';
    private readonly password = 'admin#99';

    private readonly client = 'clientForTesting';
    private readonly password2 = 'client#99';
    private readonly nif = '999999999';
    private readonly email = 'testing@tigersoft.com';
    private readonly address = 'Rua de Testes Unitários';
    private readonly firstName = 'Testing';
    private readonly lastName = 'Name';
    private readonly phoneNumber = '968157333';
    private readonly age = '40';

    // constructor(ClientRepo,  AdminRepo);
    constructor(repo: IUserRepository, repoAdmin: IUserRepository) {
        this._repo = repo;
        this._repo_admin = repoAdmin;
    }

    async bootstrapAdmin(): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this._repo_admin.findByUsername(this.admin).then(_ => {
                // admin already exists, do nothing.
                resolve('Admin already exists.');
            }).catch(async _ => {
                let pw = '';
                await PasswordService.hashPassword(this.password, 10).then(password => {
                    pw = password;
                });
                // could not find admin, will create one.
                const admin = new User(this.admin, pw, new AdminRole());
                await this._repo_admin.create(admin).then(_ => {
                        resolve('Admin was bootstrapped');
                    }
                ).catch(error => {
                    logger.error(error);
                    reject('An error has occurred bootstrapping the admin.');
                });
            })
        });
    }

    async bootstrapClient(): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this._repo.findByUsername(this.client).then(_ => {
                // client already exists, do nothing.
                resolve('Client already exists.');
            }).catch(async _ => {
                let pw = '';
                await PasswordService.hashPassword(this.password2, 10).then(password => {
                    pw = password;
                });
                // could not find client, will create one.
                const client = new User(this.client, pw, new ClientRole(this.email, this.nif, this.phoneNumber, this.age, this.address, this.firstName, this.lastName));
                await this._repo.create(client).then(_ => {
                        resolve('Client was bootstrapped');
                    }
                ).catch(error => {
                    logger.error(error);
                    reject('An error has occurred bootstrapping the client.');
                });
            })
        });
    }

}
