import mongoose from "mongoose";
import logger from "../../../utils/Logger";

export default async () => {
    const db = () => {
        mongoose.connect(process.env.DATABASE, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        }).then(() => {
            logger.info('Connection to database successful')
        }).catch(error => {
            logger.error('Error connecting to database: ', error);
            return process.exit(1);
        });
    };
    db();
}
