import {IOrderRepository} from "../../interfaces/IOrderRepository";
import OrderRepository from "./repositories/OrderRepository";
import OrderFactory from "../../../domain/services/impl/order/OrderFactory";
import {IRepositoryFactory} from "../../interfaces/IRepositoryFactory";
import {IUserRepository} from "../../interfaces/IUserRepository";
import ClientRepository from "./repositories/ClientRepository";
import AdminRepository from "./repositories/AdminRepository";

export class RepositoryFactory implements IRepositoryFactory {
    private orderRepo: IOrderRepository;
    private clientRepo: IUserRepository;
    private adminRepo: IUserRepository;

    order(): IOrderRepository {
        if (this.orderRepo == null) {
            this.orderRepo = new OrderRepository(new OrderFactory());
        }
        return this.orderRepo;
    }

    client(): IUserRepository {
        if (this.clientRepo == null) {
            this.clientRepo = new ClientRepository();
        }
        return this.clientRepo;
    }

    admin(): IUserRepository {
        if (this.adminRepo == null) {
            this.adminRepo = new AdminRepository();
        }
        return this.adminRepo;
    }
}
