import mongoose from 'mongoose';
import UserModel from "./UserModel";

const RoleClientSchema = UserModel.discriminator('Client', new mongoose.Schema({
    _role: {
        _email: {
            type: String,
            required: false,
            unique: true,
            select: true
        },
        _address: {
            type: String,
            unique: false,
            required: true,
            select: true
        },
        _nif: {
            type: String,
            unique: false,
            required: true,
            select: true
        },

        _age: {
            type: String,
            unique: false,
            required: true,
            select: true
        },
        _phoneNumber: {
            type: String,
            unique: false,
            required: true,
            select: true
        },
        _lastUsedCard: {
            type: String,
            unique: false,
            required: true,
            select: true
        },
        _priority: {
            type: String,
            unique: false,
            required: true,
            select: true
        },
        _firstName: {
            type: String,
            unique: false,
            required: true,
            select: true
        },
        _lastName: {
            type: String,
            unique: false,
            required: true,
            select: true
        }
    }
}, {
    versionKey: false
}));

export default mongoose.model('Client');


