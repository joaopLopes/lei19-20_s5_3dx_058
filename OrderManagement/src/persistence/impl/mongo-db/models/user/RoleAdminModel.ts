import mongoose from 'mongoose';
import UserModel from "./UserModel";

const RoleAdminSchema = UserModel.discriminator('Admin', new mongoose.Schema({

}, {
    versionKey: false
}));

export default mongoose.model('Admin');
