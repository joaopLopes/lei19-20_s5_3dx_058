import mongoose from 'mongoose';
import {User} from "../../../../../domain/models/user/User";


const UserSchema = new mongoose.Schema({
    _username: {
        type: String,
        unique: true,
        required: true,
        select: true
    },
    _password: {
        type: String,
        unique: false,
        required: true,
        select: true
    },
    _state: {
        type: String,
        unique: false,
        required: true,
        select: true
    }
}, {
    discriminatorKey: 'userType',
    collection: 'users',
    versionKey: false
});

UserSchema.loadClass(User);

export default mongoose.model('User', UserSchema)
