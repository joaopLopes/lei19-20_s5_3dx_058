import mongoose from 'mongoose';
import Order from "../../../../../domain/models/order/Order";

export const OrderSchema = new mongoose.Schema({
    _reference: {
        type: String,
        unique: true,
        required: true,
        select: true
    },
    _products: [{
        _name: {
            type: String,
            unique: false,
            required: true,
            select: true
        }, _price: {
            type: Number,
            unique: false,
            required: true,
            select: true
        }, _quantity: {
            type: Number,
            unique: false,
            required: true,
            select: true
        }, _deliveryDate: {
            type: Date,
            unique: false,
            required: true,
            select: true
        }
    }],
    _client: {
        type: String,
        unique: false,
        required: true,
        select: true
    },
    _totalPrice: {
        type: Number,
        unique: false,
        required: true,
        select: true
    },
    _creditCardUsed: {
        type: String,
        unique: false,
        required: true,
        select: true
    },
    _placementDate: {
        type: Date,
        unique: false,
        required: true,
        select: true
    },
    _state: {
        type: String,
        unique: false,
        required: true,
        select: true
    },
    _shippingAddress: {
        type: String,
        unique: false,
        required: true,
        select: true
    }
}, {
    versionKey: false
});

OrderSchema.loadClass(Order);

export default mongoose.model('Order', OrderSchema)
