import RepositoryBase from "../RepositoryBase";
import {IUserRepository} from "../../../interfaces/IUserRepository";
import ClientModel from "../models/user/RoleClientModel";
import {UserMapper} from "../../../../mappers/UserMapper";
import {Document} from "mongoose";
import {User} from "../../../../domain/models/user/User";
import logger from "../../../../utils/Logger";
import crypto from 'crypto';

class ClientRepository extends RepositoryBase<User> implements IUserRepository {
    private static readonly NO_CLIENT_FOUND = 'No Client was found.';
    private static readonly SPECIFIC_Client_NOT_FOUND = 'Could not find Client with username: ';

    constructor() {
        super(ClientModel);
    }

    // Saves the entity
    async create(body: User): Promise<User> {
        const mapper = new UserMapper();
        const document = new this.model(body);
        return new Promise<User>((resolve, reject) => {
            document.save().then(client => {
                const doc: Document = client.toObject();
                const newClient = mapper.MongooseModelToDomainClient(doc);
                resolve(newClient);
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            })
        });
    }

    // finds and returns all occurrences of the entity
    async findAll(): Promise<User[]> {
        return new Promise<User[]>((resolve, reject) => {
            let clients: User[] = [];
            ClientModel.find({ _state: { $nin: [("1")] } }).then(result => {
                if (result.length > 0) {
                    const mapper: UserMapper = new UserMapper();
                    result.forEach(function (value) {
                        const doc = value.toObject();
                        let tmpClient: User;
                        tmpClient = mapper.MongooseModelToDomainClient(doc);
                        clients.push(tmpClient);
                    });
                    resolve(clients);
                } else {
                    logger.error("No Clients in Database.");
                    reject(ClientRepository.NO_CLIENT_FOUND);
                }
            }).catch(error => {
                logger.error(error.toString());
                reject(ClientRepository.NO_CLIENT_FOUND);
            });
        });
    }

    async findByUsername(username: string): Promise<User> {
        return new Promise((resolve, reject) => {
            ClientModel.findOne({
                '_username': username
            }).then((foundClient) => {
                if (foundClient === null) {
                    reject(ClientRepository.SPECIFIC_Client_NOT_FOUND + username);
                    return;
                }
                const mapper = new UserMapper();
                const doc = foundClient.toObject();
                let client: User;
                client = mapper.MongooseModelToDomainClient(doc);
                resolve(client);
            }).catch((error) => {
                logger.error(error);
                reject(ClientRepository.SPECIFIC_Client_NOT_FOUND + username);
            });
        });
    }

    /**
     * Database method that allows to update the username or address.
     * @param client The user that is going to be updated.
     * @param username The old username of the user.
     */
    async update(client: User, username: string): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this.model.updateOne({'_username': username}, {$set: {  "_role._firstName": client.role.getFirstName().getName(),
                    "_role._lastName": client.role.getLastName().getName(),
                    "_role._address": client.role.getAddress().toString()}}).then(() => {
                resolve('Client was updated!');
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            });
        });
    }

    // This method is necessary to delete users from the database.
    async forgetUser(client: User, username: string): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            let intAux: number;
            let id = crypto.randomBytes(40).toString('hex'); //This way, we ensure the name is almost certainly not duplicated
            let stringAux = 'deleted' + id;
            await this.model.updateOne({'_username': username}, {$set: {"_username": client.username.toString(),
                    "_password": 'deleted',
                    "_role._firstName": client.role.getFirstName().getName(),
                    "_role._lastName": client.role.getLastName().getName(),
                    "_role._address": client.role.getAddress().toString(),
                    "_role._email": stringAux,
                    "_role._age":stringAux,
                    "_role._phoneNumber": stringAux,
                    "_role._lastUsedCard": client.role.getLastUsedCard().toString(),
                    "_role._priority": client.role.getPriority(),
                    "_state":'1'}}).then(() => {
                resolve('The Client\'s personal information was deleted!');
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            });
        });
    }
}

export = ClientRepository;
