import RepositoryBase from "../RepositoryBase";
import {IUserRepository} from "../../../interfaces/IUserRepository";
import AdminModel from "../models/user/RoleAdminModel";
import {UserMapper} from "../../../../mappers/UserMapper";
import {Document} from "mongoose";
import {User} from "../../../../domain/models/user/User";
import logger from "../../../../utils/Logger";

class AdminRepository extends RepositoryBase<User> implements IUserRepository {
    private static readonly NO_CLIENT_FOUND = 'No Client was found.';
    private static readonly SPECIFIC_Admin_NOT_FOUND = 'Could not find Admin with username: ';

    constructor() {
        super(AdminModel);
    }

    // Saves the entity
    async create(body: User): Promise<User> {
        const mapper = new UserMapper();
        const document = new this.model(body);
        return new Promise<User>((resolve, reject) => {

            document.save().then(client => {
                const doc: Document = client.toObject();
                const newClient = mapper.MongooseModelToDomainAdmin(doc);
                resolve(newClient);
            }).catch(error => {
                reject(error);
            })
        });
    }

    // finds and returns all occurrences of the entity
    async findAll(): Promise<User[]> {
        return new Promise<User[]>((resolve, reject) => {
            let clients: User[] = [];
            AdminModel.find().then(result => {
                if (result.length > 0) {
                    const mapper: UserMapper = new UserMapper();
                    result.forEach(function (value) {
                        const doc = value.toObject();
                        let tmpClient: User;
                        tmpClient = mapper.MongooseModelToDomainAdmin(doc);
                        clients.push(tmpClient);
                    });
                    resolve(clients);
                } else {
                    console.error("No Clients in Database.");
                    reject(AdminRepository.NO_CLIENT_FOUND);
                }
            }).catch(error => {
                console.error(error);
                reject(AdminRepository.NO_CLIENT_FOUND);
            });
        });
    }

    async findByUsername(username: string): Promise<User> {
        return new Promise((resolve, reject) => {
            AdminModel.findOne({
                '_username': username
            }).then((foundClient) => {
                if (foundClient) {
                    const mapper = new UserMapper();
                    const doc = foundClient.toObject();
                    let client: User;
                    client = mapper.MongooseModelToDomainAdmin(doc);
                    resolve(client);
                }else{
                    logger.info('Username ' + username + ' is not admin.');
                    reject(AdminRepository.SPECIFIC_Admin_NOT_FOUND + username);
                }
            }).catch((error) => {
                logger.error(error);
                reject(AdminRepository.SPECIFIC_Admin_NOT_FOUND + username);
            });
        });
    }

    update(admin: User, username: string): Promise<string> {
        throw new Error("Administrator can not update name.");
    }

    forgetUser(client: User, username: string): Promise<string> {
        throw new Error("Admin can not be forgotten - TO BE IMPLEMENTED");
    }
}

export = AdminRepository;
