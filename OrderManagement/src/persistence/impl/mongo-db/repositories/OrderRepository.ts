import RepositoryBase from "../RepositoryBase";
import Order from "../../../../domain/models/order/Order";
import {IOrderRepository} from "../../../interfaces/IOrderRepository";
import OrderFactory from "../../../../domain/services/impl/order/OrderFactory";
import OrderModel from "../models/order/OrderModel";
import {OrderMapper} from "../../../../mappers/OrderMapper";
import {Document} from "mongoose";
import logger from "../../../../utils/Logger";


class OrderRepository extends RepositoryBase<Order> implements IOrderRepository {
    private factory: OrderFactory;
    private static readonly NO_ORDER_FOUND = 'No Order was found.';
    private static readonly SPECIFIC_ORDER_NOT_FOUND = 'Could not find Order with reference number: ';

    constructor(factory: OrderFactory) {
        super(OrderModel);
        this.factory = factory;
    }

    getProductTimesOrdered(product: string): Promise<number> {
        let retValue: number;
        return new Promise<number>(async (resolve, reject) => {
            await this.model.countDocuments({
                '_products._name': product
            }).then(value => {
                retValue = value;
                resolve(retValue);
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            });
        });
    }

    getQuantityOrderedForProduct(product: string) {
        return new Promise<number>(async (resolve, reject) => {
            await this.model.aggregate([
                // $unwind - Deconstructs an array field from the input documents to output a document for each element.
                {$unwind: "$_products"},
                {$match: {"_products._name": product}},
                {$group: {_id: null, total: {$sum: "$_products._quantity"}}}
            ]).then(foundValues => {
                const stringValue = JSON.stringify(foundValues);
                const jsonValue = JSON.parse(stringValue);
                //If the following line fails, it means that the aggregator didn't find anything.
                if (jsonValue[0]) {
                    const actualValue = jsonValue[0].total;
                    resolve(actualValue);
                } else {
                    resolve(0);
                }
            }).catch(error => {
                logger.error(error + " at OrderRepository.getQuantityOrderedForProduct.");
                reject("An error has occurred. Please try again.");
            });
        });
    }

    // Saves the entity
    async create(order: Order): Promise<Order> {
        const mapper: OrderMapper = new OrderMapper();
        return new Promise<Order>(async (resolve, reject) => {
            await this.model.create(order).then(order => {
                const doc: Document = order.toObject();
                const newOrder: Order = mapper.MongooseModelToDomain(doc);
                resolve(newOrder);
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            })
        });
    }

    // finds and returns all occurrences of the entity
    async findAll(): Promise<Order[]> {
        return new Promise<Order[]>((resolve, reject) => {
            let orders: Order[] = [];
            this.model.find().then(result => {
                if (result.length > 0) {
                    result.forEach(function (value) {
                        const mapper: OrderMapper = new OrderMapper();
                        const doc: Document = value.toObject();
                        const tmpOrder: Order = mapper.MongooseModelToDomain(doc);
                        orders.push(tmpOrder);
                    });
                    resolve(orders);
                } else {
                    logger.error(OrderRepository.NO_ORDER_FOUND);
                    reject(OrderRepository.NO_ORDER_FOUND);
                }

            }).catch(error => {
                logger.error(error.toString());
                reject(OrderRepository.NO_ORDER_FOUND);
            });
        });
    }


    async findById(reference: string): Promise<Order> {
        return new Promise(async (resolve, reject) => {
            await OrderModel.findOne({
                '_reference': reference
            }).then((foundOrder) => {
                if (!foundOrder) {
                    reject(OrderRepository.SPECIFIC_ORDER_NOT_FOUND + reference);
                    return;
                }
                const mapper: OrderMapper = new OrderMapper();
                const doc: Document = foundOrder.toObject();
                const order: Order = mapper.MongooseModelToDomain(doc);
                resolve(order);
            }).catch((error) => {
                logger.error(error.toString());
                reject(OrderRepository.SPECIFIC_ORDER_NOT_FOUND + reference);
            });
        });
    }

    async update(order: Order): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this.model.updateOne({'_reference': order.reference}, order).then(() => {
                resolve('Order was updated.');
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            });
        });
    }

    findByClient(username: string): Promise<Order[]> {
        return new Promise<Order[]>((resolve, reject) => {
            let orders: Order[] = [];
            this.model.find({'_client': username}).then(foundOrders => {
                if (foundOrders.length > 0) {
                    foundOrders.forEach(function (value) {
                        const mapper: OrderMapper = new OrderMapper();
                        const doc: Document = value.toObject();
                        const tmpOrder: Order = mapper.MongooseModelToDomain(doc);
                        orders.push(tmpOrder);
                    });
                    resolve(orders);
                } else {
                    logger.error(OrderRepository.NO_ORDER_FOUND);
                    reject(OrderRepository.NO_ORDER_FOUND);
                }
            }).catch(error => {
                logger.error(error.toString());
                reject(OrderRepository.NO_ORDER_FOUND);
            });
        });
    }

    findOngoing(startDate: Date, endDate: Date): Promise<Order[]> {
        return new Promise<Order[]>((resolve, reject) => {
            let orders: Order[] = [];
            this.model.find({_state: 'ONGOING'}).then(result => {
                if (result.length > 0) {
                    result.forEach(function (value) {
                        const mapper: OrderMapper = new OrderMapper();
                        const doc: Document = value.toObject();
                        const tmpOrder: Order = mapper.MongooseModelToDomain(doc);
                        orders.push(tmpOrder);
                    });
                    resolve(orders);
                } else {
                    logger.error(OrderRepository.NO_ORDER_FOUND);
                    reject(OrderRepository.NO_ORDER_FOUND);
                }
            }).catch(error => {
                logger.error(error.toString());
                reject(OrderRepository.NO_ORDER_FOUND);
            });
        });
    }
}

export = OrderRepository;
