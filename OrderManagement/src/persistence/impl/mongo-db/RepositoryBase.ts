import {Entity} from "../../../domain/models/Entity";
import {Model, Document} from "mongoose";

abstract class RepositoryBase<T extends Entity<T>> {
    protected model: Model<Document>;

    // should receive the mongo-db model relevant to the object
    protected constructor(model: Model<Document>) {
        this.model = model;
    }
}

export = RepositoryBase;
