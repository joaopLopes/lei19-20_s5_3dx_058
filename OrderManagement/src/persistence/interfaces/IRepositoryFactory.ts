import {IOrderRepository} from "./IOrderRepository";
import {IUserRepository} from "./IUserRepository";

export interface IRepositoryFactory {
    order(): IOrderRepository;
    client(): IUserRepository;
    admin(): IUserRepository;

    // other repositories...
}
