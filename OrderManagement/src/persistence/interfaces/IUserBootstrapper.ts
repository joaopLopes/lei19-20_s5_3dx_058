export interface IUserBootstrapper {
    bootstrapAdmin(): Promise<string>;
    bootstrapClient(): Promise<string>;
}
