import {ClientDto} from "../../dtos/user/ClientDto";
import {User} from "../../domain/models/user/User";

export interface IUserRepository {
    findByUsername(username: string): Promise<User>;

    create(user: User): Promise<User>;

    findAll(): Promise<User[]>;

    update(user: User, username: string): Promise<string>

    forgetUser(user: User, username: string): Promise<string>

}
