import Order from "../../domain/models/order/Order";

export interface IOrderRepository {
    getProductTimesOrdered(product: string): Promise<number>;

    getQuantityOrderedForProduct(product: string): Promise<number>;

    findById(reference: string): Promise<Order>;

    create(order: Order): Promise<Order>;

    update(order: Order): Promise<string>

    findAll(): Promise<Order[]>;

    findByClient(username: string): Promise<Order[]>;

    findOngoing(startDate: Date, endDate: Date): Promise<Order[]>;
}
