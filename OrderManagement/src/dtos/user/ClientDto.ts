export class ClientDto {
    username: string;
    password: string;
    address: string;
    firstName: string;
    lastName: string;
    nif: string;
    phoneNumber: string;
    email: string;
    age: string;
    priority: number;

    constructor(username: string, password: string, address: string, nif: string, phoneNum: string, email: string, age: string,firstName:string, lastName:string, priority: number) {
        this.username = username;
        this.password = password;
        this.address = address;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.nif = nif;
        this.phoneNumber = phoneNum;
        this.priority = priority;
    }
}
