export class ClientUpdateDTO {
    firstName: string;
    lastName: string;
    address: string;
    /**
     * Constructor that allows a Client Update DTO to be built with all the arguments passed as a parameter.
     * @param firstName The first name of the client. of the client.
     * @param lastName The last name of the client.
     * @param address The address of the client.
     */
    constructor(firstName: string, lastName: string, address: string){
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }
}