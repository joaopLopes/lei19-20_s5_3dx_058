import {ProductDto} from "../product/ProductDto";

export class OrderDto {
    reference: string;
    products: ProductDto[];
    shippingAddress: string;
    client: string;
    totalPrice: number;
    creditCardUsed: string;
    state: string;
    placementDate: Date;

    constructor(products: ProductDto[], client: string,
                creditCard: string, date: Date, shippingAddress: string, price?: number, state?: string, reference?: string) {
        if (reference) this.reference = reference;
        if (state) this.state = state;
        this.client = client;
        this.shippingAddress = shippingAddress;
        this.products = products;
        this.creditCardUsed = creditCard;
        this.totalPrice = price;
        this.placementDate = date;
    }
}
