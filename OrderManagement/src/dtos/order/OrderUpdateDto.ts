export class OrderUpdateDto {
    shippingAddress: string;

    constructor(shippingAddress: string) {
        this.shippingAddress = shippingAddress;
    }
}
