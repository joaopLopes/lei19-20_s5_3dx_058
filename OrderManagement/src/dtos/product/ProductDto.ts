export class ProductDto {
    name: string;
    price: number;
    quantity: number;
    deliveryDate: string;

    constructor(name: string, price: number, quantity?: number, date?: string) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.deliveryDate = date;
    }
}
