export class PermissionType {
    static readonly LIST_CLIENTS = new PermissionType('list-clients', 'all', 'none', null, 'name-only');
    static readonly CONSULT_CLIENT = new PermissionType('consult-client', 'any', 'none', 'himself', 'name-only');
    static readonly UPDATE_CLIENT = new PermissionType('update-client', 'any', 'none', 'himself');
    static readonly DELETE_CLIENT = new PermissionType('delete-client', 'any', 'none', 'himself');
    static readonly LIST_ORDERS = new PermissionType('list-orders', 'any', 'none', 'himself');
    static readonly CONSULT_ORDER = new PermissionType('consult-order', 'any', 'none', 'himself');
    static readonly CREATE_ORDER = new PermissionType('create-order', 'yes', 'no');
    static readonly UPDATE_ORDER = new PermissionType('update-order', 'any', 'none', 'himself');
    static readonly CANCEL_ORDER = new PermissionType('cancel-order', 'any', 'none', 'himself');

    // private to disallow creating other instances of this type
    private constructor(private readonly key: string, private readonly allowedValue: string, private readonly disallowedValue: string, private readonly himself?: string, private readonly limited_info?: string) {
    }

    isAllowed(permission: string): string {
        switch (permission) {
            case this.allowedValue:
                return Permission.ALLOWED;
            case this.disallowedValue:
                return Permission.NOT_ALLOWED;
            case this.himself:
                return Permission.ONLY_HIMSELF;
            case this.limited_info:
                return Permission.LIMITED_INFO;
            default:
                return Permission.NOT_ALLOWED;
        }
    }

    toString() {
        return this.key;
    }
}

export enum Permission {
    LIMITED_INFO = 'limited-information',
    ALLOWED = 'allowed',
    ONLY_HIMSELF = 'only-himself',
    NOT_ALLOWED = 'not-allowed'
}
