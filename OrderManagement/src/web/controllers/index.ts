import {Router, Request, Response} from "express";
import order from "./OrderController";
import client from "./ClientController"
import admin from "./AdminController"
import product from "./ProductController"
// contains all the controllers of the server
const routes = Router();

routes.use("/orders", order);
routes.use("/products", product);
routes.use("/clients", client);
routes.use("/admins", admin);

export default routes;
