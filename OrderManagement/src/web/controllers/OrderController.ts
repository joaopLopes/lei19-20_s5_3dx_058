import {Router, Request, Response} from 'express';
import {PersistenceContext} from "../../persistence/PersistenceContext";
import {OrderMapper} from "../../mappers/OrderMapper";
import {OrderService} from "../../domain/services/impl/order/OrderService";
import {OrderDto} from "../../dtos/order/OrderDto";
import {MasterDataProductionAdapter} from "../../domain/services/impl/adapters/MasterDataProductionAdapter";
import {IOrderService} from "../../domain/services/interfaces/order/IOrderService";
import OrderFactory from "../../domain/services/impl/order/OrderFactory";
import {OrderUpdateDto} from "../../dtos/order/OrderUpdateDto";
import {hasAccess, isUserAssociatedWithResource, isUserOrderOwner} from "../middlewares/AuthorizationService";
import {PermissionType} from "../services/PermissionType";
import {AuthService} from "../../domain/services/impl/user/AuthService";
import {AdminService} from "../../domain/services/impl/user/AdminService";
import {toddMMyyyyDate} from "../../utils/DateUtils";

// router for orders
const router = Router();


// GET /orders
router.route('/').get([hasAccess(PermissionType.LIST_ORDERS)], (req: Request, res: Response) => {
    if (req.headers['himself'] === 'true') {
        res.status(401).send('Not allowed');
        return;
    }
    const repoFactory = PersistenceContext.repositoryFactory();
    const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
    service.getAllOrders().then(orders => {
        res.status(200).json(orders)
    }).catch(error => {
        res.status(404).json('Error: ' + error);
    });
});

// GET /orders/scheduled?startDate=03/01/2020&endDate=01/02/2020
router.route('/scheduled?:startDate?:endDate').get([hasAccess(PermissionType.LIST_ORDERS)], (req: Request, res: Response) => {
        if (req.headers['himself'] === 'true') {
            res.status(401).send('Not allowed');
            return;
        }
        const repoFactory = PersistenceContext.repositoryFactory();
        const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
            PersistenceContext.repositoryFactory().admin(),
            new AdminService(PersistenceContext.repositoryFactory().admin()));
        const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
        try {
            const startDate = toddMMyyyyDate(req.query.startDate);
            const endDate = toddMMyyyyDate(req.query.endDate);
            service.getOrdersForScheduling(startDate, endDate).then(orders => {
                res.status(200).json(orders)
            }).catch(error => {
                res.status(404).json('Error: ' + error);
            });
        } catch (err) {
            res.status(400).json('Error: ' + err);
        }
    }
);


// GET /orders/1
router.route('/:reference').get([hasAccess(PermissionType.CONSULT_ORDER)], [isUserOrderOwner], (req: Request, res: Response) => {
    // call getOrder controller
    const repoFactory = PersistenceContext.repositoryFactory();
    const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
    service.getOrder(req.params.reference).then(order => {
            res.status(200).json(order)
        }, _ => {
            res.status(404).send('Order not found.');
        }
    ).catch(error => {
        res.status(404).json("Error: " + error);
    });
});

// GET /orders/client/username
router.route('/client/:username').get([hasAccess(PermissionType.LIST_ORDERS)], [isUserAssociatedWithResource], (req: Request, res: Response) => {
    const repoFactory = PersistenceContext.repositoryFactory();
    const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
    service.getOrdersByClient(req.params.username).then(orders => {
        res.status(200).json(orders)
    }).catch(error => {
        res.status(404).json('Error: ' + error);
    });
});

// POST /orders
router.route('/').post([hasAccess(PermissionType.CREATE_ORDER)], (req: Request, res: Response) => {
    const body = req.body;
    const mapper = new OrderMapper();
    // converts JSON to DTO
    const dto: OrderDto = mapper.JSONToOrderDTO(body);
    // Calls the service to create a new Order.
    const repoFactory = PersistenceContext.repositoryFactory();
    const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
    service.createOrder(dto).then((newOrder) => {
        res.status(201).json(newOrder);
    }).catch(error => {
        res.status(500).json(error.toString());
    });
});

// PUT /orders/1/cancel
router.route('/:reference/cancel').put([hasAccess(PermissionType.CANCEL_ORDER)], [isUserOrderOwner], (req: Request, res: Response) => {
    const repoFactory = PersistenceContext.repositoryFactory();
    const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
    service.cancelOrder(req.params.reference).then(_ => {
        res.status(204).send();
    }).catch(error => {
        res.status(404).json(error.toString());
    });
});

// PUT /orders/1
router.route('/:reference').put([hasAccess(PermissionType.UPDATE_ORDER)], [isUserOrderOwner], (req: Request, res: Response) => {
    const body = req.body;
    const mapper = new OrderMapper();
    // converts JSON to DTO
    const dto: OrderUpdateDto = mapper.JSONToOrderUpdateDTO(body);
    const repoFactory = PersistenceContext.repositoryFactory();
    const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
    service.updateOrder(req.params.reference, dto).then(_ => {
        res.status(204).send();
    }).catch(error => {
        res.status(400).json(error.toString());
    });
});
export default router;
