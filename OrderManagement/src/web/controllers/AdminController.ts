import {Router, Request, Response} from 'express';
import {PersistenceContext} from "../../persistence/PersistenceContext";
import {AdminService} from "../../domain/services/impl/user/AdminService";

// router for orders
const router = Router();

// GET /admins/username
router.route('/:username').get((req: Request, res: Response) => {
    // call ge controller
    const service: AdminService = new AdminService(PersistenceContext.repositoryFactory().admin());
    service.getAdmin(req.params.username).then(admin => {
        res.status(200).json(admin);
    }).catch(error => {
        res.status(404).json("Error: " + error);
    });
});

// GET /admins
router.route('/').get((req: Request, res: Response) => {
    const service: AdminService = new AdminService(PersistenceContext.repositoryFactory().admin());
    service.getAllAdmins().then(admins => {
        res.status(200).json(admins)
    }).catch(error => {
        res.status(404).json(error);
    });
});


export default router;

