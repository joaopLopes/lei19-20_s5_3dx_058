import {Router, Request, Response} from 'express';
import {PersistenceContext} from "../../persistence/PersistenceContext";
import {AuthService} from "../../domain/services/impl/user/AuthService";
import {AdminService} from "../../domain/services/impl/user/AdminService";
import {UserMapper} from "../../mappers/UserMapper";
import {ClientDto} from "../../dtos/user/ClientDto";
import {UserService} from "../../domain/services/impl/user/UserService";
import logger from "../../utils/Logger";
import {AdminDto} from "../../dtos/user/AdminDto";

// router for orders
const auth = Router();

//Login via POST with credentials in body
auth.route('/login').post(async (req: Request, res: Response) => {
    const body = req.body;
    const service = new AuthService(PersistenceContext.repositoryFactory().client(),
        PersistenceContext.repositoryFactory().admin(),
        new AdminService(PersistenceContext.repositoryFactory().admin()));
    await service.login(body.username, body.password).then(token => {
        res.setHeader("Access-Control-Expose-Headers", "Authorization");
        res.status(201).setHeader('Authorization', token);
        res.send();
    }, error => {
        logger.error(error);
        res.status(401).send();
    }).catch(err => {
        logger.error(err);
        res.status(401).send();
    });
});

// POST /create
auth.route('/accounts').post((req: Request, res: Response) => {
    const body = req.body;
    const mapper = new UserMapper();
    // converts JSON to DTO
    const dto: ClientDto = mapper.JSONToClientDTO(body);
    // Calls the service to create a new Order.
    const service = new UserService(PersistenceContext.repositoryFactory().client());
    service.createClient(dto).then((client) => {
        res.status(201).send(client);
    }).catch(error => {
        res.status(500).json(error);
    });
});

// POST /admins
auth.route('/accounts/admins').post((req: Request, res: Response) => {
    const body = req.body;
    const mapper = new UserMapper();
    // converts JSON to DTO
    const dto: AdminDto = mapper.JSONToAdminDTO(body);
    // Calls the service to create a new Order.
    const service = new AdminService(PersistenceContext.repositoryFactory().admin());
    service.createAdmin(dto).then((admin) => {
        res.status(201).send(admin);
    }).catch(error => {
        res.status(400).json(error.toString());
    });
});


export = auth;
