import {Router, Request, Response} from 'express';
import {PersistenceContext} from "../../persistence/PersistenceContext";
import {MasterDataProductionAdapter} from "../../domain/services/impl/adapters/MasterDataProductionAdapter";
import {ProductService} from "../../domain/services/impl/product/ProductService";
import {ProductDto} from "../../dtos/product/ProductDto";
import {AuthService} from "../../domain/services/impl/user/AuthService";
import {AdminService} from "../../domain/services/impl/user/AdminService";
import ClientRepository from "../../persistence/impl/mongo-db/repositories/ClientRepository";
import AdminRepository from "../../persistence/impl/mongo-db/repositories/AdminRepository";

const router = Router();

// GET /products/most-times
router.route('/sortby/:criteria').get( (req: Request, res: Response) => {
    const repoFactory = PersistenceContext.repositoryFactory();
    const adminRepo = new AdminRepository();
    const admin = new AdminService(adminRepo);
    const client = new ClientRepository();
    const auth = new AuthService(client, adminRepo, admin);
    const service: ProductService = new ProductService(new MasterDataProductionAdapter(auth),repoFactory.order());
    service.sortBy(req.params.criteria).then(list => {
        res.status(201).json(list);
    }).catch(error => {
        res.status(500).send(error);
    });
});

// GET /products/:name
router.route('/:name').get( (req: Request, res: Response) => {
    const repoFactory = PersistenceContext.repositoryFactory();
    const adminRepo = new AdminRepository();
    const admin = new AdminService(adminRepo);
    const client = new ClientRepository();
    const auth = new AuthService(client, adminRepo, admin);
    const service: ProductService = new ProductService(new MasterDataProductionAdapter(auth),repoFactory.order());
    service.getProduct(req.params.name).then(product => {
        res.status(201).json(product);
    }).catch(error => {
        res.status(500).send(error);
    });
});

// GET /products/
router.route('/').get( (req: Request, res: Response) => {
    const repoFactory = PersistenceContext.repositoryFactory();
    const adminRepo = new AdminRepository();
    const admin = new AdminService(adminRepo);
    const client = new ClientRepository();
    const auth = new AuthService(client, adminRepo, admin);
    const service: ProductService = new ProductService(new MasterDataProductionAdapter(auth),repoFactory.order());
    service.getAllProducts().then(list => {
        res.status(201).json(list);
    }).catch(error => {
        res.status(500).send(error);
    });
});

export default router;
