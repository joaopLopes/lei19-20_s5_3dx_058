import {Router, Request, Response} from 'express';
import {PersistenceContext} from "../../persistence/PersistenceContext";
import {UserMapper} from "../../mappers/UserMapper";
import {ClientUpdateDTO} from "../../dtos/user/ClientUpdateDTO";
import {UserService} from '../../domain/services/impl/user/UserService';
import {hasAccess, isUserAssociatedWithResource} from "../middlewares/AuthorizationService";
import {IUserService} from "../../domain/services/interfaces/user/IUserService";
import {PermissionType} from "../services/PermissionType";

// router for orders
const router = Router();

// GET /clients/username
router.route('/:username').get([hasAccess(PermissionType.CONSULT_CLIENT)], [isUserAssociatedWithResource], (req: Request, res: Response) => {
    // call ge controller
    const service: IUserService = new UserService(PersistenceContext.repositoryFactory().client());
    if (req.headers['limitedInfo'] === 'true') {
        service.getClientName(req.params.username).then(client => {
            res.status(200).json(client);
        }).catch(error => {
            res.status(404).json("Error: " + error);
        });
    } else {
        service.getClient(req.params.username).then(client => {
            res.status(200).json(client);
        }).catch(error => {
            res.status(404).json("Error: " + error);
        });
    }
});

// GET /clients
router.route('/').get([hasAccess(PermissionType.LIST_CLIENTS)], (req: Request, res: Response) => {
    const service: IUserService = new UserService(PersistenceContext.repositoryFactory().client());
    if (req.headers['limitedInfo'] === 'true') {
        service.getAllClientsName().then(clients => {
            res.status(200).json(clients);
        }).catch(error => {
            res.status(404).json("Error: " + error);
        });
    } else {
        service.getAllClients().then(clients => {
            res.status(200).json(clients)
        }).catch(error => {
            res.status(404).json(error);
        });
    }
});

/**
 * Method that allows the client to be updated. Uses the mappers already available in the mappers of client.
 */
//PUT /clients/:username of the client
router.route('/:username').put([hasAccess(PermissionType.UPDATE_CLIENT)], [isUserAssociatedWithResource], (req: Request, res: Response) => {
        const body = req.body;
        const mapper = new UserMapper();
        // Converts JSON to Dto that can update the user.
        const dto: ClientUpdateDTO = mapper.JSONToClientUpdateDTO(body);
        const repoFactory = PersistenceContext.repositoryFactory();
        const service: IUserService = new UserService(repoFactory.client()); // Should be programmed to interface.
        service.updateClient(req.params.username, dto).then(message => {
            res.status(200).send();
        }).catch(error => {
            res.status(204).json("Error: " + error);
        });
    }
);

/**
 * Method to 'delete' client.
 */
router.route('/:username').delete([hasAccess(PermissionType.DELETE_CLIENT)], [isUserAssociatedWithResource], (req: Request, res: Response) => {
        // Converts JSON to Dto that can update the user.
        const repoFactory = PersistenceContext.repositoryFactory();
        const service: IUserService = new UserService(repoFactory.client());
        service.updateClientDeletion(req.params.username).then(message => {
            res.status(200) .send();
        }).catch(error => {
            res.status(204).json("Error: " + error);
        });
    }
);

export default router;

