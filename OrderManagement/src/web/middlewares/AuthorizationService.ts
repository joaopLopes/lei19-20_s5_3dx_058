import {Request, Response, NextFunction} from "express";
import jsonwebtoken from "jsonwebtoken";
import {Permission, PermissionType} from "../services/PermissionType";
import logger from "../../utils/Logger";
import {getRoleFromToken, getUserFromToken} from "../../domain/services/impl/permissions/TokenService";
import {PersistenceContext} from "../../persistence/PersistenceContext";
import {IOrderService} from "../../domain/services/interfaces/order/IOrderService";
import {OrderService} from "../../domain/services/impl/order/OrderService";
import OrderFactory from "../../domain/services/impl/order/OrderFactory";
import {MasterDataProductionAdapter} from "../../domain/services/impl/adapters/MasterDataProductionAdapter";
import {getRolePermissions} from "../../domain/services/impl/permissions/PermissionsService";
import {AuthService} from "../../domain/services/impl/user/AuthService";
import {AdminService} from "../../domain/services/impl/user/AdminService";


export const isUserOrderOwner = (req: Request, res: Response, next: NextFunction) => {
    if (req.headers['himself'] === 'true') {
        const repoFactory = PersistenceContext.repositoryFactory();
        const authService = new AuthService(PersistenceContext.repositoryFactory().client(),
            PersistenceContext.repositoryFactory().admin(),
            new AdminService(PersistenceContext.repositoryFactory().admin()));
        const service: IOrderService = new OrderService(new OrderFactory(), repoFactory.order(), repoFactory.client(), new MasterDataProductionAdapter(authService));
        service.getOrder(req.params.reference).then(order => {
            const token = <string>req.headers["token"];
            if (order.client === getUserFromToken(token)) {
                next();
            } else {
                res.status(401).send('Not allowed.');
            }
        }, error => {
            logger.error(error);
            res.status(404).send('Order not found');
        });
    } else {
        next();
    }
};

export const isUserAssociatedWithResource = (req: Request, res: Response, next: NextFunction) => {
    const token = <string>req.headers["token"];
    const header = req.headers['himself'];
    if (!header || header === 'false') {
        logger.info('himself header was null or false');
        next();
        return;
    } else {
        if (header === 'true') {
            const resourceUser = req.params['username'];
            const sessionUser = getUserFromToken(token);
            if (!resourceUser || !sessionUser) {
                logger.info('resource user or session user were not defined.');
                res.status(401).send('Not allowed.');
                return;
            }
            if (resourceUser === sessionUser) {
                next();
                return;
            } else {
                logger.info('session user and resource users did not match.');
                res.status(401).send('Not allowed.');
                return;
            }
        } else {
            logger.error('himself header was neither true, false or undefined');
            res.status(500).send('An error occurred');
        }
    }
};

export function hasAccess(reqType: PermissionType) {
    return function (req: Request, res: Response, next: NextFunction) {
        try {
            const token = <string>req.headers["token"];
            const role = getRoleFromToken(token);
            // Reads permissions file
            const permissions = getRolePermissions(role);
            const permission = permissions[reqType.toString()];
            if (!permission) {
                logger.error('Could not find permission for ' + reqType);
                res.status(500).send('An error occurred loading permissions.');
                return;
            }
            const permittedValue = reqType.isAllowed(permission);
            if (permittedValue === Permission.ALLOWED) {
                // any user can access any resource.
                logger.info('resource can be accessed by role\nhimself header set to false');
                req.headers.himself = 'false';
                next();
            } else {
                if (permittedValue === Permission.ONLY_HIMSELF) {
                    // only the user associated can access the resource.
                    // this function does not have access to enough information to make this validation, and thus
                    // it will delegate it to individual functions for each request with the header himself set to true
                    logger.info('resource is locked to associated users\nhimself header set to true');
                    req.headers.himself = 'true';
                    next();
                    return;
                } else if (permittedValue === Permission.NOT_ALLOWED) {
                    // not allowed
                    logger.info('user did not have access to resource');
                    res.status(401).send('Not allowed.');
                    return;
                } else if (permittedValue === Permission.LIMITED_INFO) {
                    // the user can access certain parts of the resource.
                    req.headers.limitedInfo = 'true';
                    next();
                    return;
                }
            }
        } catch (e) {
            logger.error(e);
            res.status(500).send('An error occurred loading permissions.');
        }
    }
}


// Checks if the token in the request is valid
export const isTokenValid = (req: Request, res: Response, next: NextFunction) => {
    const secret_client = process.env.JWT_TOKEN;
    const token = <string>req.headers["token"];
    if (!token) {
        res.status(401).send("No token");
        return;
    } else {
        try {
            const name = getUserFromToken(token);
            let payload;
            let role = getRoleFromToken(token);
            if (role == null || name == null) {
                res.status(404).json('Some of the required data was not found.').send();
            } else {
                let jwtPayload;
                //Try to validate the token and get data
                try {
                    jwtPayload = <any>jsonwebtoken.verify(token, secret_client);
                    if (role == 'client') {
                        res.locals.jwtPayload = jwtPayload;
                        payload = {user: req.body.username, role: 'client'};
                    } else if (role == 'admin') {
                        res.locals.jwtPayload = jwtPayload;
                        payload = {user: req.body.username, role: 'admin'};
                    } else {
                        //If role is not valid, respond with 400 (bad request)
                        res.status(401).send();
                        return;
                    }
                } catch (error) {
                    //If token is not valid, respond with 401 (unauthorized)
                    res.status(401).send();
                    return;
                }
                const domain = process.env.DOMAIN;
                const options = {expiresIn: 86400, issuer: domain};
                const newToken = jsonwebtoken.sign(payload, secret_client, options);
                res.setHeader("token", newToken);
            }
        } catch (e) {
            res.status(401).send(e.toString());
            return;
        }
    }
    next();
};
