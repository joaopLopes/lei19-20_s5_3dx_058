import {ProductForOrder} from "../../../models/product/ProductForOrder";

export interface IMasterDataProductionAdapter {
    findProductByName(prodName: string): Promise<ProductForOrder>;
    findAllProducts(): Promise<ProductForOrder[]>;

}
