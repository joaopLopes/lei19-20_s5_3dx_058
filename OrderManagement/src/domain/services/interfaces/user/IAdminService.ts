import {AdminDto} from "../../../../dtos/user/AdminDto";

export interface IAdminService{

    getAdmin(username: string): Promise<AdminDto>;
    getAllAdmins(): Promise<AdminDto[]>;
    createAdmin(dto: AdminDto): Promise<AdminDto>;

}
