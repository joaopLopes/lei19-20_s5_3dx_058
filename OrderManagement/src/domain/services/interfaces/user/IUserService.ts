import {ClientDto} from "../../../../dtos/user/ClientDto";
import {ClientUpdateDTO} from "../../../../dtos/user/ClientUpdateDTO";
import {ClientLimitedInformationDTO} from "../../../../dtos/user/ClientLimitedInformationDTO";

export interface IUserService {
    getClient(username: string): Promise<ClientDto>;

    getAllClients(): Promise<ClientDto[]>;

    createClient(dto: ClientDto): Promise<ClientDto>;

    updateClient(username: string, updateData: ClientUpdateDTO): Promise<string>;

    updateClientDeletion(username: string): Promise<string>;

    getClientName(username: string): Promise<ClientLimitedInformationDTO>;

    getAllClientsName(): Promise<ClientLimitedInformationDTO[]>;
}
