import {ProductForOrder} from "../../../models/product/ProductForOrder";
import Order from "../../../models/order/Order";
import {User} from "../../../models/user/User";

export interface IOrderFactory {
    newOrder(products: ProductForOrder[], client: User, price: number, creditCard: string, date: Date, shippingAddress: string): Order;
}
