import {OrderDto} from "../../../../dtos/order/OrderDto";
import {OrderUpdateDto} from "../../../../dtos/order/OrderUpdateDto";

export interface IOrderService {
    getOrder(reference: string): Promise<OrderDto>;

    getAllOrders(): Promise<OrderDto[]>;

    createOrder(dto: OrderDto): Promise<OrderDto>;

    cancelOrder(reference: string): Promise<string>;

    updateOrder(reference: string, updateData: OrderUpdateDto): Promise<string>;

    getOrdersByClient(username: string): Promise<OrderDto[]>;

    getOrdersForScheduling(startDate: Date, endDate: Date): Promise<OrderDto[]>;
}
