import {ProductForOrder} from "../../../models/product/ProductForOrder";
import {IMasterDataProductionAdapter} from "../../interfaces/adapters/IMasterDataProductionAdapter";
import logger from "../../../../utils/Logger";
import {Pair} from "./Pair";
import {IOrderRepository} from "../../../../persistence/interfaces/IOrderRepository";
import {ProductDto} from "../../../../dtos/product/ProductDto";
import {ProductMapper} from "../../../../mappers/ProductMapper";

export class ProductService {
    private adapter: IMasterDataProductionAdapter;
    private repo: IOrderRepository;
    private mapper: ProductMapper;

    constructor(adapter: IMasterDataProductionAdapter, repo: IOrderRepository) {
        this.adapter = adapter;
        this.repo = repo;
        this.mapper = new ProductMapper();
    }

    getAllProducts(): Promise<ProductDto[]> {
        return new Promise<ProductDto[]>(async (resolve, reject) => {
            await this.adapter.findAllProducts().then(list => {
                let retList: ProductDto[] = [];
                for (const elem of list) {
                    retList.push(this.mapper.DomainToDtoStore(elem));
                }
                resolve(retList);
            }).catch(error => {
                reject(error);
            });
        })
    }

    getProduct(name: string): Promise<ProductDto> {
        return new Promise<ProductDto>(async (resolve, reject) => {
            await this.adapter.findProductByName(name).then(product => {
                resolve(this.mapper.DomainToDtoStore(product));
            }).catch(error => {
                reject(error);
            });
        });
    }

    async sortBy(crit: string): Promise<ProductDto[]> {
        let list: ProductForOrder[] = [];
        if (crit == 'times') {
            await this.adapter.findAllProducts().then(ret => {
                list = ret;
            }).catch(error => {
                logger.error(error.toString() + ' - at ProductService.sortBy');
                throw new Error('An unexpected error has occurred.');
            });
            let res: ProductForOrder[];
            await this.sortByTimesOrdered(list).then(list2 => {
                res = list2;
            }).catch(error => {
                logger.error(error);
                throw new Error('An error has occurred while sorting the list of Products.');
            });
            let retList: ProductDto[] = [];
            for (const elem of res) {
                retList.push(this.mapper.DomainToDtoStore(elem));
            }
            return retList;
        } else if (crit == 'quantity') {
            await this.adapter.findAllProducts().then(ret => {
                list = ret;
            }).catch(error => {
                logger.error(error.toString() + ' - at ProductService.sortBy');
                throw new Error('An unexpected error has occurred.');
            });
            let res: ProductForOrder[];
            await this.sortByQuantityOrdered(list).then(value => {
                res = value;
            }).catch(error => {
                throw new Error('An unexpected error has occurred.');
            });
            let retList: ProductDto[] = [];
            for (const elem of res) {
                retList.push(this.mapper.DomainToDtoStore(elem));
            }
            return retList;
        } else if (crit == 'production-time') {
            await this.adapter.findAllProducts().then(ret => {
                list = ret;
            }).catch(error => {
                logger.error(error.toString() + ' - at ProductService.sortBy');
                throw new Error('An unexpected error has occurred.');
            });
            const sortedList = this.sortByProductionTime(list);
            let retList: ProductDto[] = [];
            for (const elem of sortedList) {
                retList.push(this.mapper.DomainToDtoStore(elem));
            }
            retList.reverse(); // Our Merge Sort algorithm sorts by the biggest value of production time, we must reverse it now.
            return retList;
        } else {
            throw new Error('Sort criteria is invalid.');
        }
    }

    private async sortByTimesOrdered(listProducts: ProductForOrder[]): Promise<ProductForOrder[]> {
        let listPairs: Pair[] = [];
        if (listProducts) {
            const rep = this.repo;
            for (const product of listProducts) {
                let value;
                await rep.getProductTimesOrdered(product.name).then((value2: number) => {
                    value = value2;
                    listPairs.push(new Pair(product, value));
                }).catch((error: Error) => {
                    logger.error(error.toString());
                });
            }
            let returnList: ProductForOrder[] = [];
            const sortedList = this.mergeSort(listPairs);
            for (const pair of sortedList) {
                returnList.push(pair.key);
            }
            return returnList;
        } else {
            throw new Error('An error has occurred.');
        }
    }

    private async sortByQuantityOrdered(listProducts: ProductForOrder[]): Promise<ProductForOrder[]> {
        let listPairs: Pair[] = [];
        if (listProducts) {
            const rep = this.repo;
            for (const product of listProducts) {
                let value;
                await rep.getQuantityOrderedForProduct(product.name).then((value2: number) => {
                    value = value2;
                    listPairs.push(new Pair(product, value));
                }).catch((error: Error) => {
                    logger.error(error.toString());
                });
            }
            let returnList: ProductForOrder[] = [];
            const sortedList = this.mergeSort(listPairs);
            for (const pair of sortedList) {
                returnList.push(pair.key);
            }
            return returnList;
        } else {
            throw new Error('An error has occurred.');
        }
    }

    private sortByProductionTime(list: ProductForOrder[]): ProductForOrder[] {
        let pairArray: Pair[] = [];
        list.forEach(function (value) {
            pairArray.push(new Pair(value, value.productionTime));
        });
        let sortedArrayPair = this.mergeSort(pairArray);
        let returnList: ProductForOrder[] = [];
        for (const pair of sortedArrayPair) {
            returnList.push(pair.key);
        }
        return returnList;
    }

    // Merge Sort Implementation (Recursion)
    private mergeSort(unsortedArray: Pair[]): Pair[] {
        // No need to sort the array if the array only has one element or empty
        if (unsortedArray.length <= 1) {
            return unsortedArray;
        }
        // In order to divide the array in half, we need to figure out the middle
        const middle = Math.floor(unsortedArray.length / 2);

        // This is where we will be dividing the array into left and right
        const left = unsortedArray.slice(0, middle);
        const right = unsortedArray.slice(middle);

        // Using recursion to combine the left and right
        return this.merge(
            this.mergeSort(left), this.mergeSort(right)
        );
    }

    private merge(left: Pair[], right: Pair[]) {
        let resultArray: Pair[] = [], leftIndex: number = 0, rightIndex: number = 0;

        // We will concatenate values into the resultArray in order
        while (leftIndex < left.length && rightIndex < right.length) {
            if (left[leftIndex].value > right[rightIndex].value) {
                resultArray.push(left[leftIndex]);
                leftIndex++; // move left array cursor
            } else {
                resultArray.push(right[rightIndex]);
                rightIndex++; // move right array cursor
            }
        }

        // We need to concat here because there will be one element remaining
        // from either left OR the right
        return resultArray
            .concat(left.slice(leftIndex))
            .concat(right.slice(rightIndex));
    }
}
