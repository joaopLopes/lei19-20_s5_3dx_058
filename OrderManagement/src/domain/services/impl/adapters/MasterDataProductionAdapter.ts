import axios from 'axios'
import https from 'https'
import {ProductForOrder} from "../../../models/product/ProductForOrder";
import {IMasterDataProductionAdapter} from "../../interfaces/adapters/IMasterDataProductionAdapter";
import logger from "../../../../utils/Logger";
import {IAuthService} from "../../interfaces/user/IAuthService";

export class MasterDataProductionAdapter implements IMasterDataProductionAdapter {
    private _authService: IAuthService;

    constructor(authService: IAuthService) {
        this._authService = authService;
    }

    private static createInstance() {
        return axios.create({
            // needed for self-signed certs
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });
    }

    async findProductByName(name: string): Promise<ProductForOrder> {
        const MDP_URL: string = process.env.MDP;
        const url = MDP_URL + 'products/' + name;
        const instance = MasterDataProductionAdapter.createInstance();

        return new Promise<ProductForOrder>(async (resolve, reject) => {
            let config: any = '';
            await this.getConfigToken().then(cfg => {
                config = cfg;
            }).catch(err => {
                reject(err);
            });
            await instance.get<ProductForOrder>(url, config)
                .then(function (response) {
                    if (response.status === 404) {
                        reject("ProductForOrder " + name + " does not exist.");
                        logger.error('Get product ' + name + ' returned 404.');
                        return;
                    }
                    const data = response.data;
                    resolve(new ProductForOrder(data.name, data.price));
                    logger.info('Get product ' + name + ' was successful.');
                })
                .catch(function (error) {
                    reject(error);
                    logger.error(error.toString());
                });
        });
    }

    async findAllProducts(): Promise<ProductForOrder[]> {
        const MDP_URL: string = process.env.MDP;
        const url = MDP_URL + 'products';
        const instance = MasterDataProductionAdapter.createInstance();
        return new Promise<ProductForOrder[]>(async (resolve, reject) => {
            let config: any = '';
            await this.getConfigToken().then(cfg => {
                config = cfg;
            }).catch(err => {
                logger.error(err);
                reject(err);
            });
            await instance.get<ProductForOrder[]>(url, config)
                .then(function (response) {
                    if (response.status === 404) {
                        reject("There are no products in the system.");
                        logger.error('Get products returned 404.');
                        return;
                    }
                    const data = response.data;
                    let list: ProductForOrder[] = [];
                    data.forEach(function (product) {
                        list.push(new ProductForOrder(product.name, product.price, 1, null, product.productionTime));
                    })
                    resolve(list);
                    logger.info('Get products was successful.');
                })
                .catch(function (error) {
                    reject(error);
                    logger.error(error.toString());
                });
        });
    }

    private async getConfigToken(): Promise<any> {
        return new Promise<any>(async (resolve, reject) => {
            let token = '';
            await this._authService.login('admin', 'admin#99').then(adminToken => {
                token = adminToken;
            }).catch(err => {
                logger.error(err);
                reject('No Admin in system.');
            });
            let config = {
                headers: {
                    token: token,
                }
            };
            resolve(config);
        });
    }
}
