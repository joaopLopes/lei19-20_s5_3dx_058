import {IUserRepository} from "../../../../persistence/interfaces/IUserRepository";
import {PasswordService} from "./PasswordService";
import {AdminService} from "./AdminService";
import logger from "../../../../utils/Logger";
import jsonwebtoken from "jsonwebtoken";
import {IAuthService} from "../../interfaces/user/IAuthService";
import {resolve} from "url";
import {getRolePermissions} from "../permissions/PermissionsService";
import {rejects} from "assert";


export class AuthService implements IAuthService {

    private repo_client: IUserRepository;
    private repo_admin: IUserRepository;
    private adminService: AdminService;

    constructor(repo: IUserRepository, repo_admin: IUserRepository, serv: AdminService) {
        this.repo_client = repo;
        this.repo_admin = repo_admin;
        this.adminService = serv;
    }

    private isAdmin(username: string) {
        return new Promise<boolean>((async (resolve) => {
            await this.adminService.getAdmin(username).then(async admin => {
                resolve(true);
            }).catch(error => {
                logger.error('AuthService.IsAdmin - ' + error.toString());
                resolve(false);
            });
        }));
    }

    private async validatePassForUser(name: string, pass: string): Promise<boolean> {
        return new Promise<boolean>((async (resolve, reject) => {
            await this.repo_client.findByUsername(name).then(async client => {
                await PasswordService.passwordMatch(pass, client.password.toString()).then(result2 => {
                    resolve(result2);
                });
            }).catch(async _ => {
                await this.repo_admin.findByUsername(name).then(async admin => {
                    await PasswordService.passwordMatch(pass, admin.password.toString()).then(result2 => {
                        resolve(result2);
                        return;
                    })
                }).catch(_ => {
                    reject('No user found.');
                    return;
                });
            });
        }));
    }

    public async login(username: string, password: string): Promise<string> {
        return new Promise<string>((async (resolve, reject) => {
            let result: boolean;
            await this.validatePassForUser(username, password).then(bool => {
                result = bool;
            }).catch(error => {
                logger.error(error);
                reject(error);
                return;
            });
            if (!result) {
                reject('User not found');
                return;
            } else {
                if (result) {
                    let payload;
                    await this.getPayload(username).then(result => {
                        payload = result;
                    });
                    let secret = process.env.JWT_TOKEN;
                    const domain = process.env.DOMAIN;
                    const options = {expiresIn: 86400, issuer: domain};
                    const token = jsonwebtoken.sign(payload, secret, options);
                    resolve(token);
                } else {
                    logger.error('Credentials do not match any user.');
                    reject('Credentials to not match.');
                    return;
                }
            }
        }));
    }

    private async getPayload(username: string): Promise<any> {
        return new Promise<any>((async (resolve, reject) => {
            try {
                let payload;
                if (await this.isAdmin(username)) {
                    const permissions = getRolePermissions('admin');
                    payload = {
                        user: username,
                        role: 'admin',
                        permissions: permissions
                    };
                } else {
                    const permissions = getRolePermissions('client');
                    payload = {
                        user: username,
                        role: 'client',
                        permissions: permissions
                    };
                }
                resolve(payload);
            } catch (e) {
                reject(e);
            }
        }));
    }

    export = AuthService;
}
