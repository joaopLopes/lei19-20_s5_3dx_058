import * as bcrypt from 'bcrypt';
/* istanbul ignore file */
export class PasswordService{
    public static REGEX = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

    public static async hashPassword(password: string, rounds: number) : Promise<string> {
        if(this.passwordValidation(password)) {
            return await bcrypt.hash(password, rounds);
        }else{
            throw new Error('Password must have one number, one letter, and at least 8 characters.');
        }
    }

    public static passwordValidation(pass: string){
        return pass.match(PasswordService.REGEX);
    }

    public static async passwordMatch(insertedPass: string, actualHash: string){
        return bcrypt.compare(insertedPass, actualHash);
    }

    export = PasswordService;
}
