import {IUserRepository} from "../../../../persistence/interfaces/IUserRepository";
import {UserMapper} from "../../../../mappers/UserMapper";
import {PasswordService} from "./PasswordService";
import {User} from "../../../models/user/User";
import {AdminDto} from "../../../../dtos/user/AdminDto";
import {AdminRole} from "../../../models/user/admin/AdminRole";
import {IAdminService} from "../../interfaces/user/IAdminService";
import logger from "../../../../utils/Logger";


export class AdminService implements IAdminService {

    private repo_client: IUserRepository;
    private mapper: UserMapper;

    constructor(repo: IUserRepository) {
        this.repo_client = repo;
        this.mapper = new UserMapper();
    }

    public getAdmin(username: string) {
        return new Promise<AdminDto>((resolve, reject) => {
            this.repo_client.findByUsername(username).then(client => {
                const dto: AdminDto = this.mapper.DomainToAdminDTO(client);
                resolve(dto);
            }).catch(error => {
                logger.error('AdminService.getAdmin - ' + error.toString());
                reject(error);
            });
        });
    }

    getAllAdmins(): Promise<AdminDto[]> {
        return new Promise<AdminDto[]>(((resolve, reject) => {
            const dtos: AdminDto[] = [];
            this.repo_client.findAll().then(clients => {
                clients.forEach((client) => {
                    const dto = this.mapper.DomainToAdminDTO(client);
                    dtos.push(dto);
                });
                resolve(dtos);
            }).catch(error => {
                reject(error);
            })
        }));
    }

    createAdmin(dto: AdminDto) {
        let pw: string;
        return new Promise<AdminDto>((async (resolve, reject) => {
            await PasswordService.hashPassword(dto.password, 10).then(hash => {
                pw = hash;
            }).catch(error => {
                reject(error);
                return;
            });
            try {
                const admin: User = new User(dto.username, pw, new AdminRole());
                this.repo_client.create(admin).then(newAdmin => {
                    const adminDto = this.mapper.DomainToAdminDTO(newAdmin);
                    resolve(adminDto);
                }).catch(error => {
                    reject(error);
                });
            } catch (e) {
                reject(e);
                return;
            }
        }));
    }


}
