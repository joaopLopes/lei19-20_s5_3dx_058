import {ClientDto} from "../../../../dtos/user/ClientDto";
import {UserMapper} from "../../../../mappers/UserMapper";
import {IUserRepository} from "../../../../persistence/interfaces/IUserRepository";
import {User} from "../../../models/user/User";
import {ClientRole} from "../../../models/user/client/ClientRole";
import {ClientUpdateDTO} from "../../../../dtos/user/ClientUpdateDTO";
import logger from "../../../../utils/Logger";
import {PasswordService} from "./PasswordService";
import {IUserService} from "../../interfaces/user/IUserService";
import {ClientLimitedInformationDTO} from "../../../../dtos/user/ClientLimitedInformationDTO";

export class UserService implements IUserService {

    private repo: IUserRepository;
    private mapper: UserMapper;

    constructor(repo: IUserRepository) {
        this.repo = repo;
        this.mapper = new UserMapper();
    }

    public getClient(username: string) {
        return new Promise<ClientDto>((resolve, reject) => {
            this.repo.findByUsername(username).then(client => {
                const dto: ClientDto = this.mapper.DomainToClientDTO(client);
                resolve(dto);
            }).catch(error => {
                reject(error);
            });
        });
    }

    getAllClients(): Promise<ClientDto[]> {
        return new Promise<ClientDto[]>(((resolve, reject) => {
            const dtos: ClientDto[] = [];
            this.repo.findAll().then(clients => {
                clients.forEach((client) => {
                    const dto = this.mapper.DomainToClientDTO(client);
                    dtos.push(dto);
                });
                resolve(dtos);
            }).catch(error => {
                reject(error);
            })
        }));
    }

    createClient(dto: ClientDto) {
        let pw: string;
        return new Promise<ClientDto>((async (resolve, reject) => {
            await PasswordService.hashPassword(dto.password, 10).then(hash => {
                pw = hash;
            });
            let client: User;
            try {
                client = new User(dto.username, pw, new ClientRole(dto.email, dto.nif, dto.phoneNumber, dto.age, dto.address, dto.firstName, dto.lastName));
            } catch (error) {
                reject(error.toString());
            }
            this.repo.create(client).then(newClient => {
                const clientDTO = this.mapper.DomainToClientDTO(newClient);
                resolve(clientDTO);
            }).catch(error => {
                reject(error);
            });
        }));
    }

    /**
     * Method that allows the user to update the username or address or both.
     * @param username The identifier of the client.
     * @param updateData The updated of the user.
     */
    async updateClient(username: string, updateData: ClientUpdateDTO): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this.repo.findByUsername(username).then(client => {
                this.updateClientData(client, updateData);
                this.repo.update(client, username).then((message) => {
                    logger.info(message);
                    resolve(message);
                }).catch(error => {
                    reject(error);
                    logger.error(error.toString());
                    return;
                });
            }).catch(error => {
                logger.error(error.toString());
                resolve(error);
            })
        })
    }

    async updateClientDeletion(username: string): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this.repo.findByUsername(username).then(client => {
                client.delete();
                this.repo.forgetUser(client, username).then((message) => {
                    logger.info(message);
                    resolve(message);
                }).catch(error => {
                    reject(error);
                    logger.error(error.toString());
                    return;
                });
            }).catch(error => {
                logger.error(error.toString());
                resolve(error);
            })
        })
    }


    private updateClientData(client: User, updateData: ClientUpdateDTO) {
        if ((!updateData.firstName && !updateData.address && !updateData.lastName) || ((updateData.firstName && !updateData.lastName) && !updateData.address) || ((!updateData.firstName && updateData.lastName) && !updateData.address)) {
            throw Error("No data to update.");
        } else if ((!updateData.firstName || !updateData.lastName) && updateData.address) {
            client.updateAddress(updateData.address);
        } else if ((!updateData.address) && (updateData.firstName && updateData.lastName)) {
            client.updateName(updateData.firstName, updateData.lastName);
        } else {
            client.updateAddress(updateData.address);
            client.updateName(updateData.firstName, updateData.lastName);
        }
    }

    getClientName(username: string): Promise<ClientLimitedInformationDTO> {
        return new Promise<ClientLimitedInformationDTO>((resolve, reject) => {
            this.repo.findByUsername(username).then(client => {
                const dto: ClientLimitedInformationDTO = this.mapper.DomainToClientDTONameOnly(client);
                resolve(dto);
            }).catch(error => {
                reject(error);
            });
        });
    }

    getAllClientsName(): Promise<ClientLimitedInformationDTO[]> {
        return new Promise<ClientLimitedInformationDTO[]>(((resolve, reject) => {
            const dtos: ClientLimitedInformationDTO[] = [];
            this.repo.findAll().then(clients => {
                clients.forEach((client) => {
                    const dto = this.mapper.DomainToClientDTONameOnly(client);
                    dtos.push(dto);
                });
                resolve(dtos);
            }).catch(error => {
                reject(error);
            })
        }));
    }
}
