import Order from "../../../models/order/Order";
import {ProductForOrder} from "../../../models/product/ProductForOrder";
import {IOrderFactory} from "../../interfaces/order/IOrderFactory";
import {User} from "../../../models/user/User";

class OrderFactory implements IOrderFactory {
    newOrder(products: ProductForOrder[], client: User, price: number, creditCard: string, date: Date, shippingAddress: string): Order {
        return new Order(products, client.username.toString(), price, creditCard, date, shippingAddress);
    }
}

export = OrderFactory;
