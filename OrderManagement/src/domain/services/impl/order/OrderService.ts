import {IOrderRepository} from "../../../../persistence/interfaces/IOrderRepository";
import {OrderMapper} from "../../../../mappers/OrderMapper";
import Order from "../../../models/order/Order";
import {OrderDto} from "../../../../dtos/order/OrderDto";
import {ProductForOrder} from "../../../models/product/ProductForOrder";
import {IMasterDataProductionAdapter} from "../../interfaces/adapters/IMasterDataProductionAdapter";
import {IOrderService} from "../../interfaces/order/IOrderService";
import {IOrderFactory} from "../../interfaces/order/IOrderFactory";
import {OrderUpdateDto} from "../../../../dtos/order/OrderUpdateDto";
import logger from "../../../../utils/Logger";
import {User} from "../../../models/user/User";
import {IUserRepository} from "../../../../persistence/interfaces/IUserRepository";

export class OrderService implements IOrderService {
    private orderRepo: IOrderRepository;
    private mapper: OrderMapper;
    private clientRepo: IUserRepository;
    private mdpAdapter: IMasterDataProductionAdapter;
    private orderFactory: IOrderFactory;

    constructor(fact: IOrderFactory, repo: IOrderRepository, clientRepo: IUserRepository, mdpAdapter: IMasterDataProductionAdapter) {
        this.orderRepo = repo;
        this.mdpAdapter = mdpAdapter;
        this.clientRepo = clientRepo;
        this.mapper = new OrderMapper();
        this.orderFactory = fact;
    }

    getOrder(reference: string): Promise<OrderDto> {
        return new Promise<OrderDto>((resolve, reject) => {
            this.orderRepo.findById(reference).then(order => {
                const dto: OrderDto = this.mapper.DomainToOrderDTO(order);
                resolve(dto);
            }).catch(error => {
                reject(error);
                logger.error(error.toString());
            });
        });
    }

    getAllOrders(): Promise<OrderDto[]> {
        return new Promise<OrderDto[]>((async (resolve, reject) => {
            const dtos: OrderDto[] = [];
            await this.orderRepo.findAll().then(orders => {
                orders.forEach((order) => {
                    const dto = this.mapper.DomainToOrderDTO(order);
                    dtos.push(dto);
                });
                resolve(dtos);
            }).catch(error => {
                reject(error);
                logger.error(error.toString());
            })
        }));
    }

    createOrder(dto: OrderDto): Promise<OrderDto> {
        return new Promise<OrderDto>((async (resolve, reject) => {
            if (!OrderService.validateInput(dto)) {
                logger.error('Input dto from json is missing information: ' + dto);
                reject(Error('Missing information.'));
            }
            try {
                let client: User = null;
                await this.findClient(dto.client).then(found => {
                    client = found;
                }).catch(error => {
                    reject(error);
                    logger.error(error.toString());
                    return;
                });
                const products: ProductForOrder[] = [];
                let totalPrice: number = 0;
                for (const prodDto of dto.products) {
                    await this.mdpAdapter.findProductByName(prodDto.name).then(prod => {
                        prod.deliveryDate = new Date(prodDto.deliveryDate);
                        prod.quantity = prodDto.quantity;
                        products.push(prod);
                        totalPrice += prod.totalPrice();
                    }).catch(error => {
                        reject(error);
                        logger.error(error.toString());
                        return;
                    });
                }
                const order: Order = this.orderFactory.newOrder(products, client, totalPrice, dto.creditCardUsed, dto.placementDate, dto.shippingAddress);
                await this.orderRepo.create(order).then(newOrder => {
                    const consultDto = this.mapper.DomainToOrderDTO(newOrder);
                    resolve(consultDto);
                }).catch(error => {
                    reject(error);
                    logger.error(error.toString());
                    return;
                });
            } catch (e) {
                logger.error(e);
                reject(e);
            }
        }));
    }

    private static validateInput(dto: OrderDto): boolean {
        if (!dto.client) {
            logger.error('Client is missing');
            return false;
        }
        if (!dto.placementDate) {
            logger.error('Placement Date is missing');
            return false;
        }
        if (!dto.products) {
            logger.error('Products are missing');
            return false;
        }
        if (!dto.creditCardUsed) {
            logger.error('Credit Card is missing');
            return false;
        }
        if (!dto.shippingAddress) {
            logger.error('Shipping Address is missing');
            return false;
        }
        return true;
    }

    private async findClient(username: string): Promise<User> {
        return new Promise<User>(async (resolve, reject) => {
            if (!username) {
                reject(Error('Username does not correspond to an existing account.'));
                logger.error('No client found with username: ' + username);
                return;
            }
            await this.clientRepo.findByUsername(username).then(found => {
                resolve(found);
            }).catch(error => {
                reject(error);
                logger.error(error.toString());
            });
        });
    }

    cancelOrder(reference: string): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this.orderRepo.findById(reference).then(order => {
                order.cancel(); // cancels order.
                this.orderRepo.update(order).then(message => {
                    resolve(message);
                }).catch(error => {
                    reject(error);
                    logger.log(error.toString());
                    return;
                });
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            });
        });
    }

    async updateOrder(reference: string, updateData: OrderUpdateDto): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            await this.orderRepo.findById(reference).then(order => {
                OrderService.updateOrderData(order, updateData); // updates order.
                this.orderRepo.update(order).then((message) => {
                    logger.info(message);
                    resolve(message);
                }).catch(error => {
                    reject(error);
                    logger.error(error.toString());
                    return;
                });
            }).catch(error => {
                logger.error(error.toString());
                reject(error);
            });
        });
    }

    private static updateOrderData(order: Order, updateData: OrderUpdateDto) {
        if (!updateData.shippingAddress) {
            throw Error('No data to update.');
        } else {
            order.shippingAddress = updateData.shippingAddress;
        }
    }

    getOrdersByClient(username: string): Promise<OrderDto[]> {
        return new Promise<OrderDto[]>((async (resolve, reject) => {
            const dtos: OrderDto[] = [];
            await this.orderRepo.findByClient(username).then(orders => {
                orders.forEach((order) => {
                    const dto = this.mapper.DomainToOrderDTO(order);
                    dtos.push(dto);
                });
                resolve(dtos);
            }).catch(error => {
                reject(error);
                logger.error(error.toString());
            })
        }));
    }

    /**
     * Retrieves all ongoing orders with atleast a product to be delivered between the date interval.
     * @param startDate
     * @param endDate
     */
    getOrdersForScheduling(startDate: Date, endDate: Date): Promise<OrderDto[]> {
        return new Promise<OrderDto[]>((async (resolve, reject) => {
            const dtos: OrderDto[] = [];
            await this.orderRepo.findOngoing(startDate, endDate).then(orders => {
                orders.forEach((order) => {
                    let flag: boolean = false;
                    order.products.forEach(prod => {
                        if (prod.deliveryDate >= startDate && prod.deliveryDate <= endDate) {
                            flag = true;
                        }
                    });
                    if (flag) {
                        const dto = this.mapper.DomainToOrderDTO(order);
                        dtos.push(dto);
                    }
                });
                resolve(dtos);
            }).catch(error => {
                reject(error);
                logger.error(error.toString());
            })
        }));
    }
}
