import jsonwebtoken from 'jsonwebtoken';

export function getRoleFromToken(token: any): string {
    const actualToken = jsonwebtoken.decode(token);
    const stringActualToken = JSON.stringify(actualToken);
    const jsonActualToken = JSON.parse(stringActualToken);
    let role: string = jsonActualToken.role;
    role = role.toLowerCase();
    return role;
}

export function getUserFromToken(token: string) {
    const actualToken = jsonwebtoken.decode(token);
    const stringActualToken = JSON.stringify(actualToken);
    const jsonActualToken = JSON.parse(stringActualToken);
    let user: string = jsonActualToken.user;
    return user;
}
