import * as chokidar from "chokidar";
import appRoot from "app-root-path";
import * as fs from "fs";
import * as yaml from "js-yaml";

function loadPermissions(): any {
    try {
        chokidar.watch(appRoot + '/dist/resources/permissions.yml');
        let permissionsFile = fs.readFileSync(appRoot + '/dist/resources/permissions.yml', 'utf8');
        return yaml.safeLoad(permissionsFile);
    } catch (e) {
        throw Error('Error loading permissions file.');
    }
}

export function getRolePermissions(role: string): any {
    const permissions = loadPermissions();
    const authorizations = permissions['profiles'][role];
    if (!authorizations) throw Error('Role ' + role + ' not found in permissions file');
    return authorizations;
}
