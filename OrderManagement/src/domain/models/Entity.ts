export abstract class Entity<T> {
    protected id: any;

    protected constructor(_id?: any) {
        if (_id == null) {
            this.id = '';
        } else {
            this.id = _id;
        }
    }

    public getId(){
        return this.id
    }

    public toString() {
        return this.id;
    }
}
