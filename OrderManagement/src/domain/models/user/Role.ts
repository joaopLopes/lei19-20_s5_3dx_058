import {Address} from "./client/Address";
import {Nif} from "./client/Nif";
import {Email} from "./client/Email";
import {Age} from "./client/Age";
import {PhoneNumber} from "./client/PhoneNumber";
import {CreditCard} from "./client/CreditCard";
import {Name} from "./client/Name";

export interface Role {
    getRole(): string;
    getAddress(): Address;
    getNif(): Nif;
    getEmail(): Email;
    getFirstName(): Name;
    getLastName() : Name;
    getAge(): Age;
    getPhoneNumber(): PhoneNumber;
    updateAddress(newAddress: string): void;
    updateName(newFirstName: string, newLastName: string): void;
    getLastUsedCard(): CreditCard;
    getPriority(): string;

    toString(): string;

}
