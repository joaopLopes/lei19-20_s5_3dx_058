export class Username {
    private usernameRegex = /^[a-zA-Z0-9]+$/;
    private username : string;

    constructor(name: string) {
        if(this.usernameValidator(name)){
            this.username = name;
        }else{
            throw new TypeError("Username contains illegal characters. Only letters and numbers are allowed.");
        }
    }

    public toString(){
        return this.username;
    }

    usernameValidator(name: string){
        return name.match(this.usernameRegex);
    }

    export = Username;
}
