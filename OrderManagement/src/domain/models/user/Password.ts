export class Password{
    public _password: string;

    constructor(pass: string){
        this._password = pass;
    }

    public toString(){
        return this._password;
    }

    export = Password;

}
