import {Entity} from "../Entity";
import {Password} from "./Password";
import {Username} from "./Username";
import {Role} from "./Role";
import {State} from "./State";


export class User extends Entity<User> {
    private _username: Username;
    private _password: Password;
    private _role: Role;
    private _state: State;

    get state(): State {
        return this._state;
    }

    get username(): Username {
        return this._username;
    }

    get password(): Password {
        return this._password;
    }

    get role(): Role {
        return this._role;
    }

    /**
     * Updates the username of the user.
     * @param newLastName The new Last Name
     * @param newFirstName The new First name
     */
    updateName(newFirstName: string, newLastName: string): void {
        this.role.updateName(newFirstName, newLastName);
    }

    /**
     * Updates the address of the user.
     * @param newAddress the new address of the user.
     */
    updateAddress(newAddress: string): void {
        this.role.updateAddress(newAddress);
    }

    constructor(username: string, password: string, role: Role, _id?: any, _state?: number) {
        super(_id);
        this._username = new Username(username);
        this._password = new Password(password);
        this._role = role;
        if (!_state || _state === 0) {
            this._state = State.active;
        } else {
            this._state = State.deleted;
        }
    }

    public toString(): any {
        return super.toString() + ' ' + this._username.toString() + ' ' + this._password.toString() + ' ' + this._role.toString();
    }

    public delete() {
        this._state = State.deleted;
    }
}
