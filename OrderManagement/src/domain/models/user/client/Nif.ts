export class Nif {

    private nifnumber: string

    constructor(num: string) {
        if (this.validateNIF(num) || num === '999999999') {
            this.nifnumber = num;
        } else {
            throw new TypeError("NIF is invalid");
        }
    }

    validateNIF(nif: string) {
        const validationSets = {
            one: ['1', '2', '3', '5', '6', '8'],
            two: ['45', '70', '71', '72', '74', '75', '77', '79', '90', '91', '98', '99']
        };

        if (nif.length !== 9) {
            return false;
        }

        if (!validationSets.one.includes(nif.substr(0, 1)) && !validationSets.two.includes(nif.substr(0, 2))) {
            return false;
        }

        const total = Number(nif.charAt(0)) * 9 + Number(nif.charAt(1)) * 8 + Number(nif.charAt(2)) * 7 + Number(nif.charAt(3)) * 6 + Number(nif.charAt(4)) * 5 + Number(nif.charAt(5)) * 4 + Number(nif.charAt(6)) * 3 + Number(nif.charAt(7)) * 2;
        const modulo11 = (Number(total) % 11);
        const checkDigit = modulo11 < 2 ? 0 : 11 - modulo11;
        return checkDigit === Number(nif[8]);
    }

    public toString() {
        return this.nifnumber;
    }

    export = Nif;
}
