export class Address {
    private address: string;

    constructor(address: string) {
        if (this.addressValidator(address)) {
            this.address = address;
        }
    }

    addressValidator(address: string) {
        if (!address) {
            throw Error('Address must exist');
        }
        return true;
    }

    public toString() {
        return this.address;
    }

}
