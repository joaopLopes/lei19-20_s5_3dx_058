export class PhoneNumber {
    private phoneNum: string;

    constructor(num: string) {
        if (this.phoneNumberValidator(num)) {
            this.phoneNum = num;
        } else {
            throw new TypeError("Inserted phone number is invalid.");
        }
    }

    public toString() {
        return this.phoneNum;
    }

    phoneNumberValidator(num: string) {
        return Number(num) && num.length == 9;
    }

    export = PhoneNumber;
}
