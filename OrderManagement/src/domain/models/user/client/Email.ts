export class Email {
    private mail: string;

    constructor(mail: string){
        if(this.emailValidation(mail)){
            this.mail = mail;
        }else{
            throw new TypeError("Email is invalid.");
        }
    }

    emailValidation(mail: string){
          const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return mail.match(regexp);
    }

    public toString(){
        return this.mail;
    }

    export = Email;
}
