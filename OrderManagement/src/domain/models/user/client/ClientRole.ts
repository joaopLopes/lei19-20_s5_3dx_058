import {Email} from "./Email";
import {Role} from "../Role";
import {Nif} from "./Nif";
import {PhoneNumber} from "./PhoneNumber";
import {Address} from "./Address";
import {CreditCard} from "./CreditCard";
import {Age} from "./Age";
import {Name} from "./Name";


export class ClientRole implements Role{
    private _email: Email;
    private _nif: Nif;
    private _phoneNumber: PhoneNumber;
    private _address: Address;
    private _firstName: Name;
    private _lastName: Name;
    private _age: Age;
    private _lastUsedCard: CreditCard;
    private _priority: string;

    getEmail(): Email {
        return this._email;
    }

    getNif(): Nif {
        return this._nif;
    }

    getPhoneNumber(): PhoneNumber {
        return this._phoneNumber;
    }

    getAddress(): Address {
        return this._address;
    }

    getAge(): Age {
        return this._age;
    }

    getLastUsedCard() : CreditCard {
        return this._lastUsedCard;
    }

    constructor(email: string, nif: string, phonenum: string, age: string, address: string, firstName:string,lastName:string, _id?: any) {
        this._address = new Address(address);
        this._email = new Email(email);
        this._nif = new Nif(nif);
        this._age = new Age(age);
        this._phoneNumber = new PhoneNumber(phonenum);
        this._firstName = new Name(firstName);
        this._lastName = new Name(lastName);
        this._lastUsedCard = new CreditCard('PT509999999999999999');
        this._priority = String(Math.floor(Math.random() * 200) + 1);
    }

    public toString(): any {
        return this._email.toString() + this._nif.toString() + this._firstName.getName()+ this._lastName.getName() +this._phoneNumber.toString() + this._address.toString() + this._age.toString();
    }

    getRole(): string {
        return "Client";
    }

    export = ClientRole;

    /**
     * Updates the address of the user.
     * @param newAddress
     */
    updateAddress(newAddress: string): void {
        this._address = new Address(newAddress);
    }

    getFirstName(): Name {
        return this._firstName;
    }

    getLastName(): Name {
        return this._lastName;
    }

    updateName(newFirstName: string, newLastName: string): void {
        this._firstName = new Name(newFirstName);
        this._lastName = new Name(newLastName);
    }

    getPriority(): string {
        return this._priority;;
    }
}
