export class Age {
    private ageNum: string;

    constructor(ageNum: string){
        if(this.ageValidator(ageNum)) {
            this.ageNum = ageNum;
        }else{
            throw new Error('Age is invalid.');
        }
    }

    public toString(){
        return this.ageNum;
    }

    ageValidator(ageNum: string){
        return Number.parseInt(ageNum) > 17;
    }

    export = Age;
}
