export class Name{
    // Name does not have a validating regex due to the nature of naming.
    private name: string;

    constructor(name: string){
            this.name = name;

    }


    public toString(){
        return this.name;
    }

    public getName(){
        return this.name;
    }


}
