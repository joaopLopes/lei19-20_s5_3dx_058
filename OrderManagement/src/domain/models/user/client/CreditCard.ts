/**
 * In real world we would use an external api to handle this sensitive information and transactions
 * But for simplicity we will simply create a mocking of such service.
 * Credit card only works for portuguese IBAN accounts.
 */
export class CreditCard {
    /**
     * IBAN relative to the credit card.
     */
    private iban: string;

    constructor(iban: string) {
        CreditCard.validateIban(iban);
        this.iban = iban;
    }


    /**
     * Validates if the credit card matches a portuguese IBAN.
     * @param iban
     */
    private static validateIban(iban: string) {
        if (!iban) {
            throw Error('Credit Card must exist');
        }
        if (iban.length != 20) {
            throw Error('IBAN is invalid.');
        }
        if (!iban.startsWith('PT50')) {
            throw Error('IBAN only works with portuguese accounts.');
        }
    }

    toString() {
        return this.iban;
    }

}
