import {Role} from "../Role";
import {Address} from "../client/Address";
import {Age} from "../client/Age";
import {Email} from "../client/Email";
import {CreditCard} from "../client/CreditCard";
import {Nif} from "../client/Nif";
import {PhoneNumber} from "../client/PhoneNumber";
import {Name} from "../client/Name";

export class AdminRole implements Role {

    constructor() {
    }

    getRole(): string {
        return "Admin";
    }

    export = AdminRole;

    getAddress(): Address {
        throw new Error('Admin has no address.');
    }

    getAge(): Age {
        throw new Error('Admin has no age.');
    }

    getEmail(): Email {
        throw new Error('Admin has no email.');
    }

    getLastUsedCard(): CreditCard {
        throw new Error('Admin has no credit card.');
    }

    getNif(): Nif {
        throw new Error('Admin has no NIF.');
    }

    getPhoneNumber(): PhoneNumber {
        throw new Error('Admin has no phone number.');
    }

    updateAddress(newAdress: string): void {
        throw new Error("Admin has no address");
    }

    getFirstName(): Name {
        throw new Error("Admin has no First Name.");
    }

    getLastName(): Name {
        throw new Error("Admin has no Last Name");
    }

    updateName(newFirstName: string, newLastName: string): void {
        throw new Error("Admin can not update Names.");
    }

    getPriority(): string {
        throw new Error("Admin has no priority");
    }





}
