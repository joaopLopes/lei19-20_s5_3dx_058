export class Quantity {
    private readonly quantity: number;

    constructor(quantity: number) {
        Quantity.validateQuantity(quantity);
        this.quantity = quantity;
    }

    private static validateQuantity(quantity: number): void {
        if (quantity < 1) {
            throw Error("Quantity must be atleast 1.");
        }
    }

    getQuantity(): number {
        return this.quantity;
    }

    toString(): string {
        return this.quantity.toString();
    }
}
