import {Quantity} from "./Quantity";

export class ProductForOrder {
    /**
     * Name and id of the product
     */
    private readonly _name: string;
    /**
     * Price of a single product.
     */
    private readonly _price: number;
    /**
     * Quantity of the product.
     */
    private _quantity: Quantity;
    /**
     * Conclusion Time for the product to be done.
     */
    private _deliveryDate: Date;
    /**
     * Production time of the product
     */
    private _productionTime: number;

    constructor(product: string, price: number, quantity?: number, deliveryDate?: Date,productionTime?: number) {
        this._name = product;
        this._price = price;
        this._quantity = new Quantity(quantity);
        this._deliveryDate = deliveryDate;
        this._productionTime = productionTime;
    }


    get name(): string {
        return this._name;
    }

    get price(): number {
        return this._price;
    }

    get quantity(): number {
        return this._quantity.getQuantity();
    }

    set quantity(value: number) {
        this._quantity = new Quantity(value);
    }

    get deliveryDate(): Date {
        return this._deliveryDate;
    }

    set deliveryDate(value: Date) {
        this._deliveryDate = value;
    }

    get productionTime(): number {
        return this._productionTime;
    }

    set productionTime(newTime: number) {
        this._productionTime = newTime;
    }

    public toString() {
        return this._name + this._quantity + this.deliveryDate + this.price;
    }

    totalPrice() {
        return this.quantity * this.price;
    }
}
