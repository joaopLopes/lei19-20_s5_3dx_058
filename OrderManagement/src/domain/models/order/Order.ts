import {Entity} from "../Entity";
import Reference from "./Reference";
import {Price} from "./Price";
import {CreditCard} from "../user/client/CreditCard";
import {OrderState} from "./OrderState";
import {Address} from "../user/client/Address";
import {ProductForOrder} from "../product/ProductForOrder";

class Order extends Entity<Order> {
    private readonly _reference: Reference;
    private _products: ProductForOrder[];
    private _shippingAddress: Address;
    private _client: string;
    private readonly _totalPrice: Price;
    private readonly _creditCardUsed: CreditCard;
    private _placementDate: Date;
    private _state: OrderState;

    constructor(products: ProductForOrder[], client: string, price: number, creditCard: string,
                date: Date, shippingAddress: string, state?: string, reference?: string, _id?: any) {
        super(_id);
        if (reference == null || reference.length == 0) {
            this._reference = new Reference();
        } else {
            this._reference = new Reference(reference);
        }
        this.setProducts(products);
        this.setState(state);
        this.setClient(client);
        this._totalPrice = new Price(price);
        this._creditCardUsed = new CreditCard(creditCard);
        this._shippingAddress = new Address(shippingAddress);
        this.setDate(date);
    }

    get state(): OrderState {
        return this._state;
    }

    get reference(): string {
        return this._reference.toString();
    }

    get price(): number {
        return this._totalPrice.price;
    }

    get products(): ProductForOrder[] {
        return this._products;
    }

    get shippingAddress(): string {
        return this._shippingAddress.toString();
    }

    set shippingAddress(value: string) {
        if (this._state === OrderState.Ongoing) {
            this._shippingAddress = new Address(value);
        } else {
            throw Error('Operation is not ongoing, can not be updated.');
        }
    }

    get client(): string {
        return this._client;
    }

    get placementDate(): Date {
        return this._placementDate;
    }

    get creditCardUsed(): string {
        return this._creditCardUsed.toString();
    }

    cancel(): void {
        if (this._state === OrderState.Ongoing) {
            this._state = OrderState.Cancelled;
        } else {
            throw Error("Order is not ongoing. Unable to cancel.");
        }
    }

    public toString(): string {
        return "Reference: " + this._reference.toString() + "\n"
            + "Client: " + this._client + "\n"
            + "Date: " + this._placementDate.toDateString() + "\n"
            + "Payment Method: " + this._creditCardUsed + "\n"
            + "Products: " + this._products + "\n"
            + "Price: " + this._totalPrice.toString() + '\n'
            + "State: " + this._state;
    }

    private setState(state: string) {
        if (state) {
            if (state.toUpperCase() === 'CANCELLED') {
                this._state = OrderState.Cancelled;
            } else {
                if (state.toUpperCase() === 'FINISHED') {
                    this._state = OrderState.Finished;
                } else {
                    if (state.toUpperCase() === 'ONGOING') {
                        this._state = OrderState.Ongoing;
                    } else {
                        throw Error('The order\'s state is invalid.');
                    }
                }
            }
        } else {
            // new order should have current state.
            this._state = OrderState.Ongoing;
        }
    }

    private setProducts(products: ProductForOrder[]) {
        if (!products || products.length == 0) {
            throw Error('Order must have atleast a product.');
        } else {
            this._products = products;
        }

    }

    private setClient(client: string) {
        if (!client || client.length == 0) {
            throw Error('There must be a client');
        } else {
            this._client = client;
        }
    }

    private setDate(date: Date) {
        if (!date) {
            throw Error('Date must exist');
        }
        this._placementDate = date;
    }

}

export = Order
