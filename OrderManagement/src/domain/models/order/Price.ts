import Dinero from "dinero.js";

export class Price {
    /**
     * Price in cents in euro currency.
     */
    private _price: Dinero.Dinero;

    constructor(price: number) {
        Price.validatePrice(price);
        this._price = Dinero({amount: price, currency: 'EUR'});
    }

    /**
     * Validates the price.
     * @param price
     */
    private static validatePrice(price: number) {
        if (!price) {
            throw Error('Price is not valid.');
        }
        if (price % 1 != 0) {
            throw Error('Price must be in cents.');
        }
        if (price < 0) {
            throw Error("Price must not be lower than 0");
        }
    }

    get price(): number {
        return this._price.getAmount();
    }

    /**
     * Returns the price in cents, in a string.
     */
    toString(): string {
        return this._price.getAmount().toString();
    }
}
