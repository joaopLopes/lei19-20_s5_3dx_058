export enum OrderState {
    Ongoing = 'ONGOING',
    Cancelled = 'CANCELLED',
    Finished = 'FINISHED'
}
