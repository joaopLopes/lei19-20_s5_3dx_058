import uuid from 'uuid'

class Reference {
    private readonly _reference: string;

    constructor(reference?: string) {
        if (reference == null) {
            this._reference = uuid.v4();
        } else {
            this._reference = reference;
        }
    }

    toString() {
        return this._reference;
    }
}

export = Reference;
