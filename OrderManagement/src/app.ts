import express from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import routes from "./web/controllers";
import db from './persistence/impl/mongo-db/MongooseConfig'
import * as dotenv from "dotenv";
import logger from "./utils/Logger";
import auth from "./web/controllers/AuthController";
import {IUserBootstrapper} from "./persistence/interfaces/IUserBootstrapper";
import {UserBootstrapper} from "./persistence/impl/bootstrap/UserBootstrapper";
import {PersistenceContext} from "./persistence/PersistenceContext";
import {isTokenValid} from "./web/middlewares/AuthorizationService";

class App {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
    }

    private async config() {
        let path;
        logger.info('Environment: ' + process.env.NODE_ENV);
        switch (process.env.NODE_ENV) {
            case "development":
                path = '.env.development';
                break;
            case "alex":
                path = '.env.alex';
                break;
            default:
                path = '.env';
        }
        dotenv.config({path: path});
        // support web/json type post data
        this.app.use(bodyParser.json());
        //support web/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({extended: false}));
        //Enables cors
        const whitelist = ['https://my-own-cutlery.azurewebsites.net',
            'https://masterdatafactory3dxg058.firebaseapp.com',
            'http://localhost:4200',
            '192.168.1.5',
            'http://localhost:4201',
            '193.136.60.225'];
        const corsOptions = {
            origin: function (origin: string, callback: any) {
                if (!origin || whitelist.indexOf(origin) !== -1) {
                    callback(null, true)
                } else {
                    callback(new Error('Not allowed by CORS'))
                }
            },
            credentials: true
        };
        this.app.use(cors(corsOptions));
        this.app.use('/', auth);
        this.app.use('/', isTokenValid, routes);
        await db();
        logger.info('Application config successful.');
        const bootstrapper: IUserBootstrapper = new UserBootstrapper(PersistenceContext.repositoryFactory().client(),PersistenceContext.repositoryFactory().admin());
        await bootstrapper.bootstrapAdmin().then(message => {
            logger.info(message);
        }).catch(err => {
            logger.error(err);
        });
        await bootstrapper.bootstrapClient().then(message => {
            logger.info(message);
        }).catch(err => {
            logger.error(err);
        });
    }
}

export default new App().app;
