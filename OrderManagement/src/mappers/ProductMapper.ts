import {ProductDto} from "../dtos/product/ProductDto";
import {ProductForOrder} from "../domain/models/product/ProductForOrder";

export class ProductMapper {

    DomainToDtoOrder(prod: ProductForOrder): ProductDto {
        return new ProductDto(prod.name, prod.price, prod.quantity, prod.deliveryDate.toISOString());
    }

    DomainToDtoStore(prod: ProductForOrder): ProductDto {
        return new ProductDto(prod.name, prod.price);
    }
}
