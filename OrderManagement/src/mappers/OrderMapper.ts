import Order from "../domain/models/order/Order";
import {OrderDto} from "../dtos/order/OrderDto";
import {ProductMapper} from "./ProductMapper";
import {ProductDto} from "../dtos/product/ProductDto";
import {ProductForOrder} from "../domain/models/product/ProductForOrder";
import {OrderUpdateDto} from "../dtos/order/OrderUpdateDto";
import moment from "moment";

export class OrderMapper {
    /**
     * Maps a JSON object to a NewOrderDTO, with all the data it contains.
     * @param body
     * @constructor
     */
    JSONToOrderDTO(body: any): OrderDto {
        const products = body.products;
        const client = body.client;
        const creditCard = body.creditCardUsed;
        const now = moment();
        now.seconds(0).milliseconds(0);
        return new OrderDto(products, client, creditCard, now.toDate(), body.shippingAddress);
    }

    /**
     * Maps an Order from domain to a ConsultOrderDTO
     * @param order
     * @constructor
     */
    DomainToOrderDTO(order: Order): OrderDto {
        const products = order.products;
        const client = order.client;
        const price = order.price;
        const creditCard = order.creditCardUsed;
        const date = order.placementDate;
        const state = order.state;
        const address = order.shippingAddress;
        const id = order.reference;
        const prodDtos: ProductDto[] = [];
        const prodMapper = new ProductMapper();
        products.forEach((prod: ProductForOrder) => {
            prodDtos.push(prodMapper.DomainToDtoOrder(prod));
        });
        return new OrderDto(prodDtos, client, creditCard, date, address, price, state, id);
    }

    /**
     * Maps a Mongoose Database Model to a Domain Model.
     * @param schema
     * @constructor
     */
    MongooseModelToDomain(schema: any): Order {
        const prods: ProductForOrder[] = [];
        schema._products.forEach((prod: any) => {
            const tmp = new ProductForOrder(prod._name, prod._price, prod._quantity, prod._deliveryDate);
            prods.push(tmp);
        });
        return new Order(prods, schema._client, schema._totalPrice, schema._creditCardUsed, schema._placementDate, schema._shippingAddress, schema._state, schema._reference, schema._id);
    }

    JSONToOrderUpdateDTO(body: any): OrderUpdateDto {
        if (!body.shippingAddress) {
            throw Error("Shipping address missing");
        }
        return new OrderUpdateDto(body.shippingAddress);
    }
}
