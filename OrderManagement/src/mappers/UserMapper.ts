import {ClientDto} from "../dtos/user/ClientDto";
import {User} from "../domain/models/user/User";
import {AdminDto} from "../dtos/user/AdminDto";
import {AdminRole} from "../domain/models/user/admin/AdminRole";
import {ClientRole} from "../domain/models/user/client/ClientRole";
import {ClientUpdateDTO} from "../dtos/user/ClientUpdateDTO";
import {ClientLimitedInformationDTO} from "../dtos/user/ClientLimitedInformationDTO";


export class UserMapper {

    /**
     * Maps a JSON object to a ClientDto, with all the data it contains.
     * @param body
     * @constructor
     */
    JSONToClientDTO(body: any): ClientDto {
        const clientId = body.username;
        const pass = body.password;
        const address = body.address;
        const nif = body.nif;
        const phoneNumber = body.phoneNumber;
        const email = body.email;
        const age = body.age;
        const firstName = body.firstName;
        const lastName = body.lastName;
        return new ClientDto(clientId, pass, address, nif, phoneNumber, email, age, firstName, lastName, 0);
    }

    JSONToAdminDTO(body: any): AdminDto {
        const clientId = body.username;
        const pass = body.password;
        return new AdminDto(clientId, pass);
    }

    /**
     * Maps a Client from domain to a ClientDto
     * @param client
     * @constructor
     */
    DomainToClientDTO(client: User): ClientDto {
        return new ClientDto(client.username.toString(), client.password.toString(), client.role.getAddress().toString(), client.role.getNif().toString(), client.role.getPhoneNumber().toString(), client.role.getEmail().toString(), client.role.getAge().toString(), client.role.getFirstName().getName(), client.role.getLastName().getName(), Number(client.role.getPriority()));
    }

    DomainToAdminDTO(admin: User): AdminDto {
        return new AdminDto(admin.username.toString(), admin.password.toString());
    }

    /**
     * Maps a Mongoose Database Model to a Domain Model.
     * @param schema
     * @constructor
     */
    MongooseModelToDomainClient(schema: any): User {
        const role = schema._role;
        return new User(schema._username, schema._password, new ClientRole(role._email, role._nif, role._phoneNumber, role._age, role._address, role._firstName, role._lastName), schema._id, schema._state);
    }

    MongooseModelToDomainAdmin(schema: any): User {
        return new User(schema._username, schema._password, new AdminRole(), schema._id, schema._state);
    }

    /**
     * Method that allows the transformation of a JSON to a dto of client.
     * @param body The body is the json passed as a parameter, that is going to be transformed in a client update dto.
     * @constructor. Builds a Client Update DTO.
     */
    JSONToClientUpdateDTO(body: any): ClientUpdateDTO {
        return new ClientUpdateDTO(body.firstName, body.lastName, body.address);
    }


    DomainToClientDTONameOnly(client: User): ClientLimitedInformationDTO {
        return new ClientLimitedInformationDTO(client.role.getFirstName().getName(), client.role.getLastName().getName(), client.username.toString());
    }
}
