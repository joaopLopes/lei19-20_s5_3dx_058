:- dynamic agenda_maq/2. %Predicado para o sprint 3 que tem como argumentos em primeiro lugar, a maquina (é a sua agenda), a lista de tarefas a ser feita naquela máquina
:- dynamic auxLinhaTarefa/2.
:- dynamic auxiliarQnt/2.
:- dynamic aux_Op_Opt/3.

% F�BRICA

% Linhas

%linhas([lA,lB,lC,lD]).


% Maquinas


%maquinas([ma,mb,mc,md,me]).


% Ferramentas


%ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj]).


% Maquinas que constituem as Linhas

%tipos_maq_linha(lA,[ma,mb,mc,md,me]).
%tipos_maq_linha(lB,[ma,mb,mc,md,me]).
%tipos_maq_linha(lC,[ma,mb,mc,md,me]).
%tipos_maq_linha(lD,[ma,mb,mc,md,me]).
% ...


% Opera��es

%tipo_operacoes([opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 30 operacoes 6 lotes de produtos * 5 operacoes por produto

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo cada maquina tera' 6 operacoes atribuidas, uma por cada lote de produtos

% classif_operacoes/2 deve ser criado dinamicamente 
%no exemplo teremos 30 factos deste tipo, um para cada operacao


% Afeta��o de tipos de opera��es a tipos de m�quinas
% com ferramentas, tempos de setup e tempos de execucao)

%operacao_maquina(opt1,ma,fa,1,1).
%operacao_maquina(opt2,mb,fb,2.5,2).
%operacao_maquina(opt3,mc,fc,1,3).
%operacao_maquina(opt4,md,fd,1,1).
%operacao_maquina(opt5,me,fe,2,3).
%operacao_maquina(opt6,mb,ff,1,4).
%operacao_maquina(opt7,md,fg,2,5).
%operacao_maquina(opt8,ma,fh,1,6).
%operacao_maquina(opt9,me,fi,1,7).
%operacao_maquina(opt10,mc,fj,20,2).





%...


% PRODUTOS

%produtos([pA,pB,pC,pD,pE,pF]).

%operacoes_produto(pA,[opt1,opt2,opt3,opt4,opt5]).
%operacoes_produto(pB,[opt1,opt6,opt3,opt4,opt5]).
%operacoes_produto(pC,[opt1,opt2,opt3,opt7,opt5]).
%operacoes_produto(pD,[opt8,opt2,opt3,opt4,opt5]).
%operacoes_produto(pE,[opt1,opt2,opt3,opt4,opt9]).
%operacoes_produto(pF,[opt1,opt2,opt10,opt4,opt5]).



% ENCOMENDAS

%Clientes

%clientes([clA,clB,clC]).


% prioridades dos clientes

%prioridade_cliente(clA,2).
%prioridade_cliente(clB,1).
%prioridade_cliente(clC,3).

% ...

% Encomendas do cliente, 
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

%encomenda(clA,[e(pA,4,50),e(pB,4,70)]).
%encomenda(clB,[e(pC,3,30),e(pD,5,200)]).
%encomenda(clC,[e(pE,4,60),e(pF,6,120)]).
%encomenda(clA,[e(pC,1,500),e(pE,20,450)]).
%encomenda(clB,[e(pA,4,100)]).
%encomenda(clC,[e(pB,3,100)]).
% ...



% com este predicado vamos calcular os makespans e adicionar as tarefas � base de conhecimento relativas ao Prolog, tarefas estas que sao lidas pelo Algoritmo Gen�tico.

:- dynamic tarefas/1.
:- dynamic auxiliarQuantidade/3.
:- dynamic tarefa/4.
:- dynamic auxiliarAgendas/3. % Predicado auxiliar para calcular as agendas das máquinas.
:- dynamic auxiliarAgendaOMenorPossivel/3. % Predicado auxiliar que mantem o mais pequeno que é essencial para criar a agenda das m
 
 % O predicado introduzir tarefas basicamente conta-me todas as tarefas existentes e vai ser necessario dentro Algoritmo Genetico. No fundo, constroi o predicado o tarefas.
introduzir_tarefas:-
	findall(encomenda(A,B),encomenda(A,B),ListaComEncomendas), %Encontra todas as encomendas e mete-as numa lista.
	contar_tarefas(ListaComEncomendas,ContarTamanhoQueEONumeroDeTarefas), %Predicado que conta o tamanho que e o numero de tarefas.
	assertz(tarefas(ContarTamanhoQueEONumeroDeTarefas)).  % Constroi o facto tarefas com a quantidade de vezes distintas correspondente as operacoes produto com produto diferente.


contar_tarefas([],0). %Condicao de termino

contar_tarefas([encomenda(_,B)|RestoEncomendas],ContarTamanhoQueEONumeroDeTarefas):- %Condicao de nao termino.
	contar_tarefas(RestoEncomendas,ContarTamanhoQueEONumeroDeTarefas1),
	length(B,TamanhoDesta),
	ContarTamanhoQueEONumeroDeTarefas is ContarTamanhoQueEONumeroDeTarefas1 + TamanhoDesta.

% O predicado introduzir tarefa basicamente constroi o facto tarefa tal e qual como o Algoritmo Genetico necessita.



introduzir_tarefa_alternativo:-
	introduzir_tarefas,
	findall(encomenda(A,B),encomenda(A,B),ListaComEncomendas), %Encontra todas as encomendas e mete-as numa lista.
    tarefas_desta_encomenda(ListaComEncomendas).


tarefas_desta_encomenda([]). %Condicao de termino

tarefas_desta_encomenda([encomenda(A,B)|RestoEncomendas]):-
	assert_dos_produtos(B,A),!, %Vamos fazer assert da lista de produtos B do client A sendo que cada um dos produtos em B é uma tarefa do cliente A
	tarefas_desta_encomenda(RestoEncomendas).



assert_dos_produtos([],_). %Condicao de termino.

assert_dos_produtos([e(Produto,Quantidade,Conclusao)|RestoProdutos],Cliente):-
		operacoes_produto(Produto,La), % Vai buscar um produto e a sua lista de tipos de opera�ao, necessaria para contar depois o makespan
		buscar_tempo_processamento(La,TempoProcessamento,MaiorOperacao), % Vai buscar o tempo de processamento de um produto numa encomenda
		TempoProcessamentoSemMaiorOperacao is TempoProcessamento - MaiorOperacao,
		MaiorOperacaoComQuantidade is MaiorOperacao * Quantidade, % Acrescenta o conceito de v�rios produtos iguais numa encomenda.
		TempoProcessamentoComQuantidade is TempoProcessamentoSemMaiorOperacao + MaiorOperacaoComQuantidade,
		reverse(La,LaInvertida),  % Altera a ordem da lista para calcular o tempo de setup.
		buscar_tempo_processamento_tirando_setup(_,LaInvertida,TempoProcessamento,TempoSetup), % Calcula o Tempo De Setup Consoante as aulas TP
		TempoProcessamentoComQuantidadeSemSetup is TempoProcessamentoComQuantidade - TempoSetup, % Acrescentamos este tempo de setup ( ou retiramos se rapido)
		prioridade_cliente_busca(Cliente,Penalizacao),
		cria_identificador(Produto,Cliente,IdentificadorNovoProduto),
		assertz(auxiliarQuantidade(Cliente,Produto,Quantidade)),
		assertz(auxiliarQnt(IdentificadorNovoProduto,Quantidade)),
		assertz(tarefa(IdentificadorNovoProduto,TempoProcessamentoComQuantidadeSemSetup,Conclusao,Penalizacao)), % Acrescentamos o dado a base de conhecimento Prolog
		assert_dos_produtos(RestoProdutos,Cliente).



prioridade_cliente_busca(Cliente,Penalizacao):- %Predicado sem método de término.
	prioridade_cliente(Cliente,Prioridade),
	Penalizacao is 10*(1/Prioridade),!. % Forma de fazer um cliente com menos prioridade na verdade valer mais!

cria_identificador(Produto,Cliente,IdentificadorNovoProdutoFinal):-
	string_concat(Produto,'_',ProdutoComBarrinha),
	string_concat(ProdutoComBarrinha,Cliente,IdentificadorNovoProdutoFinal).
	



%O predicado aqui abaixo é o antigo que foi avaliado no sprint 2.

introduzir_tarefa:-
	operacoes_produto(Pb,La), % Vai buscar um produto e a sua lista de tipos de opera�ao, necessaria para contar depois o makespan
	buscar_quantidade(Pb,Quantidade), % Vai buscar a quantidade de produtos de um produto numa encomenda.
    buscar_tempo_processamento(La,TempoProcessamento,MaiorOperacao), % Vai buscar o tempo de processamento de um produto numa encomenda
	TempoProcessamentoSemMaiorOperacao is TempoProcessamento - MaiorOperacao,
	MaiorOperacaoComQuantidade is MaiorOperacao * Quantidade, % Acrescenta o conceito de v�rios produtos iguais numa encomenda.
	TempoProcessamentoComQuantidade is TempoProcessamentoSemMaiorOperacao + MaiorOperacaoComQuantidade,
	reverse(La,LaInvertida),  % Altera a ordem da lista para calcular o tempo de setup.
	buscar_tempo_processamento_tirando_setup(Pb,LaInvertida,TempoProcessamento,TempoSetup), % Calcula o Tempo De Setup Consoante as aulas TP
	TempoProcessamentoComQuantidadeSemSetup is TempoProcessamentoComQuantidade - TempoSetup, % Acrescentamos este tempo de setup ( ou retiramos se rapido)
	buscar_conclusao(Pb,TempoConclusao), % Vai diretamente buscar o tempo de conclusao logo para introduzir na tarefa
	buscar_prioridade(Pb,Penalizacao), % De acordo com a prioridade relativa ao cliente calcula a penalizacao usada pelo Algoritmo Genetico
	assertz(tarefa(Pb,TempoProcessamentoComQuantidadeSemSetup,TempoConclusao,Penalizacao)). % Acrescentamos o dado a base de conhecimento Prolog
	
	
buscar_prioridade(Pb,Penalizacao):-
	op_prod_client(_,_,_,Pb,Client,_,_,_,_),
	prioridade_cliente(Client,Prioridade),
	Penalizacao is 10*(1/Prioridade),!. % Forma de fazer um cliente com menos prioridade na verdade valer mais!
	
	
buscar_quantidade(Pb,Quantidade):-
	op_prod_client(_,_,_,Pb,_,Qnt,_,_,_),
	Quantidade is Qnt,!.
	
	
buscar_conclusao(Pb,TempoConclusao):-
	op_prod_client(_,_,_,Pb,_,_,Concl,_,_),
	TempoConclusao is Concl,!.
	
buscar_tempo_processamento([],0,-1).

buscar_tempo_processamento([Operacao|RestoDasOperacoes],TempoProcessamento,MaiorOperacao):-
	buscar_tempo_processamento(RestoDasOperacoes,TempoProcessamento1,MaiorOperacao1),
	operacao_maquina(Operacao,_,_,_,TempoUsadoExecDestaVez),
	TempoProcessamento is TempoProcessamento1 + TempoUsadoExecDestaVez,
	((TempoUsadoExecDestaVez > MaiorOperacao1,!,MaiorOperacao is TempoUsadoExecDestaVez);MaiorOperacao is MaiorOperacao1).
	
	
buscar_tempo_processamento_tirando_setup(_,[],0,10000).  % Valor arbitrario muito alto para comecar, nenhum e suposto ser mais alto que isto

buscar_tempo_processamento_tirando_setup(Pb,[Operacao|RestoDasOperacoes],TempoProcessamento,TempoSetup):-
	buscar_tempo_processamento_tirando_setup(Pb,RestoDasOperacoes,TempoProcessamento1,TempoSetup1),
	operacao_maquina(Operacao,_,_,TempoParado,TempoUsadoExecDestaVez),
	TempoProcessamento is TempoProcessamento1 + TempoUsadoExecDestaVez,
	((TempoProcessamento1 - TempoParado < TempoSetup1,!,TempoSetup is TempoProcessamento1-TempoParado,(retract(auxiliarAgendaOMenorPossivel(Pb,_,_));(!,true)),!, asserta(auxiliarAgendaOMenorPossivel(Pb,TempoSetup,Operacao)), assertz(auxiliarAgendas(Pb,TempoSetup,Operacao)));
	(TempoSetup is TempoSetup1,TempoSetupAuxiliar is TempoProcessamento1-TempoParado, assertz(auxiliarAgendas(Pb,TempoSetupAuxiliar,Operacao)))).



% cria_op_enc - fizeram-se correcoes face a versao anterior
:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Prod,Cliente,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(_,_,_,0,_,Ni,Ni):-!.
cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni2):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(aux_Op_Opt(Opt,M,Op)),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)),
	Qt2 is Qt -1,
	cria_ops_prod_cliente2(Opt,Prod,Client,Qt2,TConc,Ni,Ni2).

























	

%Predicado que cria as agendas das maquinas.
criar_agenda_das_maquinas(Linha,ListaTarefas):-
	tipos_maq_linha(Linha,MaquinasExistentes),
	operacoes_por_fazer_satisfazer_encomenda(ListaTarefas,ListaOperacoesDasTarefas),!, % Buscado termo compound com tarefa e oope e termos associados.
	predicado_gera_agenda(Linha,MaquinasExistentes,ListaOperacoesDasTarefas,ListaTarefas).
	%deslizarEsquerda(Linha,MaquinasExistentes).


operacoes_por_fazer_satisfazer_encomenda([],[]). %Condição de Término, quando a Lista de Tarefas a fazer chega ao fim, e instanciamos a outra lista.

% Este predicado é um predicado auxiliar que no fundo, transforma as Tarefas existentes num termal compound que contem o produto daquela tarefa assim como a sua lista de operações necessaria a sua criacao
operacoes_por_fazer_satisfazer_encomenda([Tarefa|ListaTarefas],[par_tarefa_operacoes(SoProdutoAtom,OperacoesDaTarefa)|OperacoesDasOutrasTarefas]):-
	split_string(Tarefa,"_","",ListaStrings),
	reverse(ListaStrings,ListaStringsReversed),
	last(ListaStringsReversed,SoProduto),
	atom_string(SoProdutoAtom,SoProduto), %Tira aspas
	operacoes_produto(SoProduto,OperacoesDaTarefa),
	operacoes_por_fazer_satisfazer_encomenda(ListaTarefas,OperacoesDasOutrasTarefas).


predicado_gera_agenda(_,[],_,_). % A condicao de termino sera quando ja tivermos feito a agenda de todas as máquinas

predicado_gera_agenda(Linha,[Maquina|OutrasMaquinas],ListaOperacoesDasTarefas,ListaTarefas):-
	busca_operacoes_maquina(Maquina,OperacoesQueAMaquinaConsegueFazer),
	predicado_ve_operacoes_da_maquina(Maquina,ListaOperacoesDasTarefas,ListaOperacoesDasTarefasSemAsOperacoesDaMaquina,ListaOperacoesDaMaquinaEmEspecifico,OperacoesQueAMaquinaConsegueFazer),
	busca_ts(ListaOperacoesDaMaquinaEmEspecifico,ListaTs,ListaTarefas,0),
	%tirar_ferramentas_repetidas(ListaTs,ListaSemFerramentasRepetidas), % Predicado que tira as ferramentas repetidas.
	string_concat(Linha,"_", Temp),
	string_concat(Temp,Maquina,LinhaMaquina), %Merge do Nome
	assertz(agenda_maq(LinhaMaquina,ListaTs)),!,
	predicado_gera_agenda(Linha,OutrasMaquinas,ListaOperacoesDasTarefasSemAsOperacoesDaMaquina,ListaTarefas),!.


predicado_ve_operacoes_da_maquina(_,[],[],[],_):-!. %Condiçao de termino que instancia as outras duas lista.

predicado_ve_operacoes_da_maquina(Maquina,[par_tarefa_operacoes(Tarefa,OperacoesDaTarefa)|OperacoesDasOutrasTarefas],[par_tarefa_operacoes(Tarefa,OperacoesDaTarefaSemAsFeitasPelaMaquina)|OperacoesDasOutrasTarefas2],[triplo_tarefa_operacoes(Maquina,Tarefa,OperacoesFeitaPelaMequina)|RestoDasOperacoesDaMaquinaDasOutrasTarefas],OperacoesQueAMaquinaConsegueFazer):-	
	interseccao(OperacoesQueAMaquinaConsegueFazer,OperacoesDaTarefa,OperacoesFeitaPelaMequina), % Interseção equivale, neste caso, ás opeacões de uma determinada tarefa que a máquina permite fazer.
	difference(OperacoesFeitaPelaMequina,OperacoesDaTarefa,OperacoesDaTarefaSemAsFeitasPelaMaquina), % Operacoes que a maquina nao faz de uma determinada tarefa.
	predicado_ve_operacoes_da_maquina(Maquina,OperacoesDasOutrasTarefas,OperacoesDasOutrasTarefas2,RestoDasOperacoesDaMaquinaDasOutrasTarefas,OperacoesQueAMaquinaConsegueFazer).


interseccao([ ],_,[ ]).  % Predicado implementado em aula que faz a intersecao entre duas listas.
interseccao([X|L1],L2,[X|LI]):-member(X,L2),!,interseccao(L1,L2,LI).
interseccao([_|L1],L2, LI):- interseccao(L1,L2,LI).
	
difference(L1, L2, L3) :-  % Predicado implementado em aula que faz a diferenca entre duas listas
    difference2(L1, L2, LAux),
    difference2(L2, L1, LAux2),
    uniao(LAux, LAux2, L3).

uniao([], R, R).
uniao([X|R1], R2, R3) :-
    member(X, R2),
    !,
    uniao(R1, R2, R3).

uniao([X|R1], R2, [X|R3]) :-
    uniao(R1, R2, R3).

difference2([],_,[]).

difference2([X|L1], L2, L3):-
    member(X,L2),
    !,
    difference2(L1, L2, L3).

difference2([X|R1],  R2, [X|R3]):-
    difference2(R1, R2, R3).

busca_operacoes_maquina(Maquina,OptsMaquina):-
	findall(Opts,operacao_maquina(Opts,Maquina,_,_,_),OptsMaquina).

busca_ts([],[],_,_). % Condição de término final do método que vai buscar as ts e, no fundo construir a lista fundamental na construcao das agendas das máquinas

busca_ts([triplo_tarefa_operacoes(_,_,[])|RestoProdutos],OutrasTsParaAMaquina,ListaTarefas,Ordem):- % Condição de término
	busca_ts(RestoProdutos,OutrasTsParaAMaquina,ListaTarefas,Ordem).

busca_ts([triplo_tarefa_operacoes(Machine,Tarefa,[Tarefas|RestoTarefas])|RestoProdutos],[t(InicioSetup,FimSetup,setup,Ferramentas),t(FimSetup,FimExec,exec,[Tarefas,Quantidade,Tarefa,Cliente,Tarefa],Ordem)|OutrasTsParaAMaquina],ListaTarefas,Ordem):- % Fim Setup no compund term de exe pois começa a operacao mal acabe o setup da sua ferramenta.
	findall(Ferr,operacao_maquina(Tarefas,Machine,Ferr,_,_),Ferramentas),
	makespan_ate_aqui(ListaTarefas,Tarefa,MakespanAteEstaTarefa),
	get_para_agendas_tempos_setup(Machine,Tarefas,Tarefa,InicioSetup,FimSetup,MakespanAteEstaTarefa), % Faz uso dos predicados auxiliares.
	atom_string(Tarefa,TarefaString),
	buscar_informacoes_adicionais(TarefaString,Machine,_,Quantidade,Cliente), % Predicado auxiliar que vai buscar as informacoes adicionais da agenda da maquina.
	string_concat(TarefaString,"_",TarefaComBarra),
	string_concat(TarefaComBarra,Cliente,Identificador),
	calcula_fim_exec(Machine,Tarefas,Tarefa,FimSetup,FimExec, MakespanAteEstaTarefa,Identificador), % Calcula o Fim do tempo de execucao da tarefa. 
	Ordem1 is Ordem + 1,
	busca_ts([triplo_tarefa_operacoes(Machine,Tarefa,RestoTarefas)|RestoProdutos],OutrasTsParaAMaquina,ListaTarefas,Ordem1).

%Predicado que me conta os tempos de setup de acordo com as regras pedidas na TP.
get_para_agendas_tempos_setup(Machine,Tarefas,Tarefa,InicioSetup,FimSetup,MakespanAteEstaTarefa):-
	((isFolgaMaiorOperacao(Machine,Tarefas,Tarefa,TempoQueDemoraSetup),!,InicioSetup is 0+MakespanAteEstaTarefa, FimSetup is TempoQueDemoraSetup+MakespanAteEstaTarefa); % Caso seja o começo do makespan
	calcularQuandoNaoEOMelhor(Machine,Tarefas,Tarefa,InicioSetup,FimSetup, MakespanAteEstaTarefa)). % Caso nao seja o começo do makespan e necessario fazer contas segundo TP.
	
	%Calcula setup times quando e o melhor.
isFolgaMaiorOperacao(Machine,Tarefas,Tarefa,TempoQueDemoraSetup):-
	auxiliarAgendaOMenorPossivel(Tarefa,_,Tarefas),operacao_maquina(Tarefas,Machine,_,TempoQueDemoraSetup,_).

	%Calcula setup quando nao e o melhor.
calcularQuandoNaoEOMelhor(Machine,Tarefas,Tarefa,InicioSetup,FimSetup, MakespanAteEstaTarefa):-
	auxiliarAgendas(Tarefa,Tempo,Tarefas),
	auxiliarAgendaOMenorPossivel(Tarefa,TempoMelhor,_),
	InicioSetupSemAbsoluto is TempoMelhor - Tempo,
	abs(InicioSetupSemAbsoluto,InicioSetupFaltaMakespan),
	InicioSetup is InicioSetupFaltaMakespan+ MakespanAteEstaTarefa,
	operacao_maquina(Tarefas,Machine,_,TempoQueDemoraSetup,_),
	FimSetup is InicioSetup + TempoQueDemoraSetup .

calcula_fim_exec(Machine,Tarefas,_,FimSetup,FimExec, _,Identificador):- % Predicado que calcula o fim da execucao da tarefa para as quantidades que ele quiser.
	operacao_maquina(Tarefas,Machine,_,_,TempoQueDemoraExec),
	%op_prod_client(_,Machine,_,Tarefa,_,Qnt,_,_,_),
	auxiliarQnt(Identificador,Qnt),
	TempoQueDemoraExecTodos is TempoQueDemoraExec * Qnt,
	FimExec is FimSetup + TempoQueDemoraExecTodos .

buscar_informacoes_adicionais(Tarefa,Machine,Operacao,Quantidade,Cliente) :- % Ainda tenho a simplificacao que o nome da tarefa e a ferramente no futuro mudar isso
	op_prod_client(Operacao,Machine,_,Cliente,Tarefa,Quantidade,_,_,_),!.

makespan_ate_aqui(ListaTarefas,Tarefa,MakespanAteEstaTarefa):- % Predicado que conta o makespan que demorou ate termos chegado a esta tarefa.
		before(Tarefa,ListaTarefas,ListaTarefasAteTarefa), !,	%Aqui damos split da lista de tarefas para ver que tarefas foram feitas ate aqui.
		delete(ListaTarefasAteTarefa,Tarefa,ListaTarefasAteTarefaSemTarefa),
		calcularMakeSpan(ListaTarefasAteTarefaSemTarefa,MakespanAteEstaTarefa).


calcularMakeSpan([],0). % Condição de término que instancia o make span ate la.

calcularMakeSpan([Tarefa|RestoDasTarefas],MakespanAteEstaTarefa):-
	calcularMakeSpan(RestoDasTarefas,MakespanAteEstaTarefa1),
	split_string(Tarefa,"_","",ListaStrings),
	reverse(ListaStrings,ListaStringsReversed),
	last(ListaStringsReversed,SoProduto),
	atom_string(_,SoProduto),
	operacoes_produto(SoProduto,ListaOperacoes),
	auxiliarQnt(_,Qnt),
	buscar_tempo_processamento(ListaOperacoes,TempoProcessamento,MaiorOperacao), % Vai buscar o tempo de processamento de um produto numa encomenda
	TempoProcessamentoSemMaiorOperacao is TempoProcessamento - MaiorOperacao,
	MaiorOperacaoComQuantidade is MaiorOperacao * Qnt, % Acrescenta o conceito de v�rios produtos iguais numa encomenda.
	TempoProcessamentoComQuantidade is TempoProcessamentoSemMaiorOperacao + MaiorOperacaoComQuantidade,
	reverse(ListaOperacoes,ListaOperacoesInvertida),  % Altera a ordem da lista para calcular o tempo de setup.
	buscar_tempo_processamento_tirando_setup(_,ListaOperacoesInvertida,TempoProcessamento,TempoSetup), % Calcula o Tempo De Setup Consoante as aulas TP
	MakespanDestaTarefa is TempoProcessamentoComQuantidade - TempoSetup,
	MakespanAteEstaTarefa is MakespanAteEstaTarefa1 + MakespanDestaTarefa.
	


%Predicado que separa uma lista em duas consoante um elemento N
before( _, [], []).
before( N, [N|_], [N]).
before( N, [X|T], [X|Res]):- dif(N,X), before( N, T, Res).


tirar_ferramentas_repetidas(ListaTs,ListaTsSemRepetidas):- % Predicado que retira os setups com ferramentas repetidas.
	tirar_setups(ListaTs,ListaTsSoSetups), % Fazemos lista so com setups
	difference(ListaTsSoSetups,ListaTs,ListaSoComExecs), % Faz uma lista so com execs.
	acrescentar_so_setups_necessarios(ListaTsSoSetups,ListaSoComExecs,ListaTsSemRepetidas,semferramenta). % Nao tem ferramenta ainda, e a primeira operacao mas precisamos de um nome para comecar

tirar_setups([],[]).  % Condicao de termino

tirar_setups([t(A,B,setup,C)|RestoDasTarefas],[t(A,B,setup,C)|RestoDasTarefasSoSetup]):- % Predicado que tira os setups.
	tirar_setups(RestoDasTarefas,RestoDasTarefasSoSetup).

tirar_setups([t(_,_,exec,_,_)|RestoDasTarefas],RestoDasTarefasSoSetup):- %Predicado que nao passa porque e um exec.
	tirar_setups(RestoDasTarefas,RestoDasTarefasSoSetup).

acrescentar_so_setups_necessarios(_,[],[],_). % Condicao de termino das ferramentas repetidas.
acrescentar_so_setups_necessarios([],[],[],_). % Condicao de termino secundaria das ferramentas repetidas.


acrescentar_so_setups_necessarios(ListaTsSoSetups,[t(D,E,exec,[F,G,H,I,J],M)|RestoExecs],[t(A,B,setup,[C]),t(D,E,exec,[F,G,H,I,J],M)|RestoDaListaFinal],Ferramenta):- % Se for preciso acrescenta a operacao necessaria
	classif_operacoes(F,Opt),
	operacao_maquina(Opt,_,Ferramenta2,_,_),
	((Ferramenta \= Ferramenta2,!,buscar_setup_acrescentando(ListaTsSoSetups,t(A,B,setup,[C]),Ferramenta2),
	delete(ListaTsSoSetups,t(A,B,setup,[Ferramenta2]),ListaSemAFerramentaTiradaAgora),acrescentar_so_setups_necessarios(ListaSemAFerramentaTiradaAgora,RestoExecs,RestoDaListaFinal,Ferramenta2))).



acrescentar_so_setups_necessarios(ListaTsSoSetups,[t(D,E,exec,[F,G,H,I,J],Z)|RestoExecs],[t(D,E,exec,[F,G,H,I,J],Z)|RestoDaListaFinal],Ferramenta):- % Se for preciso acrescenta a operacao necessaria
	acrescentar_so_setups_necessarios(ListaTsSoSetups,RestoExecs,RestoDaListaFinal,Ferramenta). % Só avança na lista.


buscar_setup_acrescentando([t(A,B,setup,[C])|_],t(A,B,setup,[C]),C).


buscar_setup_acrescentando([],_,_). % Condicao de paragem quando nao existe a ferramenta na lista.

buscar_setup_acrescentando([t(_,_,setup,[D])|SetupsOutros],D,C):-
	buscar_setup_acrescentando(SetupsOutros,C).




deslizarEsquerda(Linha,MaquinasExistentes):- 	% Predicado que desliza para a esquerda as agendas simulando, deste modo, a simultaneidade.
	constroi_lista(Linha,MaquinasExistentes,ListaDeListasTs),!,
    tirar_execs(ListaDeListasTs,ListaDeListasTsSemExecs,ListaSoSetups),!, % Calcula.
	get_tamanho_maior_lista(ListaDeListasTsSemExecs,TamanhoDaMaiorLista), % Este predicado é importante para basicamente fazer tarefas que entram dentro de outras mas outras nao (para iterar basicamente.)
	faz_deslizamento(ListaDeListasTsSemExecs,ListaDeslizada,1,TamanhoDaMaiorLista), %Faz-se n-1 deslizamentos. As listas podem poder fazer todos os deslizamentos (N=G), ou so alguns (N<G) onde G e o numero maximo de deslizamentos(da maior lista).Começa com um porque o primeiro deslizamento e com o primeiro elemento e nao com o elemento 0.
	nova_agenda_deslizada(Linha,ListaDeslizada,ListaSoSetups),!. %Predicado que vai recriar as agendas de acordo com os deslizes de agora.




constroi_lista(_,[],[]). % Condicao de termino.

constroi_lista(Linha,[Maquina|RestoMaquinas],[row(Maquina,ListaTsDaMaquinas)|RestoDasListas]):-
	string_concat(Linha,Maquina,LinhaMaquina),
	findall(L,agenda_maq(LinhaMaquina,L),ListaTsDaMaquinas), %busca-lhe a lista.
	constroi_lista(Linha,RestoMaquinas,RestoDasListas). % Recursividade, o normal...


tirar_execs([],[],[]). % Condicao de termino

tirar_execs([row(Maquina,[EstaListaT])|RestoDasTs],[row(Maquina,[EstaListaTSoExecs])|RestoDaListaSoExecs],[row(Maquina,[EstaListaTSoSetups])|RestoListaSoSetups]):- % Vai ver uma de cada vez
	tirar_execs(RestoDasTs,RestoDaListaSoExecs,RestoListaSoSetups),
	tirar_setups(EstaListaT,EstaListaTSoSetups),
	difference(EstaListaTSoSetups,EstaListaT,EstaListaTSoExecs).


get_tamanho_maior_lista([],0). % Condicao de termino do predicado.

get_tamanho_maior_lista([row(_,[Lista])|RestantesListas],TamanhoDaMaiorLista):-
	get_tamanho_maior_lista(RestantesListas,TamanhoDaMaiorLista1),
	length(Lista,Auxiliar),
	((Auxiliar>TamanhoDaMaiorLista1,!,TamanhoDaMaiorLista is Auxiliar);TamanhoDaMaiorLista is TamanhoDaMaiorLista1).

% Até aqui está bem.

faz_deslizamento([],[],Numero,Numero). % Condicao de termino.

faz_deslizamento(Lista,Lista2,Numero,_):- %A lista 2 é a lista deslizada.
	ver_lista_inteira_menor_desta_vez(Lista,Numero,MenorDeslizar), %Menor deslizar possível.
	criar_lista_deslizada_lista_inteira(Lista,Lista2,Numero,MenorDeslizar).


ver_lista_inteira_menor_desta_vez([],_,1000000). % Condicao de termino de menor folga da lista das listas.

ver_lista_inteira_menor_desta_vez([row(_,[Lista])|RestoDasListas],Numero,MenorDeslizar):-
	ver_lista_inteira_menor_desta_vez(RestoDasListas,Numero,MenorDeslizar1),
	%Condicao de validacao relativa ao tamanho da lista. Basicamente se Lista tem tamanho menor do que o que pode entao nao pode fazer deslizamentos.
	ver_sublista_menor_desta_vez(Lista,0,Numero,MenorDeslizarAux),
	((MenorDeslizarAux<MenorDeslizar1,!,MenorDeslizar is MenorDeslizarAux);MenorDeslizar is MenorDeslizar1).




%NAO ESQUECER VALIDAR QUANDO 
ver_sublista_menor_desta_vez(_,Numero,Numero,100000). %Condicao de termino, se fez as vezes necessárias.

ver_sublista_menor_desta_vez([t(_,ExecUm,exec,_),t(C,ExecDois,exec,D)|RestoLista],Numero,NumeroParaVerDeslizamentoDestaVez,MenorDeslizarDestaVez):- % Nao condicao de termino
	Numero1 is Numero+1,
	ver_sublista_menor_desta_vez([t(C,ExecDois,exec,D)|RestoLista],Numero1,NumeroParaVerDeslizamentoDestaVez,MenorDeslizarDestaVez1),
	AuxConta is C-ExecUm,
	((AuxConta<MenorDeslizarDestaVez1,!,MenorDeslizarDestaVez is AuxConta);MenorDeslizarDestaVez is MenorDeslizarDestaVez1).


%FALTA VALIDACAO DE TAMANHO DE LISTA
criar_lista_deslizada_lista_inteira([],[],_,_). % Condicao de paragem, que vai ser dita se o numero for igual (faz ATÉ á iteração anterior).

criar_lista_deslizada_lista_inteira([row(Maquina,[ListaNaoDeslizada])|RestoDaListaNaoDeslizada],[row(Maquina,[ListaDeslizada])|RestoDaListaDeslizada],NumeroDelimitador,MenorDeslizar):-
	criar_lista_deslizada_lista_inteira(RestoDaListaNaoDeslizada,RestoDaListaDeslizada,NumeroDelimitador,MenorDeslizar),
	criar_sublista_lista_inteira(ListaNaoDeslizada,ListaDeslizada,0,NumeroDelimitador,MenorDeslizar).


criar_sublista_lista_inteira([],[],_,_,_). %Condicao de paragem.

criar_sublista_lista_inteira([t(ExecInicio,ExecFim,exec,B)|RestoDaSubListaNaoDeslizada],[t(ExecInicioDeslizado,ExecFimDeslizado,exec,B)|RestoDaSubListaDeslizada],OndeEstamos,NumeroDelimitador,MenorDeslizar):-
	OndeEstamos1 is OndeEstamos+1,
	criar_sublista_lista_inteira(RestoDaSubListaNaoDeslizada,RestoDaSubListaDeslizada,OndeEstamos1,NumeroDelimitador,MenorDeslizar),
	((OndeEstamos=NumeroDelimitador,!,ExecInicioDeslizado is ExecInicio-MenorDeslizar,ExecFimDeslizado is ExecFim-MenorDeslizar);
	ExecInicioDeslizado is ExecInicio,ExecFimDeslizado is ExecFim).

nova_agenda_deslizada(_,[],[]). %Condiçao de termino.

nova_agenda_deslizada(Linha,[row(Maquina,[AgendaDeslizadaSemSetups])|NovasAgendas],[row(Maquina,[Setups])|SoSetups]):-
	acrescentar_so_setups_necessarios(Setups,AgendaDeslizadaSemSetups,ListaFinal,semFerramenta),!,
	string_concat(Linha,Maquina,LinhaMaquina),
	retract(agenda_maq(LinhaMaquina,_)),
	assertz(agenda_maq(LinhaMaquina,ListaFinal)),
	nova_agenda_deslizada(Linha,NovasAgendas,SoSetups).


%Agendas Temporais terminam aqui




















%Heuristica de Balanceamento das Linhas.

	balancear_linhas(ListaTarefasPorLinhaEscalonada):-
		cria_op_enc,!,
		%Correr primeiro gerar tarefas para termos tarefas no sistema.
		findall(Identificador,tarefa(Identificador,_,_,_),ListaIdentificadoresTarefas),
		fazer_lista_tarefas(ListaIdentificadoresTarefas,ListaDeTarefas), %Predicado que constroi a lista de tarefas com base nos identificadores unicos.
		linhas(Linhas), %Busca as linhas do sistema
		fazer_lista_com_makespan_atual_linhas(Linhas,LinhasComMakespanAtribuido), % Predicado que vai ter um termo composto que vai dizer o makespan de cada linha no momento.
		maquinas_da_linha(Linhas,MaquinasDaLinha), %Busca as maquinas de cada linha do sistema.
		opts_da_linha(MaquinasDaLinha,OptsDaLinha), % Busca opts que a linha pode fazer (no fundo dita se o produto pode ser feito la ou nao)
		opts_da_tarefa(ListaDeTarefas,OptsDaTarefa), %Busca opts que a tarefa necessita para ser feita.
		ver_que_linhas_podem_fazer_produto(OptsDaLinha,OptsDaTarefa,LinhasQueFazemTarefa),
		construir_lista(LinhasQueFazemTarefa,LinhasQueFazemTarefaFixed),
		balancear_tarefas_pelas_linhas(ListaDeTarefas,LinhasComMakespanAtribuido,LinhasQueFazemTarefaFixed,DistribuicaoLinhasTarefas,Linhas),!, % Tudo balanceado..
		organizar_lista_distribuida(Linhas,DistribuicaoLinhasTarefas,ListaTarefasOrganizada),!, %Predicado que pega na distribuicao e organiza a Lista de forma a ser mais facil dividir as tarefas e correr o genetico de seguida.
		correr_genetico_por_linha(ListaTarefasOrganizada,ListaTarefasPorLinhaEscalonada,ListaDeTarefas),!,
		((agenda_maq(_,_),!,retractall(agenda_maq(_,_),criar_agendas_por_linha(ListaTarefasPorLinhaEscalonada)));criar_agendas_por_linha(ListaTarefasPorLinhaEscalonada)). %Cria as agendas das maquinas da linha.


		criar_agendas_por_linha([]). %Condicao de termino.

		criar_agendas_por_linha([linhaTarefasEscalonadas(Linha,TarefasComAvaliacao)|RestoDasLinhaQueAindaVamosCriarAgendas]):-
			((TarefasComAvaliacao = naoExistemTarefasNestaLinha,!,criar_agendas_por_linha(RestoDasLinhaQueAindaVamosCriarAgendas)); 
			tirar_avaliacao(TarefasComAvaliacao,SoTarefas), %Tira a avaliacao.
			criar_agenda_das_maquinas(Linha,SoTarefas), %Cria a agenda.
			criar_agendas_por_linha(RestoDasLinhaQueAindaVamosCriarAgendas)).

		tirar_avaliacao(Tarefas*_,Tarefas). %Predicado que tira a avaliacao.

			construir_lista([],[]). %Condicao de termino

	construir_lista([linhasProduto(Produto,TarefasDele)|RestoLinhas],[linhasProduto(Produto,TarefasDeleFixed)|RestoLinhasFixed]):-
		construir_lista(RestoLinhas,RestoLinhasFixed),
		alterar_tarefas(TarefasDele,TarefasDeleFixedAux),
		removeAll("naoExisto",TarefasDeleFixedAux,TarefasDeleFixed),
		base_auxiliar(Produto,TarefasDeleFixed).




	base_auxiliar(_,[]). %Condicao de termino.

	base_auxiliar(Produto,[Tarefa|RestoTarefas]):-
		base_auxiliar(Produto,RestoTarefas),
		assertz(auxLinhaTarefa(Produto,Tarefa)).


	alterar_tarefas([],[]). %Condicao de termino.

	alterar_tarefas([Tarefa|RestoTarefas],[TarefaAux|RestoTarefasFixed]):-
		alterar_tarefas(RestoTarefas,RestoTarefasFixed),
		split_string(Tarefa,"?","",ListWords),
		reverse(ListWords,ReversedListWords),
		last(ReversedListWords,Element),
		((Element = "faz",!,last(ListWords,TarefaAux));TarefaAux = "naoExisto").



removeAll(_, [], []).
removeAll(X, [X|T], L):- removeAll(X, T, L), !.
removeAll(X, [H|T], [H|L]):- removeAll(X, T, L ).



%Predicados abaixo sao para correr geneticos por linha

	correr_genetico_por_linha([],[],_). %Condicao de termino

	correr_genetico_por_linha([linhaTarefas(Linha,ListaTarefas)|RestoDasTarefasOrganizadinhas],[linhaTarefasEscalonadas(Linha,MelhorIndividuo)|RestoDasTarefasEscalonadas],ListaDeTarefas):-
		correr_genetico_por_linha(RestoDasTarefasOrganizadinhas,RestoDasTarefasEscalonadas,ListaDeTarefas),
		retract_todas_as_tarefas(ListaDeTarefas), %Mete so as tarefas necessarias para correr para a linha.
		fazer_lista_so_com_tarefas_necessarias(ListaTarefas,ListaDeTarefas,ListaSoComTarefasNecessarias), %Cria lista so com as tarefas necessarias.
		reset_todas_tarefas(ListaSoComTarefasNecessarias), %Da assert so nas tarefas necessarias.
		((length(ListaSoComTarefasNecessarias,0),!,MelhorIndividuo = 'naoExistemTarefasNestaLinha',retract_todas_as_tarefas(ListaSoComTarefasNecessarias),reset_todas_tarefas(ListaDeTarefas))
		;length(ListaSoComTarefasNecessarias,M),((M<6,!,melhor_escalonamento(MelhorIndividuo,_),retract_todas_as_tarefas(ListaSoComTarefasNecessarias),reset_todas_tarefas(ListaDeTarefas)))
		;(gera(MelhorIndividuo),retract_todas_as_tarefas(ListaSoComTarefasNecessarias),reset_todas_tarefas(ListaDeTarefas))).



	%Predicados abaixo exclusivos para manusear base de conhecimento para correr o gentico por linha.


	fazer_lista_so_com_tarefas_necessarias([],_,[]).%Condicao de termino

	fazer_lista_so_com_tarefas_necessarias([Tarefa|RestoTarefas],ListaTarefas,[TarefaMatched|RestoTarefasMatched]):-
		fazer_lista_so_com_tarefas_necessarias(RestoTarefas,ListaTarefas,RestoTarefasMatched), %Recursividade
		buscarTarefaMatched(Tarefa,ListaTarefas,TarefaMatched).


		buscarTarefaMatched(_,[],_). %Condicao de termino nunca deve acontecer.

		buscarTarefaMatched(Tarefa,[tarefa(Tarefa,B,C,D)|_],tarefa(Tarefa,B,C,D)). %Condicao de termino caso encontro.

		buscarTarefaMatched(Tarefa,[tarefa(_,_,_,_)|RestoTarefas],TarefaMatched):-
			buscarTarefaMatched(Tarefa,RestoTarefas,TarefaMatched). %Recursividade

	retract_todas_as_tarefas([]). %Condicao de termino

	retract_todas_as_tarefas([tarefa(A,B,C,D)|RestoTarefas]):-
		retract(tarefa(A,B,C,D)),
		retract_todas_as_tarefas(RestoTarefas). %Recursividade.



	reset_todas_tarefas([]). %Condicao de termino

	reset_todas_tarefas([tarefa(A,B,C,D)|RestoTarefas]):- %Volta a dar reset nas tarefas todas.
		assertz(tarefa(A,B,C,D)),
		reset_todas_tarefas(RestoTarefas). %Recursividade.

%Predicados abaixo sao exclusivos para organizar
		organizar_lista_distribuida([],_,[]). %Condicao de termino que instancia lista a devolver organizada

		organizar_lista_distribuida([Linha|RestoLinhas],DistribuicaoLinhasTarefas,[linhaTarefas(Linha,ListaTarefas)|RestoLinhaTarefas]):-
			organizar_lista_distribuida(RestoLinhas,DistribuicaoLinhasTarefas,RestoLinhaTarefas),
			lista_tarefas_desta_linha(Linha,DistribuicaoLinhasTarefas,ListaTarefas).


		lista_tarefas_desta_linha(_,[],[]). %Condicao de termino.

		lista_tarefas_desta_linha(Linha,[linhaTarefa(linhaMakespan(Linha, _), Tarefa)|RestoDasListasNaoOrganizadas],[Tarefa|RestoTarefas]):-
			lista_tarefas_desta_linha(Linha,RestoDasListasNaoOrganizadas,RestoTarefas).

		lista_tarefas_desta_linha(Linha,[linhaTarefa(linhaMakespan(_,_),_)|RestoDasListasNaoOrganizadas],RestoTarefas):-
			lista_tarefas_desta_linha(Linha,RestoDasListasNaoOrganizadas,RestoTarefas).


	fazer_lista_tarefas([],[]). % Condicao de termino

	fazer_lista_tarefas([Identificador|RestoIdentificadores],[tarefa(Identificador,Processamento,Conclusao,Penalizacao)|RestoTarefas]):-
		fazer_lista_tarefas(RestoIdentificadores,RestoTarefas),
		tarefa(Identificador,Processamento,Conclusao,Penalizacao).


	fazer_lista_com_makespan_atual_linhas([],[]). %Condicao de termino da lista de linhas com makespans.

	fazer_lista_com_makespan_atual_linhas([Linha|RestoDasLinhas],[linhaMakespan(Linha,0)|RestoDasLinhasMakespan]):- %Predicado que constroi as linhas com makespan para mais tarde fazer a distribuicao do makespan.
		fazer_lista_com_makespan_atual_linhas(RestoDasLinhas,RestoDasLinhasMakespan).


	maquinas_da_linha([],[]). %Condicao de termino das maquinas de qualquer linha.

	maquinas_da_linha([Linha|RestoDasLinhas],[maquinasLinha(Linha,MaquinasLinha)|RestoDasMaquinasLinha]):-
		maquinas_da_linha(RestoDasLinhas,RestoDasMaquinasLinha),
		tipos_maq_linha(Linha,MaquinasLinha).


	opts_da_linha([],[]). %Condicao de termino de uma empresa.

	opts_da_linha([maquinasLinha(Linha,MaquinasLinha)|RestoDasMaquinasLinha],[optsLinha(Linha,NoDuplicateOpts)|RestoDasOptsLinha]):-
		opts_da_linha(RestoDasMaquinasLinha,RestoDasOptsLinha), % Recursividade.
		buscar_opts_linha(MaquinasLinha,Opts),
		flatten(Opts,FlatsOpts),
		sort(FlatsOpts,NoDuplicateOpts).


	buscar_opts_linha([],[]). % Condicao de termino, quando acaba as maquinas da linha podemos instanciar a lista de opts.

	buscar_opts_linha([Maquina|RestoDasMaquinas],[OptsDaMaquina|RestoDasOptsDaMaquinas]):-
		buscar_opts_linha(RestoDasMaquinas,RestoDasOptsDaMaquinas),
		findall(Opts,operacao_maquina(Opts,Maquina,_,_,_),OptsDaMaquina).

	opts_da_tarefa([],[]). %Condicao de termino.

	%Predicado que vai buscar as tarefas dos tipos de operação.
	opts_da_tarefa([tarefa(TarefaIdentificador,_,_,_)|RestoDasTarefas],[optsTarefa(TarefaIdentificador,OptsDaTarefaNoDuplicates)|RestoDasOptsTarefas]):-
		opts_da_tarefa(RestoDasTarefas,RestoDasOptsTarefas),
		split_string(TarefaIdentificador, "_", "", L),% Obtenho String separada pla_ca tenho pla e ca tenho que tirar a cabeça da lista
		tirar_cabeca_lista(L,ProdutoQueEAcCabeca), %Produto que e a cabeca
		atom_string(ProdutoQueEAcCabeca,StringProdutoCabeca),
		operacoes_produto(StringProdutoCabeca,OptsDaTarefaNoDuplicates).


	tirar_cabeca_lista([Cabeca|_],CabecaAtom):-%Tira Cabeca
		atom_string(CabecaAtom,Cabeca).

ver_que_linhas_podem_fazer_produto([],_,[]). %Condiçao de termino.

	ver_que_linhas_podem_fazer_produto([optsLinha(LinhaIdenfificador,NoDuplicateOpts)|RestoDasLinhasEOpts],OptsDaTarefa,[linhasProduto(LinhaIdenfificador,TarefasQueFaz)|RestoDasLinhasQueFazemTarefa]):-
		ver_que_linhas_podem_fazer_produto(RestoDasLinhasEOpts,OptsDaTarefa,RestoDasLinhasQueFazemTarefa),
		esta_linha_faz_que_produtos(OptsDaTarefa,NoDuplicateOpts,TarefasQueFaz),
		((var(TarefasQueFaz),!,TarefasQueFaz = []);true).


	esta_linha_faz_que_produtos([],_,[]). %Condicao de termino

	esta_linha_faz_que_produtos([optsTarefa(TarefaIdentificadorNaoFinal,OptsDaTarefaNoDuplicates)|RestoTarefas],NoDuplicateOpts,[TarefaIdentificador|RestoTarefasQueLinhaFaz]):- %Condicao que indica que a linha faz o produto.
		esta_linha_faz_que_produtos(RestoTarefas,NoDuplicateOpts,RestoTarefasQueLinhaFaz),
		((todos_membros(OptsDaTarefaNoDuplicates,NoDuplicateOpts),!,string_concat("faz?",TarefaIdentificadorNaoFinal,TarefaIdentificador));string_concat("naofaz?",TarefaIdentificadorNaoFinal,TarefaIdentificador)).


	todos_membros([],_). %Condicao de termino que instancia a lista que queremos que vem do predicado.

	todos_membros([Opt|OutrosOpts],NoDuplicateOpts):-
		todos_membros(OutrosOpts,NoDuplicateOpts),
		member(Opt,NoDuplicateOpts).


	balancear_tarefas_pelas_linhas([],_,_,[],_):-!. %Condicao de termino.
	
	balancear_tarefas_pelas_linhas([tarefa(TarefaIdentificador,_,B,_)|ListaTarefas],LinhasMakespan,LinhasQueFazemTarefa,[linhaTarefa(L,TarefaIdentificador)|RestoDasTarefas],Linhas):-
		findall(L,auxLinhaTarefa(L,TarefaIdentificador),LinhasTarefas),
	%	linhas_faz_tarefa(TarefaIdentificador,LinhasQueFazemTarefa,LinhasTarefa),!,
		filtrar_linhas_makespan_que_fazem_tarefa(LinhasTarefas,LinhasMakespan,LinhaMakespanDasLinhasQueFazemTarefa), %Predicado que filtra a lista dos makespan para a lista do makespan das linhas que fazem a tarefa
		bubblesort3(LinhaMakespanDasLinhasQueFazemTarefa,ListaSortedDasLinhasQueFazemTarefa), %Bubble sort. Organiza as linhas pelo seu makespan para que atribuemos a tarefa a linha com menor makespan no proximo predicado.
		reverse(ListaSortedDasLinhasQueFazemTarefa,ListaSortedDasLinhasQueFazemTarefaAoContrario),
		last(ListaSortedDasLinhasQueFazemTarefaAoContrario,L), % Bubble sort porque o primeiro sera o mais pequeno devido ao facto de termos corrido o bubble sort.
		update_makespan(L,B,LinhasMakespan,LinhasMakespanUpdated),!, %Da update nos makespans das linhas
		balancear_tarefas_pelas_linhas(ListaTarefas,LinhasMakespanUpdated,LinhasQueFazemTarefa,RestoDasTarefas,Linhas). %Recursividade.

 %sem duvida

	linhas_faz_tarefa(_,[],[]). %Condicao de termino

	linhas_faz_tarefa(TarefaIdentificador,[linhasProduto(LinhaIdentificador,TarefasFeitasPelaLinhaIdentificado)|RestoDasLinhas],[LinhaIdentificador|RestoDasLinhasQueFazemEstaTarefa]):- %Condicao se a linha fizer a tarefa.
		linhas_faz_tarefa(TarefaIdentificador,RestoDasLinhas,RestoDasLinhasQueFazemEstaTarefa), %Recursividade
		member(TarefaIdentificador,TarefasFeitasPelaLinhaIdentificado),!. %Se for membro adiciona, senão não adiciona a tarefa a lista de linhas que a fazem.

	linhas_faz_tarefa(_,[linhasProduto(_,_)|_],_).
		

	filtrar_linhas_makespan_que_fazem_tarefa([],_,[]). %Condicao de termino que instancia a lista.

	filtrar_linhas_makespan_que_fazem_tarefa([Linha|RestoDasLinhas],LinhasMakespan,[LinhaMakespanEstaLinha|Resto]):-
		filtrar_linhas_makespan_que_fazem_tarefa(RestoDasLinhas,LinhasMakespan,Resto),
		buscar_linha_makespan(Linha,LinhasMakespan,LinhaMakespanEstaLinha).


	buscar_linha_makespan(_,[],naoexiste). %Condicao de terminio caso nao encontre, em principio nunca acontecera

	buscar_linha_makespan(Linha,[linhaMakespan(Linha,Makespan)|_],linhaMakespan(Linha,Makespan)):-!.

	buscar_linha_makespan(Linha,[linhaMakespan(_,_)|RestoDasLinhasMakespan],LinhaMakespanIrrelevante):-
		buscar_linha_makespan(Linha,RestoDasLinhasMakespan,LinhaMakespanIrrelevante).



bubblesort3(InputList,SortList) :-
        swap3(InputList,List) , ! ,
        bubblesort3(List,SortList).
bubblesort3(SortList,SortList).

swap3([linhaMakespan(Linha1,Makespan1),linhaMakespan(Linha2,Makespan2)|List],[linhaMakespan(Linha2,Makespan2),linhaMakespan(Linha1,Makespan1)|List]) :- 
	Makespan1 > Makespan2.
swap3([Z|List],[Z|List1]) :- 
	swap3(List,List1).

%Abaixo Update Makespans

	update_makespan(linhaMakespan(_,_),_,[],[]). %Condicao de termino.

	update_makespan(linhaMakespan(LinhaIdenfificador,Makespan),B,[linhaMakespan(LinhaIdenfificador,Makespan)|RestoDaMakespanAntiga],[linhaMakespan(LinhaIdenfificador,C)|RestoDaMakespanNovo]):- %Condicao de quando encontrei o igual.
			update_makespan(linhaMakespan(LinhaIdenfificador,Makespan),B,RestoDaMakespanAntiga,RestoDaMakespanNovo), %Recursividade
			C is Makespan + B.

	update_makespan(linhaMakespan(LinhaIdenfificador,Makespan),B,[linhaMakespan(LinhaIdenfificador2,Makespan2)|RestoDaMakespanAntiga],[linhaMakespan(LinhaIdenfificador2,Makespan2)|RestoDaMakespanNovo]):- %Condicao de quando nao encontrei o igual.
			update_makespan(linhaMakespan(LinhaIdenfificador,Makespan),B,RestoDaMakespanAntiga,RestoDaMakespanNovo).

	







calcular_timespan_produto(Produto,Datespan):-
	get_time(Now),
	busca_lista_operacoes_produto(Produto,Operacoes),
	busca_makespan_das_operacoes_do_produto(Operacoes,TempoGasto),
	TempoGastoHoras is TempoGasto * 3600, %Simular horas.
	TempoGastoFinal is TempoGastoHoras + Now,
	stamp_date_time(TempoGastoFinal,Datespan,'UTC').
	


busca_lista_operacoes_produto(Produto,ListaOperacoes):- %Vai buscar a lista de operacoes.
	split_string(Produto,"?","",List),
	get_produto_out_lista(List,Produto2,1,0),
	operacoes_produto(Produto2,ListaOperacoes).

get_produto_out_lista([Produto2|_],Produto2,1,1).

get_produto_out_lista([_|RestoProdutos],Produto2,1,G):-
	G1 is G+1,
	get_produto_out_lista(RestoProdutos,Produto2,1,G1).



busca_makespan_das_operacoes_do_produto([],0). %Condicao de termino

busca_makespan_das_operacoes_do_produto([Operacao|RestoOperacoes],TempoGasto):-
	busca_makespan_das_operacoes_do_produto(RestoOperacoes,TempoGasto1),
	operacao_maquina(Operacao,_,_,Processamento,Conclusao),
	TempoGasto is Processamento + Conclusao + TempoGasto1.

%Calcular TimeSpan Lista (para ARQSI)

calcular_timespan(CompoundTermListaEPenalizacaoComTempo, ListaTarefaMaisDateSpan):-
		get_time(TimeNow), %Tempo de Agora.
		separa_lista(CompoundTermListaEPenalizacaoComTempo,ListaComTempo), % Tira o asterisco ficando assim so com a lista
		calcula_tempo_que_demora(ListaComTempo,ListaTarefaMaisDateSpan,_,TimeNow).  %Tambem vou dar um assertzinho



separa_lista(Lista*_,Lista). %Predicado cuja funcao e exclusivamente separar a Lista da penalizacao.

calcula_tempo_que_demora([],[],TimeNow,TimeNow). %Condicao de termino

%Saliente-se ainda que o timespan aqui consideramos como sendo horas daí a multiplicação por 3600

calcula_tempo_que_demora([Tarefa|RestoTarefas],[tarefa(Tarefa,Datespan)|RestoTarefasComDatespan],TempoAteAgora,TimeNow):- %Condicao de nao termino.
	calcula_tempo_que_demora(RestoTarefas,RestoTarefasComDatespan,TempoAteAgora1,TimeNow),
	tarefa(Tarefa,Processamento,_,_),
	ProcessamentoHoras is Processamento * 3600, %Simular horas.
	TempoAteAgora is TempoAteAgora1 + ProcessamentoHoras,
	stamp_date_time(TempoAteAgora,Datespan,'UTC').
