#!/usr/bin/env bash
docker login --username=_ --password=$HEROKU_TOKEN registry.heroku.com

docker build --no-cache -t prod-planning .

docker tag prod-planning registry.heroku.com/production-planning/web 

docker push registry.heroku.com/production-planning/web

export DOCKER_IMAGE_ID=$(docker inspect prod-planning --format='{{.Id}}')

curl -n -X PATCH https://api.heroku.com/apps/production-planning/formation \
    -d '{
        "updates": [
            {
                "type": "web",
                "docker_image": "'"$DOCKER_IMAGE_ID"'"
            }
        ]
    }' \
    -H "Content-Type: application/json" \
    -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
    -H "Authorization: Bearer $HEROKU_TOKEN"
