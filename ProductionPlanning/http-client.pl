:- ['ag.txt']. %include
:- use_module(library(http/json)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_open)).
:- use_module(library(http/http_ssl_plugin)).
:- use_module(library(http/http_json)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/http_unix_daemon)).
:- use_module(library(http/thread_httpd)).
:- set_setting(http:cors, [*]).


% :- [data].
:- (dynamic linhas/1).  %Feito asserted.
:- (dynamic maquinas/1). % Feito asserted.
:- (dynamic ferramentas/1). %Feito asserted.
:- (dynamic tipos_maq_linha/2). %Feito asserted.
:- (dynamic tipo_operacoes/1). %Feito asserted
:- (dynamic operacao_maquina/5).
:- (dynamic produtos/1). %Feito Asserted
:- (dynamic operacoes_produto/2). %Feito Asserted
:- (dynamic clientes/1). %Feito Asserted.
:- (dynamic prioridade_cliente/2). %Feito Asserted
:- (dynamic encomenda/2). %Feito Asserted
:- (dynamic classif_operacoes/2). %Feito Asserted
:- (dynamic operacoes_atrib_maq/2). %Feito Asserted
:- (dynamic auxiliarPredicado/2). %Predicado Auxiliar Para Fazer Ligacao Dominio ARQSI-ALGAV
:- (dynamic auxiliarOperacao/4). %Predicado Auxiliar
:- (dynamic auxiliarMaquina/2). %Predicado Auxiliar
:- (dynamic auxiliarEncomenda/2). %Predicado Auxiliar
:- (dynamic maquinaEstadoTrabalho/2). % Predicado Auxiliar de máquina que vai indicar qual o seu estado de trabalho, se está desativada ou ativada.
:- (dynamic agenda_maq/2). % Predicado Auxiliar de máquina que vai indicar qual o seu estado de trabalho, se está desativada ou ativada.
:- http_handler('/generate', gera_base_dados, []).			
:- http_handler('/produto', calcula_data_produto, []).
:- http_handler('/product-delivery-date', p_json, []).
:- http_handler('/schedule', p_json2, []).
:- http_handler('/balance-lines', p_json3, []).
:- http_handler('/activate-machine', activate_machine, []). 
:- http_handler('/deactivate-machine', deactivate_machine,[]).

% Pedidos http
%login_url("http://localhost:5004/login"). %Predicado de login.
%operations_url("https://localhost:5001/mdf/operations").
%machines_url("https://localhost:5001/mdf/machines").
%productionLines_url("https://localhost:5001/mdf/productionLines").
%machineTypes_url("https://localhost:5001/mdf/machineTypes").
%products_url("https://localhost:5002/mdp/products").
%clients_url("https://localhost:5005/clients").
%orders_url("https://localhost:5005/orders/scheduled/").
login_url("https://order-management-58.herokuapp.com/login"). %Predicado de login.
operations_url("https://master-data-factory-3dx1.azurewebsites.net/mdf/operations").
machines_url("https://master-data-factory-3dx1.azurewebsites.net/mdf/machines").
productionLines_url("https://master-data-factory-3dx1.azurewebsites.net/mdf/productionLines").
machineTypes_url("https://master-data-factory-3dx1.azurewebsites.net/mdf/machineTypes").
products_url("https://master-data-production-3dx1.azurewebsites.net/mdp/products").
clients_url("https://order-management-58.herokuapp.com/clients").
orders_url("https://order-management-58.herokuapp.com/orders/scheduled/").


:- json_object orders(orders:string).
:- json_object message(mensagem:string).
:- json_object student(name:string, number:integer).
:- json_object data_produto(ano:integer, mes:integer, dia:integer).




erase_database:-
    retractall(linhas(_)),
    retractall(maquinas(_)),
    retractall(ferramentas(_)),
    retractall(tipos_maq_linha(_,_)),
    retractall(tipo_operacoes(_)),
    retractall(operacao_maquina(_,_,_,_,_)),
    retractall(produtos(_)),
    retractall(operacoes_produto(_,_)),
    retractall(clientes(_)),
    retractall(prioridade_cliente(_,_)),
    retractall(encomenda(_,_)),
    retractall(classif_operacoes(_,_)),
    retractall(operacoes_atrib_maq(_,_)),
    retractall(auxiliarPredicado(_,_)),
    retractall(auxiliarOperacao(_,_,_,_)),
    retractall(auxiliarMaquina(_,_)),
    retractall(auxiliarEncomenda(_,_)),
    retractall(maquinaEstadoTrabalho(_,_)),
    retractall(agenda_maq(_,_)),
    retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),

    retractall(aux_Op_Opt(_,_,_)),
    retractall(auxLinhaTarefa(_,_)),
    retractall(auxiliarQnt(_,_)),
    retractall(tarefas(_)),
    retractall(auxiliarQuantidade(_,_,_)),
    retractall(tarefa(_,_,_,_)).




activate_machine(Request):-
    cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
    http_parameters(Request, [machine(Referencia,[])
                              ]), 
     split_string(Referencia, "'","", ListMachine),
     last(ListMachine,Machine),
    retract(maquinaEstadoTrabalho(Machine,_)),
    assertz(maquinaEstadoTrabalho(Machine,"Activated")),
    Message = "Success Activating Machine.",
    R = message(Message),
    prolog_to_json(R, JSONObject),
    reply_json(JSONObject, [json_object(dict)]).
               



deactivate_machine(Request):-
    cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
    http_parameters(Request, [machine(Referencia,[])
                              ]), 
    split_string(Referencia, "'","", ListMachine),
    last(ListMachine,Machine),
    retract(maquinaEstadoTrabalho(Machine,_)),
    assertz(maquinaEstadoTrabalho(Machine,"Deactivated")),
    Message = "Success Deactivating Machine.",
    R = message(Message),
    prolog_to_json(R, JSONObject),
    reply_json(JSONObject, [json_object(dict)]).




p_json(Request) :-
   cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
	http_parameters(Request, [reference(Referencia,[]),
                              product(Produto,[])]),
    split_string(Referencia,"?","",List),
    get_produto_out_lista(List,Referencia2,1,0),
    split_string(Produto,"?","",List2),
    get_produto_out_lista(List2,ProdutoAux,1,0),
    auxiliarEncomenda(Encomenda,Referencia2),
    buscar_lista_da_encomenda(Encomenda,Lista),
    encontrarProduto(Lista,ProdutoAux,_),
    calcular_timespan_produto(Produto,DateSpan),
    get_data_dia(DateSpan,Ano,Mes,Dia),
	R = data_produto(Ano,Mes,Dia),
	prolog_to_json(R, JSONObject),
	reply_json(JSONObject, [json_object(dict)]).

buscar_lista_da_encomenda(encomenda(_,Lista),Lista). %Vai buscar a lista so 

encontrarProduto([e(Produto,_,_)|_],Produto,Produto). %Condicao de termino

encontrarProduto([e(_,_,_)|RestoProdutosEncomenda],Produto2,Produto3):- 
    encontrarProduto(RestoProdutosEncomenda,Produto2,Produto3). %Recursividade.


get_data_dia(date(Ano,Mes,Dia,_,_,_,_,_,_),Ano,Mes,Dia). %So vai buscar.


% Cria��o de servidor HTTP no porto 'Port'					
server(Port) :-						
        http_server(http_dispatch, [port(Port)]).

p_json3(Request) :-
   cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
        ((agenda_maq(_,_),!);balancear_linhas(_)),
      Message = "Success balancing.",
      R = message(Message),
      prolog_to_json(R, JSONObject),
      reply_json(JSONObject, [json_object(dict)]).




p_json2(Request) :-
   cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
	http_parameters(Request, [linha(Linha,[])
                             ]),
    split_string(Linha, "METERPLICA","", TmpL),
    last(TmpL,LinhaAspas),
    fazer_texto(LinhaAspas,RestoJson),
    get_data_dia(_,_,_,_),
	R = orders(RestoJson),
	prolog_to_json(R, JSONObject),
	reply_json(JSONObject, [json_object(dict)]).



balancear_linhas_http(Request) :-
    cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
    http_parameters(Request, [reference(_,[]),
                              product(_,[])]),
    linhas(LinhasQueExistem), %Vai buscar todas as linhas existentes
    fazer_json(LinhasQueExistem,_),
    %R = orders(RestoJson),
    R = data_produto(1,1,1),
    prolog_to_json(R, JSONObject),
	reply_json(JSONObject, [json_object(dict)]).



string_it(_,"_").



gera_base_dados(Request):-
    cors_enable(Request,
                  [ methods([get,post,delete])
                  ]),
    http_parameters(Request, [startDate(StartDate, []),
                  endDate(EndDate, [])]),
                  ((linhas(_),!,erase_database,generate(StartDate, EndDate));generate(StartDate, EndDate)),
                  Message = "Success generating database.",
                  R = message(Message),
                  prolog_to_json(R, JSONObject),
                  reply_json(JSONObject, [json_object(dict)]).

generate(StartDate, EndDate):-
    login_admin(Token2),
    operations_data(_,_,Token2), %11-2 = 9
    clients_data(_,Token2), % Consegui direito
    products_data(_,ListaProdutos,Token2), % Consegui direito
    preencherPlanoFabrico(ListaProdutos,Token2), %Consegui direito
    orders_data(_,Token2, StartDate, EndDate), % Consegui direito
    machines_data(_,ListaMaquinas,Token2), % Consegui direito
    productionLines_data(_,Token2),% Consegui direito
    machineTypes_data(_,Token2),!, %Consegui direito
    criarPredicadoOperacoesMaquina(ListaMaquinas),
    introduzir_tarefa_alternativo.

fazer_texto(Linha,VariavelConcatted):- %[AgendaMaquinasDasLinhas|RestoAgendas],
    findall(Identificador,agenda_maq(Identificador,_),ListaIdentificadores),
    StringVirgula = " , ",
    buscar_maquinas_linha_por_agenda_maquina(Linha,ListaIdentificadores,AgendaMaquinasDasLinhas),
    produtos(P),
    fazer_json_per_produto(P,AgendaMaquinasDasLinhas,JsonFabricationPlan),
     Aux = ",porBarra,",
    Aux2 = ",porCurvaBarra,",
    StringProductionLine = "productionLine: ",
    StringLinha = Linha,
    string_concat(StringProductionLine,StringLinha,StringConcatenada),
    string_concat(StringConcatenada,StringVirgula,StringConcatenadaVirgula),
    StringFabrications = "fabricationsOrders: ",
    StringFabricos = JsonFabricationPlan,
    string_concat(StringFabricos,Aux2,StringFabricosBarra),
    string_concat(StringFabricosBarra,Aux,StringFabricosBarraECurva),
    string_concat(StringFabricosBarraECurva,Aux2,StringFabricosBarraECurva2),
    string_concat(StringFabrications,StringFabricosBarraECurva2,StringConcatenada2),
    string_concat(StringConcatenada2,StringVirgula,StringConcatenada2Virgula),
    string_concat(StringConcatenadaVirgula,StringConcatenada2Virgula,VariavelConcatted).
    % Variavel = {"productionLine":Linha, "fabricationOrders":JsonFabricationPlan}.
    

buscar_maquinas_linha_por_agenda_maquina(_,[],[]). %Condicao de termino

buscar_maquinas_linha_por_agenda_maquina(Linha,[Identificador|RestoIdentificadores],[Agenda|RestoAgendas]):-
        buscar_maquinas_linha_por_agenda_maquina(Linha,RestoIdentificadores,RestoAgendas),
    	split_string(Identificador,"_","",ListaStrings),
        reverse(ListaStrings,ListaStringsReversed),
        last(ListaStringsReversed,IdentificadorLinha),
        agenda_maq(Identificador,Agenda),
        Linha=IdentificadorLinha.


buscar_maquinas_linha_por_agenda_maquina(Linha,[_|RestoIdentificadores],RestoAgendas):-
    buscar_maquinas_linha_por_agenda_maquina(Linha,RestoIdentificadores,RestoAgendas).

fazer_json_per_produto([],_," , startFabrications: , "). %Condicao de termino

fazer_json_per_produto([Produto|RestoProdutos], AgendaMachine,VariavelConcatted):-
    fazer_json_per_produto(RestoProdutos,AgendaMachine,VariavelAteAqui), %Recursividade
    fazer_json_maquina(AgendaMachine,MaquinasJson,Produto), %Predicado que constroi o JSON das maquinas.
    Aux = ",porBarra,",
    Aux2 = ",porCurvaBarra,",
    Aux3 = ",porVirgula,",
    StringVirgula = " , ",
    string_concat(MaquinasJson,Aux2,MaquinasJsonPorBarra),
    string_concat(MaquinasJsonPorBarra,Aux,MaquinasJsonPorBarraCurva),
    string_concat(MaquinasJsonPorBarraCurva,Aux2,MaquinasJsonPorBarraCurva2), 
    string_concat(MaquinasJsonPorBarraCurva2,Aux3,MaquinasJsonPorBarraCurvaVirgula),
    StringProduct = "product: ",
    StringProduto = Produto,
    string_concat(StringProduct,StringProduto,StringConcatenada),
    string_concat(StringConcatenada,StringVirgula,StringConcatenadaVirgula),
    StringMachines = "machines: ",
    StringMaquinas = MaquinasJsonPorBarraCurvaVirgula,
    string_concat(StringMachines,StringMaquinas,StringConcatenada2),
    string_concat(StringConcatenada2,StringVirgula,StringConcatenada2Virgula),
    string_concat(StringConcatenadaVirgula,StringConcatenada2Virgula,StringConcatenada3),
    string_concat(VariavelAteAqui,StringConcatenada3,VariavelConcatted).
   %& Variavel = {"product":Produto, "machines":MaquinasJson}.


fazer_json_maquina([]," , startMachine: , ",_). %Condicao de termino.

fazer_json_maquina([AgendaDeMaquina|RestoAgendaDasMaquinas],VariavelConcatted,Produto):-
    fazer_json_maquina(RestoAgendaDasMaquinas,VariavelAteAqui,Produto), %Recursividade
    StringVirgula = " , ",
    agenda_maq(Maquina,AgendaDeMaquina),
    split_string(Maquina,"_","",ListaStrings),
    last(ListaStrings, IdentificadorMaquina),
    atom_string(ProdutoAtom,Produto),
     Aux = ",porBarra,",
    Aux2 = ",porCurvaBarra,",
    fazer_json_ocupations(AgendaDeMaquina,OcupationsJson,ProdutoAtom),
    string_concat(OcupationsJson,Aux,OcupationsJsonPorBarra),
    string_concat(OcupationsJsonPorBarra,Aux2,OcupationsJsonPorBarraCurva),
    StringMachine= "machine: ",
    StringMaquina = IdentificadorMaquina,
    string_concat(StringMachine,StringMaquina,StringConcatenada),
    string_concat(StringConcatenada,StringVirgula,StringConcatenadaVirgula),
    StringOcupations = "ocupations: ",
    %StringOcupacao = OcupationsJson,
    string_concat(StringOcupations,OcupationsJsonPorBarraCurva,StringConcatenada2),
    string_concat(StringConcatenada2,StringVirgula,StringConcatenada2Virgula),
    string_concat(StringConcatenadaVirgula,StringConcatenada2Virgula,StringConcatenada3Virgula),
    string_concat(VariavelAteAqui,StringConcatenada3Virgula,VariavelConcatted).
    % Variavel = {"machine":IdentificadorMaquina, "ocupations":OcupationsJson}.


fazer_json_ocupations([]," , startOcupation: , ",_). %Condicao de termino.

fazer_json_ocupations([t(_,_,exec,[C,_,Produto,_,Produto],H)|Resto],VariavelConcatted,Produto):-
    fazer_json_ocupations(Resto,VariavelAteAqui,Produto),
    StringVirgula = " , ",
    StringOperation = "operation: ",
    operacao_maquina(C,_,_,Setup,Execucao),
    StringOperacao = C,
    string_concat(StringOperation,StringOperacao,StringConcatenada),
    string_concat(StringConcatenada,StringVirgula,StringConcatenadaVirgula),
    StringProcessingTime = "processingTime: ",
    Aux2 = ",porCurvaBarra,",
    Virgula = ",",
    StringTempoProcessamento = Execucao,
    string_concat(StringProcessingTime,StringTempoProcessamento,StringConcatenada2),
    string_concat(StringConcatenada2,StringVirgula,StringConcatenada2Virgula),
    string_concat(StringConcatenadaVirgula,StringConcatenada2Virgula,StringConcatenada3),
    StringSetupTime = "setupTime: ",
    StringTempoSetup = Setup,
    string_concat(StringSetupTime,StringTempoSetup,StringConcatenada4),
    string_concat(StringConcatenada4,StringVirgula,StringConcatenada4Virgula),
    string_concat(StringConcatenada3,StringConcatenada4Virgula,StringConcatenada5),
    StringTimeSlot = "timeSlot: ",
    StringEspacoTempo = H,
    string_concat(StringTimeSlot,StringEspacoTempo,StringConcatenada6),
    string_concat(StringConcatenada6,Aux2,StringConcatenada10),
    string_concat(StringConcatenada10,Virgula,StringConcatenada11),
    string_concat(StringConcatenada11,StringVirgula,StringConcatenada6Virgula),
    string_concat(StringConcatenada5,StringConcatenada6Virgula,StringConcatenada7),
    string_concat(VariavelAteAqui,StringConcatenada7,VariavelConcatted).

fazer_json_ocupations([t(_,_,exec,[_,_,Produto2,_,Produto2],_)|Resto],RestoVariaveis,Produto):-
    fazer_json_ocupations(Resto,RestoVariaveis,Produto).



fazer_json_ocupations([t(_,_,setup,[_])|Resto],RestoVariaveis,Produto):-
    fazer_json_ocupations(Resto,RestoVariaveis,Produto).



    

    



    


%Predicado para logar.

login_admin(L):-
    login_url(URL),
    Data = json([username=admin,password='admin#99']),
      http_open(URL, Stream,
              [post(json(Data)), 
              status_code(_),
              header(authorization,L)
              ]),
            close(Stream).

criarPredicadoOperacoesMaquina([]):-!.

criarPredicadoOperacoesMaquina([Maquina|RestoDasMaquina]):-
    auxiliarMaquina(Maquina,TipoMaquinaDaMaquina),
    auxiliarPredicado(TipoMaquinaDaMaquina,OperacoesDoTipoDeMaquina),
    assertz(operacoes_atrib_maq(Maquina,OperacoesDoTipoDeMaquina)),
    predicadoQueFazAssertsDaMaquinaOperacao(Maquina,OperacoesDoTipoDeMaquina),
    criarPredicadoOperacoesMaquina(RestoDasMaquina).



predicadoQueFazAssertsDaMaquinaOperacao(_,[]):-!.

predicadoQueFazAssertsDaMaquinaOperacao(Maquina,[Operacao|RestoDasOperacoes]):-
    auxiliarOperacao(Operacao,Tool,ExecutionTime,SetupTime),
    assertz(operacao_maquina(Operacao,Maquina,SetupTime,Tool,ExecutionTime   )),
    predicadoQueFazAssertsDaMaquinaOperacao(Maquina,RestoDasOperacoes).



preencherPlanoFabrico([],_).

preencherPlanoFabrico([Produto|RestoProdutos],Token2):-
    fabrication_plan_data(_,Produto,Token2),
    preencherPlanoFabrico(RestoProdutos,Token2).

% OPERACOES BASES DE DADOS
operations_data(Data,ListaOperacoes,Token2) :-
    operations_url(URL),
    setup_call_cleanup(
        http_open(URL, In,[request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)
    ), operations(Data,Test),
        criarPredicadoOperacoes(Test,ListaOperacoes),
        assertz(tipo_operacoes(ListaOperacoes)),
        criarFerramentas(Test,ListaFerramentas),
        sort(ListaFerramentas,ListaFerramentasUnicas),
        assertz(ferramentas(ListaFerramentasUnicas)).

operations(Data, Compounds) :-
    dicts_to_compounds(Data, [description,executionTime,setupTime,tool], dict_fill(null), Compounds).

%CLIENTES BASES DE DADOS

clients_data(Data,Token2):-
    clients_url(ClientsUrl),
     setup_call_cleanup(
        http_open(ClientsUrl, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)
    ), clientes(Data,ClientsTest),
        criarPredicadoClientes(ClientsTest,ClientesToBeAsserted),
        assertz(clientes(ClientesToBeAsserted)),
        criarPredicadoPrioridade(ClientsTest).

    clientes(Data, Compounds) :-
    dicts_to_compounds(Data, [username], dict_fill(null), Compounds).

% PRODUTOS BASES DE DADOS

products_data(Data,ProdutosAssert,Token2):-  
    products_url(ProductsURL),
     setup_call_cleanup(
        http_open(ProductsURL, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)
    ),  products(Data,ProductsTest),
        criarPredicadoProduto(ProductsTest,ProdutosAssert),
        assertz(produtos(ProdutosAssert)).

products(Data, Compounds) :-
    dicts_to_compounds(Data, [name,price], dict_fill(null), Compounds).

%FABRICATION PLAN - A SER CHAMADO POR PRODUTO

fabrication_plan_data(Data,Produto,Token2):-
     products_url(ProductsURL),
     string_concat(ProductsURL,'/',ProductsURLComBarrinha),
     string_concat(ProductsURLComBarrinha,Produto,ProductsURLComBarrinhaEProduto),
     string_concat(ProductsURLComBarrinhaEProduto,'/',ProductsURLComBarrinhaEProdutoEMaisUmaBarrinha),
     string_concat(ProductsURLComBarrinhaEProdutoEMaisUmaBarrinha,'fabrication-plan',FabricationPlanURL),
      setup_call_cleanup(
        http_open(FabricationPlanURL, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)
    ), fabricationPlans(Data,FabPlans),
    assertz(operacoes_produto(Produto,FabPlans)).


fabricationPlans(Data,Test) :-	
	 get_dict(operations,Data,Test).

%ENCOMENDA BASES DE DADOS

orders_data(Data,Token2, StartDate, EndDate):- 
    orders_url(OrdersURL),
    string_concat(OrdersURL, '?startDate=', OrdersURLdates1),
    string_concat(OrdersURLdates1, StartDate, OrdersURLdates2),
    string_concat(OrdersURLdates2, '&endDate=', OrdersURLdates3),
    string_concat(OrdersURLdates3, EndDate, OrdersURLDates),
     setup_call_cleanup(
        http_open(OrdersURLDates, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)
    ), orders(Data,OrdersTest),
        ordersIdentificadores(Data,OrdersTestsIdentificadores),
    buscarProdutosEAssert(OrdersTest,OrdersTestsIdentificadores).

orders(Data, Compounds) :-
    dicts_to_compounds(Data, [client,products], dict_fill(null), Compounds).

ordersIdentificadores(Data, Compounds) :-
    dicts_to_compounds(Data, [reference], dict_fill(null), Compounds).

buscarProdutosEAssert([],[]).
buscarProdutosEAssert([row(Cliente,ProdutosDoCliente)|RestoDosClientesEProdutosRespetivos],[row(IdentificadorEncomenda)|RestoIdentificadores]):-
         dicts_to_compounds(ProdutosDoCliente, [name,quantity], dict_fill(null), BlaBla),
         fazerAssertEncomenda(BlaBla,ListaCriada),
         assertz(encomenda(Cliente,ListaCriada)),
         assertz(auxiliarEncomenda(encomenda(Cliente,ListaCriada),IdentificadorEncomenda)),
         buscarProdutosEAssert(RestoDosClientesEProdutosRespetivos,RestoIdentificadores).
     
fazerAssertEncomenda([],[]).

fazerAssertEncomenda([row(Name,Quantity)|RestoDosProdutosDesteCliente],[e(Name,Quantity,1)|EncomendasJaAdicionadas]):-
    fazerAssertEncomenda(RestoDosProdutosDesteCliente,EncomendasJaAdicionadas).

% MAQUINAS BASES DE DADOS
machines_data(_,ListaMaquinas,Token2) :-
	machines_url(MachinesURL),
	setup_call_cleanup( 
		http_open(MachinesURL, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)),
		machines(Data,MachinesTest),
        criarPredicadoMaquinas(MachinesTest,ListaMaquinas),
        assertz(maquinas(ListaMaquinas)).
		
	

machines(Data,Compounds) :-	
	 dicts_to_compounds(Data, [serialNumber,machineWorkingState,model,brand,machineType], dict_fill(null), Compounds).

% LINHAS DE PRODUCAO BASES DE DADOS
productionLines_data(_,Token2) :-
	productionLines_url(ProductionLinesURL),
	setup_call_cleanup( 
		http_open(ProductionLinesURL, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)),
		productionLines(Data,ProductionLinesTests),
        criarPredicadoLinhas(ProductionLinesTests,ListaLinhas),
        criarPredicadoLigacaoLinhaMaquina(ProductionLinesTests),
        assertz(linhas(ListaLinhas)).
		
	

productionLines(Data,Compounds) :-	
	 dicts_to_compounds(Data, [productionLineIdentifier,listAssociatedMachinesDto], dict_fill(null), Compounds).
	
%TIPOS DE MAQUINAS BASES DE DADOS

machineTypes_data(_,Token2):-
    machineTypes_url(MachineTypesURL),
    setup_call_cleanup( 
		http_open(MachineTypesURL, In, [request_header('token'=Token2),request_header('Accept'='application/json'),cert_verify_hook(cert_accept_any)
        ]),
        json_read_dict(In, Data),
        close(In)),
        machineTypes(Data,MachineTypeTests),
        criarPredicadoAuxiliar(MachineTypeTests).

	

machineTypes(Data,Compounds) :-	
	 dicts_to_compounds(Data, [name,listMacTypeOperations], dict_fill(null), Compounds).
 

 %AUX CRIACAO DE PREDICADOS AUXILIARES

criarPredicadoLinhas([],[]).

criarPredicadoLinhas([row(LinhaIdentificador,_)|RestoDasLinhas],[LinhaIdentificador|LinhasJaAdicionadas]):-
    criarPredicadoLinhas(RestoDasLinhas,LinhasJaAdicionadas).


criarPredicadoMaquinas([],[]).

criarPredicadoMaquinas([row(MaquinasIdentificador,EstadoTrabalho,_,_,TipoMaquina)|RestoDasMaquinas],[MaquinasIdentificador|MaquinasJaAdicionadas]):-
    assertz(maquinaEstadoTrabalho(MaquinasIdentificador,EstadoTrabalho)),
    assertz(auxiliarMaquina(MaquinasIdentificador,TipoMaquina)),
    criarPredicadoMaquinas(RestoDasMaquinas,MaquinasJaAdicionadas).

criarPredicadoLigacaoLinhaMaquina([]).

criarPredicadoLigacaoLinhaMaquina([row(LinhaIdentificador,LinhasMaquinasAssociadas)|RestoDasLinhas]):-
        organizar(LinhasMaquinasAssociadas,LinhasMaquinasAssociadasMelhor),
        assertz(tipos_maq_linha(LinhaIdentificador,LinhasMaquinasAssociadasMelhor)),
    criarPredicadoLigacaoLinhaMaquina(RestoDasLinhas).


    organizar([],[]). %Condicao de termino

    organizar([Compound|RestoLinhasAssociadas],[Test|RestoLinhasAssociadasOrganizadas]):-
         get_dict(machineSerialNumber,Compound,Test),
    organizar(RestoLinhasAssociadas,RestoLinhasAssociadasOrganizadas).



criarPredicadoOperacoes([],[]).

criarPredicadoOperacoes([row(OperacaoIdentificador,Tool,ExecutionTime,SetupTime)|RestoDasOperacoes],[OperacaoIdentificador|RestoDasOperacoesJaAdicionadas]):-
    assertz(auxiliarOperacao(OperacaoIdentificador,Tool,ExecutionTime,SetupTime)),
    assertz(classif_operacoes(OperacaoIdentificador,OperacaoIdentificador)),
    criarPredicadoOperacoes(RestoDasOperacoes,RestoDasOperacoesJaAdicionadas).

criarFerramentas([],[]).

criarFerramentas([row(_,_,_,Tool)|RestoFerramentas],[Tool|FerramentasAdicionadas]):-
    criarFerramentas(RestoFerramentas,FerramentasAdicionadas).


criarPredicadoProduto([],[]).

criarPredicadoProduto([row(NameProduto,_)|RestoProdutos],[NameProduto|ProdutosAdicionados]):-
    criarPredicadoProduto(RestoProdutos,ProdutosAdicionados).

criarPredicadoClientes([],[]).

criarPredicadoClientes([row(Username)|RestoClientes],[Username|ClientesAdicionados]):-
    criarPredicadoClientes(RestoClientes,ClientesAdicionados).

criarPredicadoPrioridade([]).

criarPredicadoPrioridade([row(Username)|RestoClientes]):-
    assertz(prioridade_cliente(Username,1)),
    criarPredicadoPrioridade(RestoClientes).

criarPredicadoAuxiliar([]).

criarPredicadoAuxiliar([row(MachineTypeName,MachineTypeOperations)|RestoDasMachineTypes]):-
    assertz(auxiliarPredicado(MachineTypeName,MachineTypeOperations)),
    criarPredicadoAuxiliar(RestoDasMachineTypes).





% permuta_tempo/3 faz uma permuta��o das opera��es atribu�das a uma maquina e calcula tempo de ocupa��o incluindo trocas de ferramentas

permuta_tempo(M,LP,Tempo):- operacoes_atrib_maq(M,L),
permuta(L,LP),soma_tempos(semfer,M,LP,Tempo).


soma_tempos(_,_,[],0).
soma_tempos(Fer,M,[Op|LOp],Tempo):- classif_operacoes(Op,Opt),
	operacao_maquina(Opt,M,Fer1,Tsetup,Texec),
	soma_tempos(Fer1,M,LOp,Tempo1),
	((Fer1==Fer,!,Tempo is Texec+Tempo1);
			Tempo is Tsetup+Texec+Tempo1).






