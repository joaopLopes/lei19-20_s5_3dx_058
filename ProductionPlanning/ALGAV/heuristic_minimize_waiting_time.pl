:-['data']. %include		
		
% Segunda Heuristica aplicando a heurist�ca EDD (Early Due Date), como expresso pelo enunciado fornecido, n�o existe uma heuristica que garanta a solu��o �tima

h_m_minimizar_somatorio_atraso(C,Lm,TAtraso):-
		get_time(Ti),   % Tempo que come�amos a realizar a opera��o.
		findall(X,op_prod_client(X,_,_,C,_,_,_,_,_),LAuxiliar), 			% Busca todas as opera��es para a encomenda relativa ao cliente C
		bubblesort(LAuxiliar,Lm),			% Ordena as opera��es pelo tempo de conclus�o menor, basicamente � a aplica��o da heur�stica EDD
		calcular_Atraso(Lm,TTempoGasto,TConcl),
		TAtraso is TConcl - TTempoGasto,
		get_time(Tf),Tcomp is Tf-Ti,     % Subtrai um e outro para ver quanto tempo � que demora a realizar a heurista toda.
		% bubbleSort(LAuxiliar,Lm),          % Recorrendo a um bubblesort vamos organizar a lista come�ando pelo elemento que tem tempo de conclus�o mais baixo para o que tem maior. No fundo, estamos a aplicar um Early Due Date. 
		write('GERADO EM '),write(Tcomp),  % Text
		write(' SEGUNDOS'),nl.				% Text
		
		
bubblesort(InputList,SortList) :-
        swap(InputList,List) , ! ,
        bubblesort(List,SortList).
bubblesort(SortList,SortList).

swap([X,Y|List],[Y,X|List]) :- 
	op_prod_client(X,_,_,_,_,_,TConca,_,_),
	op_prod_client(Y,_,_,_,_,_,TConcb,_,_),
	TConca > TConcb.
swap([Z|List],[Z|List1]) :- 
	swap(List,List1).


calcular_Atraso(Lops,TTempoGasto,TConclusaoMaior):- 
	calcular_Atraso2(semferr,Lops,TTempoGasto),
	calcular_TConclusaoMaior(semferr, Lops, TConclusaoMaior).
	

calcular_Atraso2(Ferr,[],0):-!.
calcular_Atraso2(Ferr,[Op|Lops],TTempoGasto):-
	op_prod_client(Op,_,Ferr2,_,_,_,TConcl,TSetup,TExec),
	calcular_Atraso2(Ferr2,Lops,TTempoGasto1),
	((Ferr2 = Ferr,!,TTempoGasto is TExec+TTempoGasto1);
	TTempoGasto is TSetup+TExec+TTempoGasto1).


calcular_TConclusaoMaior(Ferr,[],0):-!.
calcular_TConclusaoMaior(Ferr,[Op|Lops],TConclusaoMaior):-
	op_prod_client(Op,_,Ferr2,_,_,_,TConcl,TSetup,TExec),
	calcular_TConclusaoMaior(Ferr2,Lops,TConclusaoMaior1),
	((TConcl > TConclusaoMaior1,!,TConclusaoMaior is TConcl);TConclusaoMaior is TConclusaoMaior1).




% Fim da segunda Heuristica
