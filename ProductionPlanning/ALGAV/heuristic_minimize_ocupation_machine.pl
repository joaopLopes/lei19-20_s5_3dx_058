:-['data']. %include	
% Primeira Heurista, melhor tempo de escalonamento para uma m�quina.
% A l�gica utilizada � que uma m�quina M utiliza uma dada ferramenta F, se uma opera��o utiliza a mesma ferramenta F
% uma heurista que, garantidamente, assegura a melhor solu��o.

h_m_tempo_ocupacao(M,Lm,Tm):- 
				get_time(Ti),   % Tempo que come�amos a realizar a opera��o.
				operacoes_atrib_maq(M,Lops), % Vou buscar as opera��es.
				 bubblesort(Lops,Lm),
				 soma_espera(Lm,Tm),
				get_time(Tf),Tcomp is Tf-Ti,     % Subtrai um e outro para ver quanto tempo � que demora a realizar a heurista toda.
				write('GERADO EM '),write(Tcomp),  % Text
				write(' SEGUNDOS'),nl.				% Text



 bubblesort(InputList,SortList) :-
        swap(InputList,List) , ! ,
        bubblesort(List,SortList).
 bubblesort(SortList,SortList).

swap([X,Y|List],[Y,X|List]) :- classif_operacoes(X,Opt),
	operacao_maquina(Opt,_,Ferr,_,_),
	classif_operacoes(Y,Opt2),
	operacao_maquina(Opt2,_,Ferr2,_,_),
	Ferr @< Ferr2.
swap([Z|List],[Z|List1]) :- swap(List,List1).


soma_espera([Op|Lm],Tm):-classif_operacoes(Op,Opt),
						operacao_maquina(Opt,_,Ferr,TSetup,TExec),
						soma_espera2(Ferr,Lm,Tm1),
						Tm is TSetup+TExec+Tm1.

soma_espera2(Ferr,[],0).
soma_espera2(Ferr,[Op|Lm],Tm) :-
		classif_operacoes(Op,Opt),
		operacao_maquina(Opt,_,Ferr2,TSetup,TExec),
		soma_espera2(Ferr2,Lm,Tm1),
		((Ferr2 = Ferr,!,Tm is TExec+Tm1);
		Tm is TSetup+TExec+Tm1).

% Fim Heurista ocupacao maquina
				
