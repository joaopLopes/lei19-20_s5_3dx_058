% F�BRICA

% Linhas

linhas([lA]).


% Maquinas


maquinas([ma,mb,mc,md,me]).



% Ferramentas


ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[ma,mb,mc,md,me]).
% ...


% Opera��es

tipo_operacoes([opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 30 operacoes 6 lotes de produtos * 5 operacoes por produto

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo cada maquina tera' 6 operacoes atribuidas, uma por cada lote de produtos

% classif_operacoes/2 deve ser criado dinamicamente 
%no exemplo teremos 30 factos deste tipo, um para cada operacao


% Afeta��o de tipos de opera��es a tipos de m�quinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,1,1).
operacao_maquina(opt2,mb,fb,2.5,2).
operacao_maquina(opt3,mc,fc,1,3).
operacao_maquina(opt4,md,fd,1,1).
operacao_maquina(opt5,me,fe,2,3).
operacao_maquina(opt6,mb,ff,1,4).
operacao_maquina(opt7,md,fg,2,5).
operacao_maquina(opt8,ma,fh,1,6).
operacao_maquina(opt9,me,fi,1,7).
operacao_maquina(opt10,mc,fj,20,2).





%...


% PRODUTOS

produtos([pA,pB,pC,pD,pE,pF]).

operacoes_produto(pA,[opt1,opt2,opt3,opt4,opt5]).
operacoes_produto(pB,[opt1,opt6,opt3,opt4,opt5]).
operacoes_produto(pC,[opt1,opt2,opt3,opt7,opt5]).
operacoes_produto(pD,[opt8,opt2,opt3,opt4,opt5]).
operacoes_produto(pE,[opt1,opt2,opt3,opt4,opt9]).
operacoes_produto(pF,[opt1,opt2,opt10,opt4,opt5]).



% ENCOMENDAS

%Clientes

clientes([clA,clB,clC]).


% prioridades dos clientes

prioridade_cliente(clA,2).
prioridade_cliente(clB,1).
prioridade_cliente(clC,3).

% ...

% Encomendas do cliente, 
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

encomenda(clA,[e(pA,4,50),e(pB,4,70)]).
encomenda(clB,[e(pC,3,30),e(pD,5,200)]).
encomenda(clC,[e(pE,4,60),e(pF,6,120)]).
% ...



% com este predicado vamos calcular os makespans e adicionar as tarefas � base de conhecimento relativas ao Prolog, tarefas estas que sao lidas pelo Algoritmo Gen�tico.

:- dynamic tarefas/1.
:- dynamic tarefa/4.
 
 % O predicado introduzir tarefas basicamente conta-me todas as tarefas existentes e vai ser necessario dentro Algoritmo Genetico. No fundo, constroi o predicado o tarefas.
introduzir_tarefas:-
	aggregate_all(count,operacoes_produto(Pa,_),Count), % Conta todas as operacoes com produto distinto e insere na variavel count.
	assertz(tarefas(Count)).  % Constroi o facto tarefas com a quantidade de vezes distintas correspondente as operacoes produto com produto diferente.

% O predicado introduzir tarefa basicamente constroi o facto tarefa tal e qual como o Algoritmo Genetico necessita.

introduzir_tarefa:-
	operacoes_produto(Pb,La), % Vai buscar um produto e a sua lista de tipos de opera�ao, necessaria para contar depois o makespan
	buscar_quantidade(Pb,Quantidade), % Vai buscar a quantidade de produtos de um produto numa encomenda.
    buscar_tempo_processamento(La,TempoProcessamento), % Vai buscar o tempo de processamento de um produto numa encomenda
	TempoProcessamentoComQuantidade is TempoProcessamento * Quantidade, % Acrescenta o conceito de v�rios produtos iguais numa encomenda.
	reverse(La,LaInvertida),  % Altera a ordem da lista para calcular o tempo de setup.
	buscar_tempo_processamento_tirando_setup(LaInvertida,TempoProcessamento,TempoSetup), % Calcula o Tempo De Setup Consoante as aulas TP
	TempoProcessamentoComQuantidadeSemSetup is TempoProcessamentoComQuantidade - TempoSetup, % Acrescentamos este tempo de setup ( ou retiramos se rapido)
	buscar_conclusao(Pb,TempoConclusao), % Vai diretamente buscar o tempo de conclusao logo para introduzir na tarefa
	buscar_prioridade(Pb,Penalizacao), % De acordo com a prioridade relativa ao cliente calcula a penalizacao usada pelo Algoritmo Genetico
	assertz(tarefa(Pb,TempoProcessamentoComQuantidadeSemSetup,TempoConclusao,Penalizacao)), % Acrescentamos o dado a base de conhecimento Prolog
	write('Introduziu com sucesso a tarefa relativa ao produto '),  % Informacao
	write(Pb). % Informacao
	
	
buscar_prioridade(Pb,Penalizacao):-
	op_prod_client(_,_,_,Pb,Client,_,_,_,_),
	prioridade_cliente(Client,Prioridade),
	Penalizacao is 10*(1/Prioridade),!. % Forma de fazer um cliente com menos prioridade na verdade valer mais!
	
	
buscar_quantidade(Pb,Quantidade):-
	op_prod_client(_,_,_,Pb,_,Qnt,_,_,_),
	Quantidade is Qnt,!.
	
	
buscar_conclusao(Pb,TempoConclusao):-
	op_prod_client(_,_,_,Pb,_,_,Concl,_,_),
	TempoConclusao is Concl,!.
	
buscar_tempo_processamento([],0).

buscar_tempo_processamento([Operacao|RestoDasOperacoes],TempoProcessamento):-
	buscar_tempo_processamento(RestoDasOperacoes,TempoProcessamento1),
	operacao_maquina(Operacao,_,_,_,TempoUsadoExecDestaVez),
	TempoProcessamento is TempoProcessamento1 + TempoUsadoExecDestaVez.
	
	
buscar_tempo_processamento_tirando_setup([],0,10000).  % Valor arbitrario muito alto para comecar, nenhum e suposto ser mais alto que isto

buscar_tempo_processamento_tirando_setup([Operacao|RestoDasOperacoes],TempoProcessamento,TempoSetup):-
	buscar_tempo_processamento_tirando_setup(RestoDasOperacoes,TempoProcessamento1,TempoSetup1),
	operacao_maquina(Operacao,_,_,TempoParado,TempoUsadoExecDestaVez),
	TempoProcessamento is TempoProcessamento1 + TempoUsadoExecDestaVez,
	((TempoProcessamento1 - TempoParado < TempoSetup1,!,TempoSetup is TempoProcessamento1-TempoParado);(TempoSetup is TempoSetup1)).



% cria_op_enc - fizeram-se correcoes face a versao anterior

:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).



:-cria_op_enc.


	
