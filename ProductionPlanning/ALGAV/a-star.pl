:-['data']. %include
aStar(M,Cam,Custo):- 
    operacoes_atrib_maq(M,Lfaltam), % vai buscar as operações da máquina e retira a primeira operação
    aStar2([(_,0,[],Lfaltam)],Cam,Custo). % começa o algoritmo, custo inicial é 0

% acaba quando a lista de ops que faltam estiver vazia
aStar2([(_,Custo,T,[])|_],Cam,Custo):-
	reverse(T,Cam). % inverte a lista final

% predicado recursivo
aStar2([(_,Ca,LTratadas,Lfaltam)|Outros],Cam,Custo):-
	findall((CEX,CaX,[X|LTratadas],Lfaltam2), % elabora todas as hipotesses de operações para onde se pode ir a seguir
				(Lfaltam\==[], % se a operação ainda não foi percorrido
				apaga1(X,Lfaltam,Lfaltam2), % retira-se a proxima operação de Lfaltam e guarda-se essa lista em Lfaltam2
				classif_operacoes(X,Opt),  % vai buscar o tipo de máquina da operação
				operacao_maquina(Opt,M,Fer,TS,_), % vai buscar o Tempo de Setup  da operação,a máquina e a ferramenta usada
				% vai ver se a ferramenta é a mesma que está carregada na máquina, ou se não está nenhuma na máquina
                ((mesma_ferramenta(M,LTratadas, Fer),!,CustoX is 0);CustoX is TS), % se a ferramenta for a mesma não é contabilizado o tempo de setup pois não vai haver mudança
                \+ member(X,Lfaltam2), % verifica se a operação não é membro da lista que falta
				CaX is CustoX + Ca, % Soma o custo das operações anteriores
				elimina_mesma_ferramenta(M,Fer,Lfaltam2, LSemFer), % elimina da lista para a estimativa os elementos com a mesma ferramenta
				estimativa(LSemFer,EstX), % Realiza a estimativa
				CEX is CaX +EstX), % Adiciona ao custo com a estimativa os novos valores
				Novos), % Guarda os resultados em Novos
	append(Outros,Novos,Todos), % Junta os Outros e os Novos em Todos. 
	sort(Todos,TodosOrd), % Ordena  tudo e guarda em TodosOrd
	filtrar_cabeca(TodosOrd, CabecaSorted), % filtra a cabeça para meter as listas com o menor CEX mas menor tamanho de Lfaltam
    aStar2(CabecaSorted,Cam,Custo). % Chama o predicado recursivamente

mesma_ferramenta(M, [OpAct|_], Fer):-
	buscar_ferramenta(M, OpAct, Fer). % retorna true se a lista não estiver vazia e a Op a ser tratada estiver associada com a máquina M e a ferramenta Fer

buscar_ferramenta(M, Actual,Fer):-
	classif_operacoes(Actual, Tipo), % vai buscar o tipo da operação
	operacao_maquina(Tipo,M,Fer,_,_). % Vai ver se existe uma instancia desse tipo com a ferramenta Fer e a máquina M

filtrar_cabeca(L,S):-
	swap_len(L,LS), !, filtrar_cabeca(LS,S).
filtrar_cabeca(S,S).

swap_len([(CEX1,CaX1,Lpercorridas1,Lfaltam1),(CEX2,CaX2,Lpercorridas2,Lfaltam2)|T],[(CEX2,CaX2,Lpercorridas2,Lfaltam2) ,(CEX1,CaX1,Lpercorridas1,Lfaltam1)|T]):-
	CEX1==CEX2, %só filtra se o custo for igual
	length(Lfaltam1, Len1), % tamanho da lista 1
	length(Lfaltam2, Len2), % tamanho da lista 2
	Len1>Len2. % só troca se a lista 1 for maior que a 2 (ordem crescente)	
swap_len([Z|T],[Z|TT]):- swap_len(T,TT). % chamada recursiva.

%elimina todas as ops que usem a mesma ferramenta passada por param de uma lista, para uma dada máquina
elimina_mesma_ferramenta(_,_,[], []).

elimina_mesma_ferramenta(M, Fer, [Op|Lfaltam], L1):-
	((buscar_ferramenta(M, Op, Fer),!,elimina_mesma_ferramenta(M,Fer, Lfaltam, L1));false).

elimina_mesma_ferramenta(M, Fer, [Op|Lfaltam], [Op|L1]):-
	elimina_mesma_ferramenta(M, Fer, Lfaltam,L1).


estimativa(LOp,Estimativa):-
	findall(p(FOp,Tsetup),
	(member(Op,LOp),op_prod_client(Op,_,FOp,_,_,_,_,Tsetup,_)),
	LFTsetup),
	elimina_repetidos(LFTsetup,L),
	soma_setups(L,Estimativa).

elimina_repetidos([],[]).
elimina_repetidos([X|L],L1):-member(X,L),!,elimina_repetidos(L,L1).
elimina_repetidos([X|L],[X|L1]):-elimina_repetidos(L,L1).

soma_setups([],0).
soma_setups([p(_,Tsetup)|L],Ttotal):- soma_setups(L,T1), Ttotal is Tsetup+T1.