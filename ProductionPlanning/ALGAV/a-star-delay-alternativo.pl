:-['data']. %include
:- dynamic melhor_sol_to/2.
:- dynamic custo_atual/1.
aStar(M,Cam,CustoAtraso):- 
	retractall(custo_atual(_)),
    operacoes_atrib_maq(M,Lfaltam), % vai buscar as operações da máquina e retira a primeira operação
	apaga1(X,Lfaltam, Lfaltam2),
	op_prod_client(X,_,Fer,_,_,_,_,Tsetup,Texec),
	Aux is Tsetup + Texec,
	asserta(custo_atual(Aux)),
    aStar2([(_,0,[X],Lfaltam2)],Cam,CustoAtraso,Fer). % começa o algoritmo, custo inicial é 0

% acaba quando a lista de ops que faltam estiver vazia
aStar2([(_,Custo,T,[])|_],Cam,Custo,_):-
	reverse(T,Cam). % inverte a lista final

% predicado recursivo
aStar2([(_,Ca,LTratadas,Lfaltam)|Outros],Cam,CustoAtraso,Fer):-
	asserta(melhor_sol_to(_,-100000000)),
	findall((CEX,CaX,[OpComMaiorAtraso|LTratadas],Lfaltam2), % elabora todas as hipotesses de operações para onde se pode ir a seguir
				(Lfaltam\==[], % se a operação ainda não foi percorrido
				custo_atual(CustoSetEx),
				maior_atraso(Lfaltam,CustoSetEx,Fer), % qual é a operação com maior atraso, das que faltam 
				melhor_sol_to(OpComMaiorAtraso,AtrasoMaior),
				op_prod_client(Op,_,Fer1,_,_,_,_,_,_),
				delete(Lfaltam,OpComMaiorAtraso,Lfaltam2),!, % retira-se a proxima operação de Lfaltam e guarda-se essa lista em Lfaltam2
				\+ member(OpComMaiorAtraso,Lfaltam2), % verifica se a operação não é membro da lista que falta
				CaX is AtrasoMaior + Ca, % Soma o custo das operações anteriores
				CEX is CaX),
				Novos), % Guarda os resultados em Novos
	append(Outros,Novos,Todos), % Junta os Outros e os Novos em Todos. 
	sort(Todos,TodosOrd), % Ordena  tudo e guarda em TodosOrd
	filtrar_cabeca(TodosOrd, CabecaSorted), % filtra a cabeça para meter as listas com o menor CEX mas menor tamanho de Lfaltam
    aStar2(CabecaSorted,Cam,CustoAtraso,Fer1). % Chama o predicado recursivamente

mesma_ferramenta(M, [OpAct|_], Fer):-
	buscar_ferramenta(M, OpAct, Fer). % retorna true se a lista não estiver vazia e a Op a ser tratada estiver associada com a máquina M e a ferramenta Fer

buscar_ferramenta(M, Actual,Fer):-
	classif_operacoes(Actual, Tipo), % vai buscar o tipo da operação
	operacao_maquina(Tipo,M,Fer,_,_). % Vai ver se existe uma instancia desse tipo com a ferramenta Fer e a máquina M

filtrar_cabeca(L,S):-
	swap_len(L,LS), !, filtrar_cabeca(LS,S).
filtrar_cabeca(S,S).

swap_len([(CEX1,CaX1,Lpercorridas1,Lfaltam1),(CEX2,CaX2,Lpercorridas2,Lfaltam2)|T],[(CEX2,CaX2,Lpercorridas2,Lfaltam2) ,(CEX1,CaX1,Lpercorridas1,Lfaltam1)|T]):-
	CEX1==CEX2, %só filtra se o custo for igual
	length(Lfaltam1, Len1), % tamanho da lista 1
	length(Lfaltam2, Len2), % tamanho da lista 2
	Len1>Len2. % só troca se a lista 1 for maior que a 2 (ordem crescente)	
swap_len([Z|T],[Z|TT]):- swap_len(T,TT). % chamada recursiva.

%elimina todas as ops que usem a mesma ferramenta passada por param de uma lista, para uma dada máquina
elimina_mesma_ferramenta(_,_,[], []).

elimina_mesma_ferramenta(M, Fer, [Op|Lfaltam], L1):-
	((buscar_ferramenta(M, Op, Fer),!,elimina_mesma_ferramenta(M,Fer, Lfaltam, L1));false).

elimina_mesma_ferramenta(M, Fer, [Op|Lfaltam], [Op|L1]):-
	elimina_mesma_ferramenta(M, Fer, Lfaltam,L1).

maior_atraso([],_,_):-!.
maior_atraso([Op|LOp],CustoSetEx,Fer):-
	classif_operacoes(Op,Opt),  % vai buscar o tipo de máquina da operação
	operacao_maquina(Opt,M,_,_,_), % vai buscar o Tempo de Setup  da operação,a máquina e a ferramenta usada
	op_prod_client(Op,M,Fer2,_,_,_,Tconc,Tsetup,Texec),
	((Fer2==Fer,!,Custo2 is CustoSetEx+Texec);Custo2 is CustoSetEx+Tsetup+Texec),
	melhor_sol_to(_,Tm),AnotherAuxiliary is Custo2-Tconc,positivo_ou_0(AnotherAuxiliary,ActualAtraso),
	((ActualAtraso>Tm,!,retract(custo_atual(_)),retract(melhor_sol_to(_,_)),asserta(custo_atual(Custo2)),asserta(melhor_sol_to(Op,ActualAtraso)),maior_atraso(LOp,CustoSetEx,Fer));true),
     maior_atraso(LOp,CustoSetEx,Fer).

positivo_ou_0(Num,NumRet):-
	((Num<0,!,NumRet is 0); NumRet is Num).

elimina_repetidos([],[]).
elimina_repetidos([X|L],L1):-member(X,L),!,elimina_repetidos(L,L1).
elimina_repetidos([X|L],[X|L1]):-elimina_repetidos(L,L1).

soma_Atrasos([],0).
soma_Atrasos([p(_,Tatraso)|L],Ttotal):- 
	soma_Atrasos(L,T1), 
	Ttotal is Tatraso+T1.
	