% F�BRICA
:-['data']. %include
:- dynamic melhor_sol_to/2.

% permuta/2 gera permuta��es de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).

permuta_tempo2(M,LP,Atraso):- 
	operacoes_atrib_maq(M,L),
	permuta(L,LP),
	soma_tempos2(semfer,M,LP,Tempo,Atraso).
	
soma_tempos2(_,_,[],_,0).
soma_tempos2(Fer, M, [Op|LOp], TempoSoma, Atraso):-
	op_prod_client(Op, M, Fer1,_,_,_,Tconp, Tsetup, Texec),
	((Fer1==Fer,!,Tempo is Texec);
			Tempo is Tsetup+Texec),
	append([Tempo],TempoSoma,TempoSoma1),
	soma_tempos2(Fer1,M,LOp,TempoSoma1,Atraso1),
	list_sum(TempoSoma1,ResSum),
	((ResSum>Tconp,!,Atraso is Atraso1 + (ResSum-Tconp));Atraso is Atraso1,true).
	

list_sum([Item], Item).
list_sum([Item1,Item2 | Tail], Total) :-
	ItemAux is (Item1+Item2),
    list_sum([ItemAux|Tail], Total).
	
% melhor escalonamento com findall, gera todas as solucoes e escolhe melhor

melhor_escalonamento(M,Lm,Tm):-
				get_time(Ti),
				findall(p(LP,Tempo), 
				permuta_tempo(M,LP,Tempo), LL),
				melhor_permuta(LL,Lm,Tm),
				get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.

melhor_permuta([p(LP,Tempo)],LP,Tempo):-!.
melhor_permuta([p(LP,Tempo)|LL],LPm,Tm):- 							
		melhor_permuta(LL,LP1,T1),
		((Tempo<T1,!,Tm is Tempo,LPm=LP);(Tm is T1,LPm=LP1)).


%melhor escalonamento SEM findall, gera todas as solucoes e escolhe a melhor de forma mais eficiente para grande n�mero de opera��es
melhor_escalonamento2(M,Lm,Tm):-
	get_time(Ti),
	cria_op_enc,
	(melhor_escalonamento22(M,Atraso);true),retract(melhor_sol_to(Lm,Tm)),
	get_time(Tf),Tcomp is Tf-Ti,
	write('GERADO EM '),write(Tcomp),
	write(' SEGUNDOS'),nl.

%auxiliar de melhor_escalonamento2
melhor_escalonamento22(M,Atraso):- 
			asserta(melhor_sol_to(_,10000)),!,
			permuta_tempo2(M,LP,Atraso),
			atualiza(LP,Atraso),
			fail.

atualiza(LP,T):-
	melhor_sol_to(_,Tm),
	T<Tm,retract(melhor_sol_to(_,_)),asserta(melhor_sol_to(LP,T)),!.
