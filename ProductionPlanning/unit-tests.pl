
:-['sistema-produtivo']. %include

tests:- %Correr este predicado para correr os testes unitários ao prolog.
    test_calculate_operacoesTempoQueDemora1,
    test_calculate_operacoesTempoQueDemora2,
    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso,
    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso2,
    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso3,
    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso4,
    test_cria_identificador_faz_com_sucesso,
    test_cria_identificador2_faz_com_sucesso,
    test_bubblesort_faz_com_sucesso,
    test_bubblesort_faz_com_sucesso2,
    test_difference_faz_com_sucesso1,
    test_difference_faz_com_sucesso2,
    test_conta_penalizacao_em_conta_com_prioridade_sucesso1,
    test_conta_penalizacao_em_conta_com_prioridade_sucesso2,
    test_busca_quantidade_com_sucesso1,
    test_busca_quantidade_com_sucesso2,
    test_busca_conclusao_com_sucesso1,
    test_busca_conclusao_com_sucesso2,
    test_tira_tarefas_sucesso1, %Testado
    test_tira_tarefas_sucesso2. %Testado




    test_bubblesort_faz_com_sucesso:-
         SentVariable = [linhaMakespan(l1,20),linhaMakespan(l2,19),linhaMakespan(l3,10),linhaMakespan(l4,50)],
         ExpectedVariable = [linhaMakespan(l3,10),linhaMakespan(l2,19),linhaMakespan(l1,20),linhaMakespan(l4,50)],
         bubblesort3(SentVariable,Result),
         ((ExpectedVariable = Result,!,nl,write('Teste Bubblesort3 1 Passou Com Sucesso!'));nl,write('Teste Bubblesort3 1 FALHOU!')).

    test_bubblesort_faz_com_sucesso2:-
         SentVariable = [linhaMakespan(l1,20),linhaMakespan(l2,20),linhaMakespan(l3,20),linhaMakespan(l4,20)],
         ExpectedVariable = [linhaMakespan(l1,20),linhaMakespan(l2,20),linhaMakespan(l3,20),linhaMakespan(l4,20)],
         bubblesort3(SentVariable,Result),
         ((ExpectedVariable = Result,!,nl,write('Teste Bubblesort3 1 Passou Com Sucesso!'));nl,write('Teste Bubblesort3 1 FALHOU!')).



    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso:-
        SentVariable = [encomenda("daniel",[e(1,2,3),e(1,2,3),e(1,2,3)]),encomenda("daniel",[e(1,2,3),e(1,2,3),e(1,2,3)])],
        ExpectedVariable = 6,
        contar_tarefas(SentVariable,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Introduzir Tarefas Conta Tarefas Passou Com Sucesso!'));nl,write('Teste Introduzir Tarefas Conta Tarefas FALHOU!')).

    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso2:-
        SentVariable = [encomenda("daniel",[]),encomenda("daniel",[e(1,2,3),e(1,2,3),e(1,2,3)])],
        ExpectedVariable = 3,
        contar_tarefas(SentVariable,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Introduzir Tarefas Conta Tarefas 2 Passou Com Sucesso!'));nl,write('Teste Introduzir Tarefas Conta Tarefas 2 FALHOU!')).

     test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso3:-
        SentVariable = [encomenda("daniel",[]),encomenda("daniel",[])],
        ExpectedVariable = 0,
        contar_tarefas(SentVariable,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Introduzir Tarefas Conta Tarefas 3 Passou Com Sucesso!'));nl,write('Teste Introduzir Tarefas Conta Tarefas 3 FALHOU!')).
    

    test_introduzir_tarefa_alternativo_conta_todas_as_tarefas_com_sucesso4:-
        SentVariable = [],
        ExpectedVariable = 0,
        contar_tarefas(SentVariable,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Introduzir Tarefas Conta Tarefas 4 Passou Com Sucesso!'));nl,write('Teste Introduzir Tarefas Conta Tarefas 4 FALHOU!')).


    test_tira_tarefas_sucesso1:-
        SentVariable = [1,2,3]*23,  
        ExpectedVariable = [1,2,3],
        tirar_avaliacao(SentVariable,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Tira Avaliacao 1 Passou Com Sucesso!'));nl,write('Teste Tira Avaliacao 1 FALHOU!')).



    test_tira_tarefas_sucesso2:-
        SentVariable =[]*10,
        ExpectedVariable = [],
        tirar_avaliacao(SentVariable,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Tira Avaliacao 2 Passou Com Sucesso!'));nl,write('Teste Tira Avaliacao 2 FALHOU!')).
    
    test_calculate_operacoesTempoQueDemora1:-
        SentVariable = [op1,op2,op3],
        ExpectVariable = 30,
        assertz(operacao_maquina(op1,maquina,fa,5,5)),
        assertz(operacao_maquina(op2,maquina,fa,5,5)),
        assertz(operacao_maquina(op3,maquina,fa,5,5)),
        busca_makespan_das_operacoes_do_produto(SentVariable,Result),
        retract(operacao_maquina(op1,maquina,fa,5,5)),
        retract(operacao_maquina(op2,maquina,fa,5,5)),
        retract(operacao_maquina(op3,maquina,fa,5,5)),
        ((ExpectVariable = Result,!,nl,write('Teste Calculate Operacoes Tempo 1 Passou Com Sucesso!'));nl,write('Teste Calcula Operacoes Tempo 1 FALHOU!')).


    test_calculate_operacoesTempoQueDemora2:-
      SentVariable = [],
      ExpectedVariable = 0,
      busca_makespan_das_operacoes_do_produto(SentVariable,Result),
       ((ExpectedVariable = Result,!,nl,write('Teste Calculate Operacoes Tempo 2 Passou Com Sucesso!'));nl,write('Teste Calcula Operacoes Tempo 2 FALHOU!')).


    test_cria_identificador_faz_com_sucesso:-
        SentVariable1 = 'prod1',
        SentVariable2 = 'clA',
        ExpectedVariable = "prod1_clA",
        cria_identificador(SentVariable1,SentVariable2,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Cria Identificacoes 1 Passou Com Sucesso!'));nl,write('Teste Cria Identificacoes 1 FALHOU!')).

    test_cria_identificador2_faz_com_sucesso:-
        SentVariable1 = '',
        SentVariable2 = '',
        ExpectedVariable = "_",
        cria_identificador(SentVariable1,SentVariable2,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Cria Identificacoes 2 Passou Com Sucesso!'));nl,write('Teste Cria Identificacoes 1 FALHOU!')).


    test_difference_faz_com_sucesso1:-
        SentVariable = [1,2,3,4],
        SentVariable2 = [2,3],
        ExpectedVariable = [1,4],
        difference(SentVariable,SentVariable2,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Diferenca 1 Listas Passou Com Sucesso!'));nl,write('Teste Diferencas 1 FALHOU!')).

    test_difference_faz_com_sucesso2:-
        SentVariable = [1,2,3,4],
        SentVariable2 = [],
        ExpectedVariable = [1,2,3,4],
        difference(SentVariable,SentVariable2,Result),
        ((ExpectedVariable = Result,!,nl,write('Teste Diferenca 2 Listas Passou Com Sucesso!'));nl,write('Teste Diferencas 2 FALHOU!')).


    test_conta_penalizacao_em_conta_com_prioridade_sucesso1:-
        SentVariable = "dani",
        ExpectedVariable = 10,
        assertz(prioridade_cliente("dani",1)),
        prioridade_cliente_busca(SentVariable,Result),
        retract(prioridade_cliente("dani",1)),
        ((ExpectedVariable = Result,!,nl,write('Teste Conta Penalizacao 1 Passou Com Sucesso!'));nl,write('Teste Conta Penalizacao 1 FALHOU!')).



    test_conta_penalizacao_em_conta_com_prioridade_sucesso2:-
        SentVariable = "joao",
        ExpectedVariable = 5.0,
        assertz(prioridade_cliente("joao",2)),
        prioridade_cliente_busca(SentVariable,Result),
        retract(prioridade_cliente("joao",2)),
        ((ExpectedVariable = Result,!,nl,write('Teste Conta Penalizacao 2 Passou Com Sucesso!'));nl,write('Teste Conta Penalizacao 2 FALHOU!')).


    test_busca_quantidade_com_sucesso1:-
        ExpectedVariable = 5,
        assertz(op_prod_client(a,b,c,"pA",d,5,m,z,x)),
        buscar_quantidade("pA",Result),
        retract(op_prod_client(a,b,c,"pA",d,5,m,z,x)),
        ((ExpectedVariable = Result,!,nl,write('Teste Busca Quantidade 1 Passou Com Sucesso!'));nl,write('Teste Busca Quantidade 1 FALHOU')).

    test_busca_quantidade_com_sucesso2:-
        ExpectedVariable = 1,
        assertz(op_prod_client(a,b,c,"pAcc",d,1,m,z,x)),
        buscar_quantidade("pAcc",Result),
        retract(op_prod_client(a,b,c,"pAcc",d,1,m,z,x)),
        ((ExpectedVariable = Result,!,nl,write('Teste Busca Quantidade 2 Passou Com Sucesso!'));nl,write('Teste Busca Quantidade 2 FALHOU')).

    test_busca_conclusao_com_sucesso1:-
        ExpectedVariable = 4,
        assertz(op_prod_client(a,b,c,"pA",d,5,4,z,x)),
        buscar_conclusao("pA",Result),
        retract(op_prod_client(a,b,c,"pA",d,5,4,z,x)),
        ((ExpectedVariable = Result,!,nl,write('Teste Busca Conclusao 1 Passou Com Sucesso!'));nl,write('Teste Busca Conclusao 1 FALHOU')).

    test_busca_conclusao_com_sucesso2:-
        ExpectedVariable = 2,
        assertz(op_prod_client(a,b,c,"pAcc",d,1,2,z,x)),
        buscar_conclusao("pAcc",Result),
        retract(op_prod_client(a,b,c,"pAcc",d,1,2,z,x)),
        ((ExpectedVariable = Result,!,nl,write('Teste Busca Conclusao 2 Passou Com Sucesso!'));nl,write('Teste Busca Conclusao 2 FALHOU')).