﻿:-['sistema-produtivo']. %include

:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.


% tarefa(Id,TempoProcessamento,TempConc,PesoPenalizacao).
%tarefa(t1,2,5,1).
% tarefa(t2,4,7,6).
% tarefa(t3,1,11,2).
% tarefa(t4,3,9,3).
% tarefa(t5,3,8,2).
% tarefa(t6,2,6,4).

% tarefas(NTarefas).
%tarefas(1).



gera(MelhorIndividuo):-
	NG is 5, (retract(geracoes(_));true), asserta(geracoes(NG)),
	DP is 1, (retract(populacao(_));true), asserta(populacao(DP)),
	P1 is 1, PC is P1/100, 
	(retract(prob_cruzamento(_));true), asserta(prob_cruzamento(PC)),
	P2 is 1, 	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	gera_populacao(Pop),
	write('Pop='),write(Pop),nl,
	avalia_populacao(Pop,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	populacao(TamPop),
	((length(PopOrd,1),!,saca_melhor_individuo(PopOrd,MelhorIndividuo));gera_geracao(0,NG,PopOrd,TamPop,MelhorPop),
	saca_melhor_individuo(MelhorPop,MelhorIndividuo)).


	saca_melhor_individuo(PopOrd,MelhorIndividuo):- %So gira e usa o last/2
		reverse(PopOrd,PopOrdReversed),
		last(PopOrdReversed,MelhorIndividuo).


gera_populacao(Pop):-
	populacao(TamPop),
	tarefas(_),
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	length(ListaTarefas,NumTNovo),
	gera_populacao(TamPop,ListaTarefas,NumTNovo,Pop).

gera_populacao(0,_,_,[]):-!.

gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	((TamPop1 == 0,!,heuristica_edd_criar_individuo(Ind),not(member(Ind,Resto))); % Buscar elementos já bons através de heuristicas.
	(TamPop1 == 1,!,((heuristica_edd_criar_individuo(Ind1),heuristica_spt_criar_individuo(Ind2),\+ same(Ind1,Ind2),!,heuristica_spt_criar_individuo(Ind),not(member(Ind,Resto)));gera_individuo(ListaTarefas,NumT,Ind),not(member(Ind,Resto))));
	gera_individuo(ListaTarefas,NumT,Ind),not(member(Ind,Resto))).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).

same([],[]). %Condicao de termino que verifica se duas listas sao iguais.

same([H1|R1],[H2|R2]):-
    H1 = H2,
    same(R1, R2).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(T,Dur,Prazo,Pen),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 0)
  ;
		(VT is (InstFim-Prazo)*Pen)
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).


gera_geracao(G,G,Pop,_,Pop):-!,
	write('Geracao '), write(G), write(':'), nl, write(Pop), nl.

gera_geracao(N,G,Pop,TamPop,MelhorPop):-
	write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	random_permutation(Pop,RandomPop), % O metodo nativo do prolog garante, deste modo, que os cruzamentos nao sao estaticos, ao randomizar, o ultimo pode cruzar com o do meio removendo assim a limitacao.
	cruzamento(RandomPop,NPop1),
	mutacao(NPop1,NPop2),
	avalia_populacao(NPop2,NPop1Avaliado),

	% Até aqui está ordenado randomly.


	append(RandomPop,NPop1Avaliado,ListaComGeracaoAntigaEAtual), % Ao fazer append da lista da geracao antiga com a lista da geracao atual vamos poder fazer um torneio entre grupos de dois individuos havendo assim a chance de passar os melhores de geracoes antigas e preservar, com alguma sorte porque vai ter um peso menor, os piores de geracoes, removendo assim elitismo e podendo preservar os melhores.
	random_permutation(ListaComGeracaoAntigaEAtual, ListaComGeracaoAntigaEAtualReordenada), % Reordena para, no fundo garantir um bocado que o jogo de torneios entre um e outro nao vai ser estatico, ou seja, que existeum bocao de aletoriedade de como o torneio se vai realizar.
	remove_duplicados(ListaComGeracaoAntigaEAtualReordenada,ListaComGeracaoAntigaEAtualSemRepetidos),
	tarefas(NumeroDeTarefasExistendos), % Conta o numero de tarefas existendos.
	NumeroDeTarefasExistendosAuxiliar is NumeroDeTarefasExistendos -2,
	ordena_populacao(ListaComGeracaoAntigaEAtualSemRepetidos,ListaComGeracaoAntigaEAtualReordenadaSemRepetidos),
	removerMelhoresTarefasDasGeracoes(ListaComGeracaoAntigaEAtualReordenadaSemRepetidos,NumeroDeTarefasExistendos,NumeroDeTarefasExistendosAuxiliar,ListaSoComMelhoresGeracaoPai), 

	% Até aqui saco os pais para o torneio


	diffSet(ListaComGeracaoAntigaEAtualReordenadaSemRepetidos,ListaSoComMelhoresGeracaoPai,ListaSemPais), 
	random_permutation(ListaSemPais,ListaSemPaisDesordenada),
	length(ListaSemPaisDesordenada,NumeroElementos), % Quantos individuos ainda tenho.
	TamPopAuxiliar is TamPop -2, % Porque já meti os pais, TamPopAuxiliar significa quantos individuos ainda me faltam. Este valor * 2 define o numero de individuos para torneios
	EspacosQueFaltam is TamPopAuxiliar, % Quantos espacos me faltam.
	QuantosTorneiosPossoFazer is NumeroElementos/2, % Quantidade de torneios que posso fazer.
	((NumeroElementos == TamPopAuxiliar,!,append(ListaSemPaisDesordenada,ListaSoComMelhoresGeracaoPai,ListaProximaGeracao),
	ordena_populacao(ListaProximaGeracao,ListaProximaGeracaoMutada2),N1 is N+1,gera_geracao(N1,G,ListaProximaGeracaoMutada2,TamPop,MelhorPop));

	(TamPopAuxiliar*2 =< NumeroElementos,torneioFull(EspacosQueFaltam,ListaSemPaisDesordenada,ListaCriada), append(ListaCriada,ListaSoComMelhoresGeracaoPai,ListaProximaGeracao),
	ordena_populacao(ListaProximaGeracao,ListaProximaGeracaoMutada2),N1 is N+1, gera_geracao(N1,G,ListaProximaGeracaoMutada2,TamPop,MelhorPop));
	
	(floor(QuantosTorneiosPossoFazer,QuantosTorneiosPossoFazer2),torneioComRepescagem(QuantosTorneiosPossoFazer2,ListaSemPaisDesordenada, ListaCriada, ListaPerdedores),
	 EspacosAposTorneioRepescagem is EspacosQueFaltam - QuantosTorneiosPossoFazer2, ordena_populacao(ListaPerdedores,ListaPerdedoresOrganizada), retiraMelhoresPerdedores(EspacosAposTorneioRepescagem,ListaPerdedoresOrganizada,ListaMelhoresPerdedores),
	 append(ListaCriada,ListaSoComMelhoresGeracaoPai,ListaIncompletaProximaGeracao), append(ListaIncompletaProximaGeracao,ListaMelhoresPerdedores,ListaCompletaDesorganizadaProxGeracao),
	 ordena_populacao(ListaCompletaDesorganizadaProxGeracao,ListaCompletaOrganizadaProximaaGeracaoMutada), N1 is N+1, gera_geracao(N1,G,ListaCompletaOrganizadaProximaaGeracaoMutada,TamPop,MelhorPop))).



	retiraMelhoresPerdedores(0,[],[]).
	retiraMelhoresPerdedores(0,_,[]).

	retiraMelhoresPerdedores(EspacosAposTorneioRepescagem,[MelhorPerdedor|RestoPerdedores], [MelhorPerdedor|RestoMelhoresPerdedores]):-
		EspacosAposTorneioRepescagem1 is EspacosAposTorneioRepescagem -1,
		retiraMelhoresPerdedores(EspacosAposTorneioRepescagem1,RestoPerdedores,RestoMelhoresPerdedores).



	torneioComRepescagem(0,_,[],[]).
	torneioComRepescagem(0,[],[],[]).
	torneioComRepescagem(QuantosTorneiosPossoFazer2,[Tarefa*Aa,Tarefa2*Ab|RestoDasTarefas],[Tarefa*Aa|ListaCriada],[Tarefa2*Ab|ListaPerdedores]):-
		SomaUm is Aa+Ab,
		DivisaoUm is Aa/SomaUm,
		DivisaoDois is Ab/SomaUm,
		random(0.0,DivisaoUm,NumeroIndividuoUm),
		random(0.0,DivisaoDois,NumeroIndividuoDois),
		((NumeroIndividuoUm>=NumeroIndividuoDois,!, QuantosTorneiosPossoFazer3 is QuantosTorneiosPossoFazer2 - 1, 
		torneioComRepescagem(QuantosTorneiosPossoFazer3,RestoDasTarefas,ListaCriada,ListaPerdedores))).

	torneioComRepescagem(QuantosTorneiosPossoFazer2,[Tarefa*Aa,Tarefa2*Ab|RestoDasTarefas],[Tarefa2*Ab|ListaCriada],[Tarefa*Aa|ListaPerdedores]):-
	!, QuantosTorneiosPossoFazer3 is QuantosTorneiosPossoFazer2 -1,
	torneioComRepescagem(QuantosTorneiosPossoFazer3,RestoDasTarefas,ListaCriada,ListaPerdedores).




	torneioFull(0,_,[]).
	torneioFull(0,[],[]).
	torneioFull(EspacosQueFaltam,[Tarefa*Aa,_*Ab|RestoDasTarefas],[Tarefa*Aa|ListaCriada]):-
		SomaUm is Aa + Ab,
		DivisaoUm is Aa/SomaUm,
		DivisaoDois is Ab/SomaUm,
		random(0.0,DivisaoUm,NumeroIndividuoUm),  % Numero Random da primeira tarefa.
		random(0.0,DivisaoDois,NumeroIndividuoDois),    % Numero Random da segunda Tarefa.
	((NumeroIndividuoUm>=NumeroIndividuoDois,!,EspacosQueFaltam1 is EspacosQueFaltam - 1, torneioFull(EspacosQueFaltam1,RestoDasTarefas,ListaCriada))).

	torneioFull(EspacosQueFaltam,[_*_,Tarefa2*Ab|RestoDasTarefas],[Tarefa2*Ab|ListaCriada]):- 
	!, EspacosQueFaltam1 is EspacosQueFaltam - 1, 
	torneioFull(EspacosQueFaltam1 , RestoDasTarefas, ListaCriada).


	diffSet([], X, X).

	diffSet([H|T1],Set,Z):-
 	member(H, Set),       % NOTE: arguments swapped!
 	!, delete(T1, H, T2), % avoid duplicates in first list
 	delete(Set, H, Set2), % remove duplicates in second list
 	diffSet(T2, Set2, Z).

	diffSet([H|T], Set, [H|Set2]) :-
 	diffSet(T,Set,Set2).



	removerMelhoresTarefasDasGeracoes([],G,G,[]).
	removerMelhoresTarefasDasGeracoes(_,G,G,[]).

	removerMelhoresTarefasDasGeracoes([Melhor|ListaComGeracaoAntigaEAtualReordenadaSemRepetidosDentro],NumeroDeTarefasExistendos,Auxiliar,[Melhor|ListaComGeracaoAntigaEAtualReordenadaSemRepetidosAuxiliar]) :-
			Auxiliar2 is NumeroDeTarefasExistendos -1,
			removerMelhoresTarefasDasGeracoes(ListaComGeracaoAntigaEAtualReordenadaSemRepetidosDentro,Auxiliar2,Auxiliar,ListaComGeracaoAntigaEAtualReordenadaSemRepetidosAuxiliar).
		

	remove_duplicados([],[]).

	remove_duplicados([H | T], List) :-    
     member(H, T),
     remove_duplicados( T, List).

	remove_duplicados([H | T], [H|T1]) :- 
      \+member(H, T),
      remove_duplicados( T, T1).


		
	
gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).


% heuristica EDD para criar um BOM individuo.

heuristica_edd_criar_individuo(IndividuoEDD):-
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	bubblesort(ListaTarefas,IndividuoEDD).
	
bubblesort(InputList,SortList) :-
        swap(InputList,List) , ! ,
        bubblesort(List,SortList).
bubblesort(SortList,SortList).

swap([X,Y|List],[Y,X|List]) :- 
	tarefa(X,_,ConclusaoX,_),
	tarefa(Y,_,ConclusaoY,_),
	ConclusaoX > ConclusaoY.
swap([Z|List],[Z|List1]) :- 
	swap(List,List1).

 
% Heuristica usando SPT (Shortest Processing Time).

heuristica_spt_criar_individuo(IndividuoSPT):-
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	bubblesort2(ListaTarefas,IndividuoSPT).
	
bubblesort2(InputList,SortList) :-
        swap2(InputList,List) , ! ,
        bubblesort2(List,SortList).
bubblesort2(SortList,SortList).
	
swap2([X,Y|List],[Y,X|List]) :- 
	tarefa(X,TProcessamentoX,_,_),
	tarefa(Y,TProcessamentoY,_,_),
	TProcessamentoX > TProcessamentoY.
swap2([Z|List],[Z|List1]) :- 
	swap2(List,List1).


% melhor escalonamento com findall, gera todas as solucoes e escolhe melhor , PODE ESTOURAR A STACK!

melhor_escalonamento(MelhorOperacao,_):-
				get_time(Ti),
				
				findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas), % Até aqui gera-me uma lista agora tenho que as permutar.
				tarefas(_),
				findall(AllListaTarefas,permuta(ListaTarefas,AllListaTarefas),ListaPermutacoesTarefas),
				avalia_populacao(ListaPermutacoesTarefas,ListaPermutacoesTarefasAvaliadas),
				ordena_populacao(ListaPermutacoesTarefasAvaliadas,ListaPermutacoesTarefasOrdenadas),
				tiraMelhor(ListaPermutacoesTarefasOrdenadas, MelhorOperacao),
				nl,
				get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.


tiraMelhor([AMelhorOperacao*Ab|_],AMelhorOperacao*Ab):-
	nl,
	write('A melhor operacao e: '), write(AMelhorOperacao),
	nl,
	write('Tempo de : '), write(Ab).

% permuta/2 gera permuta��es de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).

% permuta_tempo/3 faz uma permuta��o das opera��es atribu�das a uma maquina e calcula tempo de ocupa��o incluindo trocas de ferramentas

