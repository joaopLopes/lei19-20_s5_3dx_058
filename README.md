# Team Members

- Alexandre Rocha - 1171096
- Daniel Oliveira - 1171139
- João Lopes - 1171145
- Leonardo Estrela - 1171145


# Team Rules
  
* Use cases will have a code that follows the following format: [MDP|MDF][0-9]{2}
* Every commit must be associated with an issue, specifying the use case 
    * e.g. "#\<issue number> - Hello World method saying Bye World bug fixed".
* Any documentation should be on the folder docs.

 
# List of Use Cases

### [Use Cases](docs/use-cases.md)


# Documentation

### [Level 1 - System Context](docs/levels/level1-system/level1-system.md)
### [Level 2 - Container](docs/levels/level2-container/level2-container.md)
### [Level 3 - Component](docs/levels/level3-component/level3-component.md)
### [Level 4 - Code](docs/levels/level4-code/level4-code.md)